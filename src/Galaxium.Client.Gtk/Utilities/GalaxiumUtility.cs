/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Client;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

using Anculus.Core;

namespace Galaxium.Client.GtkGui
{
	public static class GalaxiumUtility
	{
		private static MainWindow _mainWindow;
		private static TransferWindow _transferWindow;
		private static DebugWindow _debugWindow;
		
		public const string Version = "0.8";
		
		static GalaxiumUtility ()
		{
			
		}

		internal static void Initialize (MainWindow mainWindow)
		{
			_mainWindow = mainWindow;
			_transferWindow = new TransferWindow ();
			_debugWindow = new DebugWindow ();
		}

		public static MainWindow MainWindow
		{
			get { return _mainWindow; }
		}
		
		public static TransferWindow TransferWindow
		{
			get { return _transferWindow; }
		}
		
		public static DebugWindow DebugWindow
		{
			get { return _debugWindow; }
		}
		
		public static ISessionWidget<Widget> GetSessionWidget (ISession session)
		{
			ThrowUtility.ThrowIfNull ("session", session);

			return _mainWindow.GetSessionWidget (session);
		}

		public static ISessionWidget<Widget> ActiveSessionWidget
		{
			get { return GetSessionWidget (SessionUtility.ActiveSession); }
		}
		
		public static void Shutdown ()
		{
			_transferWindow.Destroy ();
			_debugWindow.Destroy ();
			_mainWindow.Shutdown ();
		}
	}
}