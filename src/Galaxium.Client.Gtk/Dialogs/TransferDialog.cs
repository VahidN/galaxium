/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Client.GtkGui
{
	public sealed class TransferDialog
	{
		[Widget ("FileTransferDialog")]
		private Dialog _dialog;
		[Widget ("dialogImage")]
		private Image _imgDialog;
		[Widget ("labelAccount")]
		private Label _labelAccount;
		[Widget ("labelContact")]
		private Label _labelContact;
		[Widget ("labelFilename")]
		private Label _labelFilename;
		[Widget ("labelFilesize")]
		private Label _labelFilesize;
		[Widget ("boxPreview")]
		private VBox _boxPreview;
	//	[Widget ("labelPreview")]
	//	private Label _labelPreview;
		
	// CHECKTHIS
	//	private ISession _session;
	//	private IFileTransfer _transfer;
		
		public TransferDialog(ISession session, IFileTransfer transfer)
		{
		//	_session = session;
		//	_transfer = transfer;
			
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (TransferDialog).Assembly, "TransferDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-transfer-receive", IconSizes.Small);
			_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-transfer-receive", IconSizes.Large);
			
			_labelAccount.Text = transfer.Session.Account.UniqueIdentifier;
			_labelContact.Text = transfer.Contact.UniqueIdentifier;
			_labelFilename.Text = transfer.FileName;
			_labelFilesize.Text = BaseUtility.SizeString (transfer.TotalBytes);
			
			if (transfer.Preview.Length > 0)
			{
				try
				{
					Image img = new Image (new MemoryStream (transfer.Preview));
					
					_boxPreview.Add (img);
					_boxPreview.ShowAll ();
				}
				catch
				{
					_boxPreview.Hide ();
				}
			}
			else
				_boxPreview.Hide ();
			
			_dialog.Show();
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
	}
}