/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Protocol;

namespace Galaxium.Client.GtkGui
{
	public class CreateSessionCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MenuItem item = MenuItem as MenuItem;
			
			if(item != null)
			{
				// Check whether there are sessions to stop.
				if (SessionUtility.AddingSession)
					item.Sensitive = false;
				else
					item.Sensitive = true;
			}
		}
		
		public override void Run ()
		{
			GalaxiumUtility.MainWindow.ShowAccountWidget ();
		}
	}

	public class StopSessionCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MenuItem item = MenuItem as MenuItem;
			
			if(item != null)
			{
				// Check whether there are sessions to stop.
				if(SessionUtility.Count >= 1 && !SessionUtility.AddingSession)
					item.Sensitive = true;
				else
					item.Sensitive = false;
			}
		}
		
		public override void Run ()
		{
			GalaxiumUtility.MainWindow.CloseActiveSession ();
		}
	}

	public class StopAllSessionsCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MenuItem item = MenuItem as MenuItem;
			
			if(item != null)
			{
				// Check whether there are sessions to stop.
				if(SessionUtility.Count >= 1)
					item.Sensitive = true;
				else
					item.Sensitive = false;
			}
		}
		
		public override void Run ()
		{
			GalaxiumUtility.MainWindow.CloseAllSessions ();
		}
	}
	
	public class FileTransfersCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			GalaxiumUtility.TransferWindow.Show ();
		}
	}
	
	public class PreferencesCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			new PreferencesDialog ();
		}
	}
	
	public class QuitCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			GalaxiumUtility.Shutdown ();
		}
	}
}