/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2009 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;
using Galaxium.Protocol.Msn;

using Anculus.Core;

namespace Galaxium.Client.GtkGui
{
	public class SetDisplayImageCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			if (SessionUtility.ActiveSession is GeneralSession)
			{
				SetDisplayImageDialog dialog = new SetDisplayImageDialog((GeneralSession)SessionUtility.ActiveSession);
				
				if ((ResponseType)dialog.Run() == ResponseType.Ok)
				{
					//MsnDisplayImage image = new MsnDisplayImage(SessionUtility.ActiveSession as GeneralSession);
					
					//image.Filename = dialog.Filename;
					
					//GeneralSession session = SessionUtility.ActiveSession as GeneralSession;
					
					//session.Account.DisplayImage = image;
				}
				
				dialog.Destroy();
			}
		}
	}
}