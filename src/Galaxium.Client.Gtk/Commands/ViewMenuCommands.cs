/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Client.GtkGui
{
	public class ShowProfileDetailsCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowProfileDetails.Name, Configuration.ContactList.ShowProfileDetails.Default);
		}
		
		public override void RunDefault ()
		{
			if (_using_defaults)
			{
				Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowProfileDetails.Name, (MenuItem as CheckMenuItem).Active);
				SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("ShowProfileDetails"));
			}
			else
				SessionUtility.ActiveSession.Account.ShowProfileDetails = (MenuItem as CheckMenuItem).Active;
		}
	}
	
	public class DetailCompactCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = (ContactTreeDetailLevel)Configuration.ContactList.Section.GetInt (Configuration.ContactList.DetailLevel.Name, Configuration.ContactList.DetailLevel.Default) == ContactTreeDetailLevel.Compact;
		}
		
		public override void RunDefault ()
		{
			if ((MenuItem as RadioMenuItem).Active)
			{
				Configuration.ContactList.Section.SetInt (Configuration.ContactList.DetailLevel.Name, (int)ContactTreeDetailLevel.Compact);
				SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("DetailLevel"));
			}
		}
	}

	public class DetailNormalCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = (ContactTreeDetailLevel)Configuration.ContactList.Section.GetInt (Configuration.ContactList.DetailLevel.Name, Configuration.ContactList.DetailLevel.Default) == ContactTreeDetailLevel.Normal;
		}
		
		public override void RunDefault ()
		{
			if ((MenuItem as RadioMenuItem).Active)
			{
				Configuration.ContactList.Section.SetInt (Configuration.ContactList.DetailLevel.Name, (int)ContactTreeDetailLevel.Normal);
				SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("DetailLevel"));
			}
		}
	}

	public class DetailDetailedCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = (ContactTreeDetailLevel)Configuration.ContactList.Section.GetInt (Configuration.ContactList.DetailLevel.Name, Configuration.ContactList.DetailLevel.Default) == ContactTreeDetailLevel.Detailed;
		}
		
		public override void RunDefault ()
		{
			if ((MenuItem as RadioMenuItem).Active)
			{
				Configuration.ContactList.Section.SetInt (Configuration.ContactList.DetailLevel.Name, (int)ContactTreeDetailLevel.Detailed);
				SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("DetailLevel"));
			}
		}
	}

	public class SortByAlphabetCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = Configuration.ContactList.Section.GetBool (Configuration.ContactList.SortAlphabetic.Name, Configuration.ContactList.SortAlphabetic.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.SortAlphabetic.Name, (MenuItem as RadioMenuItem).Active);
			SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("SortAlphabetic"));
		}
	}

	public class SortByStatusCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = Configuration.ContactList.Section.GetBool (Configuration.ContactList.SortStatus.Name, Configuration.ContactList.SortStatus.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.SortStatus.Name, (MenuItem as RadioMenuItem).Active);
			SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("SortAlphabetic"));
		}
	}

	public class SortAscendingCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = Configuration.ContactList.Section.GetBool (Configuration.ContactList.SortAscending.Name, Configuration.ContactList.SortAscending.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.SortAscending.Name, (MenuItem as RadioMenuItem).Active);
			SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("SortAscending"));
		}
	}

	public class SortDescendingCommand : DefaultMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = Configuration.ContactList.Section.GetBool (Configuration.ContactList.SortDescending.Name, Configuration.ContactList.SortDescending.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.SortDescending.Name, (MenuItem as RadioMenuItem).Active);
			SessionUtility.ActiveSession.Account.EmitDisplaySettingsChange (new PropertyEventArgs ("SortAscending"));
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public class ViewByGroupCommand : Galaxium.Gui.GtkGui.SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = Configuration.ContactList.Section.GetBool (Configuration.ContactList.ViewByGroup.Name, Configuration.ContactList.ViewByGroup.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.ViewByGroup.Name, (MenuItem as RadioMenuItem).Active);
			GalaxiumUtility.MainWindow.GeneralSessionWidget.Update();
		}
	}
	
	public class ViewByStatusCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as RadioMenuItem).Active = !Configuration.ContactList.Section.GetBool (Configuration.ContactList.ViewByGroup.Name, Configuration.ContactList.ViewByGroup.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.ViewByGroup.Name, !(MenuItem as RadioMenuItem).Active);
			GalaxiumUtility.MainWindow.GeneralSessionWidget.Update();
		}
	}
	
	public class ShowSearchCheckCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowSearchBar.Name, Configuration.ContactList.ShowSearchBar.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowSearchBar.Name, (MenuItem as CheckMenuItem).Active);
			GalaxiumUtility.MainWindow.GeneralSessionWidget.Update();
		}
	}
	
	public class ShowDisplayImagesCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active =  Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowContactImages.Name, Configuration.ContactList.ShowContactImages.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowContactImages.Name, (MenuItem as CheckMenuItem).Active);
			GalaxiumUtility.MainWindow.GeneralSessionWidget.Update();
		}
	}
	
	public class ShowDisplayNamesCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active =  Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowContactNames.Name, Configuration.ContactList.ShowContactNames.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowContactNames.Name, (MenuItem as CheckMenuItem).Active);
			GalaxiumUtility.MainWindow.GeneralSessionWidget.Update();
		}
	}
	
	public class ShowPersonalMessagesCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active =  Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowContactMessages.Name, Configuration.ContactList.ShowContactMessages.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowContactMessages.Name, (MenuItem as CheckMenuItem).Active);
			GalaxiumUtility.MainWindow.GeneralSessionWidget.Update();
		}
	}
	
	public class ShowContactNicknamesCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowContactNicknames.Name, Configuration.ContactList.ShowContactNicknames.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowContactNicknames.Name, (MenuItem as CheckMenuItem).Active);
			GalaxiumUtility.MainWindow.GeneralSessionWidget.Update();
		}
	}
	
	public class ShowEmptyGroupsCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active =  Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowEmptyGroups.Name, Configuration.ContactList.ShowEmptyGroups.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowEmptyGroups.Name, (MenuItem as CheckMenuItem).Active);
			GalaxiumUtility.MainWindow.GeneralSessionWidget.Update();
		}
	}
	
	public class ShowOfflineContactsCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = Configuration.ContactList.Section.GetBool (Configuration.ContactList.ShowOfflineContacts.Name, Configuration.ContactList.ShowOfflineContacts.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.ShowOfflineContacts.Name, (MenuItem as CheckMenuItem).Active);
			GalaxiumUtility.MainWindow.GeneralSessionWidget.Update();
		}
	}
	
	public class GroupOfflineContactsCommand : SessionMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active =  Configuration.ContactList.Section.GetBool (Configuration.ContactList.GroupOfflineContacts.Name, Configuration.ContactList.GroupOfflineContacts.Default);
		}
		
		public override void RunDefault ()
		{
			Configuration.ContactList.Section.SetBool (Configuration.ContactList.GroupOfflineContacts.Name, (MenuItem as CheckMenuItem).Active);
			GalaxiumUtility.MainWindow.GeneralSessionWidget.Update();
		}
	}
}