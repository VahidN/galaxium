/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections;

using Gtk;
using Gdk;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Client.GtkGui
{
	// FIXME: there is nothing Gtk-specific in this class.. 
	//        I think it should be renamed and moved to the Galaxium.Client 
	public class GtkNotification : INotification
	{
		private IActivity _activity;
		private string _action;
		private string _title;
		private string _body;
		private byte[] _icon;
		
		public IActivity Activity { get { return _activity; } }
		public string Action { get { return _action; } set { _action = value; } }
		public string Title { get { return _title; } }
		public string Body { get { return _body; } }
		public byte[] Icon { get { return _icon; } }
		
		public GtkNotification (IActivity activity, string action, string title, string body, byte[] icon)
		{
			_activity = activity;
			_action = action;
			_title = title;
			_body = body;
			_icon = icon;
		}
	}
}