/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Timers;

using Gdk;
using Gtk;

using Galaxium.Protocol;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Client.GtkGui
{
	public class GtkTrayWidget
	{
		private StatusIcon _statusIcon = null;
		private ITrayListener _listener;
		
		public bool Blinking { get { return(_statusIcon.Blinking); } set { _statusIcon.Blinking = value; } }
		public Gdk.Pixbuf Pixbuf { get { return(_statusIcon.Pixbuf); } set { _statusIcon.Pixbuf = value; } }
		
		public GtkTrayWidget(ITrayListener listener)
		{
			_listener = listener;
			
			
			Initialize();
		}
		
		private void Initialize()
		{
			// Create the tray icon for the gnome panel.
			_statusIcon = new Gtk.StatusIcon ();
			_statusIcon.Tooltip = "Galaxium Messenger";
			_statusIcon.Activate += ActivateEvent;
			_statusIcon.PopupMenu += PopupMenuEvent;
		}
		
		public void ActivateEvent (object o, EventArgs args)
		{
			_listener.TrayActivated();
		}
		
		public void PopupMenuEvent (object o, Gtk.PopupMenuArgs args)
		{
			_listener.TrayPopupActivated();
		}
		
		public void GetPosition(out Gdk.Screen screen, out Gdk.Rectangle area)
		{
			Gtk.Orientation orientation;
			_statusIcon.GetGeometry(out screen, out area, out orientation);
		}
		
		public void ShowMenu(Gtk.Menu menu)
		{
			_statusIcon.PresentMenu(menu, 2, 0);
			menu.Popup();
		}
	}
}