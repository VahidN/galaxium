/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//#define USEANCULUS

// WARNING:
//  If you enable DEBUGSYNC, make sure to comment out the Log.MessageLogged
//  handler assignment in DebugWindow
//#define DEBUGSYNC

using System;
using System.Diagnostics;
using System.Threading;

using Gtk;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;

namespace Galaxium.Client.GtkGui
{
#if USEANCULUS
	public class GtkThreadDispatcher : IThreadDispatcher
	{
		int _gtkThreadId;
		
		static GtkThreadDispatcher ()
		{
			Anculus.Gui.Dispatch.Initialize (new GtkGuiDispatcher ());
		}
		
		public GtkThreadDispatcher ()
		{
			_gtkThreadId = Thread.CurrentThread.ManagedThreadId;
		}
		
		public bool Check ()
		{
			return Thread.CurrentThread.ManagedThreadId == _gtkThreadId;
		}
		
		public void Dispatch (Delegate d, params object[] parameters)
		{
			Anculus.Gui.Dispatch.GuiDispatch (delegate
			{
				d.DynamicInvoke (parameters);
			});
		}
		
		public object SyncDispatch (Delegate d, params object[] parameters)
		{
			object ret = null;
			
			Anculus.Gui.Dispatch.GuiSyncDispatch (delegate
			{
				ret = d.DynamicInvoke (parameters);
			});
			
			return ret;
		}
	}
#else
	public class GtkThreadDispatcher : IThreadDispatcher
	{
		int _gtkThreadId;
		
		public GtkThreadDispatcher ()
		{
			_gtkThreadId = Thread.CurrentThread.ManagedThreadId;
		}
		
		public bool Check ()
		{
			return Thread.CurrentThread.ManagedThreadId == _gtkThreadId;
		}
		
		public void Dispatch (Delegate d, params object[] parameters)
		{
			if (Check ())
			{
				// No invoke required
				d.DynamicInvoke (parameters);
				return;
			}
			
			Gtk.Application.Invoke (delegate
			{
				d.DynamicInvoke (parameters);
			});
		}
		
		public object SyncDispatch (Delegate d, params object[] parameters)
		{
			if (Check ())
			{
				// No invoke required
				return d.DynamicInvoke (parameters);
			}
			
			ManualResetEvent handle = new ManualResetEvent  (false);
			object ret = null;
			
#if DEBUGSYNC
			DateTime start = DateTime.Now;
#endif
			
			Gtk.Application.Invoke (delegate
			{
				try
				{
#if DEBUGSYNC
					Log.Debug ("In Gtk Thread");
#endif
					
					ret = d.DynamicInvoke (parameters);
				}
#if DEBUGSYNC
				catch (Exception ex)
				{
					Log.Error (ex, "Error in invoked method");
				}
#endif
				finally
				{
#if DEBUGSYNC
					Log.Debug ("Invoke done, ret={0}", (ret != null) ? ret.ToString () : "null");
#endif
					handle.Set ();
				}
			});
			
#if DEBUGSYNC
			while (!handle.WaitOne (5000, false))
			{
				StackTrace trace = new StackTrace (2, true);
				
				Log.Error ("Gtk invoke from thread {0} hasn't returned after {1}s\n\t{2}.{3}\n{4}",
				           Thread.CurrentThread.ManagedThreadId, (DateTime.Now - start).TotalSeconds,
				           d.Target.GetType ().Name, d.Method.Name,
				           trace.ToString ());
			}
			
			Log.Debug ("Invoke complete - return {0}", (ret != null) ? ret.ToString () : "null");
#else
			handle.WaitOne ();
#endif
			
            handle.Close ();
			
			return ret;
		}
	}
#endif
}
