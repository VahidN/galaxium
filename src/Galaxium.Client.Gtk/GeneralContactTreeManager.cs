/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Text;

using Gdk;
using Gtk;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;

namespace Galaxium.Client.GtkGui
{
	public class GeneralContactTreeManager : BasicContactTreeManager
	{
		Dictionary<IProtocol, IContactTreeManager> _treeManagers = new Dictionary<IProtocol, IContactTreeManager> ();
		
		public override IConfigurationSection Config
		{
			get
			{
				return Configuration.ContactList.Section;
			}
		}
		
		public override ContactTreeDetailLevel Detail
		{
			get { return (ContactTreeDetailLevel)Config.GetInt (Configuration.ContactList.DetailLevel.Name, Configuration.ContactList.DetailLevel.Default); }
			set { Config.SetInt (Configuration.ContactList.DetailLevel.Name, (int)value); }
		}
		
		IContactTreeManager GetManager (IProtocol protocol)
		{
			if (_treeManagers.ContainsKey (protocol))
			{
				_treeManagers[protocol].Detail = Detail;
				return _treeManagers[protocol];
			}
			
			foreach (ContactTreeManagerExtension node in AddinUtility.GetExtensionNodes("/Galaxium/Gui/ContactTreeManagers"))
			{
				if (Array.IndexOf (node.Protocol.Split (','), protocol.Name) >= 0)
				{
					// We know this extension should be used for this contact.
					IContactTreeManager manager = node.CreateInstance () as IContactTreeManager;
					manager.Session = Session;
					manager.Detail = Detail;
					manager.Tree = Tree;
					
					_treeManagers.Add (protocol, manager);
					
					return manager;
				}
			}
			
			Log.Warn ("Unable to find contact tree manager for protocol '{0}'", protocol.Name);
			
			return null;
		}
		
		public override void RenderText (object data, CellRendererContact renderer)
		{
			// FIXME: What we need to do still here, is have the other manager setup WHAT shows up, but not HOW.
			// We still need to be able to have the user change the settings for the All Sessions view as well,
			// this could potentially be the "default view" but i think it would be cool to be able to save it
			// seperately AND use default just like all the others.
			
			ISession session = null;
			IContactTreeManager manager = null;
			
			if (data is IContact)
				session = (data as IContact).Session;
			else if (data is ContactTreeRealGroup)
				session = (data as ContactTreeRealGroup).Group.Session;
			
			if (session != null)
				manager = GetManager (session.Account.Protocol);
			
			if (manager != null)
			{
				manager.Session = session;
				manager.RenderText (data, renderer);
			}
			else
				base.RenderText (data, renderer);
		}
		
		public override void RenderLeftImage (object data, CellRendererPixbuf renderer)
		{
			// FIXME: See comments above, they apply here.
			
			ISession session = null;
			IContactTreeManager manager = null;
			
			if (data is IContact)
				session = (data as IContact).Session;
			else if (data is ContactTreeRealGroup)
				session = (data as ContactTreeRealGroup).Group.Session;
			
			if (session != null)
				manager = GetManager (session.Account.Protocol);
			
			if (manager != null)
			{
				manager.Session = session;
				manager.RenderLeftImage (data, renderer);
			}
			else
				base.RenderLeftImage (data, renderer);
		}
		
		public override void RenderRightImage (object data, CellRendererPixbuf renderer)
		{
			// FIXME: See comments above, they apply here.
			
			ISession session = null;
			IContactTreeManager manager = null;
			
			if (data is IContact)
				session = (data as IContact).Session;
			else if (data is ContactTreeRealGroup)
				session = (data as ContactTreeRealGroup).Group.Session;
			
			if (session != null)
				manager = GetManager (session.Account.Protocol);
			
			if (manager != null)
			{
				manager.Session = session;
				manager.RenderRightImage (data, renderer);
			}
			else
				base.RenderRightImage (data, renderer);
		}
		
		public override string GetMenuExtensionPoint (object data)
		{
			return base.GetMenuExtensionPoint (data);
		}
				
		public override InfoTooltip GetTooltip (object data)
		{
			return base.GetTooltip (data);
		}
		
		public override int Compare (object data1, object data2)
		{
			return base.Compare (data1, data2);
		}
	}
}
