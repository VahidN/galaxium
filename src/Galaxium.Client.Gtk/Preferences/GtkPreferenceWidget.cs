/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

using Anculus.Core;

namespace Galaxium.Client.GtkGui
{
	public class GtkPreferenceWidget : IPreferenceWidget<Widget>
	{
		// Contact List
		[Widget("checkShowOnStartup")] CheckButton _checkShowOnStartup;
		[Widget("checkShowAllSessions")] CheckButton _checkShowAllSessions;
		[Widget("checkShowBubbleImages")] CheckButton _checkShowBubbleImages;
		
		[Widget("radioGroupByGroup")] RadioButton _radioGroupByGroup;
		[Widget("radioGroupByStatus")] RadioButton _radioGroupByStatus;
		[Widget("radioCompact")] RadioButton _radioCompact;
		[Widget("radioNormal")] RadioButton _radioNormal;
		[Widget("radioDetailed")] RadioButton _radioDetailed;
		[Widget("radioSortAlphabet")] RadioButton _radioSortAlphabet;
		[Widget("radioSortStatus")] RadioButton _radioSortStatus;
		[Widget("radioSortAscending")] RadioButton _radioSortAscending;
		[Widget("radioSortDescending")] RadioButton _radioSortDescending;
		
		[Widget("checkProfileDetails")] CheckButton _checkProfileDetails;
		[Widget("checkSearchBar")] CheckButton _checkSearchBar;
		[Widget("checkContactImages")] CheckButton _checkContactImages;
		[Widget("checkContactNames")] CheckButton _checkContactNames;
		[Widget("checkContactNicknames")] CheckButton _checkContactNicknames;
		[Widget("checkContactMessages")] CheckButton _checkContactMessages;
		[Widget("checkEmptyGroups")] CheckButton _checkEmptyGroups;
		[Widget("checkOfflineContacts")] CheckButton _checkOfflineContacts;
		[Widget("checkGroupOffline")] CheckButton _checkGroupOffline;
		
		// Conversations
		[Widget("checkMinimized")] CheckButton _checkMinimized;
		
		[Widget("radioAlways")] RadioButton _radioAlways;
		[Widget("radioNever")] RadioButton _radioNever;
		[Widget("radioWhenAway")] RadioButton _radioWhenAway;
		
		[Widget("checkTabs")] CheckButton _checkTabs;
		
		[Widget("PlacementCombo")] ComboBox _placementCombo;
		
		[Widget("radioAll")] RadioButton _radioAll;
		[Widget("radioSession")] RadioButton _radioSession;
		[Widget("radioProtocol")] RadioButton _radioProtocol;
		
		[Widget("checkShake")] CheckButton _checkShake;
		
		[Widget("checkActivityToolbar")] CheckButton _checkActivityToolbar;
		[Widget("checkInputToolbar")] CheckButton _checkInputToolbar;
		[Widget("checkAccountImage")] CheckButton _checkAccountImage;
		[Widget("checkContactImage")] CheckButton _checkContactImage;
		[Widget("checkIdentification")] CheckButton _checkIdentification;
		
		[Widget("checkLogging")] CheckButton _checkLogging;
		[Widget("checkSounds")] CheckButton _checkSounds;
		
		// Activities
		[Widget("checkNewContacts")] Gtk.CheckButton _checkNewContacts;
		[Widget("checkNewConversations")] Gtk.CheckButton _checkNewConversations;
		[Widget("checkPresenceChanges")] Gtk.CheckButton _checkPresenceChanges;
		[Widget("checkNameChanges")] Gtk.CheckButton _checkNameChanges;
		[Widget("checkImageChanges")] Gtk.CheckButton _checkImageChanges;
		[Widget("checkMessageChanges")] Gtk.CheckButton _checkMessageChanges;
		[Widget("checkTransfers")] Gtk.CheckButton _checkTransfers;
		[Widget("checkWebcamAudio")] Gtk.CheckButton _checkWebcamAudio;
		[Widget("checkProtocolSpecific")] Gtk.CheckButton _checkProtocolSpecific;
		[Widget("checkDetectHardware")] Gtk.CheckButton _checkDetectHardware;
		
		private Widget _nativeWidget;
		
		public void Initialize ()
		{
			_nativeWidget = GladeUtility.ExtractWidget<Widget> (GladeUtility.GetGladeResourceStream (typeof (GtkPreferenceWidget).Assembly, "GtkPreferenceWidget.glade"), "widget", this);
			
			// Conversations
			LoadConversationSettings ();
			
			_radioAlways.Toggled += ConversationSettingChanged;
			_radioWhenAway.Toggled += ConversationSettingChanged;
			_radioAll.Toggled += ConversationSettingChanged;
			_checkTabs.Toggled += ConversationSettingChanged;
			_checkShake.Toggled += ConversationSettingChanged;
			_checkMinimized.Toggled += ConversationSettingChanged;
			_checkActivityToolbar.Toggled += ConversationSettingChanged;
			_checkInputToolbar.Toggled += ConversationSettingChanged;
			_checkAccountImage.Toggled += ConversationSettingChanged;
			_checkContactImage.Toggled += ConversationSettingChanged;
			_checkIdentification.Toggled += ConversationSettingChanged;
			_checkLogging.Toggled += ConversationSettingChanged;
			_checkSounds.Toggled += ConversationSettingChanged;
			_placementCombo.Changed += ConversationSettingChanged;
			
			// Contact List
			LoadContactListSettings ();
			
			_checkShowOnStartup.Toggled += ContactListSettingChanged;
			_checkShowAllSessions.Toggled += ContactListSettingChanged;
			_radioGroupByGroup.Toggled += ContactListSettingChanged;
			_radioGroupByStatus.Toggled += ContactListSettingChanged;
			_radioCompact.Toggled += ContactListSettingChanged;
			_radioNormal.Toggled += ContactListSettingChanged;
			_radioDetailed.Toggled += ContactListSettingChanged;
			_radioSortAlphabet.Toggled += ContactListSettingChanged;
			_radioSortStatus.Toggled += ContactListSettingChanged;
			_radioSortAscending.Toggled += ContactListSettingChanged;
			_radioSortDescending.Toggled += ContactListSettingChanged;
			_checkProfileDetails.Toggled += ContactListSettingChanged;
			_checkSearchBar.Toggled += ContactListSettingChanged;
			_checkContactImages.Toggled += ContactListSettingChanged;
			_checkContactNames.Toggled += ContactListSettingChanged;
			_checkContactNicknames.Toggled += ContactListSettingChanged;
			_checkContactMessages.Toggled += ContactListSettingChanged;
			_checkEmptyGroups.Toggled += ContactListSettingChanged;
			_checkOfflineContacts.Toggled += ContactListSettingChanged;
			_checkGroupOffline.Toggled += ContactListSettingChanged;
			
			// Activities
			LoadActivitiesSettings ();
			
			_checkNewContacts.Toggled += ActivitySettingChanged;
			_checkNewConversations.Toggled += ActivitySettingChanged;
			_checkPresenceChanges.Toggled += ActivitySettingChanged;
			_checkNameChanges.Toggled += ActivitySettingChanged;
			_checkImageChanges.Toggled += ActivitySettingChanged;
			_checkMessageChanges.Toggled += ActivitySettingChanged;
			_checkTransfers.Toggled += ActivitySettingChanged;
			_checkWebcamAudio.Toggled += ActivitySettingChanged;
			_checkProtocolSpecific.Toggled += ActivitySettingChanged;
			_checkDetectHardware.Toggled += ActivitySettingChanged;
			_checkShowBubbleImages.Toggled += ActivitySettingChanged;
			
			ChangeWidgetSensitivity ();
			
			_nativeWidget.ShowAll ();
		}

		public Widget NativeWidget
		{
			get { return _nativeWidget; }
		}
		
		private void LoadConversationSettings ()
		{
			IConfigurationSection Section = Configuration.Conversation.Section;
			
			_placementCombo.Active = Section.GetInt (Configuration.Conversation.TabPlacement.Name, Configuration.Conversation.TabPlacement.Default);
			_radioNever.Active = Section.GetBool (Configuration.Conversation.QueueNever.Name, Configuration.Conversation.QueueNever.Default);
			_radioAlways.Active = Section.GetBool (Configuration.Conversation.QueueAlways.Name, Configuration.Conversation.QueueAlways.Default);
			_radioWhenAway.Active = Section.GetBool (Configuration.Conversation.QueueWhenAway.Name, Configuration.Conversation.QueueWhenAway.Default);
			_checkMinimized.Active = Section.GetBool (Configuration.Conversation.WindowMinimized.Name, Configuration.Conversation.WindowMinimized.Default);
			_checkShake.Active = Section.GetBool (Configuration.Conversation.WindowShake.Name, Configuration.Conversation.WindowShake.Default);
			_checkActivityToolbar.Active = Section.GetBool (Configuration.Conversation.ShowActivityToolbar.Name, Configuration.Conversation.ShowActivityToolbar.Default);
			_checkInputToolbar.Active = Section.GetBool (Configuration.Conversation.ShowInputToolbar.Name, Configuration.Conversation.ShowInputToolbar.Default);
			_checkAccountImage.Active = Section.GetBool (Configuration.Conversation.ShowAccountImage.Name, Configuration.Conversation.ShowAccountImage.Default);
			_checkContactImage.Active = Section.GetBool (Configuration.Conversation.ShowContactImage.Name, Configuration.Conversation.ShowContactImage.Default);
			_checkIdentification.Active = Section.GetBool (Configuration.Conversation.ShowIdentification.Name, Configuration.Conversation.ShowIdentification.Default);
			_checkLogging.Active = Section.GetBool (Configuration.Conversation.EnableLogging.Name, Configuration.Conversation.EnableLogging.Default);
			_checkSounds.Active = Section.GetBool (Configuration.Conversation.EnableSounds.Name, Configuration.Conversation.EnableSounds.Default);
			
			WindowGroupBehavior groupBehavior = (WindowGroupBehavior)Section.GetInt (Configuration.Conversation.WindowGroupBehavior.Name, Configuration.Conversation.WindowGroupBehavior.Default);
			
			switch (groupBehavior)
			{
				case WindowGroupBehavior.GroupAllWindows:
					_checkTabs.Active = true;
					_radioAll.Active = true;
					break;
				
				case WindowGroupBehavior.GroupSessionWindows:
					_checkTabs.Active = true;
					_radioSession.Active = true;
					break;
				
				case WindowGroupBehavior.GroupProtocolWindows:
					_checkTabs.Active = true;
					_radioProtocol.Active = true;
					break;
				
				case WindowGroupBehavior.DontGroupWindows:
					_checkTabs.Active = false;
					_radioAll.Active = true;
					break;
			}
		}
		
		private void LoadContactListSettings ()
		{
			IConfigurationSection Section = Configuration.ContactList.Section;
			
			_checkShowOnStartup.Active = Section.GetBool(Configuration.ContactList.ShowOnStartup.Name, Configuration.ContactList.ShowOnStartup.Default);
			_checkShowAllSessions.Active = Section.GetBool (Configuration.ContactList.ShowAllSessions.Name, Configuration.ContactList.ShowAllSessions.Default);
			
			_radioGroupByGroup.Active = Section.GetBool (Configuration.ContactList.ViewByGroup.Name, Configuration.ContactList.ViewByGroup.Default);
			_radioGroupByStatus.Active = Section.GetBool (Configuration.ContactList.ViewByStatus.Name, Configuration.ContactList.ViewByStatus.Default);
			
			ContactTreeDetailLevel level = (ContactTreeDetailLevel)Section.GetInt (Configuration.ContactList.DetailLevel.Name, Configuration.ContactList.DetailLevel.Default);
			switch(level)
			{
				case ContactTreeDetailLevel.Compact:
					_radioCompact.Active = true;
					break;
				case ContactTreeDetailLevel.Normal:
					_radioNormal.Active = true;
					break;
				case ContactTreeDetailLevel.Detailed:
					_radioDetailed.Active = true;
					break;
			}
			
			_radioSortAlphabet.Active = Section.GetBool (Configuration.ContactList.SortAlphabetic.Name, Configuration.ContactList.SortAlphabetic.Default);
			_radioSortStatus.Active = Section.GetBool (Configuration.ContactList.SortStatus.Name, Configuration.ContactList.SortStatus.Default);
			_radioSortAscending.Active = Section.GetBool (Configuration.ContactList.SortAscending.Name, Configuration.ContactList.SortAscending.Default);
			_radioSortDescending.Active = Section.GetBool (Configuration.ContactList.SortDescending.Name, Configuration.ContactList.SortDescending.Default);
			_checkProfileDetails.Active = Section.GetBool (Configuration.ContactList.ShowProfileDetails.Name, Configuration.ContactList.ShowProfileDetails.Default);
			_checkSearchBar.Active = Section.GetBool (Configuration.ContactList.ShowSearchBar.Name, Configuration.ContactList.ShowSearchBar.Default);
			_checkContactImages.Active = Section.GetBool (Configuration.ContactList.ShowContactImages.Name, Configuration.ContactList.ShowContactImages.Default);
			_checkContactNames.Active = Section.GetBool (Configuration.ContactList.ShowContactNames.Name, Configuration.ContactList.ShowContactNames.Default);
			_checkContactNicknames.Active = Section.GetBool (Configuration.ContactList.ShowContactNicknames.Name, Configuration.ContactList.ShowContactNicknames.Default);
			_checkContactMessages.Active = Section.GetBool (Configuration.ContactList.ShowContactMessages.Name, Configuration.ContactList.ShowContactMessages.Default);
			_checkEmptyGroups.Active = Section.GetBool (Configuration.ContactList.ShowEmptyGroups.Name, Configuration.ContactList.ShowEmptyGroups.Default);
			_checkOfflineContacts.Active = Section.GetBool (Configuration.ContactList.ShowOfflineContacts.Name, Configuration.ContactList.ShowOfflineContacts.Default);
			_checkGroupOffline.Active = Section.GetBool (Configuration.ContactList.GroupOfflineContacts.Name, Configuration.ContactList.GroupOfflineContacts.Default);
		}
		
		private void LoadActivitiesSettings ()
		{
			IConfigurationSection Section = Configuration.Activity.Section;
			
			_checkNewContacts.Active = Section.GetBool(Configuration.Activity.NewContacts.Name, Configuration.Activity.NewContacts.Default);
			_checkNewConversations.Active = Section.GetBool(Configuration.Activity.NewConversations.Name, Configuration.Activity.NewConversations.Default);
			_checkPresenceChanges.Active = Section.GetBool(Configuration.Activity.PresenceChanges.Name, Configuration.Activity.PresenceChanges.Default);
			_checkNameChanges.Active = Section.GetBool(Configuration.Activity.NameChanges.Name, Configuration.Activity.NameChanges.Default);
			_checkImageChanges.Active = Section.GetBool(Configuration.Activity.ImageChanges.Name, Configuration.Activity.ImageChanges.Default);
			_checkMessageChanges.Active = Section.GetBool(Configuration.Activity.MessageChanges.Name, Configuration.Activity.MessageChanges.Default);
			_checkTransfers.Active = Section.GetBool(Configuration.Activity.Transfers.Name, Configuration.Activity.Transfers.Default);
			_checkWebcamAudio.Active = Section.GetBool(Configuration.Activity.WebcamAudio.Name, Configuration.Activity.WebcamAudio.Default);
			_checkProtocolSpecific.Active = Section.GetBool(Configuration.Activity.ProtocolSpecific.Name, Configuration.Activity.ProtocolSpecific.Default);
			_checkDetectHardware.Active = Section.GetBool(Configuration.Activity.DetectHardware.Name, Configuration.Activity.DetectHardware.Default);
			_checkShowBubbleImages.Active = Section.GetBool(Configuration.Activity.ShowImages.Name, Configuration.Activity.ShowImages.Default);
		}
		
		private void ActivitySettingChanged (object sender, EventArgs args)
		{
			IConfigurationSection Section = Configuration.Activity.Section;
			
			Section.SetBool(Configuration.Activity.NewContacts.Name, _checkNewContacts.Active);
			Section.SetBool(Configuration.Activity.NewConversations.Name, _checkNewConversations.Active);
			Section.SetBool(Configuration.Activity.PresenceChanges.Name, _checkPresenceChanges.Active);
			Section.SetBool(Configuration.Activity.NameChanges.Name, _checkNameChanges.Active);
			Section.SetBool(Configuration.Activity.ImageChanges.Name, _checkImageChanges.Active);
			Section.SetBool(Configuration.Activity.MessageChanges.Name, _checkMessageChanges.Active);
			Section.SetBool(Configuration.Activity.Transfers.Name, _checkTransfers.Active);
			Section.SetBool(Configuration.Activity.WebcamAudio.Name, _checkWebcamAudio.Active);
			Section.SetBool(Configuration.Activity.ProtocolSpecific.Name, _checkProtocolSpecific.Active);
			Section.SetBool(Configuration.Activity.DetectHardware.Name, _checkDetectHardware.Active);
			Section.SetBool(Configuration.Activity.ShowImages.Name, _checkShowBubbleImages.Active);
			
			ChangeWidgetSensitivity ();
		}
		
		private void ConversationSettingChanged (object sender, EventArgs args)
		{
			IConfigurationSection Section = Configuration.Conversation.Section;
			
			Section.SetBool (Configuration.Conversation.QueueNever.Name, _radioNever.Active);
			Section.SetBool (Configuration.Conversation.QueueAlways.Name, _radioAlways.Active);
			Section.SetBool (Configuration.Conversation.QueueWhenAway.Name, _radioWhenAway.Active);
			
			if (!_checkTabs.Active)
				Section.SetInt (Configuration.Conversation.WindowGroupBehavior.Name, (int)WindowGroupBehavior.DontGroupWindows);
			else if (_radioAll.Active)
				Section.SetInt (Configuration.Conversation.WindowGroupBehavior.Name, (int)WindowGroupBehavior.GroupAllWindows);
			else if (_radioProtocol.Active)
				Section.SetInt (Configuration.Conversation.WindowGroupBehavior.Name, (int)WindowGroupBehavior.GroupProtocolWindows);
			else if (_radioSession.Active)
				Section.SetInt (Configuration.Conversation.WindowGroupBehavior.Name, (int)WindowGroupBehavior.GroupSessionWindows);
			
			Section.SetBool (Configuration.Conversation.WindowShake.Name, _checkShake.Active);
			Section.SetBool (Configuration.Conversation.WindowMinimized.Name, _checkMinimized.Active);
			
			Section.SetInt (Configuration.Conversation.TabPlacement.Name, _placementCombo.Active);
			
			Section.SetBool (Configuration.Conversation.ShowActivityToolbar.Name, _checkActivityToolbar.Active);
			Section.SetBool (Configuration.Conversation.ShowInputToolbar.Name, _checkInputToolbar.Active);
			Section.SetBool (Configuration.Conversation.ShowAccountImage.Name, _checkAccountImage.Active);
			Section.SetBool (Configuration.Conversation.ShowContactImage.Name, _checkContactImage.Active);
			Section.SetBool (Configuration.Conversation.ShowIdentification.Name, _checkIdentification.Active);
			Section.SetBool (Configuration.Conversation.EnableLogging.Name, _checkLogging.Active);
			Section.SetBool (Configuration.Conversation.EnableSounds.Name, _checkSounds.Active);
			
			ChangeWidgetSensitivity ();
			
			WindowUtility<Widget>.UpdateAll();
		}
		
		private void ContactListSettingChanged (object sender, EventArgs args)
		{
			IConfigurationSection Section = Configuration.ContactList.Section;
			
			Section.SetBool (Configuration.ContactList.ShowOnStartup.Name, _checkShowOnStartup.Active);
			Section.SetBool (Configuration.ContactList.ShowAllSessions.Name, _checkShowAllSessions.Active);
			Section.SetBool (Configuration.ContactList.ViewByGroup.Name, _radioGroupByGroup.Active);
			Section.SetBool (Configuration.ContactList.ViewByStatus.Name, _radioGroupByStatus.Active);
			
			if (_radioCompact.Active)
				Section.SetInt (Configuration.ContactList.DetailLevel.Name, (int)ContactTreeDetailLevel.Compact);
			else if (_radioNormal.Active)
				Section.SetInt (Configuration.ContactList.DetailLevel.Name, (int)ContactTreeDetailLevel.Normal);
			else if (_radioDetailed.Active)
				Section.SetInt (Configuration.ContactList.DetailLevel.Name, (int)ContactTreeDetailLevel.Detailed);
			
			Section.SetBool (Configuration.ContactList.SortAlphabetic.Name, _radioSortAlphabet.Active);
			Section.SetBool (Configuration.ContactList.SortStatus.Name, _radioSortStatus.Active);
			Section.SetBool (Configuration.ContactList.SortAscending.Name, _radioSortAscending.Active);
			Section.SetBool (Configuration.ContactList.SortDescending.Name, _radioSortDescending.Active);
			Section.SetBool (Configuration.ContactList.ShowProfileDetails.Name, _checkProfileDetails.Active);
			Section.SetBool (Configuration.ContactList.ShowSearchBar.Name, _checkSearchBar.Active);
			Section.SetBool (Configuration.ContactList.ShowContactImages.Name, _checkContactImages.Active);
			Section.SetBool (Configuration.ContactList.ShowContactNames.Name, _checkContactNames.Active);
			Section.SetBool (Configuration.ContactList.ShowContactNicknames.Name, _checkContactNicknames.Active);
			Section.SetBool (Configuration.ContactList.ShowContactMessages.Name, _checkContactMessages.Active);
			Section.SetBool (Configuration.ContactList.ShowEmptyGroups.Name, _checkEmptyGroups.Active);
			Section.SetBool (Configuration.ContactList.ShowOfflineContacts.Name, _checkOfflineContacts.Active);
			Section.SetBool (Configuration.ContactList.GroupOfflineContacts.Name, _checkGroupOffline.Active);
			
			ChangeWidgetSensitivity ();
			
			GalaxiumUtility.MainWindow.UpdateAll ();
		}
		
		private void ChangeWidgetSensitivity ()
		{
			bool sens = _checkTabs.Active;
			
			_radioAll.Sensitive = sens;
			_radioSession.Sensitive = sens;
			_radioProtocol.Sensitive = sens;
			_placementCombo.Sensitive = sens;
		}
	}
}