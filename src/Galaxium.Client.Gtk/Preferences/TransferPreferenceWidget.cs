/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Core;

namespace Galaxium.Client.GtkGui
{
	public class TransferPreferenceWidget : IPreferenceWidget<Widget>
	{
		[Widget ("folderIncompleteButton")]
		private FileChooserButton _folderIncompleteButton;
		[Widget ("folderCompleteButton")]
		private FileChooserButton _folderCompleteButton;
		[Widget ("checkAutoDestination")]
		private CheckButton _checkAutoDestination;
		[Widget ("radioAlwaysAccept")]
		private RadioButton _radioAlwaysAccept;
		[Widget ("radioContactAccept")]
		private RadioButton _radioContactAccept;
		[Widget ("radioNeverAccept")]
		private RadioButton _radioNeverAccept;
		[Widget ("radioAlwaysOverwrite")]
		private RadioButton _radioAlwaysOverwrite;
		[Widget ("radioRenameOverwrite")]
		private RadioButton _radioRenameOverwrite;
		[Widget ("radioDeclineOverwrite")]
		private RadioButton _radioDeclineOverwrite;
		[Widget ("radioPromptOverwrite")]
		private RadioButton _radioPromptOverwrite;
		[Widget ("radioAutoDecline")]
		private RadioButton _radioAutoDecline;
		[Widget ("radioAutoRename")]
		private RadioButton _radioAutoRename;
		[Widget ("radioAutoOverwrite")]
		private RadioButton _radioAutoOverwrite;
		
		private Widget _nativeWidget;
		
		public void Initialize ()
		{
			_nativeWidget = GladeUtility.ExtractWidget<Widget> (GladeUtility.GetGladeResourceStream (typeof (TransferPreferenceWidget).Assembly, "TransferPreferenceWidget.glade"), "widget", this);
			
			_folderIncompleteButton.SetCurrentFolder(FileTransferUtility.DestinationFolder);
			_folderCompleteButton.SetCurrentFolder(FileTransferUtility.CompleteFolder);
			
			_folderIncompleteButton.SelectionChanged += SelectionChanged;
			_folderCompleteButton.SelectionChanged += CompleteSelectionChanged;
			
			_checkAutoDestination.Toggled += CheckAutoDestinationToggled;
			_checkAutoDestination.Active = !FileTransferUtility.AutoDestination;
			
			switch (FileTransferUtility.Automation)
			{
				case TransferAutomationState.Always:
					_radioAlwaysAccept.Active = true;
					break;
					
				case TransferAutomationState.Never:
					_radioNeverAccept.Active = true;
					break;
				
				case TransferAutomationState.PerContact:
					_radioContactAccept.Active = true;
					break;
			}
			
			SetAutoOptions (false);
			
			switch (FileTransferUtility.Overwrite)
			{
				case TransferOverwriteState.Always:
					_radioAlwaysOverwrite.Active = true;
					break;
				
				case TransferOverwriteState.NeverDecline:
					_radioDeclineOverwrite.Active = true;
					break;
				
				case TransferOverwriteState.NeverRename:
					_radioRenameOverwrite.Active = true;
					break;
				
				case TransferOverwriteState.Prompt:
					_radioPromptOverwrite.Active = true;
					SetAutoOptions (true);
					break;
			}
			
			switch (FileTransferUtility.OverwriteDefault)
			{
				case DefaultOverwriteState.Decline:
					_radioAutoDecline.Active = true;
					break;
				
				case DefaultOverwriteState.Rename:
					_radioAutoRename.Active = true;
					break;
				
				case DefaultOverwriteState.Overwrite:
					_radioAutoRename.Active = true;
					break;
			}
			
			_radioAlwaysAccept.Toggled += RadioAcceptToggled;
			_radioNeverAccept.Toggled += RadioAcceptToggled;
			_radioContactAccept.Toggled += RadioAcceptToggled;
			
			_radioAlwaysOverwrite.Toggled += RadioOverwriteToggled;
			_radioDeclineOverwrite.Toggled += RadioOverwriteToggled;
			_radioRenameOverwrite.Toggled += RadioOverwriteToggled;
			_radioPromptOverwrite.Toggled += RadioOverwriteToggled;
			
			_radioAutoDecline.Toggled += RadioAutoToggled;
			_radioAutoRename.Toggled += RadioAutoToggled;
			_radioAutoOverwrite.Toggled += RadioAutoToggled;
			
			_nativeWidget.ShowAll ();
		}
		
		public Widget NativeWidget
		{
			get { return _nativeWidget; }
		}
		
		private void RadioAcceptToggled (object sender, EventArgs args)
		{
			if (_radioAlwaysAccept.Active)
				FileTransferUtility.Automation = TransferAutomationState.Always;
			else if (_radioNeverAccept.Active)
				FileTransferUtility.Automation = TransferAutomationState.Never;
			else
				FileTransferUtility.Automation = TransferAutomationState.PerContact;
		}
		
		private void RadioOverwriteToggled (object sender, EventArgs args)
		{
			if (_radioAlwaysOverwrite.Active)
			{
				FileTransferUtility.Overwrite = TransferOverwriteState.Always;
				SetAutoOptions (false);
			}
			else if (_radioRenameOverwrite.Active)
			{
				FileTransferUtility.Overwrite = TransferOverwriteState.NeverRename;
				SetAutoOptions (false);
			}
			else if (_radioDeclineOverwrite.Active)
			{
				FileTransferUtility.Overwrite = TransferOverwriteState.NeverDecline;
				SetAutoOptions (false);
			}
			else
			{
				FileTransferUtility.Overwrite = TransferOverwriteState.Prompt;
				SetAutoOptions (true);
			}
		}
		
		private void RadioAutoToggled (object sender, EventArgs args)
		{
			if (_radioAutoDecline.Active)
				FileTransferUtility.OverwriteDefault = DefaultOverwriteState.Decline;
			else if (_radioAutoRename.Active)
				FileTransferUtility.OverwriteDefault = DefaultOverwriteState.Rename;
			else
				FileTransferUtility.OverwriteDefault = DefaultOverwriteState.Overwrite;
		}
		
		private void SelectionChanged (object sender, EventArgs args)
		{
			FileTransferUtility.DestinationFolder = _folderIncompleteButton.Filename;
		}
		
		private void CompleteSelectionChanged (object sender, EventArgs args)
		{
			FileTransferUtility.CompleteFolder = _folderCompleteButton.Filename;
		}
		
		private void CheckAutoDestinationToggled (object sender, EventArgs args)
		{
			FileTransferUtility.AutoDestination = !_checkAutoDestination.Active;
		}
		
		private void SetAutoOptions (bool state)
		{
			_radioAutoDecline.Sensitive = state;
			_radioAutoRename.Sensitive = state;
			_radioAutoOverwrite.Sensitive = state;
		}
	}
}