/*
 * Galaxium Messenger
 *
 * Copyright (C) 2008 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

using Anculus.Core;

namespace Galaxium.Client.GtkGui
{
	public class LoggingPreferenceWidget : IPreferenceWidget<Widget>
	{
		[Widget("enableLoggingCheck")] Gtk.CheckButton _enableLoggingCheck;
		[Widget("logEventsCheck")] Gtk.CheckButton _logEventsCheck;
		[Widget("showHistoryCheck")] Gtk.CheckButton _showHistoryCheck; 
		[Widget("historyItemSpin")] Gtk.SpinButton _historyItemSpin;
		[Widget("maxLogCombo")] Gtk.ComboBox _maxLogCombo;
		
		private Widget _nativeWidget;
		private IConfigurationSection _config;
		
		private ListStore _listStore;
		
		public void Initialize ()
		{
			_nativeWidget = GladeUtility.ExtractWidget<Widget> (GladeUtility.GetGladeResourceStream (typeof (LoggingPreferenceWidget).Assembly, "LoggingPreferenceWidget.glade"), "widget", this);
			
			_config = Configuration.Logging.Section;

			_listStore = new ListStore (typeof (string), typeof (int));
			_listStore.AppendValues (BaseUtility.SizeString (256 * 1024), 256 * 1024);
			_listStore.AppendValues (BaseUtility.SizeString (512 * 1024), 512 * 1024);
			_listStore.AppendValues (BaseUtility.SizeString (1024 * 1024), 1024 * 1024);
			_listStore.AppendValues (BaseUtility.SizeString (2 * 1024 * 1024), 2 * 1024 * 1024);
			_listStore.AppendValues (BaseUtility.SizeString (5 * 1024 * 1024), 5 * 1024 * 1024);
			_listStore.AppendValues (BaseUtility.SizeString (10 * 1024 * 1024), 10 * 1024 * 1024);
			_listStore.AppendValues (BaseUtility.SizeString (20 * 1024 * 1024), 20 * 1024 * 1024);
			_listStore.AppendValues (BaseUtility.SizeString (50 * 1024 * 1024), 50 * 1024 * 1024);
			_listStore.AppendValues (BaseUtility.SizeString (100 * 1024 * 1024), 100 * 1024 * 1024);
			_listStore.AppendValues (BaseUtility.SizeString (200 * 1024 * 1024), 200 * 1024 * 1024);
			_listStore.AppendValues (BaseUtility.SizeString (500 * 1024 * 1024), 500 * 1024 * 1024);
			_listStore.AppendValues (BaseUtility.SizeString (1024 * 1024 * 1024), 1024 * 1024 * 1024);
			_listStore.AppendValues (BaseUtility.SizeString (int.MaxValue), int.MaxValue);
			
			_maxLogCombo.Model = _listStore;
			
			_nativeWidget.ShowAll ();
			
			_enableLoggingCheck.Toggled += EnableLoggingToggled;
			_logEventsCheck.Toggled += EnableEventLoggingToggled;
			_showHistoryCheck.Toggled += ShowHistoryToggled;
			_historyItemSpin.LeaveNotifyEvent += HistorySizeChanged;
			_maxLogCombo.Changed += MaximumLogSizeChanged;
			
			LoadSettings ();
		}

		public Widget NativeWidget
		{
			get { return _nativeWidget; }
		}
		
		private void LoadSettings ()
		{
			_historyItemSpin.Value = _config.GetInt (Configuration.Logging.HistorySize.Name, Configuration.Logging.HistorySize.Default);
			
			_enableLoggingCheck.Active = ConversationLogUtility.EnableLogging;
			_logEventsCheck.Active = ConversationLogUtility.EnableEventLogging;
			
			_showHistoryCheck.Active = _config.GetBool (Configuration.Logging.ShowHistory.Name, Configuration.Logging.ShowHistory.Default);
			_historyItemSpin.Sensitive = _showHistoryCheck.Active;
			
			int size = ConversationLogUtility.MaximumLogSize;
			
			TreeIter iter;
			if (_listStore.GetIterFirst (out iter)) {
				TreeIter first = iter;
				do {
					int cur = (int)_listStore.GetValue (iter, 1);
					if (cur == size) {
						_maxLogCombo.SetActiveIter (iter);
						return;
					}
				} while (_listStore.IterNext (ref iter));
				
				_maxLogCombo.SetActiveIter (first);
			}
		}
		
		private void MaximumLogSizeChanged (object sender, EventArgs args)
		{
			//TODO: show a "resize logs" dialog
			TreeIter iter;
			if (_maxLogCombo.GetActiveIter (out iter)) {
				int size = (int)_listStore.GetValue (iter, 1);
				ConversationLogUtility.MaximumLogSize = size;
			}
		}
		
		private void HistorySizeChanged (object sender, LeaveNotifyEventArgs args)
		{
			_config.SetInt (Configuration.Logging.HistorySize.Name, _historyItemSpin.ValueAsInt);
		}
		
		private void EnableLoggingToggled (object sender, EventArgs args)
		{
			ConversationLogUtility.EnableLogging = _enableLoggingCheck.Active;
		}
		
		private void EnableEventLoggingToggled (object sender, EventArgs args)
		{
			ConversationLogUtility.EnableEventLogging = _logEventsCheck.Active;
		}
		
		private void ShowHistoryToggled (object sender, EventArgs args)
		{
			_historyItemSpin.Sensitive = _showHistoryCheck.Active;
			_config.SetBool (Configuration.Logging.ShowHistory.Name, _showHistoryCheck.Active);
		}
	}
}