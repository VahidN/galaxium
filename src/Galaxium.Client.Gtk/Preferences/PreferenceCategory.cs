/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Gdk;

using Galaxium.Core;
using Galaxium.Gui;

namespace Galaxium.Client.GtkGui
{
	public class PreferenceCategory : IComparable<PreferenceCategory>
	{
		private string _name;
		private List<PreferenceWidgetExtension> _plugins;
		internal TreeRowReference _ref;
		
		public string Name { get { return _name; } }
		public List<PreferenceWidgetExtension> Plugins { get { return _plugins; } }
		
		public PreferenceCategory (string name)
		{
			_name = name;
			_plugins = new List<PreferenceWidgetExtension> ();
		}
		
		public void AddPlugin (PreferenceWidgetExtension plugin)
		{
			_plugins.Add (plugin);
		}
		
		public void RemovePlugin (PreferenceWidgetExtension plugin)
		{
			_plugins.Remove (plugin);
		}
		
		public int CompareTo (PreferenceCategory other)
		{
			return _name.CompareTo (other.Name);
		}
	}
}