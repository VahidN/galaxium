/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;

using Gdk;
using Gtk;
using Glade;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;

namespace Galaxium.Client.GtkGui
{
	public class EmoticonPreferenceWidget: IPreferenceWidget<Widget>
	{
		[Widget("vbSets")] VBox _vbSets;
		[Widget("ivEmoticons")] IconView _ivEmoticons;
		[Widget("lblAuthor")] Label _lblAuthor;
		[Widget("lblDescription")] Label _lblDescription;
		[Widget("tvCustom")] TreeView _tvCustom;
		[Widget("btnAdd")] ToolButton _btnAdd;
		[Widget("btnRemove")] ToolButton _btnRemove;
		
		Widget _nativeWidget;
		ImageComboBox<IEmoticonSet> _cbSets;
		IEmoticonSet _noneSet;
		ListStore _tvCustomStore;
		ListStore _accountsStore;
		ListStore _contactsStore;
		
		public void Initialize ()
		{
			_nativeWidget = GladeUtility.ExtractWidget<Widget> (GladeUtility.GetGladeResourceStream (typeof (GtkPreferenceWidget).Assembly, "EmoticonPreferenceWidget.glade"), "widget", this);
			
			_btnAdd.Clicked += btnAddClicked;
			_btnRemove.Clicked += btnRemoveClicked;
			
			// Emoticon Sets
			
			_cbSets = new ImageComboBox<IEmoticonSet>(new ImageComboTextLookup<IEmoticonSet>(SetTextLookup), new ImageComboPixbufLookup<IEmoticonSet>(SetImageLookup));
			_vbSets.PackStart(_cbSets, false, true, 0);
			
			_noneSet = new BaseEmoticonSet("None", string.Empty, string.Empty);
			_cbSets.Append(_noneSet);
			
			foreach (IEmoticonSet emotSet in EmoticonUtility.Sets)
				_cbSets.Append(emotSet);
			
			_cbSets.Changed += SetChanged;
			
			if (EmoticonUtility.ActiveSet != null)
				_cbSets.Select(EmoticonUtility.ActiveSet);
			else
				_cbSets.Select(_noneSet);
			
			// Custom Emoticons
			
			_tvCustom.AppendColumn (GettextCatalog.GetString ("Image"), new CellRendererPixbuf(), new TreeCellDataFunc(RenderCustomImage));
			_tvCustom.AppendColumn (GettextCatalog.GetString ("Equivalents"), new CellRendererText(), new TreeCellDataFunc(RenderCustomEquivalents));
			_tvCustom.AppendColumn (GettextCatalog.GetString ("Account"), new CellRendererCombo(), new TreeCellDataFunc(RenderCustomAccount));
			_tvCustom.AppendColumn (GettextCatalog.GetString ("Contact"), new CellRendererCombo(), new TreeCellDataFunc(RenderCustomContact));
			
			((CellRendererText)_tvCustom.Columns[1].CellRenderers[0]).Editable = true;
			((CellRendererText)_tvCustom.Columns[1].CellRenderers[0]).Edited += CustomEquivalentsEdited;
			
			((CellRendererCombo)_tvCustom.Columns[2].CellRenderers[0]).Editable = true;
			((CellRendererCombo)_tvCustom.Columns[2].CellRenderers[0]).Edited += CustomAccountEdited;
			
			((CellRendererCombo)_tvCustom.Columns[3].CellRenderers[0]).Editable = true;
			((CellRendererCombo)_tvCustom.Columns[3].CellRenderers[0]).Edited += CustomContactEdited;
			
			_tvCustomStore = new ListStore(typeof(ICustomEmoticon));
			_tvCustom.Model = _tvCustomStore;
			
			_tvCustom.Selection.Changed += tvCustomSelectionChanged;
			
			// Setup Accounts Combo
			
			_accountsStore = new ListStore(typeof(string));
			_accountsStore.AppendValues (GettextCatalog.GetString ("All"));
			
			foreach (IAccount account in AccountUtility.Accounts)
				_accountsStore.AppendValues(account.UniqueIdentifier);
			
			((CellRendererCombo)_tvCustom.Columns[2].CellRenderers[0]).Model = _accountsStore;
			((CellRendererCombo)_tvCustom.Columns[2].CellRenderers[0]).TextColumn = 0;
			((CellRendererCombo)_tvCustom.Columns[2].CellRenderers[0]).HasEntry = false;
			
			// Setup Contacts Combo
			
			_contactsStore = new ListStore(typeof(string));
			_contactsStore.AppendValues (GettextCatalog.GetString ("All"));

			((CellRendererCombo)_tvCustom.Columns[3].CellRenderers[0]).Model = _contactsStore;
			((CellRendererCombo)_tvCustom.Columns[3].CellRenderers[0]).TextColumn = 0;
			((CellRendererCombo)_tvCustom.Columns[3].CellRenderers[0]).HasEntry = false;
			
			// Setup Emoticon Store
			
			foreach (ICustomEmoticon info in EmoticonUtility.Custom)
				_tvCustomStore.AppendValues(info);
			
			_nativeWidget.ShowAll ();
		}
		
		string SetTextLookup (IEmoticonSet item)
		{
			return item.Name;
		}

		Gdk.Pixbuf SetImageLookup (IEmoticonSet item, IIconSize size)
		{
			return IconUtility.GetIcon ("galaxium-preferences-themes", size);
		}
		
		void SetChanged(object sender, EventArgs args)
		{
			IEmoticonSet emotSet = _cbSets.GetSelectedItem();
			
			EmoticonUtility.ActiveSet = emotSet == _noneSet ? null : emotSet;
		
			_lblAuthor.Text = emotSet.Creator;
			_lblDescription.Text = emotSet.Description;
			
			_ivEmoticons.Model = null;
			
			if (emotSet != null)
			{
				ListStore store = new ListStore(new Type[] { typeof(string), typeof(Gdk.Pixbuf) });
				
				foreach (IEmoticon emot in emotSet.Emoticons)
					store.AppendValues(new object[] { emot.Equivalents[0], new Gdk.Pixbuf(emot.Filename) });
				
				_ivEmoticons.Model = store;
				_ivEmoticons.TextColumn = 0;
				_ivEmoticons.PixbufColumn = 1;
				_ivEmoticons.ShowAll();
			}
		}
		
		void tvCustomSelectionChanged(object sender, EventArgs args)
		{
			_btnRemove.Sensitive = true;
		}
		
		void RenderCustomImage(TreeViewColumn column, CellRenderer cell, TreeModel model, TreeIter iter)
		{
			ICustomEmoticon emot = (ICustomEmoticon)model.GetValue(iter, 0);
			if(File.Exists(emot.Filename))
				(cell as CellRendererPixbuf).Pixbuf = new Pixbuf(emot.Filename);
			else
				Log.Warn ("Custom emoticon file missing: {0}", emot.Filename);
		}
		
		void RenderCustomEquivalents(TreeViewColumn column, CellRenderer cell, TreeModel model, TreeIter iter)
		{
			ICustomEmoticon emot = (ICustomEmoticon)model.GetValue(iter, 0);
			
			(cell as CellRendererText).Text = string.Join (" ", emot.Equivalents);
		}
		
		void RenderCustomAccount(TreeViewColumn column, CellRenderer cell, TreeModel model, TreeIter iter)
		{
			ICustomEmoticon emot = (ICustomEmoticon)model.GetValue(iter, 0);
			
			(cell as CellRendererCombo).Text = string.IsNullOrEmpty (emot.Source) ? GettextCatalog.GetString ("All") : emot.Source;
		}
		
		void RenderCustomContact(TreeViewColumn column, CellRenderer cell, TreeModel model, TreeIter iter)
		{
			ICustomEmoticon emot = (ICustomEmoticon)model.GetValue(iter, 0);
			
			(cell as CellRendererCombo).Text = string.IsNullOrEmpty (emot.Destination) ? GettextCatalog.GetString ("All") : emot.Destination;
		}
		
		private void btnAddClicked(object sender, EventArgs args)
		{
			FileChooserDialog dialog = new ImageFileChooserDialog((Gtk.Window)_nativeWidget.Toplevel);
			FileFilter filter = new FileFilter ();
			filter.Name = "All Images";
			filter.AddPattern ("*.jpg");
			filter.AddPattern ("*.jpeg");
			filter.AddPattern ("*.gif");
			filter.AddPattern ("*.bmp");
			filter.AddPattern ("*.png");
			dialog.AddFilter (filter);
			dialog.Filter = filter;
			
			filter = new FileFilter();
			filter.Name = "JPEG Images";
			filter.AddPattern ("*.jpg");
			filter.AddPattern ("*.jpeg");
			dialog.AddFilter (filter);
			
			filter = new FileFilter();
			filter.Name = "GIF Images";
			filter.AddPattern ("*.gif");
			dialog.AddFilter (filter);
			
			filter = new FileFilter();
			filter.Name = "BMP Images";
			filter.AddPattern ("*.bmp");
			dialog.AddFilter (filter);
			
			filter = new FileFilter();
			filter.Name = "PNG Images";
			filter.AddPattern ("*.png");
			dialog.AddFilter (filter);
			
			dialog.SetCurrentFolder(Environment.GetFolderPath(Environment.SpecialFolder.Personal));
			
			if (dialog.Run () == (int)ResponseType.Accept)
				_tvCustomStore.AppendValues(EmoticonUtility.AddCustom(dialog.Filename));
			
			dialog.Destroy();
		}
		
		private void btnRemoveClicked(object sender, EventArgs args)
		{
			TreeIter iter;
			_tvCustom.Selection.GetSelected(out iter);
			
			if (iter.Equals(TreeIter.Zero))
				return;
			
			ICustomEmoticon emot = (ICustomEmoticon)_tvCustomStore.GetValue(iter, 0);
			
			EmoticonUtility.RemoveCustom(emot);
			
			_tvCustomStore.Remove(ref iter);
		}
		
		void CustomEquivalentsEdited(object sender, EditedArgs args)
		{
			TreeIter iter;
			_tvCustomStore.GetIter(out iter, new TreePath(args.Path));

			ICustomEmoticon emot = (ICustomEmoticon)_tvCustomStore.GetValue(iter, 0);
			emot.Equivalents = args.NewText.Split (' ');
			
			_tvCustom.ColumnsAutosize();
		}
		
		void CustomAccountEdited(object sender, EditedArgs args)
		{
			TreeIter iter;
			_tvCustomStore.GetIter(out iter, new TreePath(args.Path));

			ICustomEmoticon info = (ICustomEmoticon)_tvCustomStore.GetValue(iter, 0);
			info.Source = args.NewText == GettextCatalog.GetString ("All") ? string.Empty : args.NewText;
			
			_tvCustom.ColumnsAutosize();
		}
		
		void CustomContactEdited(object sender, EditedArgs args)
		{
			TreeIter iter;
			_tvCustomStore.GetIter(out iter, new TreePath(args.Path));

			ICustomEmoticon info = (ICustomEmoticon)_tvCustomStore.GetValue(iter, 0);
			info.Destination = args.NewText == GettextCatalog.GetString ("All") ? string.Empty : args.NewText;
			
			_tvCustom.ColumnsAutosize();
		}
		
		public Widget NativeWidget
		{
			get { return _nativeWidget; }
		}
	}
}
