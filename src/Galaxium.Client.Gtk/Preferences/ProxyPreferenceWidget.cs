/*
 * Galaxium Messenger
 *
 * Copyright (C) 2008 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

using Anculus.Core;

namespace Galaxium.Client.GtkGui
{
	public class ProxyPreferenceWidget : IPreferenceWidget<Widget>
	{
		[Widget("directOption")] Gtk.RadioButton _directOption;
		[Widget("proxyOption")] Gtk.RadioButton _proxyOption;
		
		[Widget("httpHostEntry")] Gtk.Entry _httpHostEntry;
		[Widget("httpsHostEntry")] Gtk.Entry _httpsHostEntry;
		[Widget("socksHostEntry")] Gtk.Entry _socksHostEntry;
		[Widget("httpPortSpin")] Gtk.SpinButton _httpPortSpin;
		[Widget("httpsPortSpin")] Gtk.SpinButton _httpsPortSpin;
		[Widget("socksPortSpin")] Gtk.SpinButton _socksPortSpin;
		
		[Widget("httpAuthButton")] Gtk.Button _httpAuthButton;
		[Widget("socksAuthButton")] Gtk.Button _socksAuthButton;
		[Widget("httpsAuthButton")] Gtk.Button _httpsAuthButton;
		
		[Widget("sameForAllCheck")] Gtk.CheckButton _sameForAllCheck;
		
		private Widget _nativeWidget;
		private IConfigurationSection _config;
		
		public Widget NativeWidget
		{
			get { return _nativeWidget; }
		}
		
		public void Initialize ()
		{
			_nativeWidget = GladeUtility.ExtractWidget<Widget> (GladeUtility.GetGladeResourceStream (typeof (ProxyPreferenceWidget).Assembly, "ProxyPreferenceWidget.glade"), "widget", this);
			
			_config = Configuration.Proxy.Section;
			
			_nativeWidget.ShowAll ();
			
			_directOption.Toggled += OptionToggled;
			_sameForAllCheck.Toggled += SameToggled;
			
			_socksHostEntry.Changed += SocksHostChanged;
			_httpsHostEntry.Changed += HttpsHostChanged;
			_httpHostEntry.Changed += HttpHostChanged;
			
			_socksPortSpin.ValueChanged += SocksPortChanged;
			_httpsPortSpin.ValueChanged += HttpsPortChanged;
			_httpPortSpin.ValueChanged += HttpPortChanged;
			
			LoadSettings ();
		}
		
		private void LoadSettings ()
		{
			if (_config.GetBool (Configuration.Proxy.UseProxy.Name, Configuration.Proxy.UseProxy.Default))
				_proxyOption.Active = true;
			else
				_directOption.Active = true;
			
			if (_config.GetBool (Configuration.Proxy.UseSame.Name, Configuration.Proxy.UseSame.Default))
				_sameForAllCheck.Active = true;
			else
				_sameForAllCheck.Active = false;
			
			_socksHostEntry.Text = _config.GetString (Configuration.Proxy.SocksHost.Name, Configuration.Proxy.SocksHost.Default);
			_socksPortSpin.Value = (double)_config.GetInt (Configuration.Proxy.SocksPort.Name, Configuration.Proxy.SocksPort.Default);
			
			_httpsHostEntry.Text = _config.GetString (Configuration.Proxy.HttpsHost.Name, Configuration.Proxy.HttpsHost.Default);
			_httpsPortSpin.Value = (double)_config.GetInt (Configuration.Proxy.HttpsPort.Name, Configuration.Proxy.HttpsPort.Default);
			
			_httpHostEntry.Text = _config.GetString (Configuration.Proxy.HttpHost.Name, Configuration.Proxy.HttpHost.Default);
			_httpPortSpin.Value = (double)_config.GetInt (Configuration.Proxy.HttpPort.Name, Configuration.Proxy.HttpPort.Default);
			
			if (_directOption.Active)
			{
				_httpHostEntry.Sensitive = false;
				_httpsHostEntry.Sensitive = false;
				_socksHostEntry.Sensitive = false;
				
				_httpPortSpin.Sensitive = false;
				_httpsPortSpin.Sensitive = false;
				_socksPortSpin.Sensitive = false;
				
				_httpAuthButton.Sensitive = false;
				_httpsAuthButton.Sensitive = false;
				_socksAuthButton.Sensitive = false;
				
				_sameForAllCheck.Sensitive = false;
			}
			else
			{
				_httpHostEntry.Sensitive = true;
				_httpPortSpin.Sensitive = true;
				_httpAuthButton.Sensitive = true;
				
				if (_sameForAllCheck.Active)
				{
					_httpsHostEntry.Sensitive = false;
					_socksHostEntry.Sensitive = false;
					
					_httpsPortSpin.Sensitive = false;
					_socksPortSpin.Sensitive = false;
					
					_httpsAuthButton.Sensitive = false;
					_socksAuthButton.Sensitive = false;
				}
				else
				{
					_httpsHostEntry.Sensitive = true;
					_socksHostEntry.Sensitive = true;
					
					_httpsPortSpin.Sensitive = true;
					_socksPortSpin.Sensitive = true;
					
					_httpsAuthButton.Sensitive = true;
					_socksAuthButton.Sensitive = true;
				}
				
				_sameForAllCheck.Sensitive = true;
			}
		}
		
		private void OptionToggled (object sender, EventArgs args)
		{
			if (_directOption.Active)
			{
				_httpHostEntry.Sensitive = false;
				_httpsHostEntry.Sensitive = false;
				_socksHostEntry.Sensitive = false;
				
				_httpPortSpin.Sensitive = false;
				_httpsPortSpin.Sensitive = false;
				_socksPortSpin.Sensitive = false;
				
				_httpAuthButton.Sensitive = false;
				_httpsAuthButton.Sensitive = false;
				_socksAuthButton.Sensitive = false;
				
				_sameForAllCheck.Sensitive = false;
			}
			else
			{
				_httpHostEntry.Sensitive = true;
				_httpPortSpin.Sensitive = true;
				_httpAuthButton.Sensitive = true;
				
				if (_sameForAllCheck.Active)
				{
					_httpsHostEntry.Sensitive = false;
					_socksHostEntry.Sensitive = false;
					
					_httpsPortSpin.Sensitive = false;
					_socksPortSpin.Sensitive = false;
					
					_httpsAuthButton.Sensitive = false;
					_socksAuthButton.Sensitive = false;
				}
				else
				{
					_httpsHostEntry.Sensitive = true;
					_socksHostEntry.Sensitive = true;
					
					_httpsPortSpin.Sensitive = true;
					_socksPortSpin.Sensitive = true;
					
					_httpsAuthButton.Sensitive = true;
					_socksAuthButton.Sensitive = true;
				}
				
				_sameForAllCheck.Sensitive = true;
			}
			
			_config.SetBool (Configuration.Proxy.UseProxy.Name, !_directOption.Active);
		}
		
		private void SameToggled (object sender, EventArgs args)
		{
			if (_sameForAllCheck.Active)
			{
				_httpsHostEntry.Sensitive = false;
				_socksHostEntry.Sensitive = false;
				
				_httpsPortSpin.Sensitive = false;
				_socksPortSpin.Sensitive = false;
				
				_httpsAuthButton.Sensitive = false;
				_socksAuthButton.Sensitive = false;
			}
			else
			{
				_httpsHostEntry.Sensitive = true;
				_socksHostEntry.Sensitive = true;
				
				_httpsPortSpin.Sensitive = true;
				_socksPortSpin.Sensitive = true;
				
				_httpsAuthButton.Sensitive = true;
				_socksAuthButton.Sensitive = true;
			}
			_config.SetBool (Configuration.Proxy.UseSame.Name, _sameForAllCheck.Active);
		}
		
		private void SocksHostChanged (object sender, EventArgs args)
		{
			_config.SetString (Configuration.Proxy.SocksHost.Name, _socksHostEntry.Text);
		}
		
		private void HttpsHostChanged (object sender, EventArgs args)
		{
			_config.SetString (Configuration.Proxy.HttpsHost.Name, _httpsHostEntry.Text);
		}
		
		private void HttpHostChanged (object sender, EventArgs args)
		{
			_config.SetString (Configuration.Proxy.HttpHost.Name, _httpHostEntry.Text);
		}
		
		private void SocksPortChanged (object sender, EventArgs args)
		{
			_config.SetInt (Configuration.Proxy.SocksPort.Name, (int)_socksPortSpin.Value);
		}
		
		private void HttpsPortChanged (object sender, EventArgs args)
		{
			_config.SetInt (Configuration.Proxy.HttpsPort.Name, (int)_httpsPortSpin.Value);
		}
		
		private void HttpPortChanged (object sender, EventArgs args)
		{
			_config.SetInt (Configuration.Proxy.HttpPort.Name, (int)_httpPortSpin.Value);
		}
	}
}