/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;

using Gtk;

using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Client;
using Galaxium.Protocol;
using Galaxium.Core;

using Anculus.Core;
using Anculus.Gui;

namespace Galaxium.Client.GtkGui
{
	internal static class GtkActivityListener
	{
		public static void OnReceivedMessageActivity(object sender, ReceivedMessageActivity activity)
		{
			if (activity.Conversation != null)
			{
				if (WindowUtility<Widget>.GetWindow(activity.Conversation) == null)
					WindowUtility<Widget>.Activate(activity.Conversation, false);
				
				// If the activity was requested by the queue, we will have to activate twice
				// to get the desired result for this activity.
				if (activity.Requested)
					WindowUtility<Widget>.Activate(activity.Conversation, true);
			}
		}
		
		public static void OnReceivedOtherActivity(object sender, ReceivedOtherActivity activity)
		{
			if (activity.Conversation != null)
			{
				if (WindowUtility<Widget>.GetWindow(activity.Conversation) == null)
					WindowUtility<Widget>.Activate(activity.Conversation, false);
			}
		}
		
		public static void OnReceivedFileActivity (object sender, ReceivedFileActivity activity)
		{
			bool complete = false;
			string destinationFolder = FileTransferUtility.CompleteFolder;
			
			while (complete == false)
			{
				bool accept = false, autoaccept = false, overwrite = false, rename = false;
				
				if (FileTransferUtility.Automation == TransferAutomationState.Always)
				{
					autoaccept = accept = true;
				}
				else if (FileTransferUtility.Automation == TransferAutomationState.PerContact && activity.Transfer.Contact.AutoAccept)
				{
					autoaccept = accept = true;
				}
				else
				{
					TransferDialog dialog = new TransferDialog (activity.Session, activity.Transfer);
					
					if ((ResponseType)dialog.Run() == ResponseType.Yes)
						accept = true;
					
					dialog.Destroy ();
				}
				
				if (accept)
				{
					if (!FileTransferUtility.AutoDestination && !autoaccept)
					{
						FileChooserDialog destinationDialog = new FileChooserDialog (
							GettextCatalog.GetString ("Choose Download Folder"),
							null, FileChooserAction.SelectFolder, Stock.Cancel, ResponseType.Cancel, Stock.Apply, ResponseType.Accept);
						destinationDialog.SetCurrentFolder (FileTransferUtility.CompleteFolder);
						
						if ((ResponseType)destinationDialog.Run () == ResponseType.Accept)
						{
							destinationFolder = destinationDialog.Filename;
							destinationDialog.Destroy ();
						}
						else
						{
							destinationDialog.Destroy ();
							complete = false;
							continue;
						}
					}
					
					activity.Transfer.Destination = destinationFolder;
					
					if (File.Exists (Path.Combine(destinationFolder, activity.Transfer.FileName)))
					{
						if (FileTransferUtility.Overwrite == TransferOverwriteState.Prompt)
						{
							TransferOverwriteDialog overwriteDialog = new TransferOverwriteDialog (activity.Transfer, destinationFolder, autoaccept); 
							ResponseType overwriteResponse = (ResponseType)overwriteDialog.Run ();
							
							if (overwriteResponse == ResponseType.Yes)
							{
								overwrite = true;
								rename = false;
							}
							else if (overwriteResponse == ResponseType.No)
							{
								overwrite = false;
								rename = true;
							}
							else
								overwrite = rename = false;
							
							overwriteDialog.Destroy ();
						}
						else if (FileTransferUtility.Overwrite == TransferOverwriteState.NeverDecline)
						{
							overwrite = rename = false;
						}
						else if (FileTransferUtility.Overwrite == TransferOverwriteState.NeverRename)
						{
							overwrite = false;
							rename = true;
						}
						else if (FileTransferUtility.Overwrite == TransferOverwriteState.Always)
						{
							overwrite = true;
							rename = false;
						}
						
						if (!overwrite)
						{
							if (!rename)
							{
								activity.Transfer.Decline ();
								complete = true;
								break;
							}
							else
							{
								int index = 1;
								
								string filename = activity.Transfer.FileName;
								
								while (File.Exists (Path.Combine(destinationFolder, filename)))
								{
									filename = String.Format ("{0}{1}{2}", Path.GetFileNameWithoutExtension (activity.Transfer.FileName), index.ToString(), Path.GetExtension (activity.Transfer.FileName));
									index++;
								}
								
								activity.Transfer.Rename(filename);
							}
						}
					}
					
					Log.Debug ("Accepting Transfer");
					
					activity.Transfer.Accept ();
					
					GalaxiumUtility.TransferWindow.Show ();
					
					complete = true;
					break;
				}
				else
				{
					activity.Transfer.Decline ();
					complete = true;
					break;
				}
			}
		}
		
		public static void OnNewContactActivity (object sender, NewContactActivity activity)
		{
			// The specific protocol will handle this one.
		}
	}
}