/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Threading;

//using GLib;
using Gtk;

using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Client;
using Galaxium.Protocol;
using Galaxium.Core;

using Anculus.Core;
using Anculus.Gui;

namespace Galaxium.Client.GtkGui
{
	public static class GtkActivityPreProcessor
	{
		private static IConfigurationSection Config = Configuration.Activity.Section;
		private static int MaxMessageLength = 35;
		
		private static bool ShowActivityImages 
		{
			get { return Config.GetBool (Configuration.Activity.ShowImages.Name, Configuration.Activity.ShowImages.Default); }
		}
		
		private static int NotificationLength
		{
			get { return Config.GetInt (Configuration.Activity.NotificationLength.Name, Configuration.Activity.NotificationLength.Default); }
		}
		
		public static Gdk.Pixbuf GenerateContactImage (IContact contact)
		{
			IIconSize iconSize = IconSizes.Large;
			ContactTreeDetailLevel detailLevel = ContactTreeDetailLevel.Detailed;
			Gdk.Pixbuf pixbuf = null;
			
			if (contact.DisplayImage != null && contact.DisplayImage.ImageBuffer != null && !contact.SupressImage)
			{
				try
				{
					if (contact.Presence.BasePresence == BasePresence.Idle)
						pixbuf = PixbufUtility.GetShadedPixbuf(new Gdk.Pixbuf(contact.DisplayImage.ImageBuffer), detailLevel);
					else if (contact.Presence.BasePresence == BasePresence.Offline)
						pixbuf = IconUtility.GetIcon ("galaxium-offline", iconSize);
					else
						pixbuf = PixbufUtility.GetFramedPixbuf (new Gdk.Pixbuf(contact.DisplayImage.ImageBuffer), detailLevel);
				}
				catch
				{
					if (contact.Presence.BasePresence == BasePresence.Idle)
						pixbuf = IconUtility.GetIcon ("galaxium-idle", iconSize);
					else if (contact.Presence.BasePresence == BasePresence.Offline)
						pixbuf = IconUtility.GetIcon ("galaxium-offline", iconSize);
					else
						pixbuf = IconUtility.GetIcon ("galaxium-contact", iconSize);
				}
			}
			else
			{
				if (contact.Presence.BasePresence == BasePresence.Idle)
					pixbuf = IconUtility.GetIcon ("galaxium-idle", iconSize);
				else if (contact.Presence.BasePresence == BasePresence.Offline)
					pixbuf = IconUtility.GetIcon ("galaxium-offline", iconSize);
				else
					pixbuf = IconUtility.GetIcon ("galaxium-contact", iconSize);
			}
			
			return pixbuf;
		}
		
		public static void GenerateNotification (IActivity activity, string action, string title, string message, Gdk.Pixbuf pixbuf)
		{
			ThrowUtility.ThrowIfNull ("title", title);
			ThrowUtility.ThrowIfNull ("message", message);
			INotification notification = null;
			
			if (!ShowActivityImages)
				pixbuf = null;
			
			if (pixbuf != null)
				notification = new GtkNotification (activity, action, title, message, pixbuf.SaveToBuffer("png"));
			else
				notification = new GtkNotification (activity, action, title, message, null);
			
			NotificationUtility.Show (notification, NotificationLength);
		}
		
		internal static void OnEntityPresenceChangeActivity (object sender, EntityPresenceChangeActivity activity)
		{
			if (activity.Entity is IContact)
			{
				if (Config.GetBool (Configuration.Activity.PresenceChanges.Name, Configuration.Activity.PresenceChanges.Default))
				{
					IContact contact = (IContact)activity.Entity;
					string account = activity.Session.Account.UniqueIdentifier;
					string actor = GLib.Markup.EscapeText (Message.Strip (contact.DisplayIdentifier, contact, null));
					string state = contact.Presence.State;
					
					string message = String.Format ("{0} is now {1}", actor, state, account);
					
					GenerateNotification (activity, "presence_change", "Presence Changed", message, GenerateContactImage (contact));
				}
			}
			else if (activity.Entity is IAccount)
			{
				NotificationUtility.UpdateStatus();
			}
		}
		
		internal static void OnEntityNameChangeActivity (object sender, EntityNameChangeActivity activity)
		{
			if (Config.GetBool (Configuration.Activity.NameChanges.Name, Configuration.Activity.NameChanges.Default))
			{
				IContact contact = (IContact)activity.Entity;
				string message = string.Format("{0} is now known as:\n{1}", GLib.Markup.EscapeText (Message.Strip (activity.Old, contact, null)),
				                               GLib.Markup.EscapeText (Message.Strip (contact.DisplayName, contact, null)));
				
				GenerateNotification (activity, "name_change", "Name Changed", message, GenerateContactImage (contact));
			}
		}
		
		internal static void OnEntityMessageChangeActivity (object sender, EntityMessageChangeActivity activity)
		{
			if (Config.GetBool (Configuration.Activity.MessageChanges.Name, Configuration.Activity.MessageChanges.Default))
			{
				IContact contact = (IContact)activity.Entity;
				string message = GettextCatalog.GetString ("{0} changed message to:\n{1}", GLib.Markup.EscapeText (Message.Strip (contact.DisplayIdentifier, contact, null)),
				                                GLib.Markup.EscapeText (Message.Strip (contact.DisplayMessage, contact, null)));
				
				GenerateNotification (activity, "message_change", GettextCatalog.GetString ("Message Changed"),
					message, GenerateContactImage (contact));
			}
		}
		
		internal static void OnEntityImageChangeActivity (object sender, EntityImageChangeActivity activity)
		{
			if (activity.Entity.DisplayImage.ImageBuffer.Length < 1)
				return;
			
			if (Config.GetBool (Configuration.Activity.ImageChanges.Name, Configuration.Activity.ImageChanges.Default))
			{
				IContact contact = (IContact)activity.Entity;
				string message = GettextCatalog.GetString ("{0} changed their display image.", GLib.Markup.EscapeText (Message.Strip (activity.Entity.DisplayIdentifier, activity.Entity, null)));
				
				GenerateNotification (activity, "image_change", GettextCatalog.GetString ("Image Changed"),
					message, GenerateContactImage (contact));
			}
		}
		
		internal static void OnReceivedMessageActivity (object sender, ReceivedMessageActivity activity)
		{
		
			if (WindowUtility<Widget>.GetWindow (activity.Conversation) != null)
				return;
			
			if (Config.GetBool (Configuration.Activity.NewConversations.Name, Configuration.Activity.NewConversations.Default))
			{
				IContact contact = (IContact)activity.Entity;
				string text = activity.Message.Text;
				
				if (text.Length > MaxMessageLength)
					text = text.Substring (0, MaxMessageLength) + "...";
				
				string message = GettextCatalog.GetString ("{0} says:\n{1}",
					GLib.Markup.EscapeText (Message.Strip (contact.DisplayIdentifier, contact, null)), text);
				
				GenerateNotification (activity, "received_message", GettextCatalog.GetString ("Incoming Message"),
					message, GenerateContactImage (contact));
			}
		}
		
		internal static void OnReceivedOtherActivity (object sender, ReceivedOtherActivity activity)
		{
			if (Config.GetBool (Configuration.Activity.ProtocolSpecific.Name, Configuration.Activity.ProtocolSpecific.Default))
			{
				IContact contact = (IContact)activity.Entity;
				
				string message = GettextCatalog.GetString ("{0} has performed an activity?", GLib.Markup.EscapeText (Message.Strip (contact.DisplayIdentifier, contact, null)));
				
				GenerateNotification (activity, "received_other", GettextCatalog.GetString ("Incoming Activity"),
					message, GenerateContactImage (contact));
			}
		}
		
		internal static void OnReceivedFileActivity (object sender, ReceivedFileActivity activity)
		{
			if (Config.GetBool (Configuration.Activity.Transfers.Name, Configuration.Activity.Transfers.Default))
			{
				IContact contact = (IContact)activity.Entity;
				
				string message = GettextCatalog.GetString ("{0} is trying to send you a file!",
					GLib.Markup.EscapeText (Message.Strip (contact.DisplayIdentifier, contact, null)));
				
				GenerateNotification (activity, "received_file", GettextCatalog.GetString ("File Transfer"),
					message, GenerateContactImage (contact));
			}
		}
		
		internal static void OnNewContactActivity (object sender, NewContactActivity activity)
		{
			if (Config.GetBool (Configuration.Activity.NewContacts.Name, Configuration.Activity.NewContacts.Default))
			{
				string message = GettextCatalog.GetString ("A new contact has added you:\n{0}",
					GLib.Markup.EscapeText(activity.Contact == null ? activity.UniqueIdentifier : activity.Contact.UniqueIdentifier));
				
				GenerateNotification (activity, "new_contact", GettextCatalog.GetString ("New Contact"),
					message, IconUtility.GetIcon ("galaxium-add-contact", IconSizes.Large));
			}
		}
	}
}