/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;

using Gtk;
using Glade;

using Anculus.Core;

using Galaxium.Client;
using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

using Anculus.Gui;

namespace Galaxium.Client.GtkGui
{
	public class DebugWindow
	{
		[Widget ("DebugWindow")] Window _window;
		[Widget ("textDebug")] TextView _textDebug;
		
		private int _windowX = 0, _windowY = 0, _windowH = 0, _windowW = 0;
		
		public bool Visible
		{
			get { return _window.Visible; }
			set
			{
				if (value == _window.Visible)
					return;
				
				if (value)
				{
					if ((_windowX * _windowY * _windowW * _windowH) > 0)
					{
						_window.Move (_windowX, _windowY);
						_window.Resize (_windowW, _windowH);
					}
					
					// Uncomment this if enabling DEBUGSYNC in GtkThreadDispatcher
					Log.MessageLogged += MessageLogged;
					
					_window.ShowAll ();
				}
				else
				{
					_window.GetPosition (out _windowX, out _windowY);
					_window.GetSize (out _windowW, out _windowH);
					
					Log.MessageLogged -= MessageLogged;
					
					_window.Hide ();
				}
			}
		}
		
		public DebugWindow()
		{
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (DebugWindow).Assembly, "DebugWindow.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_window);
			
			_window.Icon = IconUtility.GetIcon ("galaxium-preferences-logging", IconSizes.Small);
			_window.DeleteEvent += delegate (object sender, DeleteEventArgs args)
			{
				Visible = false;
				args.RetVal = true;
			};
		}
		
		private void MessageLogged (object sender, LogEventArgs args)
		{
			ThreadUtility.Dispatch (new VoidDelegate (delegate
			{
				string message = args.LogLevel + ": "+args.Message + "\n";
				Exception ex = args.Exception;
				
				while (ex != null)
				{
					message += ex.Message + "\n" + ex.StackTrace + "\n";
					ex = ex.InnerException;
				}
				
				TextIter iter = _textDebug.Buffer.EndIter;
				_textDebug.Buffer.Insert (ref iter, message);
				
				_textDebug.ScrollToIter (_textDebug.Buffer.EndIter, 0.4, true, 0.0, 1.0);
			}));
		}
		
		public void Destroy ()
		{
			Log.MessageLogged -= MessageLogged;
			_window.Destroy ();
		}
	}
}
