/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gtk;

using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Mono.Addins;

namespace Galaxium.Client.GtkGui
{
	public class PreferenceWidgetExtension: TypeExtensionNode
	{
		[NodeAttribute]
		string category;
		
		[NodeAttribute]
		string icon;
		
		[NodeAttribute]
		string name;
		
		public string Category
		{
			get { return category; }
		}
		
		public Gdk.Pixbuf Icon
		{
			get { return IconUtility.GetIcon(icon, IconSizes.Small); }
		}
		
		public string Name
		{
			get { return name; }
		}
	}
}
