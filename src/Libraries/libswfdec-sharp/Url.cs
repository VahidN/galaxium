/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using GLib;

namespace Swfdec
{
	public class Url : GLib.Opaque
	{
		public static GType GType
		{
			get { return new GType (Interop.swfdec_url_get_type ()); }
		}
		
		public Url (IntPtr handle)
			: base (handle)
		{
		}
		
		public Url (string str)
		{
			IntPtr ptr = GLib.Marshaller.StringToPtrGStrdup (str);
			Raw = Interop.swfdec_url_new (ptr);
			Marshaller.Free (ptr);
		}
		
		protected override void Free (IntPtr raw)
		{
			Interop.swfdec_url_free (raw);
		}
		
		public Url Copy ()
		{
			IntPtr ptr = Interop.swfdec_url_copy (Handle);
			return ptr == IntPtr.Zero ? null : (Url)GLib.Opaque.GetOpaque (ptr, typeof (Url), true);
		}
		
		public static Url FromInput (string input)
		{
			IntPtr ptr = GLib.Marshaller.StringToPtrGStrdup (input);
			Url ret = new Url (Interop.swfdec_url_new_from_input (ptr));
			Marshaller.Free (ptr);
			
			return ret;
		}
		
		public static bool Equal (IntPtr a, IntPtr b)
		{
			return Interop.swfdec_url_equal (a, b);
		}
	}
}
