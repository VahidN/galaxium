/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Runtime.InteropServices;

namespace Swfdec.Gtk
{
	internal static class GtkInterop
	{
		const string DllName = "libswfdec-gtk-0.6";
		
#region swfdec_gtk_player
		[DllImport(DllName)]
		internal static extern IntPtr swfdec_gtk_player_get_type();
		
		//SwfdecPlayer*       swfdec_gtk_player_new               (SwfdecAsDebugger *debugger);
		[DllImport (DllName)]
		internal static extern IntPtr swfdec_gtk_player_new (IntPtr debugger);
		
		//gboolean            swfdec_gtk_player_get_playing       (SwfdecGtkPlayer *player);
		[DllImport (DllName)]
		internal static extern bool swfdec_gtk_player_get_playing (IntPtr player);
		
		//void                swfdec_gtk_player_set_playing       (SwfdecGtkPlayer *player,
		//                                                         gboolean playing);
		[DllImport (DllName)]
		internal static extern void swfdec_gtk_player_set_playing (IntPtr player, bool playing);
		
		//double              swfdec_gtk_player_get_speed         (SwfdecGtkPlayer *player);
		//void                swfdec_gtk_player_set_speed         (SwfdecGtkPlayer *player,
		//                                                         double speed);
		//gboolean            swfdec_gtk_player_get_audio_enabled (SwfdecGtkPlayer *player);
		//void                swfdec_gtk_player_set_audio_enabled (SwfdecGtkPlayer *player,
		//                                                         gboolean enabled);
		//GdkWindow*          swfdec_gtk_player_get_missing_plugins_window
		//                                                        (SwfdecGtkPlayer *player);
		//void                swfdec_gtk_player_set_missing_plugins_window
		//                                                        (SwfdecGtkPlayer *player,
		//                                                         GdkWindow *window);
#endregion
		
#region swfdec_gtk_widget
		[DllImport(DllName)]
		internal static extern IntPtr swfdec_gtk_widget_get_type();
		
		//GtkWidget*          swfdec_gtk_widget_new               (SwfdecPlayer *player);
		[DllImport (DllName)]
		internal static extern IntPtr swfdec_gtk_widget_new (IntPtr player);
		
		//SwfdecPlayer*       swfdec_gtk_widget_get_player        (SwfdecGtkWidget *widget);
		[DllImport (DllName)]
		internal static extern IntPtr swfdec_gtk_widget_get_player (IntPtr widget);
		
		//void                swfdec_gtk_widget_set_player        (SwfdecGtkWidget *widget,
		//                                                         SwfdecPlayer *player);
		[DllImport (DllName)]
		internal static extern void swfdec_gtk_widget_set_player (IntPtr widget, IntPtr player);
		
		//gboolean            swfdec_gtk_widget_get_interactive   (SwfdecGtkWidget *widget);
		[DllImport (DllName)]
		internal static extern bool swfdec_gtk_widget_get_interactive (IntPtr widget);
		
		//void                swfdec_gtk_widget_set_interactive   (SwfdecGtkWidget *widget,
		//                                                         gboolean interactive);
		[DllImport (DllName)]
		internal static extern void swfdec_gtk_widget_set_interactive (IntPtr widget, bool interactive);
		
		//cairo_surface_type_t swfdec_gtk_widget_get_renderer     (SwfdecGtkWidget *widget);
		//[DllImport (DllName)]
		//internal static extern Cairo.SurfaceType swfdec_gtk_widget_get_renderer (IntPtr widget);
		
		//gboolean            swfdec_gtk_widget_uses_renderer     (SwfdecGtkWidget *widget);
		[DllImport (DllName)]
		internal static extern bool swfdec_gtk_widget_uses_renderer (IntPtr widget);
		
		//void                swfdec_gtk_widget_set_renderer      (SwfdecGtkWidget *widget,
		//                                                         cairo_surface_type_t renderer);
		//[DllImport (DllName)]
		//internal static extern void swfdec_gtk_widget_set_renderer (IntPtr widget, Cairo.SurfaceType renderer);
		
		//void                swfdec_gtk_widget_unset_renderer    (SwfdecGtkWidget *widget);
		[DllImport (DllName)]
		internal static extern void swfdec_gtk_widget_unset_renderer (IntPtr widget);
		
		//guint               swfdec_gtk_keycode_from_hardware_keycode    (guint hardware_keycode);
#endregion
	}
}
