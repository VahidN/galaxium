/*
 * (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __LIBGALAXIUM_H__
#define __LIBGALAXIUM_H__

#include <stdbool.h>
#include <gst/gst.h>
#include <cairo.h>
#include "GStreamer/videosink.h"
#include "devices.h"

typedef struct playInfo playInfo;
typedef struct videoInfoStruct videoInfo;

typedef void (* ManagedEosCallback) (void *handle);
typedef void (* ManagedErrorCallback) (void *handle, GQuark domain, gint code, const gchar *error, const gchar *debug);
typedef void (* ManagedNewFrameCallback) (void *handle);
typedef void (* ManagedNewSizeCallback) (void *handle, gint width, gint height);

struct playInfo
{
	GstElement *playbin;

	ManagedEosCallback eosCallback;
	ManagedErrorCallback errorCallback;
};

struct videoInfoStruct
{
	GalaxiumWebcamDevice *device;
	GalaxiumVideoFormat *format;
	GalaxiumFramerate *framerate;
	
	GstElement *pipeline;
	GstElement *decoder_pre;
	GstElement *decoder;
	GstElement *decoder_post;
	GstElement *encoder_pre;
	GstElement *encoder;
	GstElement *encoder_post;
	GstElement *tee;
	GstElement *scaleflt_pre;
	GstElement *scaleflt;
	GstElement *scaleflt_post;
	GstElement *sendsink;
	GalaxiumVideoSink *sink;
	
	gboolean playing;
	
	GstElement *receivesrc;
	gint receivesrc_size;
	gchar *receivesrc_buffer;
	
	ManagedNewSizeCallback newSizeCallback;
	ManagedNewFrameCallback newFrameCallback;
	ManagedErrorCallback errorCallback;
};

extern bool libgalaxium_cab_init ();
extern void libgalaxium_cab_destroy ();
extern bool libgalaxium_cab_extract (char* filename, char* directory);

// GStreamer
extern void gstreamer_init ();

// Audio
extern void gstreamer_audio_set_eos_callback (playInfo* info, ManagedEosCallback callback);
extern void gstreamer_audio_set_error_callback (playInfo* info, ManagedErrorCallback callback);
extern playInfo* gstreamer_audio_open (gchar* filename);
extern void gstreamer_audio_close (playInfo* info);
extern void gstreamer_audio_play (playInfo* info);
extern void gstreamer_audio_stop (playInfo* info);

// Video
extern GalaxiumWebcamDevice *gstreamer_video_get_devices (int *num);
extern videoInfo *gstreamer_video_new_local (GalaxiumWebcamDevice *device, GalaxiumVideoFormat *format, GalaxiumFramerate *framerate);
extern videoInfo *gstreamer_video_new_remote ();
extern void gstreamer_video_free (videoInfo *info);
extern void gstreamer_video_play (videoInfo *info);
extern void gstreamer_video_stop (videoInfo *info);
extern void gstreamer_video_write (videoInfo *info, const gchar *buf, gint count);
extern void gstreamer_video_set_decoder (videoInfo *info, gchar *decstr);
extern void gstreamer_video_set_encoder (videoInfo *info, gchar *encstr);
extern void gstreamer_video_set_surface (videoInfo *info, cairo_surface_t *surface);
extern void gstreamer_video_set_scale (videoInfo *info, gint width, gint height);
extern void gstreamer_video_set_newframe_callback (videoInfo *info, ManagedNewFrameCallback cb);
extern void gstreamer_video_set_newsize_callback (videoInfo *info, ManagedNewSizeCallback cb);
extern void gstreamer_video_set_error_callback (videoInfo *info, ManagedErrorCallback cb);

#endif
