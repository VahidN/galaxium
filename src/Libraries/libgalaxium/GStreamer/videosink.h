/*
 *  Copyright (C) 2007 OpenedHand
 *  Copyright (C) 2007 Alp Toker <alp@atoker.com>
 *  Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __VIDEOSINK_H__
#define __VIDEOSINK_H__

#include <cairo.h>
#include <glib-object.h>
#include <gst/base/gstbasesink.h>

G_BEGIN_DECLS

#define GALAXIUM_TYPE_VIDEO_SINK galaxium_video_sink_get_type()

#define GALAXIUM_VIDEO_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  GALAXIUM_TYPE_VIDEO_SINK, GalaxiumVideoSink))

#define GALAXIUM_VIDEO_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  GALAXIUM_TYPE_VIDEO_SINK, GalaxiumVideoSinkClass))

#define GALAXIUM_IS_VIDEO_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  GALAXIUM_TYPE_VIDEO_SINK))

#define GALAXIUM_IS_VIDEO_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  GALAXIUM_TYPE_VIDEO_SINK))

#define GALAXIUM_VIDEO_SINK_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  GALAXIUM_TYPE_VIDEO_SINK, GalaxiumVideoSinkClass))

typedef struct _GalaxiumVideoSink        GalaxiumVideoSink;
typedef struct _GalaxiumVideoSinkClass   GalaxiumVideoSinkClass;
typedef struct _GalaxiumVideoSinkPrivate GalaxiumVideoSinkPrivate;

typedef void (* NewFrameMethod) (void *data);
typedef void (* NewSizeMethod) (void *data, gint width, gint height);

struct _GalaxiumVideoSink
{
  /*< private >*/
  GstBaseSink                 parent;
  GalaxiumVideoSinkPrivate *priv;
};

struct _GalaxiumVideoSinkClass
{
  /*< private >*/
  GstBaseSinkClass parent_class;

  /* Future padding */
  void (* _galaxium_reserved1) (void);
  void (* _galaxium_reserved2) (void);
  void (* _galaxium_reserved3) (void);
  void (* _galaxium_reserved4) (void);
  void (* _galaxium_reserved5) (void);
  void (* _galaxium_reserved6) (void);
};

GType       galaxium_video_sink_get_type    (void) G_GNUC_CONST;
GstElement *galaxium_video_sink_new         (NewSizeMethod new_size, void *new_size_data, NewFrameMethod new_frame, void *new_frame_data);

void galaxium_video_sink_set_surface         (GalaxiumVideoSink *sink, cairo_surface_t *surface);

G_END_DECLS

#endif
