/*
 * Galaxium Messenger
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Diagnostics;

namespace Galaxium.Client
{
	/// <summary>
	/// Basic platform functionality that should be available in all platforms.
	/// </summary>
	public abstract class AbstractPlatform : IPlatform
	{
		public abstract string Identifier { get; }
		
		public virtual void Initialize ()
		{
		}
		
		public virtual void Unload ()
		{
		}
		
		public virtual void OpenFile (string filename)
		{
			System.Diagnostics.Process.Start (filename);
		}
		
		public virtual void OpenUrl (string url)
		{
			//make sure the url doesn't contain stuff like file:///usr/bin/wathever, so we can't accidentally click the wrong url
			if (url.StartsWith ("file://")) {
				url = url.Substring (7);
				
				while (File.Exists (url))
					url = Path.GetDirectoryName (url);
				
				url = "file://" + url;
			}
			
			System.Diagnostics.Process.Start (url);
		}
	}
}