// TransferEventArgs.cs created with MonoDevelop
// User: draek at 6:50 P 03/12/2007
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;

using Galaxium.Protocol;

namespace Galaxium.Client
{
	/// <summary>
	/// Event argument container for file transfer information.
	/// </summary>
	public class TransferEventArgs
	{
		private IFileTransfer _transfer;
		
		public IFileTransfer Transfer
		{
			get { return _transfer; }
		}
		
		public TransferEventArgs(IFileTransfer transfer)
		{
			_transfer = transfer;
		}
	}
}