/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Client
{
	/// <summary>
	/// If a file transfer is automated, and set to prompt but the user
	/// is not there to answer, one of the following actions will be performed
	/// 'Rename' will change the name of the file to allow it to be transfered anyway.
	/// 'Decline' will stop all transfers that have been already downloaded.
	/// 'Overwrite' will replace any data that may have been already downloaded.
	/// </summary>
	public enum DefaultOverwriteState
	{
		Rename = 0,
		Decline = 1,
		Overwrite = 2
	}
}