// WindowUtility.cs created with MonoDevelop
// User: draek at 5:17 P 17/03/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

namespace Galaxium.Client
{
	/// <summary>
	/// Utility used to manage the client's container windows using the
	/// specified platform specific window utility.
	/// </summary>
	public static class WindowUtility<Widget>
	{
		private static IWindowUtility<Widget> _utility;
		
		public static IWindowManager<Widget> WindowManager
		{
			get { return _utility.WindowManager; }
		}
		
		public static IProtocol LastConversationProtocol
		{
			get { return _utility.LastConversationProtocol; }
			set { _utility.LastConversationProtocol = value; }
		}
		
		public static void Initialize (IWindowUtility<Widget> utility)
		{
			_utility = utility;
		}
		
		public static void CloseAll ()
		{
			_utility.CloseAll ();
		}
		
		public static void UpdateAll ()
		{
			_utility.UpdateAll ();
		}
		
		public static void Activate (IConversation conversation, bool requested)
		{
			_utility.Activate (conversation, requested);
		}
		
		public static void RemoveWindow (IContainerWindow<Widget> window)
		{
			_utility.RemoveWindow (window);
		}
		
		public static void RemoveWindows (ISession session)
		{
			_utility.RemoveWindows (session);
		}
		
		public static IContainerWindow<Widget> GetWindow ()
		{
			return _utility.GetWindow ();
		}
		
		public static IContainerWindow<Widget> GetWindow (IConversation conversation)
		{
			return _utility.GetWindow (conversation);
		}
	}
}
