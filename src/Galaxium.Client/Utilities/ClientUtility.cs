/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Client
{
	/// <summary>
	/// Used to control and store basic client information and control the
	/// initialization and shutdown of major utilities.
	/// </summary>
	public static class ClientUtility
	{
		public const string ClientName = "Galaxium Messenger";
		public const string ClientVersion = "0.8.0";
		public const string ClientWebsite = "http://galaxium.googlecode.com";
		
		public static void Initialize ()
		{
			ConfigurationUtility.Initialize ();
			
			PlatformUtility.Initialize ();

			ProtocolUtility.Initialize ();
			AccountUtility.Initialize ();
			
			NetworkUtility.Initialize ();
			
			ActivityUtility.Initialize();
			NotificationUtility.Initialize();
			
			SoundUtility.Initialize ();
			
			SongPlayingUtility.Initialize ();
			
			foreach (StaticMethodExtension node in AddinUtility.GetExtensionNodes("/Galaxium/InitializeMethods"))
				node.InvokeMethod();
		}

		public static void Shutdown ()
		{
			foreach (StaticMethodExtension node in AddinUtility.GetExtensionNodes("/Galaxium/ShutdownMethods"))
				node.InvokeMethod();

			AccountUtility.Unload ();
			ProtocolUtility.Unload ();

			SoundUtility.Unload ();
			
			SongPlayingUtility.Unload ();
			
			PlatformUtility.Unload ();

			//shutdown configuration as last, so last-minute config changes can be stored
			ConfigurationUtility.Save ();
		}
	}
}