/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Anculus.Core;
using Galaxium.Core;

using Mono.Addins;

namespace Galaxium.Client
{
	/// <summary>
	/// Utility used to manage the sound backends and use them to
	/// play specific sounds required by the client.
	/// </summary>
	public static class SoundUtility
	{
		private static Dictionary<string, ISoundBackend> _backends;
		private static ISoundBackend _activeBackend;

		internal static void Initialize()
		{
			_backends = new Dictionary<string,ISoundBackend>();
			LoadBackends();
			AddinManager.ExtensionChanged += OnExtensionChanged;
		}
		
		static void LoadBackends()
		{
			_backends = new Dictionary<string, ISoundBackend>();
			ActiveBackend = null;
							
			foreach (SoundBackendExtension node in AddinUtility.GetExtensionNodes("/Galaxium/Backends/Sound"))
			{
				try
				{
					ISoundBackend backend = (ISoundBackend)node.GetInstance();
					_backends.Add(node.Name, backend);
				}
				catch (Exception e)
				{
					Log.Error("Error Loading SoundBackend " + node.Name, e);
				}
			}
				
			foreach (string backendName in _backends.Keys) {
				ISoundBackend backend = _backends[backendName];
				if (backend.IsAvailable ()) {
					ActiveBackend = backend;
					break;
				}
			}
		}

		internal static void Unload ()
		{
			ActiveBackend = null;
			_backends.Clear ();
		}

		public static ISoundBackend ActiveBackend
		{
			get { return _activeBackend; }
			set
			{
				if (_activeBackend != value)
				{
					if (_activeBackend != null)
						_activeBackend.Shutdown ();
						
					_activeBackend = value;
					
					if (_activeBackend != null)
					{
						_activeBackend.Initialize ();
						Log.Info ("Using sound backend: {0}", _activeBackend);
					}
				}
			}
		}
		
		public static ISoundPlayer Open (string filename)
		{
			if (_activeBackend == null)
				return null;
			
			return _activeBackend.Open (filename);
		}
		
		public static void Play (string filename)
		{
			ISoundPlayer player = Open (filename);
			
			if (player == null)
				return;
			
			player.EndOfStream += PlayerEOS;
			player.Play ();
		}
		
		static void PlayerEOS (object sender, EventArgs args)
		{
			ISoundPlayer player = (ISoundPlayer)sender;
			
			player.EndOfStream -= PlayerEOS;
			
			player.Stop ();
			player.Dispose ();
		}
		
		static void OnExtensionChanged (object o, ExtensionEventArgs args)
		{
			if (args.PathChanged("/Galaxium/Backends/Sound"))
				LoadBackends();
		}
	}
}