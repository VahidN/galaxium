/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

using Anculus.Core;

namespace Galaxium.Client
{
	/// <summary>
	/// Window managers are platform/toolkit specific tools used to control
	/// container windows needed by the client.
	/// </summary>
	public interface IWindowManager<Widget>
	{
		IEnumerable<IContainerWindow<Widget>> Windows { get; }
		
		IContainerWindow<Widget> GetContainerWindow (string uid);
		
		IContainerWindow<Widget> GetContainerWindow (IConversation conversation);
		
		void RemoveContainerWindow (IContainerWindow<Widget> window);
		
		void Clear ();
		void Destroy ();
	}
}
