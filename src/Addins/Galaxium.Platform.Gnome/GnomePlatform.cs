/*
 * Galaxium Messenger
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Client;

namespace Galaxium.Platform.Gnome
{
	public class GnomePlatform : AbstractPlatform
	{
		public override string Identifier
		{
			get { return "Gnome"; }
		}
		
		public override void Initialize ()
		{
			//LoadProxySettings ();
		}
		
		// CHECKTHIS
	//	private void LoadProxySettings ()
	//	{
	//		GConf.Client client = new GConf.Client ();
	//		IConfigurationSection section = Configuration.Proxy.Section;
	//		
	//		//TODO: add "copy platform proxy settings" option in GUI + default value
	//		
	//		if (!section.GetBool ("AutoDetectPlatformSettings"))
	//			return;
	//		
	//		//TODO:
//			public static DefaultValue<bool> UseProxy = new DefaultValue<bool> ("UseProxy", false);
//			public static DefaultValue<bool> UseSame = new DefaultValue<bool> ("UseSame", false);
	//		
	//		//http proxy settings
	//		bool useHttpProxy = (bool)client.Get ("/system/http_proxy/use_http_proxy");
	//		if (useHttpProxy) {
	//			string httpHost = (string)client.Get ("/system/http_proxy/host");
	//			int httpPort = (int)client.Get ("/system/http_proxy/port");
	//			
	//			section.SetString ("HttpHost", httpHost);
	//			section.SetInt ("HttpPort", httpPort);
	//			
	//			bool useHttpAuth = (bool)client.Get ("/system/http_proxy/use_authentication");
	//			if (useHttpAuth) {
	//				string httpAuthUser = (string)client.Get ("/system/http_proxy/authentication_user");
	//				string httpAuthPass = (string)client.Get ("/system/http_proxy/authentication_password");
	//				
	//				section.SetString ("HttpUsername", httpAuthUser);
	//				section.SetString ("HttpPassword", httpAuthPass);
	//			} else {
	//				section.SetString ("HttpUsername", String.Empty);
	//				section.SetString ("HttpPassword", String.Empty);
	//			}
	//		} else {
	//			section.SetString ("HttpHost", String.Empty);
	//			section.SetInt ("HttpPort", 0);
	//			
	//			section.SetString ("HttpUsername", String.Empty);
	//			section.SetString ("HttpPassword", String.Empty);
	//		}
	//		
	//		string proxyMode = (string)client.Get ("/system/proxy/mode");
	//		if (String.Compare (proxyMode, "manual", true) == 0) {
	//			string socksHost = (string)client.Get ("/system/proxy/socks_host");
	//			int socksPort = (int)client.Get ("/system/proxy/socks_host");
	//			
	//			string httpsHost = (string)client.Get ("/system/proxy/secure_host");
	//			int httpsPort = (int)client.Get ("/system/proxy/secure_host");
	//			
	//			section.SetString ("HttpsHost", httpsHost);
	//			section.SetInt ("HttpsPort", httpsPort);
	//			
	//			section.SetString ("SocksHost", socksHost);
	//			section.SetInt ("SocksPort",socksPort);
	//		} else {
	//			section.SetString ("HttpsHost", String.Empty);
	//			section.SetInt ("HttpsPort", 0);
	//			
	//			section.SetString ("SocksHost", String.Empty);
	//			section.SetInt ("SocksPort", 0);
	//		}
	//		
	//		section.SetString ("HttpsUsername", String.Empty);
	//		section.SetString ("HttpsPassword", String.Empty);
	//		
	//		section.SetString ("SocksUsername", String.Empty);
	//		section.SetString ("SocksPassword", String.Empty);
	//	}
	}
}