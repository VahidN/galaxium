/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Gui;

namespace Galaxium.AdiumThemes
{
	public class AdiumEmoticonFactory: IEmoticonFactory
	{
		string _cacheDir;
		string _systemDir;
		string _userDir;

		public void Initialize(string cacheDir)
		{
			_cacheDir = Path.Combine(cacheDir, "Adium");
			_systemDir = Path.Combine(CoreUtility.GetDataSubDirectory("Themes"), "AdiumEmoticons");
			_userDir = Path.Combine(CoreUtility.GetConfigurationSubDirectory("Themes"), "AdiumEmoticons");
			
			BaseUtility.CreateDirectoryIfNeeded(_cacheDir);
			BaseUtility.CreateDirectoryIfNeeded(_userDir);
		}
		
		public List<IEmoticonSet> Sets
		{
			get
			{
				List<IEmoticonSet> sets = new List<IEmoticonSet>();
				
				sets.AddRange (LoadSetsFromDirectory (_systemDir));
				sets.AddRange (LoadSetsFromDirectory (_userDir));
				
				return sets;
			}
		}
		
		private IEnumerable<IEmoticonSet> LoadSetsFromDirectory (string directory)
		{
			if (!Directory.Exists (directory))
			{
				Log.Error ("Emoticon directory {0} not found", directory);
			}
			else
			{
				foreach (string archivename in Directory.GetFiles (directory))
				{
					IEmoticonSet set = null;
					try
					{
						if (ArchiveUtility.IsFileSupported (archivename))
							set = new AdiumEmoticonSet (archivename, _cacheDir);
					}
					catch (Exception e)
					{
						Log.Warn (e, "Unable to load set '{0}'", archivename);
					}
					
					if (set != null)
						yield return set;
				}
			}
		}
	}
}