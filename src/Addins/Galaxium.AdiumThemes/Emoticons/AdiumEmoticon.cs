using System;

using Galaxium.Gui;

namespace Galaxium.AdiumThemes
{
	public class AdiumEmoticon: BaseEmoticon
	{
		public AdiumEmoticon(string name, string[] equivalents, string filename, byte[] data)
			: base(name, equivalents, filename, data)
		{
		}
		
		public AdiumEmoticon(string name, string[] equivalents, string filename)
			: this(name, equivalents, filename, new byte[0])
		{
		}
	}
}
