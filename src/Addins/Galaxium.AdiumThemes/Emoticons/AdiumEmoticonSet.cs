/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Galaxium.Gui;
using Galaxium.Protocol;

namespace Galaxium.AdiumThemes
{
	public class AdiumEmoticonSet: IEmoticonSet
	{
		string _archiveName;
		string _cacheDir;
		
		public AdiumEmoticonSet(string archivename, string cacheDir)
		{
			_archiveName = archivename;
			_cacheDir = Path.Combine(cacheDir, ArchiveUtility.GetArchiveNameWithoutExtension (archivename));
			
			if (!ArchiveUtility.Extract(_archiveName, _cacheDir))
				throw new ApplicationException("Unable to extract " + _archiveName);
				
			_name = ArchiveUtility.GetArchiveNameWithoutExtension (_archiveName);
			
			if (_name.Contains("."))
				_name = _name.Substring(0, _name.IndexOf("."));
			
			_emoticons = new List<IEmoticon>();
		}
		
		string _name;
		public string Name
		{
			get { return _name; }
		}
		
		string _creator = "Unknown";
		public string Creator
		{
			get { return _creator; }
		}
		
		string _description = string.Empty;
		public string Description
		{
			get { return _description; }
		}
		
		List<IEmoticon> _emoticons;
		public List<IEmoticon> Emoticons
		{
			get
			{
				if (_emoticons.Count == 0)
					LoadEmoticons();

				return _emoticons;
			}
		}
		
		public void LoadEmoticons()
		{
			_emoticons.Clear();
			
			string plistPath = ArchiveUtility.FindFile(_cacheDir, "Emoticons.plist", ".adiumemoticonset");

			if (File.Exists(plistPath))
			{
				// Load emoticons using plist
				
				string emotDir = Path.GetDirectoryName(plistPath);
				
				PList plist = new PList(plistPath);
				PListDict rootDict = (PListDict)plist[0];
				PListDict emotDict = (PListDict)rootDict["Emoticons"];
				
				foreach (string imgFilename in emotDict.Keys)
				{
					PListDict emotPropDict = (PListDict)emotDict[imgFilename];
				
					if (!File.Exists(Path.Combine(emotDir, imgFilename)))
						continue;
					
					string emotName = (string)emotPropDict["Name"];
					ArrayList arrEquivalents = (ArrayList)emotPropDict["Equivalents"];
					
					// For some reason some sets give empty strings...
					ArrayList equivalents = new ArrayList();
					foreach (string equiv in arrEquivalents)
						if (!string.IsNullOrEmpty(equiv))
							equivalents.Add(equiv);
					
					AdiumEmoticon emot = new AdiumEmoticon(
					                                       emotName,
					                                       (string[])equivalents.ToArray(typeof(string)),
					                                       Path.Combine(emotDir, imgFilename)
					                                       );
					
					_emoticons.Add(emot);
				}
			}
			else
			{
				// No plist - load emoticons from directories
				
				string maindir = ArchiveUtility.FindDir(_cacheDir, "adiumemoticonset");
				
				if (!Directory.Exists(maindir))
					return;
					
				string[] dirs = Directory.GetDirectories(maindir);
				
				foreach (string edir in dirs)
				{
					if (!edir.EndsWith(".emoticon"))
						continue;
					
					string emotName = edir.Substring(0, edir.Length - 9);
					string emotDir = Path.Combine(_cacheDir, edir);
					string imgFilename = string.Empty;
					
					foreach (string filename in Directory.GetFiles(emotDir))
					{
						if (Path.GetFileName(filename).ToLower().StartsWith("emoticon"))
							imgFilename = filename;
					}
					
					if (!File.Exists(Path.Combine(emotDir, imgFilename)))
						continue;
					
					string[] txtequivs = File.ReadAllLines(Path.Combine(emotDir, "TextEquivalents.txt"));
					
					ArrayList equivalents = new ArrayList();
					foreach (string equiv in txtequivs)
						if (!string.IsNullOrEmpty(equiv))
							equivalents.Add(equiv);
					
					AdiumEmoticon emot = new AdiumEmoticon(
					                                       emotName,
					                                       (string[])equivalents.ToArray(typeof(string)),
					                                       Path.Combine(emotDir, imgFilename)
					                                       );
					
					_emoticons.Add(emot);
				}
			}
		}
	}
}
