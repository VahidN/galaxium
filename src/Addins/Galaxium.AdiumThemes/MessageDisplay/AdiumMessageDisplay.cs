/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Client;
using Galaxium.Gui;
using Galaxium.Protocol;

namespace Galaxium.AdiumThemes
{
	public class AdiumMessageDisplay : AbstractMessageDisplay
	{
		AdiumMessageStyle _style;
		string _variant;
		
		MessageFlag _lastFlags;
		string _lastUid;
		
		AdiumSubstitutionCollection _headerFooterSubstitutions = new AdiumSubstitutionCollection ();
		AdiumSubstitutionCollection _messageSubstitutions = new AdiumSubstitutionCollection ();
		AdiumSubstitutionCollection _statusSubstitutions = new AdiumSubstitutionCollection ();
		
		IHTMLWidget HtmlWidget
		{
			get { return Widget as IHTMLWidget; }
		}
		
		public AdiumMessageStyle Style
		{
			get { return _style; }
			set
			{
				ThrowUtility.ThrowIfNull ("style", value);
				
				if (_style == value)
					return;
				
				_style = value;

				if (string.IsNullOrEmpty (_variant) || (!_style.Variants.Contains (_variant)))
					_variant = _style.DefaultVariant;
				
				Reload ();
			}
		}
		
		public string Variant
		{
			get { return _variant; }
			set
			{
				if (_variant == value)
					return;
				
				_variant = value;
				
				if (string.IsNullOrEmpty (_variant) || (!_style.Variants.Contains (_variant)))
					_variant = _style.DefaultVariant;
				
				Reload ();
			}
		}
		
		public AdiumMessageDisplay ()
			: base (HTMLUtility.CreateHTMLWidget (), true)
		{
			HtmlWidget.LinkClicked += delegate (object sender, LinkEventArgs args)
			{
				PlatformUtility.OpenUrl (args.URL);
				
				// Don't navigate
				return false;
			};
			HtmlWidget.Ready += delegate { Reload (); };
			
			_style = AdiumMessageStyleFactory.ActiveStyle;
			_variant = AdiumMessageStyleFactory.ActiveVariant;
			
			AdiumMessageStyleFactory.ActiveVariantChanged += ActiveVariantChanged;
			
			_headerFooterSubstitutions["chatName"] = new SubstitutionDelegate (delegate
			{
				return HtmlStripEncode (Conversation.PrimaryContact, (Conversation.ContactCollection.Count > 1) ? "Group Chat" : Conversation.PrimaryContact.DisplayIdentifier);
			});
			_headerFooterSubstitutions["sourceName"] = new SubstitutionDelegate (delegate
			{
				return HtmlStripEncode (Conversation.Session.Account, Conversation.Session.Account.UniqueIdentifier);
			});
			_headerFooterSubstitutions["destinationName"] = new SubstitutionDelegate (delegate
			{
				return HtmlEncode (Conversation.PrimaryContact.UniqueIdentifier);
			});
			_headerFooterSubstitutions["destinationDisplayName"] = new SubstitutionDelegate (delegate
			{
				return HtmlStripEncode (Conversation.PrimaryContact, Conversation.PrimaryContact.DisplayIdentifier);
			});
			_headerFooterSubstitutions["incomingIconPath"] = new SubstitutionDelegate (delegate
			{
				return (Conversation.PrimaryContact.DisplayImage != null) ? Conversation.PrimaryContact.DisplayImage.Filename : _style.DefaultIncomingDisplayIconFilename;
			});
			_headerFooterSubstitutions["outgoingIconPath"] = new SubstitutionDelegate (delegate
			{
				return (Conversation.Session.Account.DisplayImage != null) ? Conversation.Session.Account.DisplayImage.Filename : _style.DefaultOutgoingDisplayIconFilename;
			});
			_headerFooterSubstitutions["timeOpened"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				return HtmlEncode (FormatTime (DateTime.Now, param));
			});
			
			/*TODO:
    * %senderColor% - A color derived from the user's name. If a colon separated list of html colors is at Incoming/SenderColors.txt it will be used instead of the default colors. (Adium 1.0+)
    * %senderStatusIcon% - The path to the status icon of the sender (available, away, etc...) (Adium 1.0+)
    * %textbackgroundcolor{X}% - documentation forthcoming */
			/*
    * %messageClasses% - A space separated list of type information for messages, suitable for use as a class attribute. Currently available types are listed below. ("Adium 1.1+")
          o history - the message is from a previous conversation
          o outgoing - the message is being sent from this computer
          o incoming - the message is being sent to this computer
          o message - the message is actually a message, and not something like a status change (note: the exact meaning of this with regard to things like file transfers may change in a future version of Adium)
          o autoreply - the message is an automatic response, generally due to an away status
          o status - the message is a status change
          o event - the message is a notification of something happening (for example, encryption being turned on) */
			
			_messageSubstitutions["message"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				return GenerateMessageHtml (val as IMessage);
			});
			_messageSubstitutions["messageDirection"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				//TODO:
				return "ltr";
			});
			_messageSubstitutions["time"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				return HtmlEncode (FormatTime ((val as IMessage).TimeStamp, param));
			});
			_messageSubstitutions["shortTime"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				return HtmlEncode ((val as IMessage).TimeStamp.ToShortTimeString ());
			});
			_messageSubstitutions["service"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				return HtmlEncode ((val as IMessage).Source.Session.Account.Protocol.Name);
			});
			_messageSubstitutions["sender"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				return HtmlStripEncode ((val as IMessage).Source, (val as IMessage).Source.DisplayIdentifier);
			});
			_messageSubstitutions["senderDisplayName"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				return HtmlStripEncode ((val as IMessage).Source, (val as IMessage).Source.DisplayName);
			});
			_messageSubstitutions["senderScreenName"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				return HtmlEncode ((val as IMessage).Source.UniqueIdentifier);
			});
			_messageSubstitutions["userIconPath"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				return ((val as IMessage).Source.DisplayImage != null) ? (val as IMessage).Source.DisplayImage.Filename : ((val as IMessage).Source.Local ? _style.DefaultOutgoingDisplayIconFilename : _style.DefaultIncomingDisplayIconFilename);
			});

			/* TODO:
    * %messageClasses% - A space separated list of type information for messages, suitable for use as a class attribute. Currently available types are listed below. ("Adium 1.1+")
          o history - the message is from a previous conversation
          o outgoing - the message is being sent from this computer
          o incoming - the message is being sent to this computer
          o message - the message is actually a message, and not something like a status change (note: the exact meaning of this with regard to things like file transfers may change in a future version of Adium)
          o autoreply - the message is an automatic response, generally due to an away status
          o status - the message is a status change
          o event - the message is a notification of something happening (for example, encryption being turned on) */
			
			_statusSubstitutions["message"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				return GenerateMessageHtml (val as IMessage);
			});
			_statusSubstitutions["time"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				return HtmlEncode (FormatTime ((val as IMessage).TimeStamp, param));
			});
			_statusSubstitutions["shortTime"] = new SubstitutionDelegate (delegate (string param, object val)
			{
				return HtmlEncode ((val as IMessage).TimeStamp.ToShortTimeString ());
			});
		}
		
		public override void Dispose ()
		{
			AdiumMessageStyleFactory.ActiveVariantChanged -= ActiveVariantChanged;
			
			base.Dispose ();
		}
		
		public override void DisplayMessage (IMessage msg)
		{
			if (_style == null)
			{
				Log.Error ("No Style");
				return;
			}
			
			bool history = (msg.Flags & MessageFlag.History) == MessageFlag.History;
			bool newEntity = (msg.Source.UniqueIdentifier != _lastUid) || (msg.Flags != _lastFlags);
			
			string html = null;
			string function = null;
			
			if ((msg.Flags & MessageFlag.Message) == MessageFlag.Message)
			{
				html = _style.GenerateMessageHtml (msg, newEntity, history, _messageSubstitutions);
				function = newEntity ? "appendMessage" : "appendNextMessage";
			}
			else
			{
				html = _style.GenerateStatusHtml (msg, _statusSubstitutions);
				function = "appendMessage";
			}
			
			_lastUid = msg.Source.UniqueIdentifier;
			_lastFlags = msg.Flags;
			
			HtmlWidget.RunJavaScript (string.Format ("{0} ('{1}');", function, html));
		}
		
		public override void UpdateEmoticon (IEmoticon emot)
		{
			//getElementsByClassName
            //Written by Jonathan Snook, http://www.snook.ca/jonathan
            //Add-ons by Robert Nyman, http://www.robertnyman.com
			
			string js = 
				    "function getElementsByClassName(oElm, strTagName, strClassName)" +
					"{" +
					"    var arrElements = (strTagName == '*' && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);" +
					"    var arrReturnElements = new Array();" +
				   @"    strClassName = strClassName.replace(/\-/g, '\\-');" +
					"    var oRegExp = new RegExp('(^|\\s)' + strClassName + '(\\s|$)');" +
					"    var oElement;" +
					"    for(var i=0; i<arrElements.length; i++)" +
					"    {" +
					"        oElement = arrElements[i];" +
					"        if(oRegExp.test(oElement.className))" +
					"        {" +
					"            arrReturnElements.push(oElement);" +
					"        }" +
					"    }" +
					"    return (arrReturnElements)" +
					"}" +
					
					"var emoticons = getElementsByClassName(document, 'img', 'galaxiumEmoticon');" +
					"for (var i = 0; i < emoticons.length; i++)" +
					"{" +
					"    var url = unescape(emoticons[i].src).replace('file://', '');" +
					"    if (url != '" + emot.Filename + "')" +
					"        continue;" +
					"    emoticons[i].src = url;" +
					"    emoticons[i].style.width = '" + emot.Width.ToString () + "px';" +
					"    emoticons[i].style.height = '" + emot.Height.ToString () + "px';" +
					"}" +
					"scrollToBottom();";
			
			HtmlWidget.RunJavaScript (js);
		}
		
		public override void DisplayClear ()
		{
			_style = AdiumMessageStyleFactory.ActiveStyle;
			_variant = AdiumMessageStyleFactory.ActiveVariant;
			
			_lastUid = null;
			_lastFlags = 0;
			
			if (_style != null)
				HtmlWidget.LoadUrl (_style.GenerateBaseHtmlFile (_variant, _headerFooterSubstitutions));
			else
			{
				string html = string.Format ("<h1>Unable to load a style</h1><p>Galaxium was unable to load a message style from the directory {0}</p>",
				                             AdiumMessageStyleFactory._systemThemePath);
				
				HtmlWidget.LoadHtml (html);
			}
		}
		
		string GenerateMessageHtml (IMessage msg)
		{
			if (msg == null || msg.Chunks == null)
				return string.Empty;
			
			return GenerateHtml (msg.Chunks);
		}
		
		string GenerateHtml (IEnumerable<IMessageChunk> chunks)
		{
			string result = string.Empty;
			
			foreach (IMessageChunk chunk in chunks)
			{
				if (chunk is IEmoticonMessageChunk)
				{
					IEmoticon emot = (chunk as IEmoticonMessageChunk).Emoticon;
					
					string html = "<img src=\"";
					
					if (!string.IsNullOrEmpty (emot.Filename))
						html += emot.Filename;
					else
						Log.Warn("Emoticon With No Filename! " + emot.Name);
					
					html += "\" class=\"galaxiumEmoticon\" style=\"";
					
					if (emot.Width > 0)
						html += string.Format ("width: {0}px; ", emot.Width);
					if (emot.Height > 0)
						html += string.Format ("height: {0}px; ", emot.Height);
					
					html += string.Format ("\" title=\"{0} ({1})\"/>", HtmlEncode (emot.Name), HtmlEncode ((chunk as IEmoticonMessageChunk).Text));
					
					result += html;
				}
				else if (chunk is IURIMessageChunk)
				{
					result += string.Format ("<a href=\"{0}\">{1}</a>",
					                         XmlUtility.Encode ((chunk as IURIMessageChunk).URI),
					                         ApplyStyle (HtmlEncodeMessage ((chunk as IURIMessageChunk).Text), chunk.Style));
				}
				else if (chunk is ITextMessageChunk)
				{
					result += ApplyStyle (HtmlEncodeMessage ((chunk as ITextMessageChunk).Text), chunk.Style);
				}
			}
			
			return result;
		}
		
		string HtmlStripEncode (IEntity entity, string txt)
		{
			if (entity == null)
				return HtmlEncode (txt);
			
			return HtmlEncode (Message.Strip (txt, entity, null));
		}
		
		string HtmlEncode (string str)
		{
			string encoded = System.Web.HttpUtility.HtmlEncode (str);
			
			encoded = BaseUtility.ReplaceString (encoded, "'", "&apos;", true);
			encoded = BaseUtility.ReplaceString (encoded, "\r\n", "<br/>", true);
			encoded = BaseUtility.ReplaceString (encoded, "\n", "<br/>", true);
			encoded = BaseUtility.ReplaceString (encoded, "\r", "<br/>", true);
			encoded = BaseUtility.ReplaceString (encoded, "\\", "\\\\", true);
			
			return encoded;
		}
		
		string HtmlEncodeMessage (string str)
		{
			string ret = HtmlEncode (str);
			
			// Encode messages as adium does in AIHTMLDecoder.m
			
			ret = ret.Replace ("\t", " &nbsp;&nbsp;&nbsp;");
			
			if (ret.StartsWith (" "))
				ret = "&nbsp;" + ret.Substring (1);
			
			ret = ret.Replace ("  ", " &nbsp;");
			
			return ret;
		}
		
		string FormatTime (DateTime time, string format)
		{
			if (string.IsNullOrEmpty (format))
				return time.ToLongTimeString ();
			
			int i = 0;
			string ret = string.Empty;
			
			while (i < format.Length)
			{
				if (format[i] != '%')
				{
					ret += format[i++];
					continue;
				}
				
				char c = (format.Length > i + 1) ? format[i + 1] : '\0';
				
				switch (c)
				{
				case '%':
					ret += '%';
					break;
				case 'a':
					ret += time.ToString ("ddd");
					break;
				case 'A':
					ret += time.ToString ("dddd");
					break;
				case 'b':
					ret += time.ToString ("MMM");
					break;
				case 'B':
					ret += time.ToString ("MMMM");
					break;
				case 'c':
					ret += time.ToString ("hh:mm:ss tt dd/MM/yyyy zzz");
					break;
				case 'd':
					ret += time.ToString ("dd");
					break;
				case 'e':
					ret += time.ToString ("d");
					break;
				case 'F':
					ret += time.ToString ("FFF");
					break;
				case 'H':
					ret += time.ToString ("HH");
					break;
				case 'I':
					ret += time.ToString ("hh");
					break;
				case 'j':
					ret += time.DayOfYear.ToString ();
					break;
				case 'm':
					ret += time.ToString ("MM");
					break;
				case 'M':
					ret += time.ToString ("mm");
					break;
				case 'p':
					ret += time.ToString ("tt");
					break;
				case 'S':
					ret += time.ToString ("ss");
					break;
				case 'w':
					ret += ((int)time.DayOfWeek).ToString ();
					break;
				case 'x':
					ret += time.ToString ("dd/MM/yyyy zzz");
					break;
				case 'X':
					ret += time.ToString ("hh:mm:ss tt");
					break;
				case 'y':
					ret += time.ToString ("yy");
					break;
				case 'Y':
					ret += time.ToString ("yyyy");
					break;
				case 'Z':
					ret += TimeZone.CurrentTimeZone.StandardName;
					break;
				case 'z':
					ret += time.ToString ("zzz");
					break;
				default:
					break;
				}
				
				i += ((c == '\0') ? 1 : 2);
			}
			
			return ret;
		}
		
		string ApplyStyle (string msg, IMessageStyle style)
		{
			if (style == null)
				return msg;
			
			StringBuilder sb = new StringBuilder ();
			
			if (style.Bold)
				sb.Append ("<b>");
			if (style.Italic)
				sb.Append ("<i>");
			if (style.Underline)
				sb.Append ("<u>");
			if (style.Strikethrough)
				sb.Append ("<s>");
			
			// The font tag is old and depreciated, but adium uses it, so...
			bool font = false;
			bool fontStyle = false;
			
			if (!string.IsNullOrEmpty (style.Font))
			{
				if (!font)
				{
					sb.Append ("<font ");
					font = true;
				}
				
				sb.AppendFormat ("face=\"{0}\" ", style.Font);
			}
			
			if (style.Foreground != ARGBGradient.None)
			{
				if (!font)
				{
					sb.Append ("<font ");
					font = true;
				}
				
				sb.AppendFormat ("color=\"#{0:x6}\" ", (int)((ARGBColor)style.Foreground));
			}
			
			if (style.FontSize > 0)
			{
				if (!font)
				{
					sb.Append ("<font ");
					font = true;
				}
				
				sb.AppendFormat ("absz=\"{0}\" size=\"{1}\" ", style.FontSize, PointsToHTML (style.FontSize));
			}
			
			if (style.Background != ARGBGradient.None)
			{
				if (!fontStyle)
				{
					if (!font)
					{
						sb.Append ("<font ");
						font = true;
					}
					
					sb.Append ("style=\"");
					fontStyle = true;
				}
				
				sb.AppendFormat ("background-color: #{0:x6}; ", (int)((ARGBColor)style.Background));
			}
			
			if (fontStyle)
				sb.Append ("\"");
			if (font)
				sb.Append (">");
			
			sb.Append (msg);
			
			if (font)
				sb.Append ("</font>");
			
			if (style.Strikethrough)
				sb.Append ("</s>");
			if (style.Underline)
				sb.Append ("</u>");
			if (style.Italic)
				sb.Append ("</i>");
			if (style.Bold)
				sb.Append ("</b>");
			
			return sb.ToString ();
		}
		
		int PointsToHTML (int points)
		{
			if (points <= 9)
				return 1;
			if (points <= 10)
				return 2;
			if (points <= 12)
				return 3;
			if (points <= 14)
				return 4;
			if (points <= 18)
				return 5;
			if (points <= 24)
				return 6;
			
			return 7;
		}
		
		void ActiveVariantChanged (object sender, EventArgs args)
		{
			Reload ();
		}
	}
}
