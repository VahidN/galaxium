/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Gui;

namespace Galaxium.AdiumThemes
{
	public class AdiumMessageStyleFactory
	{
		public static event EventHandler ActiveStyleChanged;
		public static event EventHandler ActiveVariantChanged;
		
		static string _defaultStyle = "Candybars.AdiumMessageStyle.tar.gz";
		static string _defaultVariant = "Blue vs Green";
	
	// CHECKTHIS
	//	static List<string> _styleFilenames;
		static List<AdiumMessageStyle> _styles;
		
		static AdiumMessageStyle _activeStyle;
		static string _activeVariant;
		
		static IConfigurationSection _config = Configuration.MessageDisplay.Section["Adium"];
		static bool _initialized;
		
		internal static string _systemThemePath = CoreUtility.GetDataSubDirectory("Themes");
		internal static string _userThemePath = CoreUtility.GetConfigurationSubDirectory("Themes");
		internal static string _cacheThemePath = CoreUtility.GetConfigurationSubDirectory("Cache");
		
		internal static string _systemStylesPath = Path.Combine(_systemThemePath, "AdiumMessageStyles");
		internal static string _userStylesPath = Path.Combine(_userThemePath, "AdiumMessageStyles");
		internal static string _cacheStylesPath = Path.Combine(_cacheThemePath, "AdiumMessageStyles");
		
		static AdiumMessageStyleFactory()
		{
		// CHECKTHIS
		//	_styleFilenames = new List<string>();
			_styles = new List<AdiumMessageStyle>();
		}
		
		public static void Initialize()
		{
			if (_initialized)
				return;
			
			_initialized = true;
			
			BaseUtility.CreateDirectoryIfNeeded(_userStylesPath);
			BaseUtility.CreateDirectoryIfNeeded(_cacheStylesPath);
			
			LoadStyles();		
		}
		
		public static void Shutdown()
		{
			//Directory.Delete(_cacheStylesPath, true);
		}
		
		public static List<AdiumMessageStyle> Styles
		{
			get
			{
				Initialize ();
				return _styles;
			}
		}
		
		public static void LoadStyles()
		{
			Initialize ();
			_styles.Clear();
			
			string activefile = _config.GetString("Style", _defaultStyle);
			
			if (Directory.Exists (_systemStylesPath))
			{
				foreach (string file in Directory.GetFiles(_systemStylesPath))
				{
					AdiumMessageStyle style = LoadStyle(file);
					
					if (style != null)
					{
						_styles.Add(style);
						
						if (Path.GetFileName(file) == activefile)
							_activeStyle = style;
					}
				}
			}
			else
				Log.Error ("System styles path ({0}) not found", _systemStylesPath);
			
			if (Directory.Exists (_userStylesPath))
			{
				foreach (string file in Directory.GetFiles(_userStylesPath))
				{
					AdiumMessageStyle style = LoadStyle(file);
					
					if (style != null)
					{
						_styles.Add(style);
						
						if (Path.GetFileName(file) == activefile)
							_activeStyle = style;
					}
				}
			}
			else
				Log.Warn ("User styles path ({0}) not found", _userStylesPath);
			
			if (_activeStyle == null)
			{
				if (_styles.Count > 0)
					_activeStyle = _styles[0];
				else
					Log.Warn("Unable to find any message styles");
			}
			
			if (_activeStyle != null)
			{
				_activeVariant = _config.GetString("Variant", _defaultVariant);
				
				if (!_activeStyle.Variants.Contains(_activeVariant))
					_activeVariant = _activeStyle.Variants[0];
			}
		}
		
		public static AdiumMessageStyle LoadStyle (string filename)
		{
			try
			{
				return new AdiumMessageStyle (filename);
			}
			catch (Exception ex)
			{
				//TODO: handle this cleaner
				if (ex.Message.StartsWith ("Unsupported archive"))
					return null;
				
				Log.Error (ex, "Unable to load {0}", filename);
				return null;
			}
		}
		
		public static AdiumMessageStyle ActiveStyle
		{
			get
			{
				Initialize ();
				
				if (_activeStyle == null)
					Log.Warn ("No active style");
				
				return _activeStyle;
			}
			set
			{
				_activeStyle = value;
				
				_config.SetString("Style", Path.GetFileName (_activeStyle.ArchiveFilename));
				
				if (ActiveStyleChanged != null)
					ActiveStyleChanged (null, EventArgs.Empty);
			}
		}
		
		public static string ActiveVariant
		{
			get
			{
				Initialize ();
				return _activeVariant;
			}
			set
			{
				_activeVariant = value;
				
				_config.SetString("Variant", _activeVariant);
				
				if (ActiveVariantChanged != null)
					ActiveVariantChanged (null, EventArgs.Empty);
			}
		}
	}
}