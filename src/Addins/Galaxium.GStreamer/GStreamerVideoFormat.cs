/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Galaxium.Gui;

namespace Galaxium.GStreamer
{
	public class GStreamerVideoFormat : IVideoFormat
	{
		NativeGalaxiumVideoFormat _native;
		Dictionary<float, NativeGalaxiumFramerate> _framerates = new Dictionary<float, NativeGalaxiumFramerate> ();
		string _mimeType;
		int _width;
		int _height;
		
		internal NativeGalaxiumVideoFormat Native
		{
			get { return _native; }
		}
		
		public IEnumerable<float> Framerates
		{
			get
			{
				foreach (float fr in _framerates.Keys)
					yield return fr;
			}
		}
		
		public string MimeType
		{
			get { return _mimeType; }
		}
		
		public int Width
		{
			get { return _width; }
		}
		
		public int Height
		{
			get { return _height; }
		}
		
		internal unsafe GStreamerVideoFormat (NativeGalaxiumVideoFormat native)
		{
			_native = native;
			
			_mimeType = GLib.Marshaller.Utf8PtrToString (native.mimetype);
			_width = native.width;
			_height = native.height;
			
			for (int i = 0; i < native.num_framerates; i++)
			{
				//TODO: this will always get the first framerate
				NativeGalaxiumFramerate fr = native.framerates[i];
				
				float framerate = (float)fr.numerator / fr.denominator;
				
				_framerates[framerate] = fr;
			}
		}
		
		public override bool Equals (object o)
		{
			if (o is GStreamerVideoFormat)
			{
				GStreamerVideoFormat f2 = o as GStreamerVideoFormat;
				
				if (f2._mimeType != _mimeType)
					return false;
				if (f2._width != _width)
					return false;
				if (f2._height != _height)
					return false;
				if (f2._framerates.Count != _framerates.Count)
					return false;
				
				//TODO: check framerates
				/*for (int i = 0; i < _framerates.Count; i++)
				{
					if (f2._framerates[i] != _framerates[i])
						return false;
				}*/
				
				return true;
			}
			
			return base.Equals (o);
		}
		
		public override int GetHashCode ()
		{
			return _mimeType.GetHashCode () * _width.GetHashCode () * _height.GetHashCode () * _framerates.Count.GetHashCode ();
		}
		
		internal NativeGalaxiumFramerate GetHighestFramerate ()
		{
			float best = 0;
			
			foreach (float fr in _framerates.Keys)
			{
				if (fr > best)
					best = fr;
			}
			
			return _framerates[best];
		}
	}
}
