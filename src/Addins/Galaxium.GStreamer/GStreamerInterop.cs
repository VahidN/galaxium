/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Runtime.InteropServices;

using GLib;

namespace Galaxium.GStreamer
{
	internal delegate void GStreamerEosDelegate (IntPtr player);
	internal delegate void GStreamerErrorDelegate (IntPtr player, uint domain, int code, IntPtr error, IntPtr debug);
	internal delegate void GStreamerNewFrameDelegate (IntPtr video);
	internal delegate void GStreamerNewSizeDelegate (IntPtr video, int width, int height);
	
	internal static class GStreamerInterop
	{
		const string libName = "libgalaxium";
		
		[DllImport (libName)]
		internal static extern void gstreamer_init ();
		
#region Audio
		[DllImport (libName)]
		internal static extern IntPtr gstreamer_audio_open (IntPtr filename);
		
		[DllImport (libName)]
		internal static extern void gstreamer_audio_close (HandleRef player);
		
		[DllImport (libName)]
		internal static extern void gstreamer_audio_play (HandleRef player);
		
		[DllImport (libName)]
		internal static extern void gstreamer_audio_stop (HandleRef player);
		
		[DllImport (libName)]
		internal static extern void gstreamer_audio_set_eos_callback (HandleRef player, GStreamerEosDelegate cb);
		
		[DllImport (libName)]
		internal static extern void gstreamer_audio_set_error_callback (HandleRef player, GStreamerErrorDelegate cb);
#endregion
		
#region Video
		[DllImport (libName)]
		internal static unsafe extern NativeGalaxiumWebcamDevice *gstreamer_video_get_devices (int *num_devices);
		
		[DllImport (libName)]
		internal static extern IntPtr gstreamer_video_new_local (ref NativeGalaxiumWebcamDevice device, ref NativeGalaxiumVideoFormat format, ref NativeGalaxiumFramerate framerate);
		
		[DllImport (libName)]
		internal static extern IntPtr gstreamer_video_new_remote ();
		
		[DllImport (libName)]
		internal static extern void gstreamer_video_free (HandleRef handle);
		
		[DllImport (libName)]
		internal static extern void gstreamer_video_play (HandleRef handle);
		
		[DllImport (libName)]
		internal static extern void gstreamer_video_stop (HandleRef handle);
		
		[DllImport (libName)]
		internal static extern void gstreamer_video_write (HandleRef handle, byte[] buf, int count);
		
		[DllImport (libName)]
		internal static extern void gstreamer_video_set_encoder (HandleRef handle, IntPtr encoderstring);
		
		[DllImport (libName)]
		internal static extern void gstreamer_video_set_decoder (HandleRef handle, IntPtr decoderstring);
		
		[DllImport (libName)]
		internal static extern void gstreamer_video_set_surface (HandleRef handle, IntPtr surface);
		
		[DllImport (libName)]
		internal static extern void gstreamer_video_set_scale (HandleRef handle, int width, int height);
		
		[DllImport (libName)]
		internal static extern void gstreamer_video_set_newframe_callback (HandleRef handle, GStreamerNewFrameDelegate cb);
		
		[DllImport (libName)]
		internal static extern void gstreamer_video_set_newsize_callback (HandleRef handle, GStreamerNewSizeDelegate cb);
		
		[DllImport (libName)]
		internal static extern void gstreamer_video_set_error_callback (HandleRef handle, GStreamerErrorDelegate cb);
#endregion
	}
	
	[StructLayout (LayoutKind.Sequential)]
	internal struct NativeGalaxiumFramerate
	{
		internal int numerator;
		internal int denominator;
	}
	
	[StructLayout (LayoutKind.Sequential)]
	internal unsafe struct NativeGalaxiumVideoFormat
	{
		//char *mimetype;
		internal IntPtr mimetype;
		internal int width;
		internal int height;
		internal int num_framerates;
		internal NativeGalaxiumFramerate *framerates;
		//internal IntPtr framerates;
	}
	
	[StructLayout (LayoutKind.Sequential)]
	internal unsafe struct NativeGalaxiumWebcamDevice
	{
		//char *video_device;
		internal IntPtr video_device;
		//char *hal_udi;
		internal IntPtr hal_udi;
		//char *gstreamer_src;
		internal IntPtr gstreamer_src;
		//char *name;
		internal IntPtr name;
		internal int num_video_formats;
		internal NativeGalaxiumVideoFormat *video_formats;
	}
}
