/*
 * Galaxium Messenger
 *
 * Original Code By:
 * Copyright (C) 2007 Novell, Inc.
 * 
 * Modified By:
 * Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;

using Gtk;
using Gecko;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;

namespace Galaxium.Gecko
{
	public class GeckoHTMLWidget : Viewport, IHTMLWidget
	{
		public event EventHandler Ready;
		public event EventHandler Loaded;
		public event LinkEventHandler LinkClicked;
		
		WebControl _geckoWidget;
		
		Queue<string> _jsQueue = new Queue<string> ();
		bool _runJs = false;
		
		public GeckoHTMLWidget ()
		{
			BorderWidth = 0;
			ShadowType = Gtk.ShadowType.None;
			
			ParentSet += OnParentSet;
			
			_geckoWidget = new WebControl ();
			
			_geckoWidget.OpenUri += OnLinkClicked;
			_geckoWidget.Realized += OnWebControlRealized;
			_geckoWidget.ProgressAll += OnProgressAll;
			
			Add (_geckoWidget);
		}
		
		void OnLinkClicked (object sender, OpenUriArgs args)
		{
			if (args.AURI.StartsWith ("javascript:"))
				return;
			
			lock (_jsQueue)
			{
				if (!_runJs)
					return;
				
				if (LinkClicked != null)
					args.RetVal = !LinkClicked (this, new LinkEventArgs (args.AURI));
			}
		}
		
		void OnWebControlRealized (object sender, EventArgs args)
		{
			if (this.Ready != null)
				this.Ready (sender, args);
		}
		
		void OnProgressAll (object sender, ProgressAllArgs args)
		{
			if (args.Curprogress >= args.Maxprogress)
			{
				if (this.Loaded != null)
					this.Loaded (sender, args);
				
				// We have to wait before running javascript
				// or on some machines, some of the time, nothing
				// happens...
				TimerUtility.RequestCallback (delegate
				{
					lock (_jsQueue)
						_runJs = true;
					
					ProcessJSQueue ();
				}, 200);
			}
		}
		
		void OnParentSet(object sender, Gtk.ParentSetArgs args)
		{
			if (this.Parent is Gtk.ScrolledWindow)
			{
				((Gtk.ScrolledWindow)this.Parent).HscrollbarPolicy = Gtk.PolicyType.Never;
				((Gtk.ScrolledWindow)this.Parent).VscrollbarPolicy = Gtk.PolicyType.Never;
			}
		}
		
		public void LoadUrl (string url)
		{
			lock (_jsQueue)
			{
				_runJs = false;
				//_jsQueue.Clear();
			}
			
			_geckoWidget.LoadUrl (url);
		}
		
		public void LoadHtml (string html)
		{
			string filename = FileUtility.GetRandomFileName (System.IO.Path.GetTempPath ());
			File.WriteAllText (filename, html);
			LoadUrl (filename);
		}
		
		public void RunJavaScript (string js)
		{
			lock (_jsQueue)
				_jsQueue.Enqueue (js);
			
			ProcessJSQueue ();
		}
		
		// Make sure never to call ProcessJSQueue from within a lock(_jsQueue)
		void ProcessJSQueue ()
		{
			lock (_jsQueue)
			{
				if (!_runJs)
					return;
				
				while (_jsQueue.Count > 0)
				{
					string js = _jsQueue.Dequeue ();
					
					//Log.Debug ("Running {0}", js);
					_geckoWidget.LoadUrl ("javascript:" + js);
				}
			}
		}
	}
}