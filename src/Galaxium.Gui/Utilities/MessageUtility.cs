/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Globalization;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Gui
{
	public static class MessageUtility
	{
		private static Regex _regexHyperlinks;
	// CHECKTHIS
	//	private static Regex _regexEmails = new Regex (@"([\w\d\._-]+@[\w\d\._-]+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
	//	static Dictionary<IEntity, List<string>> _cachedSearchLists = new Dictionary<IEntity, List<string>> ();
	//	static Dictionary<IProtocol, IMessageSplitter> _splitters = new Dictionary<IProtocol, IMessageSplitter> ();

		static List<string> _uriSearchStrings = new List<string> ();
	
		// FIXME: already in Message
		public static string[] UriStarts = { "http://", "https://", "file://", "ftp://", "mms://", "www.", "ftp." };
		
		public static char[] WhitespaceChars = { ' ', '\t', '\r', '\n' };
		
		static MessageUtility ()
		{
			_uriSearchStrings.AddRange (UriStarts);
			
			_regexHyperlinks = new Regex (@"((" + string.Join ("|", UriStarts) + @")[\w\d\.\/\+&;=_:?-]+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
		}
		
		public static IEnumerable<IMessageChunk> ParseHyperlinks (string text, IMessageStyle style)
		{
			MatchCollection matches = _regexHyperlinks.Matches (text);
			if (matches.Count == 0) {
				yield return new TextMessageChunk (null, style, text);
			} else {
				int prev = 0;
				IMessageChunk subChunk = null;
				foreach (Match match in matches) {
					if (prev < match.Index) {
						string chunkText = text.Substring (prev, match.Index - prev);
						subChunk = new TextMessageChunk (null, style, chunkText);
						yield return subChunk;
					}
					
					subChunk = new URIMessageChunk (null, style, match.Value.ToString ());
					yield return subChunk;
					
					prev = match.Index + match.Length;
				}
				
				if (prev < (text.Length - 1)) {
					string chunkText = text.Substring (prev);
					subChunk = new TextMessageChunk (null, style, chunkText);
					yield return subChunk;
				}
			}
		}
	}
}
