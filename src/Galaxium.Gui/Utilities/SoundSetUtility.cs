/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;

using Anculus.Core;

using Galaxium.Client;
using Galaxium.Core;

using Mono.Addins;

namespace Galaxium.Gui
{
	public static class SoundSetUtility
	{
		static List<ISoundFactory> _factories;
		static List<ISoundSet> _sets;
		static ISoundSet _activeSet;
		static string _cacheDir;
		static IConfigurationSection _config = Configuration.Sound.Section;
		
		static ISoundPlayer _player;
		static Sound _playingSound;
		static object _playerLock = new object ();
		
		static SoundSetUtility()
		{
			_factories = new List<ISoundFactory>();
			_sets = new List<ISoundSet>();
			_activeSet = null;
			_cacheDir = Path.Combine(CoreUtility.GetConfigurationSubDirectory("Cache"), "Sounds");
		}
		
		public static List<ISoundSet> Sets
		{
			get { return _sets; }
		}
		
		public static ISoundSet ActiveSet
		{
			get { return _activeSet; }
			set
			{
				_activeSet = value;
				
				if (_activeSet != null) {
					_config.SetString(Configuration.Sound.Active.Name, _activeSet.Name);
					LoadSoundSetEnabledStates (_activeSet);
				}
				else
					_config.SetString(Configuration.Sound.Active.Name, string.Empty);
			}
		}
		
		public static void Initialize()
		{
			BaseUtility.CreateDirectoryIfNeeded(_cacheDir);
			LoadFactories();
			AddinManager.ExtensionChanged += OnExtensionChanged;
		}
		
		public static void Shutdown()
		{
			//if (Directory.Exists(_cacheDir))
			//	Directory.Delete(_cacheDir, true);
		}
		
		static void LoadFactories()
		{
			_factories.Clear();
			
			foreach (SoundFactoryExtension node in AddinUtility.GetExtensionNodes("/Galaxium/Themes/Sound"))
			{
				ISoundFactory factory = node.GetFactory();
				factory.Initialize(_cacheDir);
				_factories.Add(factory);
			}
			
			LoadSets();
		}
		
		static void LoadSets()
		{
			_sets.Clear();
			_activeSet = null;
			
			foreach (ISoundFactory factory in _factories)
			{
				foreach (ISoundSet soundset in factory.Sets)
				{
					_sets.Add (soundset);
				
					if (soundset.Name == _config.GetString(Configuration.Sound.Active.Name, Configuration.Sound.Active.Default))
						ActiveSet = soundset;
				}
			}
			
			if ((_activeSet == null) && !string.IsNullOrEmpty(_config.GetString(Configuration.Sound.Active.Name, Configuration.Sound.Active.Default)))
			{
				if (_sets.Count > 0)
					ActiveSet = _sets[0];
				else
					Log.Warn ("No Sound Sets Found");
			}
		}
		
		public static void Play (Sound sound)
		{
			if (_activeSet == null)
				return;
			
			if (_playingSound == sound)
				return;
			
			lock (_playerLock)
			{
				// Only play one sound at a time
				Stop ();
				
				foreach (ISound snd in _activeSet.Sounds)
				{
					if (snd.Sound == sound)
					{
						if (snd.Enabled == false)
							return;
						
						_player = SoundUtility.Open (snd.Filename);
						_playingSound = sound;
						
						if (_player != null)
						{
							_player.EndOfStream += PlayerEOS;
							_player.Play ();
						}
						
						return;
					}
				}
			}
		}
		
		static void PlayerEOS (object sender, EventArgs args)
		{
			Stop ();
		}
		
		public static void Stop ()
		{
			lock (_playerLock)
			{
				_playingSound = Sound.None;
				
				if (_player != null)
				{
					_player.EndOfStream -= PlayerEOS;
					
					_player.Stop ();
					_player.Dispose ();
					_player = null;
				}
			}
		}
		
		static void OnExtensionChanged (object o, ExtensionEventArgs args)
		{
			if (args.PathChanged("/Galaxium/Themes/Sound"))
				LoadFactories();
		}
		
		public static void LoadSoundSetEnabledStates (ISoundSet soundSet)
		{
			if (soundSet == null)
				throw new ArgumentNullException ("soundSet");
			
			IConfigurationSection section = Configuration.Sound.Section["SoundEnabledStates"][soundSet.Name];
			
			foreach (ISound sound in soundSet.Sounds)
				sound.Enabled = section.GetBool (sound.Sound.ToString (), true);
		}
		
		public static void SaveSoundSetEnabledStates (ISoundSet soundSet)
		{
			if (soundSet == null)
				throw new ArgumentNullException ("soundSet");
			
			IConfigurationSection section = Configuration.Sound.Section["SoundEnabledStates"][soundSet.Name];
			
			foreach (ISound sound in soundSet.Sounds)
				section.SetBool (sound.Sound.ToString (), sound.Enabled);
		}
	}
}
