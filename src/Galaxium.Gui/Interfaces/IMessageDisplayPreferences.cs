using System;

namespace Galaxium.Gui
{
	public interface IMessageDisplayPreferences
	{
		object Widget { get; }
	}
}
