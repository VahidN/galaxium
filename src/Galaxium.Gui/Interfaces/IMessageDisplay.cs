using System;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Gui
{
	public interface IMessageDisplay : IDisposable
	{
		object Widget { get; }
		
		void Init (IConversation conversation);
		void AddMessage (IMessage msg);
		void Clear ();
		bool Save (string filename);
		void UpdateEmoticon (IEmoticon emot);
	}
}
