using System;

namespace Galaxium.Gui
{
	public interface IHTMLWidget
	{
		event EventHandler Ready;
		event EventHandler Loaded;
		event LinkEventHandler LinkClicked;
		
		void LoadUrl (string url);
		void LoadHtml (string html);
		void RunJavaScript (string js);
	}
}
