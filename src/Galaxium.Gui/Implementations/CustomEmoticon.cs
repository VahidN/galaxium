/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Drawing;
using System.IO;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Gui
{
	public class CustomEmoticon : AbstractCustomEmoticon
	{
		IConfigurationSection _config;
		
		public CustomEmoticon (string name, string[] equivalents, string filename, IEntity allowedSrc, IEntity allowedDest)
			: base (name, equivalents, CacheFilename (filename))
		{
			_config = EmoticonUtility._customConfig[Path.GetFileName (Filename)];
			
			Source = ConfigName (allowedSrc);
			Destination = ConfigName (allowedDest);
			
			File.Copy (filename, Filename);
			
			Save ();
		}
		
		public CustomEmoticon (IConfigurationSection config)
			: base (config.Name, config.GetString ("Equivalents").Split (' '), config.GetString ("Filename"))
		{
			_config = config;
			Load ();
		}
		
		protected override int GetWidth ()
		{
			int val = -1;
			
			try
			{
				MemoryStream stream = new MemoryStream (Data);
				Image img = Image.FromStream (stream);
				val = img.Width;
				stream.Close ();
			}
			catch { }
			
			return val;
		}
		
		protected override int GetHeight ()
		{
			int val = -1;
			
			try
			{
				MemoryStream stream = new MemoryStream (Data);
				Image img = Image.FromStream (stream);
				val = img.Height;
				stream.Close ();
			}
			catch { }
			
			return val;
		}
		
		public void Save ()
		{
			_config.SetString ("Name", Name);
			_config.SetString ("Equivalents", string.Join (" ", Equivalents));
			_config.SetString ("Filename", Filename);
			_config.SetString ("Source", Source);
			_config.SetString ("Destination", Destination);
		}
		
		public void Load ()
		{
			Name = _config.GetString ("Name");
			Equivalents = _config.GetString ("Equivalents").Split (' ');
			Filename = _config.GetString ("Filename");
			Source = _config.GetString ("Source");
			Destination = _config.GetString ("Destination");
		}
		
		public void Remove ()
		{
			_config.Parent.RemoveSection (_config.Name);
			
			try
			{
				File.Delete (Filename);
			}
			catch (Exception ex)
			{
				Log.Error (ex, "Unable to delete file for emoticon '{0}'", this);
			}
		}
		
		internal static string CacheFilename (string filename)
		{
			ThrowUtility.ThrowIfFalse ("File doesn't exist", File.Exists (filename));
			
			return Path.Combine (EmoticonUtility._customDir, FileUtility.GetFileChecksum (filename));
		}
	}
}
