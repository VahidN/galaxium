/*
 * Galaxium Messenger
 *
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections;
using System.Drawing;
using System.IO;

using Galaxium.Protocol;

namespace Galaxium.Gui
{
	public class BaseEmoticon: AbstractEmoticon
	{
		public BaseEmoticon (string name, string[] equivalents, string filename, byte[] data)
			: base (name, equivalents, filename)
		{
			Data = data;
		}
		
		protected override int GetWidth ()
		{
			int val = -1;
			
			try
			{
				MemoryStream stream = new MemoryStream (Data);
				Image img = Image.FromStream (stream);
				val = img.Width;
				stream.Close ();
			}
			catch { }
			
			return val;
		}
		
		protected override int GetHeight ()
		{
			int val = -1;
			
			try
			{
				MemoryStream stream = new MemoryStream (Data);
				Image img = Image.FromStream (stream);
				val = img.Height;
				stream.Close ();
			}
			catch { }
			
			return val;
		}
	}
}