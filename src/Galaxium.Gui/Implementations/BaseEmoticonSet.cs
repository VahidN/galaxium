/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Protocol;

namespace Galaxium.Gui
{
	public class BaseEmoticonSet: IEmoticonSet
	{
		public BaseEmoticonSet(string name, string creator, string description)
		{
			_name = name;
			_creator = creator;
			_description = description;
		}

		string _name;
		public string Name
		{
			get { return _name; }
		}
		
		string _creator = "Unknown";
		public string Creator
		{
			get { return _creator; }
		}
		
		string _description = string.Empty;
		public string Description
		{
			get { return _description; }
		}
		
		public List<IEmoticon> Emoticons
		{
			get { return new List<IEmoticon>(); }
		}
	}
}
