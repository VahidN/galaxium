/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Gui
{
	public static class IconSizes
	{
		private static IIconSize _other;
		private static IIconSize _tiny;
		private static IIconSize _small;
		private static IIconSize _mediumSmall;
		private static IIconSize _medium;
		private static IIconSize _large;
		private static IIconSize _extraLarge;
		private static IIconSize _huge;
		private static IIconSize _gigantic;

		public static IIconSize Other
		{
			get
			{
				if (_other == null)
					_other = new OtherIconSize ();
				return _other;
			}
		}

		public static IIconSize Tiny
		{
			get
			{
				if (_tiny == null)
					_tiny = new TinyIconSize ();
				return _tiny;
			}
		}

		public static IIconSize Small
		{
			get
			{
				if (_small == null)
					_small = new SmallIconSize ();
				return _small;
			}
		}

		public static IIconSize MediumSmall
		{
			get
			{
				if (_mediumSmall == null)
					_mediumSmall = new MediumSmallIconSize ();
				return _mediumSmall;
			}
		}

		public static IIconSize Medium
		{
			get
			{
				if (_medium == null)
					_medium = new MediumIconSize ();
				return _medium;
			}
		}

		public static IIconSize Large
		{
			get
			{
				if (_large == null)
					_large = new LargeIconSize ();
				return _large;
			}
		}

		public static IIconSize ExtraLarge
		{
			get
			{
				if (_extraLarge == null)
					_extraLarge = new ExtraLargeIconSize ();
				return _extraLarge;
			}
		}

		public static IIconSize Huge
		{
			get
			{
				if (_huge == null)
					_huge = new HugeIconSize ();
				return _huge;
			}
		}

		public static IIconSize Gigantic
		{
			get
			{
				if (_gigantic == null)
					_gigantic = new GiganticIconSize ();
				return _gigantic;
			}
		}

	}

	public sealed class OtherIconSize : AbstractIconSize
	{
		internal OtherIconSize ()
			: base (-1, "other")
		{
		}
	}

	public sealed class TinyIconSize : AbstractIconSize
	{
		internal TinyIconSize ()
			: base (12, "tiny")
		{
		}
	}

	public sealed class SmallIconSize : AbstractIconSize
	{
		internal SmallIconSize ()
			: base (16, "small")
		{
		}
	}

	public sealed class MediumSmallIconSize : AbstractIconSize
	{
		internal MediumSmallIconSize ()
			: base (24, "medium-small")
		{
		}
	}

	public sealed class MediumIconSize : AbstractIconSize
	{
		internal MediumIconSize ()
			: base (32, "medium")
		{
		}
	}

	public sealed class LargeIconSize : AbstractIconSize
	{
		internal LargeIconSize ()
			: base (48, "large")
		{
		}
	}

	public sealed class ExtraLargeIconSize : AbstractIconSize
	{
		internal ExtraLargeIconSize ()
			: base (64, "extra-large")
		{
		}
	}

	public sealed class HugeIconSize : AbstractIconSize
	{
		internal HugeIconSize ()
			: base (96, "huge")
		{
		}
	}

	public sealed class GiganticIconSize : AbstractIconSize
	{
		internal GiganticIconSize ()
			: base (128, "gigantic")
		{
		}
	}
}