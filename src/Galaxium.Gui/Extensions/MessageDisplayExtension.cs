/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Reflection;

using Mono.Addins;

using Anculus.Core;

using Galaxium.Core;

namespace Galaxium.Gui
{
	public class MessageDisplayExtension : ExtensionNode
	{
		[NodeAttribute]
		string id = null;
		
		[NodeAttribute]
		string type = null;
		
		[NodeAttribute]
		string icon = null;
		
		public string Icon
		{
			get { return icon; }
		}
		
		public string Name
		{
			get { return id; }
		}
		
		public IMessageDisplay CreateInstance ()
		{
			return Addin.CreateInstance (type, false) as IMessageDisplay;
		}
		
		public override bool Equals (object o)
		{
			return base.Equals (o) || id.Equals (o);
		}
		
		public override int GetHashCode ()
		{
			return id.GetHashCode ();
		}
	}
}
