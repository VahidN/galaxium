/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

namespace Galaxium.Gui
{
	public class AbstractSoundSet: ISoundSet
	{
		string _name;
		string _creator;
		string _description;
		List<ISound> _sounds = new List<ISound>();
		
		public string Name { get { return _name; } }
		public string Creator {	get { return _creator; } }
		public string Description { get { return _description; } }
		public List<ISound> Sounds { get { return _sounds; } }

		public AbstractSoundSet(string name, string creator, string description)
		{
			_name = name;
			_creator = creator;
			_description = description;
		}
	}
}
