/*
 * Copyright (C) 2005-2007  Ben Motmans  <ben.motmans@gmail.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

using System;
using System.Collections.Generic;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Gui
{
	public interface ITreeBuilder
	{
		ITreeNodeSorter Sorter { get; set; }
		SortOrder SortOrder { get; set; }
		void Resort ();
		
		ITreeNodeFilter Filter { get; set; }
		void Refilter ();
		
		bool NodeExists (uint node);

		uint AddNode (object obj);
		uint AddNode (uint? parent, object obj);
		uint[] AddNodes (object parent, object obj);

		uint InsertNode (int index, object obj);
		uint InsertNode (int index, uint? parent, object obj);

		bool UpdateNode (uint node);
		bool UpdateNode (uint node, bool updateChildren);
		bool RemoveNode (uint node);

		bool SwapNode (uint node1, uint node2);

		uint[] GetNodes (object obj);
		uint[] GetNodes (uint? parent, object obj);
		
		bool GetFirstNode (object obj, out uint node);
		bool GetFirstNode (uint? parent, object obj, out uint node);
		
		object GetNodeObject (uint node);

		uint? GetParent (uint node);
		object GetParentObject (uint node);

		uint[] GetChildren ();
		uint[] GetChildren (uint? parent);
		
		bool GetFirstChild (out uint node);
		bool GetFirstChild (uint? parent, out uint node);

		int GetChildCount ();
		int GetChildCount (uint? parent);
		
		uint[] GetVisibleChildren ();
		uint[] GetVisibleChildren (uint? parent);
		
		bool GetVisibleFirstChild (out uint node);
		bool GetVisibleFirstChild (uint? parent, out uint node);

		int GetVisibleChildCount ();
		int GetVisibleChildCount (uint? parent);

		int GetNodeIndex (uint node);
		int[] GetNodeIndices (uint node);

		bool GetNextNode (uint node, out uint next);
		bool GetPreviousNode (uint node, out uint previous);
		
		uint[] GetSelectedNodes ();
		bool GetSelectedNode (out uint node);
		int GetSelectionCount ();
		bool IsNodeSelected (uint node);
		
		void ExpandNode (uint node);
		void CollapseNode (uint node);
		
		void SelectNode (uint node);
		
		TreeNodeContext GetTreeNodeContext (uint node);

		string GetComparableContent (TreeNodeContext ctx);

		void Clear ();
	}
}