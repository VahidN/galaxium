/*
 * Copyright (C) 2005-2007  Ben Motmans  <ben.motmans@gmail.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Gui
{
	public sealed class TreeNodeContext
	{
		private ITreeBuilder _treeBuilder;
		private uint _node;
		private object _object;

		public TreeNodeContext (ITreeBuilder treeBuilder, uint node, object nodeObject)
		{
			this._treeBuilder = treeBuilder;
			this._node = node;
			this._object = nodeObject;
		}

		public ITreeBuilder TreeBuilder
		{
			get { return this._treeBuilder; }
		}

		public uint Node
		{
			get { return this._node; }
		}

		public object NodeObject
		{
			get { return this._object; }
		}
	}
}