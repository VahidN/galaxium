// 
//   Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//  
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as
//   published by the Free Software Foundation, either version 3 of the
//   License, or (at your option) any later version.
//  
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//  
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//  
//  


using System;
using System.IO;
using System.Collections.Generic;
using Galaxium.Protocol.Xmpp.Library.Xml;

namespace Galaxium.Protocol.Xmpp.Library.Streams
{
	public class RequestResult
	{
		public bool Accepted { get; private set; }
		public string DeclineReason { get; private set; }
		public string Method { get; private set; }
		public Element ProfileData { get; private set; }
		
		public StreamReceivedHandler Handler { get; private set; }
		
		public RequestResult (bool accepted, string decline_reason, string method,
		                      Element profile_data, StreamReceivedHandler handler)
		{
			Accepted = accepted;
			DeclineReason = decline_reason;
			Method = method;
			ProfileData = profile_data;
			Handler = handler;
		}
	}
	
	
	public interface IStreamProfileListener
	{
		string ProfileNamespace { get; }
		
		RequestResult HandleRequest (JabberID uid, string sid, string mime_type,
		                             IEnumerable<string> methods, Element data);
	}
}
