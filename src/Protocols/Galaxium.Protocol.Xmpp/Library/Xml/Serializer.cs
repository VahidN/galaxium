// 
//   Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//  
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as
//   published by the Free Software Foundation, either version 3 of the
//   License, or (at your option) any later version.
//  
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//  
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//  
//  

using System;
using System.IO;
using System.Text;

namespace Galaxium.Protocol.Xmpp.Library.Xml
{
	public static class Serializer
	{
		public static string Serialize (Element elm)
		{
			return Serialize (elm, (int?) null);
		}
		
		public static string Serialize (Element elm, int? indentation)
		{
			if (elm.IsAnonymous)
				return Normalization.Normalize (elm.Text);
			
			var str = new StringBuilder ();
			
			str.Append (new String (' ', indentation ?? 0) + '<');
			str.Append ((elm.Prefix == null ? String.Empty : elm.Prefix + ":") + elm.Name);
			
			foreach (var attr in elm.Attributes)
				str.Append (String.Format (" {0}='{1}'", attr.Key, Normalization.Normalize (attr.Value)));
			
			if (elm.IsEmpty) {
				str.Append (" />");
			} else {
				str.Append (">");
				if (indentation != null) str.Append ("\n");
				foreach (var child in elm)
					str.Append (Serialize (child, indentation == null ? null : indentation + 2));
				str.Append (new String (' ', indentation ?? 0));
				str.Append (String.Format ("</{0}>", elm.Name));
			}
			
			if (indentation != null)
				str.Append ("\n");
			
			return str.ToString ();
		}
		
		public static void Serialize (Element elm, Stream stream)
		{
			throw new NotImplementedException ();
		}
	}
}
