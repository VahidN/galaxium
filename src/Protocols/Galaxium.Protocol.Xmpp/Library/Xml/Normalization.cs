// 
//  Copyright © 2008, 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 

using System;

namespace Galaxium.Protocol.Xmpp.Library.Xml
{
	public static class Normalization
	{
		public static string Normalize (string text)
		{
			if (text == null)
				throw new ArgumentNullException ();
			
			string result = text;
			result = result.Replace ("&", "&amp;");
			result = result.Replace ("<", "&lt;");
			result = result.Replace (">", "&gt;");
			result = result.Replace ("'", "&apos;");
			result = result.Replace ("\"", "&quot;");
			return result;
		}
		
		public static string Unnormalize (string text)
		{
			string result = text;
			result = result.Replace ("&lt;", "<");
			result = result.Replace ("&gt;", ">");
			result = result.Replace ("&apos;", "'");
			result = result.Replace ("&quot;", "\"");
			result = result.Replace ("&amp;", "&");
			return result;
		}
	}
}
