// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


// TODO: human readable representation

using System;

using Galaxium.Protocol.Xmpp.Library.Xml;

namespace Galaxium.Protocol.Xmpp.Library.Core
{
	public class StreamErrorEventArgs: EventArgs
	{
		public StreamError Error { get; private set; }
		public StreamErrorEventArgs (StreamError error) { Error = error; }
	}
	
	public class StreamError: Element
	{
		public StreamError (Element elm)
			:base (elm.Clone ())
		{
			if (elm.Name != "stream:error")
				throw new ArgumentException ();
		}
		
		public string Condition {
			get { return FirstChild (null, null).Name; }
		}
		
		public string Description {
			get {
				Element desc = FirstChild ("text", null);
				return (desc == null) ? null : desc.GetText ();
			}
		}
	}
}