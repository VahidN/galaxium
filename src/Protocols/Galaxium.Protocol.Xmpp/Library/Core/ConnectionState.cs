// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 

using System;

namespace Galaxium.Protocol.Xmpp.Library.Core
{
	public enum ConnectionState {
		Disconnected, Connecting, StreamInit, EncryptInit, Authenticating, Binding, Connected
	}
	
	public class ConnectionStateEventArgs: EventArgs
	{
		public ConnectionState State { get; private set; }
		public ConnectionStateEventArgs (ConnectionState state) { State = state; }
	}
	
	public static class ConnectionStateDescription
	{
		public static string Get (ConnectionState state)
		{
			switch (state) {
				case ConnectionState.Disconnected:
					return "You are disconnected";
				case ConnectionState.Connecting:
					return "Connecting to the server";
				case ConnectionState.StreamInit:
					return "Initiating data stream";
				case ConnectionState.EncryptInit:
					return "Initiating stream encryption";
				case ConnectionState.Authenticating:
					return "Authenticating";
				case ConnectionState.Binding:
					return "Binding resource and initiating session";
				case ConnectionState.Connected:
					return "Connected";
			}
			return null;
		}
	}
}
