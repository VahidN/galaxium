
using System;

namespace Galaxium.Protocol.Xmpp.Library.Core
{
	public interface IMessageListener
	{
		bool HandleMessage (Core.XmlMessage message);
	}
}
