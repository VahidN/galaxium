// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;
using Galaxium.Protocol.Xmpp.Library.Xml;

namespace Galaxium.Protocol.Xmpp.Library.Core
{
	public class StreamFeatures: Element
	{
		public StreamFeatures (Element elm)
			:base (elm)
		{
		}

		public bool Supports (string name, string xmlns)
		{
			return FirstChild (name, xmlns) != null;
		}

		public bool IsOptional (string name, string xmlns)
		{
			var feature = FirstChild (name, xmlns);
			if (feature == null)
				throw new ArgumentException ();
			return feature.FirstChild ("optional", null) != null;
		}
		
		public bool SupportsTLS {
			get { return Supports ("starttls", Namespaces.Tls); }
		}

		public bool SupportsSASL {
			get { return Supports ("mechanisms", Namespaces.Sasl); }
		}
		
		public bool MandatorySASL {
			get {
				var elm = FirstChild ("mechanisms", Namespaces.Sasl);
				foreach (Element child in elm) {
					if (child.Name == "required") return true;
				}
				return false;
			}
		}
		
		public bool SupportsSASLMechanism (string mechanism)
		{
			var elm = FirstChild ("mechanisms", Namespaces.Sasl);
			foreach (Element child in elm) {
				if (child.Name == "mechanism" && child.Text == mechanism) return true;
			}
			return false;
		}

	}
}