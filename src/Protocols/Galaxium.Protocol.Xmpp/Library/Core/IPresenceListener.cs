
using System;

namespace Galaxium.Protocol.Xmpp.Library.Core
{
	public interface IPresenceListener
	{
		bool HandlePresence (Core.Presence pres);
	}
}
