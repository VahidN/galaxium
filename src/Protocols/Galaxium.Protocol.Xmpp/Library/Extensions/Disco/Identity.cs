// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;

using Galaxium.Protocol.Xmpp.Library.Utility;

namespace Galaxium.Protocol.Xmpp.Library.Extensions.Disco
{
	public class Identity: IComparable<Identity>
	{		
		public string Category { get; private set; }
		public string Type     { get; private set; }
		public string Name     { get; private set; }
		public string Language { get; private set; }
		
		public Identity (string category, string type, string name, string lang)
		{
			Category = category;
			Type = type;
			Name = name;
			Language = lang;
		}
		
		public IdentityDescription GetTypeDescription ()
		{
			return IdentityDescription.Get (Category, Type);
		}
		
		public int CompareTo (Identity other)
		{
			var comparer = OctetCollationComparer.Instance;
			
			var result = comparer.Compare (Category, other.Category);
			if (result != 0) return result;
			result = comparer.Compare (Type, other.Type);
			if (result != 0) return result;
			return comparer.Compare (Language, other.Language);
		}
		
		public override string ToString ()
		{
			return string.Format("[Identity: Category={0}, Type={1}, Name={2}, Language={3}]", Category, Type, Name, Language);
		}

	}
}
