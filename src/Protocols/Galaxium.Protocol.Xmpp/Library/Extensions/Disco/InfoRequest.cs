// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;
using System.Text;
using System.Security.Cryptography;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


using Galaxium.Protocol.Xmpp.Library.Core;
using Galaxium.Protocol.Xmpp.Library.Xml;
using Galaxium.Protocol.Xmpp.Library.Utility;


namespace Galaxium.Protocol.Xmpp.Library.Extensions.Disco
{
	public class InfoRequest
	{
		public event EventHandler ReceivedEvent;
		public event EventHandler<StanzaErrorEventArgs> ErrorEvent;
		
		private CoreStream _stream;
		
		public JabberID Jid { get; private set; }
		public string Node { get; private set; }
		
		public Info Result { get; private set; }
		
		public InfoRequest (CoreStream stream, JabberID jid, string node)
		{
			_stream = stream;
			Jid = jid;
			Node = node;
		}

		public void Request ()
		{
			_stream.SendQuery (Info.CreateRequest (Jid, Node), Receive);
		}
		
		private void Receive (Iq result)
		{
			if (result.IsError || result.Query ["node"] != Node) {
				if (ErrorEvent != null)
					ErrorEvent (this, new StanzaErrorEventArgs (result.Error));
			}
			else {
				Result = new Info (result.Query);
				if (ReceivedEvent != null)
					ReceivedEvent (this, EventArgs.Empty);
			}
		}
	}
}
