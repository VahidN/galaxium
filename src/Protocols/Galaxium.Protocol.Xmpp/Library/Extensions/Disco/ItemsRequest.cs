// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;

using Galaxium.Protocol.Xmpp.Library.Core;

namespace Galaxium.Protocol.Xmpp.Library.Extensions.Disco
{
	public class ItemsRequest
	{
		public event EventHandler ReceivedEvent;
		public event EventHandler<StanzaErrorEventArgs> ErrorEvent;
		
		private CoreStream _stream;
		private JabberID _jid;
		private string _node;
		
		public Items Result { get; private set; }
		
		public ItemsRequest (CoreStream stream, JabberID jid, string node)
		{
			_stream = stream;
			_jid = jid;
			_node = node;
		}
		
		public void Request ()
		{
			_stream.SendQuery (Items.CreateRequest (_jid, _node), Receive);	
		}
		
		private void Receive (Iq result)
		{
			if (result.IsError) {
				if (ErrorEvent != null)
					ErrorEvent (this, new StanzaErrorEventArgs (result.Error));
			}
			else {
				Result = new Items (result.Query);
				if (ReceivedEvent != null)
					ReceivedEvent (this, EventArgs.Empty);
			}
		}
	}
}
