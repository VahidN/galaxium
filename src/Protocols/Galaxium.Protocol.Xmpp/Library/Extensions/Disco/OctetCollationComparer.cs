// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;
using System.Text;
using System.Collections.Generic;

namespace Galaxium.Protocol.Xmpp.Library.Extensions.Disco
{
	public class OctetCollationComparer: IComparer<string>
	{
		private static OctetCollationComparer _instance;
		
		public static OctetCollationComparer Instance {
			get {
				if (_instance == null)
					_instance = new OctetCollationComparer ();
				return _instance;
			}
		}
		
		public int Compare (string a, string b)
		{
			var bytes_a = Encoding.UTF8.GetBytes (a ?? String.Empty);
			var bytes_b = Encoding.UTF8.GetBytes (b ?? String.Empty);
			
			for (int i = 0; i < Math.Min (bytes_a.Length, bytes_b.Length); ++ i) {
				var val = bytes_a [i].CompareTo (bytes_b [i]);
				if (val != 0) return val;
			}
			
			return bytes_a.Length.CompareTo (bytes_b.Length);
		}
	}
}
