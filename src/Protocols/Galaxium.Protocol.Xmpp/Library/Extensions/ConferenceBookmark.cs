// 
//   Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//  
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as
//   published by the Free Software Foundation, either version 3 of the
//   License, or (at your option) any later version.
//  
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//  
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//  
//  

using System;

using Galaxium.Protocol.Xmpp.Library.Xml;
using Galaxium.Protocol.Xmpp.Library.Core;

namespace Galaxium.Protocol.Xmpp.Library.Extensions
{
	public class ConferenceBookmark
	{
		public ConferenceBookmark (Element elm)
		{
			if (elm == null)
				throw new ArgumentNullException ("elm");
			
			UID = elm ["jid"];
			Name = elm ["name"];
			Nickname = elm.GetTextChild ("nick", null);
			Password = elm.GetTextChild ("pass", null);
			
			var auto_join = elm ["autojoin"];
			if (auto_join != null)
				AutoJoin = auto_join.ToLower () == "true" || auto_join == "1";
		}
		
		public ConferenceBookmark (JabberID uid, string name,
		                           string nick, string pass, bool auto_join)
		{
			if (uid == null)
				throw new ArgumentNullException ("uid");
			
			UID = uid;
			Name = name;
			Nickname = nick;
			Password = pass;
			AutoJoin = auto_join;
		}
		
		public JabberID UID { get; private set; }
		public string Name { get; private set; }
		
		public string Nickname { get; private set; }
		public string Password { get; private set; }
		
		public bool AutoJoin { get; private set; }
		
		public override bool Equals (object obj)
		{
			if (!(obj is ConferenceBookmark)) return false;
			return UID.Equals ((obj as ConferenceBookmark).UID);
		}

		public override int GetHashCode ()
		{
			return UID.GetHashCode ();
		}
		
		public Element ToXml ()
		{
			var elm = new Element ("conference");
			elm ["autojoin"] = AutoJoin.ToString ();
			elm ["jid"] = UID;
			elm ["name"] = Name;
			
			if (Nickname != null)
				elm.AppendTextChild ("nick", null, Nickname);
			
			if (Password != null)
				elm.AppendTextChild ("pass", null, Password);
			
			return elm;
		}
	}
}
