// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 


using System;
using System.Text;

namespace Galaxium.Protocol.Xmpp.Library.Utility
{
	public static class StringUtils
	{
		public static string Random (int length)
		{
			Random rnd = new Random ();
			byte a = Encoding.ASCII.GetBytes ("a") [0];
			byte A = Encoding.ASCII.GetBytes ("A") [0];
			byte z = Encoding.ASCII.GetBytes ("z") [0];
			byte Z = Encoding.ASCII.GetBytes ("Z") [0];
			byte zero = Encoding.ASCII.GetBytes ("0") [0];

			StringBuilder s = new StringBuilder ();
			for (int i = 0; i < length; i++) {
				switch (rnd.Next (3)) {
				case 0:
					s.Append (Char.ConvertFromUtf32 (rnd.Next (10) + zero));
					break;
				case 1:
					s.Append (Char.ConvertFromUtf32 (rnd.Next (z - a + 1) + a));
					break;
				case 2:
					s.Append (Char.ConvertFromUtf32 (rnd.Next (Z - A + 1) + A));
					break;
				}
			}
			return s.ToString ();
		}
		
		public static string FirstLine (string text)
		{
			text = text.Trim ();
			int line_end = text.IndexOf ('\n');
			return (line_end == -1) ? text : (text.Substring (0, line_end).Trim () + " …");
		}

		/// <summary>
		/// Breaks lines based on number of characters (doesn't take character width into account)
		/// An extension method for <see cref="System.String"/>.
		/// </summary>
		/// <param name="text">
		/// Text to break (this).
		/// </param>
		/// <param name="length">
		/// The maximum line length.
		/// </param>
		/// <returns>
		/// A broken string.
		/// </returns>
		public static string BreakLines (string text, int length)
		{
			char newl = '\n';
			char space = ' ';
			int line_length;

			StringBuilder s = new StringBuilder ();
			foreach (string line in text.Split (newl)) {
				s.Append (newl);
				if (line == String.Empty) continue;
				
				line_length = 0;
				foreach (string word in line.Split (space)) {
					if ((line_length + word.Length) > length) {
						s.Append (newl);
						line_length = 0;
					}
					line_length += word.Length + 1;
					s.Append (word + space);
				}
				s.Remove (s.Length - 1, 1);  // remove the last space
			}
			s.Remove (0, 1);  // remove the first newline
			return s.ToString ();
		}

		public static string Indent (string str, int indent)
		{
			return StringUtils.PrependLines (str, new String (' ', indent));
		}

		public static string PrependLines (string str, string fragment)
		{
			str = fragment + str;
			str = str.Replace ("\n", "\n" + fragment);
			return str;
		}

		public static string LimitLength (string str, int length)
		{
			return (str.Length <= length) ? str : (str.Substring (0, length - 1) + '…');
		}
	}
}
