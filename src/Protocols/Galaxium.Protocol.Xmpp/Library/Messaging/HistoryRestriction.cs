
using System;
using Galaxium.Protocol.Xmpp.Library.Xml;

namespace Galaxium.Protocol.Xmpp.Library
{
	public abstract class HistoryRestriction
	{
		protected Element ToXml (string attr_name, string attr_value)
		{
			var elm = new Element ("history");
			elm [attr_name] = attr_value;
			return elm;
		}
		
		public abstract Element ToXml ();
	}
	
	public class CharacterNumberHistoryRestriction: HistoryRestriction
	{
		public int MaxCharacters { get; set; }
		public CharacterNumberHistoryRestriction (int max_chars) { MaxCharacters = max_chars; }
		
		public override Element ToXml ()
		{
			return ToXml ("maxchars", MaxCharacters.ToString ());
		}
	}
	
	public class StanzaNumberHistoryRestriction: HistoryRestriction
	{
		public int MaxStanzas { get; set; }
		public StanzaNumberHistoryRestriction (int max_stanzas) { MaxStanzas = max_stanzas; }
		
		public override Element ToXml ()
		{
			return ToXml ("maxstanzas", MaxStanzas.ToString ());
		}
	}
	
	public class TimeSpanHistoryRestriction: HistoryRestriction
	{
		public int Seconds { get; set; }
		public TimeSpanHistoryRestriction (int seconds) { Seconds = seconds; }
		
		public override Element ToXml ()
		{
			return ToXml ("seconds", Seconds.ToString ());
		}
	}
	
	public class TimeSinceHistoryRestriction: HistoryRestriction
	{
		public DateTime Since { get; set; }
		public TimeSinceHistoryRestriction (DateTime since) { Since = since; }
		
		public override Element ToXml ()
		{
			return ToXml ("since", Since.ToUniversalTime ()
			              .ToString ("yyyy-MM-dd'T'HH*':'mm*':'ss*'Z'"));
		}
	}
}
