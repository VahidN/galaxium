// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 



using System;

using Galaxium.Protocol.Xmpp.Library.Xml;

namespace Galaxium.Protocol.Xmpp.Library.Sasl
{
	[Serializable]
	public class SaslError: ApplicationException
	{
		private Element m_element;
		public Element ErrorElement {
			get { return m_element; }
		}
		
		public string ErrorCondition {
			get { return GetErrorCondition (m_element); }
		}
		
		public SaslError (Element elm)
			:base (GetErrorCondition (elm))
		{
			m_element = elm;
		}		
		
		private static string GetErrorCondition (Element elm)
		{
			return elm.FirstChild (null, null).Name;
		}
	}
}