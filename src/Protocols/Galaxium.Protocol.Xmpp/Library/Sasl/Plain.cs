// 
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
// 
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// 

using System;

using Galaxium.Protocol.Xmpp.Library.Xml;
using Galaxium.Protocol.Xmpp.Library.Core;
using Galaxium.Protocol.Xmpp.Library.Utility;

namespace Galaxium.Protocol.Xmpp.Library.Sasl
{
	// SASL PLAIN authentication helper (RFC2595)
	public class Plain: Mechanism
	{
		public Plain (Func<Element, Element> method): base (method)
		{
		}
		
		// Authenticate via sending password in clear-text
		public override void Auth (JabberID uid, string password)
		{
			string auth_text = uid.Bare () + "\0" + uid.Node + "\0" + password;
			Element auth = GenerateAuth ("PLAIN", Cryptography.Base64Encode (auth_text));
			
			Element response = Send (auth);
			if (response.Name != "success")
				throw new SaslError (response);
		}
	}
}
