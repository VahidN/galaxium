// 
// XmppContact.cs
// 
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Anculus.Core;
using Galaxium.Core;
using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Messaging;

namespace Galaxium.Protocol.Xmpp
{
	public class XmppMucContact: AbstractContact
	{
		public new XmppSession Session {
			get { return base.Session as XmppSession; }
		}
		
		public new XmppPresence Presence {
			get { return base.Presence as XmppPresence; }
			set { base.Presence = value; }
		}

		public XmppConference Conference { get; private set; }
		
		public bool IsBeingReplaced { get; set; }
		
		private int _pending;
		
		public int PendingMessages {
			get { return _pending; }
			set {
				_pending = value;
				Session.EmitContactChanged (new ContactEventArgs (this));
			}
		}
		
		public XmppMucContact (XmppSession session, XmppConference conference,
		                       JabberID uid, XmppPresence pres)
			:base (session, uid, uid.Resource, pres)
		{
			Conference = conference;
		}
		
		public XmppPrivateConversation GetConversation ()
		{
			return Conference.GetPrivateConversation (DisplayName);
		}
	}
}
