// 
// XmppContact.cs
// 
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Core;

namespace Galaxium.Protocol.Xmpp
{
	public class XmppContact: AbstractContact, IComparable
	{
		public Contact InternalContact { get; private set; }

		public new XmppSession Session {
			get { return base.Session as XmppSession; }
		}
		
		public new XmppPresence Presence {
			get { return base.Presence as XmppPresence; }
			set { base.Presence = value; }
		}

		public XmppContact (XmppSession session, JabberID jid)
			:base (session, jid, null, XmppPresence.Offline)
		{
			InternalContact = session.Client.Roster.GetContact (jid);
		}

		public int CompareTo (object other)
		{
			return GetHashCode ().CompareTo (other.GetHashCode ());
		}

		public override void Save()
		{
			base.Save ();
			
			if (InternalContact.IsInRoster && InternalContact.Name != Nickname)
				Session.Client.Roster.RequestContactNameChange (InternalContact, Nickname);
		}
	}
}
