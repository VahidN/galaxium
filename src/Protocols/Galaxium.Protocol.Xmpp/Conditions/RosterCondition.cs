// 
// ContactInRosterCondition.cs
// 
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Mono.Addins;

using Galaxium.Core;
using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Utility;

namespace Galaxium.Protocol.Xmpp
{
	public class RosterCondition: ConditionType
	{
		public RosterCondition ()
		{
			SessionUtility.ActiveSession.ContactChanged += (sender, args) => NotifyChanged ();
			SessionUtility.SelectedContactChanged += (sender, args) => NotifyChanged ();
		}

		public override bool Evaluate (NodeElement conditionNode)
		{
			if (!(SessionUtility.SelectedContact is XmppContact)) return false;
			var contact = SessionUtility.SelectedContact as XmppContact;
			bool in_roster = Boolean.Parse (conditionNode.GetAttribute ("in_roster"));
			return contact.InternalContact.IsInRoster == in_roster;
		}
	}
}
