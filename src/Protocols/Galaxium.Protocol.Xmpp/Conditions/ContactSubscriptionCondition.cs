// 
// ContactSubscriptionCondition.cs
// 
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using Mono.Addins;

using Galaxium.Core;
using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Utility;

namespace Galaxium.Protocol.Xmpp
{
	public class ContactSubscriptionCondition: ConditionType
	{
		public ContactSubscriptionCondition ()
		{
			SessionUtility.ActiveSession.ContactChanged += (sender, args) => NotifyChanged ();
			SessionUtility.SelectedContactChanged += (sender, args) => NotifyChanged ();
		}

		public override bool Evaluate (NodeElement conditionNode)
		{
			if (!(SessionUtility.SelectedContact is XmppContact)) return false;
			
			var contact = (SessionUtility.SelectedContact as XmppContact).InternalContact;
			
			string has_subscription = conditionNode.GetAttribute ("has_subscribed");
			string subscribed = conditionNode.GetAttribute ("subscribed");
			string waiting = conditionNode.GetAttribute ("waiting");
			string asking = conditionNode.GetAttribute ("asking");

			bool condition =
				((!String.IsNullOrEmpty (has_subscription) && contact.HasSubscription != Boolean.Parse (has_subscription))
			    || (!String.IsNullOrEmpty (subscribed) && contact.IsSubscribed != Boolean.Parse (subscribed))
			    || (!String.IsNullOrEmpty (waiting) && contact.IsWaiting != Boolean.Parse (waiting))
			    || (!String.IsNullOrEmpty (asking) && contact.IsAsking != Boolean.Parse (asking)));
			
			return !condition;
		}
	}
}
