// 
// XmppGroup.cs
// 
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Utility;

namespace Galaxium.Protocol.Xmpp
{
	public class XmppGroup: AbstractGroup
	{
		public new XmppSession Session {
			get { return base.Session as XmppSession; }
		}
		
		public override int OnlineCount {
			get { return Session.Client.Roster.OnlineInGroup (UniqueIdentifier); }
		}

		public override int OfflineCount {
			get { return Count - OnlineCount; }
		}

		public XmppGroup (XmppSession session, string group)
			:base (session, "R_" + group, String.IsNullOrEmpty (group) ? "Ungrouped" : group, false)
		{
		}

		protected override void OnNameChange (NameChangeEventArgs args)
		{
			throw new NotSupportedException ();
		}
	}

	public class XmppNotInRosterGroup: AbstractGroup
	{
		public static string Identifier { get { return "V_nroster"; }}
		
		public override int OnlineCount {
			get { return 0; }
		}

		public override int OfflineCount {
			get { return 0; }
		}
		
		public XmppNotInRosterGroup (XmppSession session)
			:base (session, Identifier, "Not in Roster", true)
		{
		}
	}
	
	public class XmppConferenceGroup: AbstractGroup
	{
		public static string Identifier { get { return "V_conferences"; }}
		
		public new XmppSession Session {
			get { return base.Session as XmppSession; }
		}
		
		public override int OnlineCount {
			get {
				return 0;//Session.GetOfflineConferenceCount ();
			}
		}

		public override int OfflineCount {
			get {
				return 0;//Session.GetOfflineConferenceCount ();
			}
		}

		public XmppConferenceGroup (XmppSession session)
			:base (session, Identifier, "Conferences", true)
		{
		}		
	}
}
