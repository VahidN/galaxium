// 
// XmppConversationManager.cs
// 
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections;
using System.Collections.Generic;

using Galaxium.Client;
using Galaxium.Core;
using Galaxium.Protocol;

using L=Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Messaging;


namespace Galaxium.Protocol.Xmpp
{
	public class XmppConversationManager: IConversationManager
	{
		// NOTE: these events are never used
		public event EventHandler<ConversationEventArgs> ConversationAdded = delegate {};
		public event EventHandler<ConversationEventArgs> ConversationRemoved = delegate {};

		private Dictionary<XmppContact, XmppConversation> _conversations =
			new Dictionary<XmppContact, XmppConversation> ();
		
		private L.Client _client;
		private XmppSession _session;
		
		public XmppConversationManager (XmppSession session, L.Client client)
		{
			_session = session;
			_client = client;
			_client.ConversationCreated += (sender, e) => {
				// we need to make the manager avare of the new conversation
				ThreadUtility.SyncDispatch (() => { WrapConversation (e.Conversation); });
			};
		}
		
		private void WrapConversation (Conversation conversation)
		{
			var contact = _session.GetContact (conversation.Jid);
			if (_conversations.ContainsKey (contact))
				throw new Exception ("Conversation already exists.");
			Add (new XmppConversation (contact, contact.Session));
		}
		
		private void Add (XmppConversation conversation)
		{
			if (_conversations.ContainsKey (conversation.Contact)) return; 
			
			_conversations [conversation.Contact] = conversation;
			ConversationAdded (this, new ConversationEventArgs (conversation));
		}

		private void Remove (XmppConversation conversation)
		{
			if (!_conversations.ContainsKey (conversation.Contact))
				throw new ArgumentOutOfRangeException ("Conversation not in the dictionary");
			
			conversation.Close ();
			_conversations.Remove (conversation.Contact);
			ConversationRemoved (this, new ConversationEventArgs (conversation));
		}

		public XmppConversation GetConversation (XmppContact contact)
		{
			if (!_conversations.ContainsKey (contact))
				_client.GetConversation (contact.InternalContact.Jid);
				
			return _conversations [contact];
		}
		
		public XmppConference GetConference (XmppConferenceContact contact)
		{
			return contact.Conference;
		}
		
		public XmppPrivateConversation GetConversation (XmppMucContact contact)
		{
			return contact.GetConversation ();
		}
		
		#region interface
		
		void IConversationManager.Add (IConversation conversation)
		{
		}
		
		void IConversationManager.Remove (IConversation conversation)
		{
			ThrowUtility.ThrowIfInvalidType<XmppConversation> ("conversation", conversation);
			Remove (conversation as XmppConversation);
		}

		IConversation IConversationManager.GetInactiveConversation ()
		{
			foreach (var conversation in _conversations.Values)
				if (!conversation.Active) return conversation;

			return null;
		}
		
		IConversation IConversationManager.GetPrivateConversation (IContact contact)
		{
			return (this as IConversationManager).GetConversation (contact);
		}
		
		IConversation IConversationManager.GetInactiveConversation (IContact contact)
		{
			ThrowUtility.ThrowIfInvalidType<XmppContact> ("contact", contact);
			if (_conversations.ContainsKey (contact as XmppContact)) {
				var conversation = _conversations [contact as XmppContact];
				if (!conversation.Active) return conversation;
			}
			return null;
		}
		
		IConversation IConversationManager.GetConversation (IContact contact)
		{
			if (contact is XmppContact)
				return GetConversation (contact as XmppContact);
			if (contact is XmppConferenceContact)
				return GetConference (contact as XmppConferenceContact);
			if (contact is XmppMucContact)
				return GetConversation (contact as XmppMucContact);
			throw new ArgumentException ("Invalid type.", "contact");
		}
		
		IEnumerable<IConversation> IConversationManager.GetGroupConversations (IContact contact)
		{
			return null;
		}

		IEnumerator<IConversation> IEnumerable<IConversation>.GetEnumerator ()
		{
			return GetEnumerator () as IEnumerator<IConversation>;
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return _conversations.Values.GetEnumerator ();
		}

		#endregion

		public IEnumerator<XmppConversation> GetEnumerator ()
		{
			return _conversations.Values.GetEnumerator ();
		}

		
	}
}
