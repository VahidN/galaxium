// 
// XmppClient.cs
// 
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

using Anculus.Core;
using Galaxium.Protocol.Xmpp.Library;

namespace Galaxium.Protocol.Xmpp
{
	public class XmppClient: Library.Client
	{
		public XmppClient (JabberID uid)
			:base (uid, null)
		{
		}

		protected override string HandleInvalidResource (JabberID jid)
		{
			throw new System.NotImplementedException ();
		}

		protected override string HandleResourceConflict (JabberID jid)
		{
			throw new System.NotImplementedException ();
		}

		protected override string HandlePasswordRequest ()
		{
			OnErrorOccured (new Exception ("Invalid account or password"));
			return null;
		}

		protected override bool ConfirmUnsecuredConnection()
		{
			throw new System.NotImplementedException ();
		}

		protected override bool ConfirmCertificate (object sender,
		                                            X509Certificate certificate,
		                                            X509Chain chain,
		                                            SslPolicyErrors sslPolicyErrors)
		{
			return true;
		}
	}
}
