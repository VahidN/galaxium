// 
// Copyright © 2007 Ben Motmans <ben.motmans@gmail.com>
// Copyright © 2007 Philippe Durand <draekz@gmail.com>
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Client;

using Anculus.Core;

using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Core;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class OpenChatCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			context.TreeView.ActivateRow (context.Path, null);
		}
	}

	public class ComposeMessageCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			var contact = context.Value as XmppContact;
			Messages.MessageWindow.ComposeNew (contact.InternalContact.Client,
			                                   contact.InternalContact.Jid);
		}
	}
	
	public class ChangeNicknameCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			var contact = context.Value as XmppContact;
			new SetNicknameDialog (contact);
		}
	}

	public class EditGroupsCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}

		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			var contact = (context.Value as XmppContact).InternalContact;
			GroupEditDialog.Open (null, new Contact[] {contact});
		}
	}

	public class RequestAuthCommand: AbstractMenuCommand
	{
		public override void SetMenuItem()
		{
			var context = Object as ContactTreeContext;
			var contact = context.Value as XmppContact;
			if (contact.InternalContact.IsSubscribed || contact.InternalContact.IsAsking)
				(MenuItem as MenuItem).Sensitive = false;
		}

		public override void Run()
		{
			var context = Object as ContactTreeContext;
			var contact = (context.Value as XmppContact).InternalContact;
			SubscriptionRequestDialog.Open (null, contact);
		}
	}

	public class ApproveAuthCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			var context = Object as ContactTreeContext;
			var contact = (context.Value as XmppContact).InternalContact;

			(MenuItem as MenuItem).Sensitive = contact.IsWaiting;
		}

		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			var contact = (context.Value as XmppContact).InternalContact;
			contact.Client.Roster.ApproveSubscription (contact.Jid);
		}
	}

	public class DenyAuthCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			var context = Object as ContactTreeContext;
			var contact = (context.Value as XmppContact).InternalContact;

			(MenuItem as MenuItem).Sensitive = contact.IsWaiting || contact.HasSubscription;
		}

		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			var contact = (context.Value as XmppContact).InternalContact;
			contact.Client.Roster.DenySubscription (contact.Jid);
		}
	}

	public class RemoveContactCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}

		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			var contact = (context.Value as XmppContact).InternalContact;
			
			QuestionDialog.Open (null, "Do you really want to remove contact " + contact.Jid + "?",
			                     () => contact.Client.Roster.RequestContactRemoval (contact.Jid));
		}
	}
	
	public class AutoAcceptCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			var context = Object as ContactTreeContext;
			var contact = context.Value as XmppContact;

			(MenuItem as Widget).Sensitive = false;
			(MenuItem as CheckMenuItem).Active = contact.AutoAccept;
		}
		
		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			var contact = context.Value as XmppContact;
			
			contact.AutoAccept = (MenuItem as CheckMenuItem).Active;
			contact.Save ();
		}
	}
	
	public class SendFileContactCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			var context = Object as ContactTreeContext;
			var contact = context.Value as XmppContact;

			(MenuItem as MenuItem).Sensitive = contact.InternalContact.IsOnline;
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			XmppContact contact = context.Value as XmppContact;
			XmppSession session = contact.Session as XmppSession;
			
			if (contact != null && contact.DisplayName != session.Account.DisplayName)
			{
				SendFileDialog dialog = new SendFileDialog (contact);
				
				// Here we would fill the dialog with the options we intend to use to send the file.
				
				ResponseType response = (ResponseType) dialog.Run ();
				
				if (response == ResponseType.Apply)
				{
					if (dialog.Filename != null && dialog.Filename.Length > 0)
					{
						contact.Session.SendFile (contact, dialog.Filename);
					}
				}
				
				dialog.Destroy();
			}
		}
	}

	public class ViewInfoCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			var contact = context.Value as XmppContact;
			InformationDialog.Open (contact.InternalContact);
		}
	}
	
	public class DisplayLogCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			var context = Object as ContactTreeContext;
			var contact = context.Value as XmppContact;
			
			new ProtocolLogWindow ((contact.Session as XmppSession).CommunicationLog,
			                       contact.UniqueIdentifier);
		}
	}
}