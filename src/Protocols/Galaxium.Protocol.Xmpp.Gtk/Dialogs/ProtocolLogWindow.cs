
using System;

using Gtk;

using Galaxium.Protocol.Xmpp.Library;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class ProtocolLogWindow: Window
	{
		private ProtocolLogWidget _log;
		
		public ProtocolLogWindow (ProtocolLog log, JabberID jid)
			:base ("Log for JID: " + jid.Bare ())
		{
			_log = new ProtocolLogWidget (log, jid);
			var scroll = new ScrolledWindow ();
			scroll.ShadowType = ShadowType.In;
			scroll.Add (_log);
			Add (scroll);
			SetDefaultSize (640, 480);
			ShowAll ();
		}

		protected override void OnDestroyed ()
		{
			_log.Destroy ();
			base.OnDestroyed ();
		}
	}
}
