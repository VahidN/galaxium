// 
// RenameDialog.cs
// 
// Copyright © 2008, 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// RenameDialog.cs is part of SpiderIM.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


using System;
using Gtk;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class RenameDialog: Dialog
	{
		private Entry _entry;
		
		private RenameDialog (Window parent, string old)
			:base (null, parent, DialogFlags.Modal | DialogFlags.NoSeparator,
			       "gtk-cancel", 0, "gtk-ok", 1)
		{
			VBox.PackStart (new Label ("Please enter a new name:"));
			_entry = new Entry (old);
			_entry.ActivatesDefault = true;
			VBox.PackStart (_entry);
			Default = ActionArea.Children [0];
			ShowAll ();
		}

		public static string Open (Window parent, string old)
		{
			RenameDialog dialog = new RenameDialog (parent, old);
			string result = (dialog.Run () == 1) ? dialog._entry.Text : null;
			dialog.Destroy ();
			return result;
		}
	}
}
