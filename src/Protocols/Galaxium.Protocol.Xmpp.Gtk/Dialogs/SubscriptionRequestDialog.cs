// 
// SubscriptionRequestDialog.cs
// 
// Copyright © 2008, 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// SubscriptionRequestDialog.cs is part of SpiderIM.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


using System;
using Gtk;
using Galaxium.Protocol.Xmpp.Library;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class SubscriptionRequestDialog: Gtk.Dialog
	{
		private static string label_text = "Please enter text to send with the request:";
		
		public static void Open (Window parent, Contact contact)
		{
			var dialog = new SubscriptionRequestDialog (parent, contact);
			if (dialog.Run () == 1) {
				var message = dialog.View.Buffer.Text;
				contact.Client.Roster.RequestSubscription (contact.Jid, message);
			}
			dialog.Destroy ();
		}

		public TextView View { get; private set; }

		private SubscriptionRequestDialog (Window parent, Contact item)
			:base ("Subscription request", parent, DialogFlags.Modal | 
			       DialogFlags.NoSeparator, "gtk-cancel", 0, "gtk-ok", 1)
		{
			Label label = new Label (label_text);
			label.Xalign = 0;
			label.Xpad = 12;
			label.Ypad = 6;
			VBox.PackStart (label, false, false, 0);

			View = new TextView ();
			View.WrapMode = WrapMode.WordChar;
			ScrolledWindow scroll = new ScrolledWindow ();
			scroll.AddWithViewport (View);
			scroll.HscrollbarPolicy = PolicyType.Never;
			scroll.VscrollbarPolicy = PolicyType.Always;
			VBox.PackStart (scroll, true, true, 6);
			VBox.ShowAll ();
		}
	}
}
