//  
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;
using System.Linq;
using Gtk;
using Glade;

using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Core;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public partial class InformationDialog
	{
		private sealed class ResourceBox
		{
			public static Widget Load (ResourceInfo resource)
			{
				var res_box = new ResourceBox (resource);
				return res_box._widget;
			}

			private ResourceInfo _resource;
			
			[Widget ("resource_box")]
			private Widget _widget;
			
			[Widget] private Label resource_label;
			[Widget] private Label priority_label;
			[Widget] private Label status_label;
			[Widget] private Label status_description_label;
			[Widget] private Image status_image;
			[Widget] private Label client_label;
			[Widget] private Label os_label;
			[Widget] private TextView features_textview;
			
			private ResourceBox (ResourceInfo resource)
			{
				var xml = new XML (null, "InformationDialog.glade", "resource_box", null);
				xml.Autoconnect (this);

				_resource = resource;
				
				var presence = resource.Presence;
				resource_label.Text = resource.Identifier;
				priority_label.Text = (presence.Priority ?? 0).ToString ();
				status_label.Text = GLib.Markup.EscapeText (StatusMethods.GetHumanRepresentation (presence));
				var status = GLib.Markup.EscapeText (presence.GetDescription ());
				status_description_label.Markup = "<i>" + status + "</i>";
			
				var icon = XmppProtocolFactory.StatusLookup (XmppPresence.Get (StatusMethods.GetPresenceStatus (presence)), IconSizes.Small);
				if (icon != null)
					status_image.Pixbuf = icon;

				resource.InfoUpdated += (s, args) => UpdateInfo ();

				UpdateInfo ();
			}

			private void UpdateInfo ()
			{
				string features = _resource.Supports (Namespaces.DiscoInfo)
					? String.Join ("\n", _resource.Features.ToArray ()) : "-- unknown --";

				Gtk.Application.Invoke (delegate {
					client_label.Text = "retrieving...";
					os_label.Text = "retrieving...";
					features_textview.Buffer.Text = features;
				});
				
				var thread = new System.Threading.Thread (() => {
					var name = _resource.GetClientName () ?? "Unknown";
					var os = _resource.GetOperatingSystem () ?? "Unknown";
					
					Gtk.Application.Invoke (delegate {
						try {
							client_label.Text = name;
							os_label.Text = os;
						}
						catch {}
					});
				});
				thread.IsBackground = true;
				thread.Start ();
			}
		}
	}
}
