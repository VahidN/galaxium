//  
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;
using Gtk;
using Galaxium.Protocol.Xmpp.Library;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class EnterJidDialog: Dialog
	{
		public static JabberID Open ()
		{
			var dialog = new EnterJidDialog ();
			dialog.ShowAll ();
			int result = dialog.Run ();
			dialog.Destroy ();

			if (result == (int) ResponseType.Ok) {
				try { return dialog.EntryContent; }
				catch {}  // TODO: error dialog
			}
			
			return JabberID.Empty;
		}

		private Entry _entry;

		public string EntryContent {
			get { return _entry.Text; }
		}
		
		private EnterJidDialog ()
		{
			Title = String.Empty;
			HasSeparator = false;
			
			var hbox = new HBox (false, 12);
			var label = new Label ();
			label.Markup = "<b>Jabber ID:</b>";
			_entry = new Entry ();

			hbox.PackStart (label, false, true, 0);
			hbox.PackStart (_entry, true, true, 0);
			VBox.PackStart (hbox, true, true, 0);

			AddActionWidget (new Button (Stock.Cancel), ResponseType.Cancel);
			AddActionWidget (new Button (Stock.Ok), ResponseType.Ok);
		}
	}
}
