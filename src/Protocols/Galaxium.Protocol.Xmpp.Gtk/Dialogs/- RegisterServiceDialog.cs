// 
// RegisterServiceDialog.cs
//
// Copyright © 2008 Philippe Durand <draekz@gmail.com>
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Core;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public sealed class RegisterServiceDialog
	{
		[Widget ("RegisterServiceDialog")]
		private Dialog _dialog;
		[Widget ("DialogTable")]
		private Table _dialogTable;
		
		private XmppSession _session;
		private ServiceIdentity _identity;
		private Dictionary<string,Widget> _fieldWidgets = new Dictionary<string,Widget> ();
		private uint _pos = 0;
		
		public RegisterServiceDialog (XmppSession session, ServiceRegistrationEventArgs args)
		{
			_session = session;
			_identity = args.Identity;
			
			XML gxml = new XML ("RegisterServiceDialog.glade", null);
			gxml.Autoconnect (this);
			
			//GtkUtility.EnableComposite (_dialog);
			
			_dialog.Icon = IconUtility.GetIcon (args.Identity.GetIconName (), IconSizes.Large);
			
			if (args.Instructions != null) // FIXME: is it allowed to have no instructions?
			{
				AppendLabelRow (args.Instructions.Message + ":");
				
				foreach (ServiceInstructionField field in args.Instructions.Fields)
				{
					switch (field.Type)
					{
						case InstructionFieldType.Entry:
							var entry = new Entry (field.Value);
							entry.ActivatesDefault = true;
							_fieldWidgets.Add (field.Variable, entry);
							AppendWidgetWithLabel (field.Label, entry, field.Variable);
							break;
						
						case InstructionFieldType.Text:
							var textentry = new Entry (field.Value);
							_fieldWidgets.Add (field.Variable, textentry);
							AppendWidgetWithLabel (field.Label, textentry, field.Variable);
							break;
						
						case InstructionFieldType.Label:
							var label = new Label ();
							label.Markup = field.Value;
							label.Xalign = 0.0f;
							label.Yalign = 0.5f;
							label.Wrap = true;
							
							AppendLabelRow (label);
							break;
					}
				}
			}
			
			_dialog.ShowAll ();
		}
		
		public Dictionary<string,string> GetValues ()
		{
			Dictionary<string,string> values = new Dictionary<string,string> ();
			
			foreach (string var in _fieldWidgets.Keys)
			{
				if (_fieldWidgets[var] is Entry)
					values.Add (var, (_fieldWidgets[var] as Entry).Text);
			}
			
			return values;
		}
		
		private void AppendLabelRow (Label label)
		{
			_dialogTable.Attach (label, 0, 2, ++_pos, _pos+1,
			                     AttachOptions.Fill | AttachOptions.Expand | AttachOptions.Shrink,
			                     AttachOptions.Fill, 0, 0);
		}
		
		private void AppendLabelRow (string label)
		{
			var labelwidget = new Label ();
			labelwidget.Markup = label;
			labelwidget.Xalign = 0.0f;
			labelwidget.Yalign = 0.5f;
			labelwidget.Wrap = true;
			
			AppendLabelRow (labelwidget);
		}
		
		private void AppendWidgetWithLabel (string label, Widget widget, string id)
		{
			var labelwidget = new Label ();
			labelwidget.Xalign = 0.0f;
			labelwidget.Markup = "<b>"+label+":</b>";
			
			_dialogTable.Attach (labelwidget, 0, 1, ++_pos, _pos+1,
			                     AttachOptions.Fill,
			                     AttachOptions.Fill, 0, 0);
			_dialogTable.Attach (widget, 1, 2, _pos, _pos+1,
			                     AttachOptions.Fill | AttachOptions.Expand | AttachOptions.Shrink,
			                     AttachOptions.Fill, 0, 0);
		}
		
		public int Run ()
		{
			return _dialog.Run ();
		}
		
		public void Destroy ()
		{
			_dialog.Destroy ();
		}
	}
}