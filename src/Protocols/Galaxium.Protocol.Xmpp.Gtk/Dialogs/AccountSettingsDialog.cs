// 
// AccountSettingsDialog.cs
//
// Copyright © 2007 Philippe Durand <draekz@gmail.com>
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public sealed class AccountSettingsDialog
	{
		[Widget ("AccountSettingsDialog")]
		private Dialog _dialog;
		[Widget ("register_check")]
		private CheckButton _register_check;
		[Widget ("connect_server_entry")]
		private Entry _connect_server_entry;
		[Widget ("port_spin")]
		private SpinButton _port_spin;
		[Widget ("http_check")]
		private CheckButton _http_check;
		
		public string ConnectServer
		{
			get { return _connect_server_entry.Text; }
			set { _connect_server_entry.Text = value; }
		}
		
		public int ConnectPort
		{
			get { return (int) _port_spin.Value; }
			set { _port_spin.Value = value; }
		}

		public bool Register
		{
			get { return _register_check.Active; }
			set { _register_check.Active = value; }
		}
		
		public bool UseHTTP
		{
			get { return _http_check.Active; }
			set { _http_check.Active = value; }
		}
		
		public AccountSettingsDialog (bool show_register)
		{
			XML gxml = new XML ("AccountSettingsDialog.glade", null);
			gxml.Autoconnect (this);
			
		//	GtkUtility.EnableComposite (_dialog); // FIXME: what is this for?
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-set", IconSizes.Small);
			
			_register_check.Active = false;
			_register_check.Visible = show_register;
			
			_dialog.ShowAll ();
		}
		
		public int Run ()
		{
			return _dialog.Run ();
		}
		
		public void Destroy ()
		{
			_dialog.Destroy ();
		}
	}
}