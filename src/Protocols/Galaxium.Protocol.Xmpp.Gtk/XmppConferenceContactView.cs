
using System;

using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class XmppConferenceContactView: ContactListView
	{
		public XmppConferenceContactView (XmppConference conference)
			:base (conference)
		{
		}
	}
}
