
using System;
using System.Text;
using System.Collections.Generic;

using Gtk;
using Gdk;
using GLib;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class XmppConferenceContactTreeManager: BasicContactTreeManager
	{
		public override void RenderText (object data, Galaxium.Gui.GtkGui.CellRendererContact renderer)
		{
			if (!(data is XmppMucContact))
				base.RenderText (data, renderer);
		
			var contact = data as XmppMucContact;
			
			renderer.Contact = contact;
			renderer.ShowEmoticons = true;
			
			if (contact.PendingMessages == 0)
				renderer.Markup = string.Format ("{0} <i>({1})</i>",
				                                 Markup.EscapeText (contact.DisplayIdentifier),
				                                 contact.Presence.State);
			else
				renderer.Markup = string.Format ("<span color='red'>{0} <i>({1})</i> [{2}]</span>",
				                                 Markup.EscapeText (contact.DisplayIdentifier),
				                                 contact.Presence.State,
				                                 contact.PendingMessages);
			
		}

		public override string GetMenuExtensionPoint (object data)
		{
			if (data is XmppMucContact)
				return "/Galaxium/Gui/XMPP/ContactTree/ContextMenu/ConferenceContact";
			return base.GetMenuExtensionPoint (data);
		}

		public override bool Visible (object data, string filter, bool caseSensitive)
		{
			return true;
		}
	}
}
