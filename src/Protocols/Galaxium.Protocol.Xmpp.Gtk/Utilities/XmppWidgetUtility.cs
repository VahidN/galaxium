// 
// XmppWidgetUtility.cs
//
// Copyright © 2007 Philippe Durand <draekz@gmail.com>
// Copyright © 2007 Ben Motmans <ben.motmans@gmail.com>
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public static class XmppWidgetUtility
	{
		private static Dictionary<XmppSession, XmppSessionWidget> _sessionWidgets
			= new Dictionary<XmppSession, XmppSessionWidget> ();
		
		public static XmppSessionWidget GetSessionWidget (XmppSession session)
		{
			if (_sessionWidgets.ContainsKey (session))
				return _sessionWidgets [session];
			else
				return null;
		}
		
		public static void AddSessionWidget (XmppSession session, XmppSessionWidget widget)
		{
			_sessionWidgets.Add (session, widget);
		}
	}
}
