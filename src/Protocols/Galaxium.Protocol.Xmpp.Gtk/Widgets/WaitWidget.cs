//  
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;

using Gtk;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class WaitWidget: Alignment
	{
		public WaitWidget (string label)
			:base (0.5f, 0.5f, 0, 0)
		{
			var lbl = new Label (label);
			var progress = new ProgressBar ();
			progress.PulseStep = 0.040;
			var box = new VBox (false, 2);
			box.PackStart (lbl, false, true, 0);
			box.PackStart (progress, false, true, 0);
			this.Add (box);

			GLib.Timeout.Add (40, delegate () {
				if (progress == null) return false;
				progress.Pulse ();
				return true;
			});
		}

		protected override void OnShown ()
		{
			base.OnShown ();
			Child.ShowAll ();
		}
	}
}
