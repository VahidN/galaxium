//  
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;

using Gtk;

using L=Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library;
using Galaxium.Protocol.Xmpp.Library.Core;
using Galaxium.Protocol.Xmpp.Library.Extensions;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class vCardWidget: Alignment
	{
		private L.Client _client;
		private vCard _vcard;
		
		public vCardWidget (L.Client client, JabberID jid)
			:base (0, 0, 1, 1)
		{
			_client = client;
			_vcard = new vCard (client, jid);
			_vcard.Finished += HandleFinished;
			_vcard.Failed += HandleFailed;
			_vcard.Retrieve ();

			Add (new WaitWidget ("Retrieving data"));
			Child.Show ();
		}

		void HandleFinished (object sender, EventArgs e)
		{
			Application.Invoke ((s, args) => {
				Child.Destroy ();
				Add (new DataFormWidget (_client, _vcard.ToForm ()));
				ShowAll ();
			});
		}

		void HandleFailed (object sender, StanzaErrorEventArgs e)
		{
			Application.Invoke ((s, args) => {
				this.BorderWidth = 12;
				Child.Destroy ();
				Add (new ErrorWidget (e.Error.GetHumanRepresentation (), e.Error.Description));
				Child.Show ();
			});
		}
	}
}
