//  
//  Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;

using Gtk;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class ErrorWidget: Alignment
	{
		public ErrorWidget (string primary, string secondary)
			:base (0.5f, 0.5f, 0, 0)
		{
			var img = new Image (Stock.DialogError, IconSize.Dialog);
			img.Yalign = 0;

			var box = new VBox (false, 12);
			var primary_label = new Label ();
			primary_label.Markup = "<span size=\"larger\" weight=\"bold\">"
				+ GLib.Markup.EscapeText (primary) + "</span>";
			primary_label.Xalign = 0;
			primary_label.Wrap = true;
			var secondary_label = new Label (secondary);
			secondary_label.Xalign = 0;
			secondary_label.Yalign = 0;
			secondary_label.Wrap = true;
			box.PackStart (primary_label, false, true, 0);
			box.PackStart (secondary_label, true, true, 0);
			var main_box = new HBox (false, 12);
			main_box.PackStart (img, false, true, 0);
			main_box.PackStart (box, true, true, 0);
			Add (main_box);
		}

		protected override void OnShown ()
		{
			Child.ShowAll ();
		}
	}
}
