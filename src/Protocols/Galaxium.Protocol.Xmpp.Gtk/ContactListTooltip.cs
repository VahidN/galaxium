// 
// ContactListTooltip.cs
//
// Copyright © 2003-2007 Philippe Durand <draekz@gmail.com>
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using GLib;
using Gdk;
using Gtk;

using Galaxium.Core;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol.Xmpp.Library;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class ContactListTooltip: InfoTooltip
	{
		private string name_format = "<span size='large'><b>{0}</b></span>";
		private string resource_format = "<small>Contact has {0} connected resources.</small>";
		
		public XmppContact _contact;

		public Table _table;
		public uint   _pos;
		
		public ContactListTooltip (XmppContact contact)
			:base ()
		{
			_contact = contact;
			
			var hbox = new HBox();
			var vbox = new VBox();
			var image = new Gtk.Image();
			
			Pixbuf pixbuf = null;
			
			if (contact.DisplayImage != null && contact.DisplayImage.ImageBuffer != null) {
				try { pixbuf = new Pixbuf (contact.DisplayImage.ImageBuffer); }
				catch {}
			}
			if (pixbuf == null)
				pixbuf = IconUtility.GetIcon ("galaxium-displayimage");

			pixbuf = PixbufUtility.GetFramedPixbuf (pixbuf, PixbufRendererFrameSize.Huge);
			
			image.Pixbuf = pixbuf;

			var name = Markup.EscapeText (_contact.InternalContact.Name
			                              ?? _contact.InternalContact.Jid);
			var name_label = new Label ();
			name_label.Markup = String.Format (name_format, name);
			name_label.UseUnderline = false;
			name_label.Xalign = 0.0f;

			vbox.PackStart (name_label, false, false, 0);

			SetupTable ();
			
			if (_contact.InternalContact.Name != null)
				AddField ("JabberID:  ", _contact.InternalContact.Jid);

			{
				var item_status = StatusMethods.GetStatus (_contact.InternalContact);
				var status = StatusMethods.GetHumanRepresentation (item_status);
				var description = _contact.InternalContact.MainPresence.GetDescription ();
				description = (description ?? String.Empty).Trim ();

				AddField ("Status:  ", status);
				if (description.Length != 0)
					AddField (null, description, true);
			}

			vbox.PackStart (_table, false, false, 0);
			
			var resource_count = _contact.InternalContact.ResourceCount;

			if (resource_count > 1) {
				var resource_label = new Label ();
				resource_label.Markup = String.Format (resource_format, resource_count);
				resource_label.Xalign = 0.0f;
				vbox.PackStart (resource_label, false, false, 0);
			}

			var align = new Alignment (0.0f, 0.0f, 0.0f, 0.0f);
			align.Add (image);
			
			hbox.PackStart (align, false, false, 0);
			hbox.PackStart (vbox, true, true, 0);
			
			vbox.Spacing = 6;
			hbox.Spacing = 6;
			
			Add (hbox);

			hbox.ShowAll ();
		}

		private void SetupTable ()
		{
			_table = new Table (0, 0, false);
			_table.RowSpacing = 2;
		}

		private void AddField (string label, string text)
		{
			AddField (label, text, false);
		}
		
		private void AddField (string label, string text, bool italic)
		{
			ThrowUtility.ThrowIfNull ("text", text);

			Label lbl;
			
			if (label != null) {
				label = "<small><b>" + Markup.EscapeText (label) + "</b></small>";
				lbl = new Label ();
				lbl.Markup = label;
				lbl.Xalign = 0.0f;
				lbl.Yalign = 0.0f;
				_table.Attach (lbl, 0, 1, _pos, _pos + 1, AttachOptions.Fill, 0, 0, 0);
			}
			
			text = "<small>" + Markup.EscapeText (text) + "</small>";
			if (italic)
				text = "<i>" + text + "</i>";
			lbl = new Label ();
			lbl.Markup = text;
			lbl.Xalign = 0.0f;
			lbl.Yalign = 0.0f;
			lbl.LineWrap = true;
			lbl.LineWrapMode = Pango.WrapMode.WordChar;
			lbl.WidthRequest = 200;
			_table.Attach (lbl, 1, 2, _pos, ++ _pos, AttachOptions.Fill, 0, 0, 0);
		}
	}
}