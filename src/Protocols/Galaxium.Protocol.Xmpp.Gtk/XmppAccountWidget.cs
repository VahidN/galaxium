// 
// XmppAccountWidget.cs
//
// Copyright © 2007 Philippe Durand <draekz@gmail.com>
// Copyright © 2009 Jiří Zárevúcky <zarevucky.jiri@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;

using G=Gtk;
using Gtk;
using Glade;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Client;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	public class XmppAccountWidget: BasicAccountWidget
	{
		protected int _connect_port = 5222;
		protected string _connect_server = null;
	//	protected bool _register = false;
	//	protected bool _use_http = false;
		
		public XmppAccountWidget ()
			:base (XmppProtocol.Instance)
		{
		}
		
		public override void Initialize ()
		{
			_presenceCombo = CreateStatusCombo (XmppPresence.Online, //the selected one
			                                    XmppPresence.Online, XmppPresence.Chat,
			                                    XmppPresence.Away, XmppPresence.XA,
			                                    XmppPresence.Dnd);//, XmppPresence.Invisible);
			
			base.Initialize ();
			
			_accountCombo.TooltipText =
				"Provide the jabber ID you wish to use. Example: user@jabber.org";
		}
		
		protected override IAccount SetAccount ()
		{
			string jid = _accountCombo.Entry.Text;
			
			XmppAccount account = GetAccount (jid) as XmppAccount;
			
			if (account == null)
			{
				account = new XmppAccount (jid);	
				AccountUtility.AddAccount (account);
			}

			account.Password = _passwordEntry.Text;
			account.AutoConnect = _autoconnectCheck.Active;
			account.RememberPassword = _rememberPassCheck.Active;
			account.InitialPresence = GetInitialPresence ();
			
			account.ConnectServer = _connect_server;
			account.ConnectPort = _connect_port;
			//	account.Register = _register;
			//	account.UseHTTP = _use_http;
				
			return account;
		}
		
		protected override void LoadAccountInfo ()
		{
			base.LoadAccountInfo ();
			
			// Here we would load all the Xmpp specific settings into the UI.
			
			XmppAccount account = Account as XmppAccount;
			
			if (account != null)
			{
				_connect_port = account.ConnectPort;
				_connect_server = account.ConnectServer;
			//	_register = false;
			//	_use_http = account.UseHTTP;
			}
		}
		
		public override void EnableFields ()
		{
			base.EnableFields ();
			
			// Also enable custom protocol widgets.
		}
		
		public override void DisableFields (bool omit_cancel)
		{
			base.DisableFields (omit_cancel);
			
			// Also disable custom protocol widgets.
		}
		
		protected override Gdk.Pixbuf PresenceImageLookup (IPresence item, IIconSize size)
		{
			return XmppProtocolFactory.StatusLookup (item, size);
		}
		
		protected override void ConnectButtonClicked (object sender, EventArgs args)
		{
			// When connect is clicked, we create a session and then we send it off to the basic handler
			
			_current_session = CreateSession () as XmppSession;
			
			if (_current_session == null) return;
			
			base.ConnectButtonClicked (sender, args);
			
			// After we make the attempt to log in, we want to remove the flag for registration.
		//	_register = false;
		}
		
		protected override void SettingsButtonClicked (object sender, EventArgs args)
		{
			AccountSettingsDialog dialog = new AccountSettingsDialog (true);
			
			dialog.ConnectPort = _connect_port;
			dialog.ConnectServer = _connect_server;
		//	dialog.Register = _register;
		//	dialog.UseHTTP = _use_http;
				
			if (dialog.Run () == (int)ResponseType.Ok)
			{
				_connect_port = dialog.ConnectPort;
				_connect_server = dialog.ConnectServer;
		//		_register = dialog.Register;
		//		_use_http = dialog.UseHTTP;
			}

			dialog.Destroy();
		}
	}
}