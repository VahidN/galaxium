
using System;

using Anculus.Core;

using Galaxium.Client.GtkGui;

namespace Galaxium.Protocol.Xmpp.GtkGui
{
	internal static class XmppActivityListener
	{
		public static void OnSubscriptionRequestedActivity (object sender,
		                                             SubscriptionRequestedActivity activity)
		{
			Log.Info ("Displaying a subscription request from contact "
			          + activity.Contact.InternalContact.Jid);

			var contact = activity.Contact.InternalContact;
			var request = contact.Client.Roster.GetSubscriptionRequest (contact.Jid);
			
			SubscriptionRequestDisplayDialog.Open (null, activity.Session.Client, request);
		}

		public static void OnMessageReceivedActivity (object sender, MessageReceivedActivity activity)
		{
		}
	}
}
