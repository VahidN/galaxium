/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;

using GLib;
using Gdk;
using Gtk;

using Galaxium.Core;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public class ContactListTooltip : InfoTooltip
	{
		MsnContact _contact;
		
		public MsnContact ListItem { get { return _contact; } }
		
		public ContactListTooltip(MsnContact contact) : base ()
		{
			_contact = contact;
			
			Gtk.HBox oBox = new Gtk.HBox();
			Gtk.VBox oVBox = new Gtk.VBox();
			Gtk.Image oImage = new Gtk.Image();
			
			Pixbuf pixbuf = null;
			
			if (contact.DisplayImage != null && !contact.SupressImage)
			{
				if ((!string.IsNullOrEmpty(contact.DisplayImage.Filename)) && File.Exists (contact.DisplayImage.Filename))
					pixbuf = PixbufUtility.GetFramedPixbuf(new Pixbuf(contact.DisplayImage.Filename), PixbufRendererFrameSize.Huge);
				else if ((contact.DisplayImage.ImageBuffer != null) && (contact.DisplayImage.ImageBuffer.Length > 0))
				{
					try
					{
						pixbuf = PixbufUtility.GetFramedPixbuf(new Pixbuf(contact.DisplayImage.ImageBuffer), PixbufRendererFrameSize.Huge);
					}
					catch
					{
						
					}
				}
			}
			
			if (pixbuf == null)
				pixbuf = PixbufUtility.GetFramedPixbuf(IconUtility.GetIcon("galaxium-displayimage"), PixbufRendererFrameSize.Huge);
			
			oImage.Pixbuf = pixbuf;
			
			Gtk.Label oNameLabel = new Gtk.Label(String.Format("<span size='large'><b>{0}</b></span>", Markup.EscapeText(_contact.UniqueIdentifier)));
			oNameLabel.UseMarkup = true;
			oNameLabel.Xalign = 0.0f;
			oNameLabel.Yalign = 0.0f;
			oNameLabel.UseUnderline = false;
			
			string sPersonalMessage = GettextCatalog.GetString ("No message");
			
			if (!string.IsNullOrEmpty(_contact.DisplayMessage) && !_contact.SupressMessage)
				sPersonalMessage = Markup.EscapeText(_contact.DisplayMessage);
			
			EmoticonLabel oPersonalLabel = new EmoticonLabel("<span size='small'>Comment: <i>" + sPersonalMessage + "</i></span>");
			oPersonalLabel.Xalign = 0.0f;
			oPersonalLabel.Yalign = 0.0f;
			oPersonalLabel.UseMarkup = true;
			oPersonalLabel.Wrap = true;
			oPersonalLabel.WidthRequest = 250;
			
			string sAlias = GettextCatalog.GetString ("None");
			
			if (_contact.HasNickname)
				sAlias = Markup.EscapeText(_contact.Nickname);
			
			EmoticonLabel oAliasLabel = new EmoticonLabel("<span size='small'>" + GettextCatalog.GetString ("Nickname") + ": <i>"+sAlias+"</i></span>");
			oAliasLabel.UseMarkup = true;
			oAliasLabel.Xalign = 0.0f;
			oAliasLabel.Yalign = 0.0f;
			
			string sDisplay = "None";
			
			if (!string.IsNullOrEmpty(_contact.DisplayName) && !_contact.SupressName)
				sDisplay = Markup.EscapeText(_contact.DisplayName);
			
			EmoticonLabel oDisplayLabel = new EmoticonLabel("<span size='small'>" + GettextCatalog.GetString ("Display") + ": <i>"+sDisplay+"</i></span>");
			oDisplayLabel.Ellipsize = Pango.EllipsizeMode.End;
			oDisplayLabel.Xalign = 0.0f;
			oDisplayLabel.Yalign = 0.0f;
			oDisplayLabel.Wrap = true;
			oDisplayLabel.WidthRequest = 250;
			
			Gtk.Label oStatusLabel = new Gtk.Label("<span size='small'>" + GettextCatalog.GetString ("Status") + ": <i>"+_contact.Presence.State+"</i></span>");
			oStatusLabel.UseMarkup = true;
			oStatusLabel.Xalign = 0.0f;
			oStatusLabel.Yalign = 0.0f;
			
			string sReverse = String.Empty;
			
			if (_contact.IsInList(MsnListType.Reverse))
				sReverse = GettextCatalog.GetString ("You are on their list.");
			else
				sReverse = GettextCatalog.GetString ("You are NOT on their list.");
			
			Gtk.Label oReverseLabel = new Gtk.Label("<span size='small'>" +  GettextCatalog.GetString ("Reverse") + ": <i>"+sReverse+"</i></span>");
			oReverseLabel.UseMarkup = true;
			oReverseLabel.Xalign = 0.0f;
			oReverseLabel.Yalign = 0.0f;
			
			oVBox.PackStart(oNameLabel, false, false, 0);
			oVBox.PackStart(oDisplayLabel, false, false, 0);
			oVBox.PackStart(oAliasLabel, false, false, 0);
			oVBox.PackStart(oStatusLabel, false, false, 0);
			oVBox.PackStart(oReverseLabel, false, false, 0);
			oVBox.PackEnd(oPersonalLabel, true, true, 0);
			
			oBox.PackStart(oImage, false, false, 0);
			oBox.PackEnd(oVBox, true, true, 0);
			
			oVBox.Spacing = 2;
			oBox.Spacing = 5;
			oBox.BorderWidth = 5;
			
			Add(oBox);
		}
	}
}
