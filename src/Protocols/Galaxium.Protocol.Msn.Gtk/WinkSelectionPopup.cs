/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gdk;
using Gtk;

using Anculus.Core;

using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public class WinkSelectionPopup : SelectionPopupDialog<MsnWink>
	{
		MsnAccount _account;
		
		public WinkSelectionPopup (MsnAccount account)
		{
			_account = account;
		}
		
		protected override void Initialize ()
		{
			base.Initialize ();
			
			IconView.Columns = 5;
		}
		
		protected override void PopulateModel (ListStore store)
		{
			foreach (MsnWink wink in WinkStorageUtility.GetWinks (_account))
			{
				try
				{
					Pixbuf pbuf = new Pixbuf (wink.Thumbnail);
					
					if (pbuf == null)
						throw new ApplicationException ();
					
					store.AppendValues (pbuf, wink);
				}
				catch
				{
					Log.Warn ("Unable to generate preview pixbuf for wink with thumbnail {0}", wink.Thumbnail);
				}
			}
		}
	}
}
