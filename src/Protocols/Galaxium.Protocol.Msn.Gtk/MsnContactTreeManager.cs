/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;

using GLib;
using Gdk;
using Gtk;

using Anculus.Core;

using Galaxium.Client.GtkGui;
using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public class MsnContactTreeManager : BasicContactTreeManager
	{
		public override void RenderText (object data, CellRendererContact renderer)
		{
			if (!(data is MsnContact))
			{
				base.RenderText (data, renderer);
				return;
			}
			
			MsnContact contact = data as MsnContact;
			StringBuilder sb = new StringBuilder ();
			
			sb.Append (Markup.EscapeText (contact.DisplayIdentifier));
			
			if (Detail != ContactTreeDetailLevel.Compact)
			{
				sb.AppendFormat ("<span foreground=\"#505050\" weight=\"light\"></span>\n");
			
				if (Detail == ContactTreeDetailLevel.Detailed)
					sb.AppendFormat ("<span size=\"small\">{0}</span>\n", Markup.EscapeText (contact.UniqueIdentifier));
			}
			
			if ((!contact.CurrentMedia.Enabled) || string.IsNullOrEmpty (contact.CurrentMedia.Display))
			{
				sb.Append ("<span foreground=\"#505050\" weight=\"light\" size=\"smaller\">");
				
				if (Detail == ContactTreeDetailLevel.Compact)
					sb.Append (" ");
				
				bool showPersonalMessages = Config.GetBool (Configuration.ContactList.ShowContactMessages.Name, Configuration.ContactList.ShowContactMessages.Default);
				
				if (showPersonalMessages && contact.DisplayMessage != null && contact.DisplayMessage.Length > 0)
					sb.Append (Markup.EscapeText (TextUtility.ReplaceNewlinesWithSpaces (contact.DisplayMessage)));	
				else
					sb.Append ("");
				
				sb.Append ("</span>");
			}
			else
			{
				string emot = string.Empty;
				
				if (EmoticonUtility.ActiveSet != null)
				{
					if (contact.CurrentMedia.Type == MsnCurrentMediaType.Music)
						emot = "(8)";
					else if (contact.CurrentMedia.Type == MsnCurrentMediaType.Games)
						emot = "(xx)";
					else if (contact.CurrentMedia.Type == MsnCurrentMediaType.Office)
						emot = "(co)";
				}
				
				sb.Append ("<span foreground=\"#505050\" weight=\"light\" size=\"smaller\">");
				sb.Append (Markup.EscapeText (emot + TextUtility.ReplaceNewlinesWithSpaces (contact.CurrentMedia.Display) + emot));
				sb.Append ("</span>");
			}
			
			renderer.Contact = contact;
			renderer.ShowEmoticons = true;
			renderer.Markup = sb.ToString ();
		}
		
		public override void RenderLeftImage (object data, CellRendererPixbuf renderer)
		{
			if (!(data is MsnContact))
			{
				base.RenderLeftImage (data, renderer);
				return;
			}
			
			MsnContact contact = data as MsnContact;
			IIconSize iconSize = IconSizes.Other;
			Pixbuf pixbuf = null;
			
			switch (Detail)
			{
				case ContactTreeDetailLevel.Compact:
					iconSize = IconSizes.Small;
					break;
					
				case ContactTreeDetailLevel.Normal:
					iconSize = IconSizes.Medium;
					break;
					
				case ContactTreeDetailLevel.Detailed:
					iconSize = IconSizes.Large;
					break;
					
				default:
					// not currently dealign with this size.
					break;
			}
			
			bool showDisplayImages = Config.GetBool (Configuration.ContactList.ShowContactImages.Name, Configuration.ContactList.ShowContactImages.Default);
			IActivity activity = GtkActivityUtility.PeekContactQueueItem(contact);
			
			if (Detail == ContactTreeDetailLevel.Compact)
			{
				if (activity != null)
				{
					pixbuf = IconUtility.GetIcon ("galaxium-conversation-active", iconSize);
				}
				else
					pixbuf = IconUtility.StatusLookup (contact.Presence, iconSize);
			}
			else
			{
				if (contact.DisplayImage != null && contact.DisplayImage.ImageBuffer != null && showDisplayImages && !contact.SupressImage)
				{
					try
					{
						if (contact.Presence == MsnPresence.Idle)
							pixbuf = PixbufUtility.GetShadedPixbuf(new Gdk.Pixbuf(contact.DisplayImage.ImageBuffer), Detail);
						else if (contact.Presence == MsnPresence.Offline)
							pixbuf = IconUtility.GetIcon ("galaxium-offline", iconSize);
						else
							pixbuf = PixbufUtility.GetFramedPixbuf (new Gdk.Pixbuf(contact.DisplayImage.ImageBuffer), Detail);
					}
					catch
					{
						if (contact.Presence == MsnPresence.Idle)
							pixbuf = IconUtility.GetIcon ("galaxium-idle", iconSize);
						else if (contact.Presence == MsnPresence.Offline)
							pixbuf = IconUtility.GetIcon ("galaxium-offline", iconSize);
						else
							pixbuf = IconUtility.GetIcon ("galaxium-contact", iconSize);
					}
				}
				else
				{
					if (contact.Presence == MsnPresence.Idle)
						pixbuf = IconUtility.GetIcon ("galaxium-idle", iconSize);
					else if (contact.Presence == MsnPresence.Offline)
						pixbuf = IconUtility.GetIcon ("galaxium-offline", iconSize);
					else
						pixbuf = IconUtility.GetIcon ("galaxium-contact", iconSize);
				}
			}
			
			// We are not in compact mode, so we need to use overlays now.
			if (Detail != ContactTreeDetailLevel.Compact)
			{
				if (activity != null)
				{
					switch (activity.Type)
					{
						case ActivityTypes.Message:
							pixbuf = PixbufUtility.GetOverlayedRightPixbuf (pixbuf, IconUtility.GetIcon ("galaxium-conversation-active", IconSizes.Small));
							break;
						case ActivityTypes.Transfer:
							pixbuf = PixbufUtility.GetOverlayedRightPixbuf (pixbuf, IconUtility.GetIcon ("galaxium-transfer-receive", IconSizes.Small));
							break;
					}
				}
				else if (contact.IsBlocked)
					pixbuf = PixbufUtility.GetOverlayedRightPixbuf (pixbuf, IconUtility.GetIcon ("galaxium-status-block", IconSizes.Small));
				else if (contact.Presence != MsnPresence.Online)
					pixbuf = PixbufUtility.GetOverlayedRightPixbuf (pixbuf, IconUtility.StatusLookup (contact.Presence, IconSizes.Small));
				
				if (!contact.IsInList(MsnListType.Reverse))
					pixbuf = PixbufUtility.GetOverlayedLeftPixbuf (pixbuf, IconUtility.GetIcon ("galaxium-status-reverse", IconSizes.Small));
			}
			
			renderer.Pixbuf = pixbuf;
			renderer.Width = pixbuf.Width;
			renderer.Visible = true;
		}
		
		public override InfoTooltip GetTooltip (object data)
		{
			if (data is MsnContact)
				return new ContactListTooltip (data as MsnContact);
			
			return base.GetTooltip (data);
		}
		
		public override int Compare (object data1, object data2)
		{
			if ((data1 is ContactTreeRealGroup) && !(data2 is ContactTreeOfflineVirtualGroup))
			{
				MsnGroup group = (data1 as ContactTreeRealGroup).Group as MsnGroup;
				
				// Handle the "Other Contacts" group
				if (group.UniqueIdentifier == "0")
					return (Tree.SortOrder == SortOrder.Ascending) ? int.MaxValue : int.MinValue;
			}
			if ((data2 is ContactTreeRealGroup) && !(data1 is ContactTreeOfflineVirtualGroup))
			{
				MsnGroup group = (data2 as ContactTreeRealGroup).Group as MsnGroup;
				
				// Handle the "Other Contacts" group
				if (group.UniqueIdentifier == "0")
					return (Tree.SortOrder == SortOrder.Ascending) ? int.MinValue : int.MaxValue;
			}
			
			bool sortAlphabet = Config.GetBool (Configuration.ContactList.SortAlphabetic.Name, Configuration.ContactList.SortAlphabetic.Default);
			
			MsnContact contact1 = data1 as MsnContact;
			
			if ((contact1 != null) && (!sortAlphabet))
			{
				MsnContact contact2 = data2 as MsnContact;
				
				int ret = GetPresenceValue (contact1.Presence).CompareTo (GetPresenceValue (contact2.Presence));
				
				if (ret != 0)
					return ret;
			}
			
			return base.Compare (data1, data2);
		}
		
		private int GetPresenceValue (IPresence presence)
		{
			if (presence == MsnPresence.Online)
				return 0;
			else if (presence == MsnPresence.Idle)
				return 1;
			else if (presence == MsnPresence.Phone)
				return 2;
			else if (presence == MsnPresence.Brb)
				return 3;
			else if (presence == MsnPresence.Busy)
				return 4;
			else if (presence == MsnPresence.Lunch)
				return 5;
			else if (presence == MsnPresence.Away)
				return 6;
			else if (presence == MsnPresence.Invisible)
				return 7;
			else
				return 8;
		}
	}
}
