/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

using Anculus.Core;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public partial class MsnPreferenceWidget : IPreferenceWidget<Widget>
	{
		Widget _nativeWidget;
		//Widget _parentLayoutWidget;
		IConfigurationSection _config = null;
		
		[Widget ("spinSOAPTimeout")] SpinButton _spinSOAPTimeout = null;
		[Widget ("spinSOAPAttempts")] SpinButton _spinSOAPAttempts = null;
		
		public Widget NativeWidget
		{
			get { return _nativeWidget; }
		}
		
		public void Initialize ()
		{
			_nativeWidget = GladeUtility.ExtractWidget<Widget> (GladeUtility.GetGladeResourceStream (typeof (MsnPreferenceWidget).Assembly, "MsnPreferenceWidget.glade"), "widget", this);
			
			_config = Configuration.Protocol.Section["MSN"];
			
			_spinSOAPTimeout.Value = (int)(_config.GetInt ("SOAPTimeout", 10000) / 1000);
			_spinSOAPAttempts.Value = _config.GetInt ("SOAPAttempts", 10);
			
			InitInterface ();
			InitWinks ();
			
			_nativeWidget.ShowAll ();
		}
		
		public void SpinSOAPTimeoutValueChanged (object sender, EventArgs args)
		{
			_config.SetInt ("SOAPTimeout", _spinSOAPTimeout.ValueAsInt * 1000);
		}
		
		public void SpinSOAPAttemptsValueChanged (object sender, EventArgs args)
		{
			_config.SetInt ("SOAPAttempts", _spinSOAPAttempts.ValueAsInt);
		}
	}
}