/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gtk;
using Pango;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Core;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public class BoldToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as ToggleToolButton).Active = (chatWidget != null) && chatWidget.MessageEntry.Bold;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				chatWidget.MessageEntry.Bold = (MenuItem as ToggleToolButton).Active;
				chatWidget.SaveFont ();
			}
		}
	}
	
	public class ItalicToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as ToggleToolButton).Active = (chatWidget != null) && chatWidget.MessageEntry.Italic;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				chatWidget.MessageEntry.Italic = (MenuItem as ToggleToolButton).Active;
				chatWidget.SaveFont ();
			}
		}
	}
	
	public class UnderlineToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as ToggleToolButton).Active = (chatWidget != null) && chatWidget.MessageEntry.Underline;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				chatWidget.MessageEntry.Underline = (MenuItem as ToggleToolButton).Active;
				chatWidget.SaveFont ();
			}
		}
	}
	
	public class StrikethroughToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as ToggleToolButton).Active = (chatWidget != null) && chatWidget.MessageEntry.Strikethrough;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				chatWidget.MessageEntry.Strikethrough = (MenuItem as ToggleToolButton).Active;
				chatWidget.SaveFont ();
			}
		}
	}
	
	public class ForegroundToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				ColorSelectionDialog dialog = new ColorSelectionDialog (GettextCatalog.GetString ("Select a color"));
				dialog.Modal = true;
				dialog.ColorSelection.CurrentColor = chatWidget.MessageEntry.Color;
				
				if (dialog.Run () == (int)ResponseType.Ok)
				{
					chatWidget.MessageEntry.Color = dialog.ColorSelection.CurrentColor;
					chatWidget.SaveFont ();
				}
				
				dialog.Destroy ();
			}
		}
	}
	
	public class FontToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				FontSelectionDialog dialog = new FontSelectionDialog (GettextCatalog.GetString ("Select a font"));
				dialog.Modal = true;
				
				FontDescription desc = new FontDescription ();
				desc.Family = chatWidget.MessageEntry.Family;
				desc.Size = (int)(chatWidget.MessageEntry.Size * Pango.Scale.PangoScale);
				if (chatWidget.MessageEntry.Bold)
					desc.Weight = Pango.Weight.Bold;
				if (chatWidget.MessageEntry.Italic)
					desc.Style = Pango.Style.Italic;
				
				string fontname = desc.ToString ();
				dialog.SetFontName (fontname);
				
				if (dialog.Run () == (int)ResponseType.Ok)
				{
					desc = FontDescription.FromString (dialog.FontName);
					
					chatWidget.MessageEntry.Family = desc.Family;
					chatWidget.MessageEntry.Bold = desc.Weight == Pango.Weight.Bold;
					chatWidget.MessageEntry.Italic = desc.Style == Pango.Style.Italic;
					chatWidget.MessageEntry.Size = desc.Size / Pango.Scale.PangoScale;
					chatWidget.SaveFont ();
				};
				
				dialog.Destroy ();
			}
		}
	}
	
	public class ResetToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				chatWidget.MessageEntry.ResetFont ();
				chatWidget.SaveFont ();
			}
		}
	}
	
	public class ClearToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				chatWidget.MessageEntry.Buffer.Clear ();
			}
		}
	}
	
	public class SpellCheckToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as ToolButton).Sensitive = false;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
			}
		}
	}
	
	public class EmoticonToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				EmoticonPopupDialog dialog = new EmoticonPopupDialog (chatWidget.Conversation.Session.Account, chatWidget.Conversation.PrimaryContact, EmoticonPopupMode.Common);
				dialog.Selected += EmoticonSelected;
				dialog.TransientFor = chatWidget.NativeWidget.Toplevel as Gtk.Window;
				dialog.Show ();
			}
		}
		
		void EmoticonSelected (object sender, IEmoticon emoticon)
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				chatWidget.MessageEntry.InsertEmoticon (emoticon, chatWidget.MessageEntry.Buffer.CursorPosition);
				(sender as EmoticonPopupDialog).Destroy ();
			}
		}
	}
	
	public class WinkToolCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as ToolButton).Sensitive = (chatWidget != null) && (chatWidget.Conversation as MsnConversation).CanSendWink;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				WinkSelectionPopup popup = new WinkSelectionPopup (chatWidget.Conversation.Session.Account as Galaxium.Protocol.Msn.MsnAccount);
				popup.Selected += WinkSelected;
				popup.TransientFor = chatWidget.NativeWidget.Toplevel as Gtk.Window;
				popup.Show ();
			}
		}
		
		void WinkSelected (object sender, MsnWink wink)
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				(chatWidget.Conversation as MsnConversation).SendWink (wink);
				chatWidget.MessageDisplay.AddEvent (GettextCatalog.GetString ("You sent the wink '{0}'", wink.Friendly));
				chatWidget.DisplayWink (wink);
			}
		}
	}
}
