/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Core;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public class SetNicknameCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnContact contact = context.Value as MsnContact;
			
			new SetNicknameDialog(contact);
		}
	}
	
	public class BlockContactCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			if (SessionUtility.ActiveSession is MsnSession)
			{
				ContactTreeContext context = Object as ContactTreeContext;
				MsnContact contact = context.Value as MsnContact;
				
				contact.Block (new ExceptionDelegate (delegate (Exception ex)
				{
					if (ex != null)
					{
						MessageDialog errordialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok,
							GettextCatalog.GetString ("An error occurred whilst attempting to block the contact\n\n") +
							ex.Message);
						
						errordialog.Run ();
						errordialog.Destroy ();
					}
				}));
			}
		}
	}
	
	public class UnblockContactCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			if (SessionUtility.ActiveSession is MsnSession)
			{
				ContactTreeContext context = Object as ContactTreeContext;
				MsnContact contact = context.Value as MsnContact;
				
				contact.Unblock (new ExceptionDelegate (delegate (Exception ex)
				{
					if (ex != null)
					{
						MessageDialog errordialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok,
							GettextCatalog.GetString ("An error occurred whilst attempting to unblock the contact\n\n") +
							ex.Message);
						
						errordialog.Run ();
						errordialog.Destroy ();
					}
				}));
			}
		}
	}
	
	public class RemoveCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			if (SessionUtility.ActiveSession is MsnSession)
			{
				ContactTreeContext context = Object as ContactTreeContext;
				MsnContact contact = context.Value as MsnContact;
			//	MsnGroup group = context.TreeView.GetContactGroup (context.Path) as MsnGroup;
				
				RemoveContactDialog dialog = new RemoveContactDialog ((MsnSession)SessionUtility.ActiveSession, true, contact.IsBlocked);
				ResponseType response = ResponseType.None;
				
				while(response == ResponseType.None)
				{
					response = (ResponseType)dialog.Run();
					
					if (response == ResponseType.Apply)
					{
						string error = String.Empty;
						bool success = false;
						
						// Block the user ahead of time, before we attempt to remove him.
						MsnSession session = SessionUtility.ActiveSession as MsnSession;
						
						success = session.RemoveContact(out error, contact.UniqueIdentifier);
						
						if (!success)
						{
							MessageDialog errordialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, error);
							errordialog.Run ();
							errordialog.Destroy ();
							response = ResponseType.None;
							continue;
						}
						
						dialog.Destroy();
						break;
					}
					else
					{
						dialog.Destroy();
						break;
					}
				}
			}
		}
	}
	
	public class RemoveFromGroupCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			// The group may be null if the contact is in a virtual group
			
			ContactTreeContext context = Object as ContactTreeContext;
			MsnGroup group = context.TreeView.GetContactGroup (context.Path) as MsnGroup;
			
			if (group == null || group.UniqueIdentifier == "0")
				(MenuItem as MenuItem).Sensitive = false;
		}
		
		public override void Run ()
		{
			if (SessionUtility.ActiveSession is MsnSession)
			{
				ContactTreeContext context = Object as ContactTreeContext;
				MsnContact contact = context.Value as MsnContact;
				MsnGroup group = context.TreeView.GetContactGroup (context.Path) as MsnGroup;
				
				RemoveContactDialog dialog = new RemoveContactDialog ((MsnSession)SessionUtility.ActiveSession, false, contact.IsBlocked);
				ResponseType response = ResponseType.None;
				
				while(response == ResponseType.None)
				{
					response = (ResponseType)dialog.Run();
					
					if (response == ResponseType.Apply)
					{
						string error = String.Empty;
						bool success = false;
						
						// Block the user ahead of time, before we attempt to remove him.
						MsnSession session = SessionUtility.ActiveSession as MsnSession;
						
						success = session.RemoveContactFromGroups (out error, contact.UniqueIdentifier, group.UniqueIdentifier);
						
						if (!success)
						{
							MessageDialog errordialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, error);
							errordialog.Run ();
							errordialog.Destroy ();
							response = ResponseType.None;
							continue;
						}
						
						dialog.Destroy();
						break;
					}
					else
					{
						dialog.Destroy();
						break;
					}
				}
			}
		}
	}
	
	public class MoveToGroupCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			// The group may be null if the contact is in a virtual group
			
			ContactTreeContext context = Object as ContactTreeContext;
			MsnContact contact = context.Value as MsnContact;
			MsnGroup group = context.TreeView.GetContactGroup (context.Path) as MsnGroup;
			
			(MenuItem as Gtk.MenuItem).Sensitive = (contact != null) && (group != null);
		}
		
		public override void Run ()
		{
			if (SessionUtility.ActiveSession is MsnSession)
			{
				ContactTreeContext context = Object as ContactTreeContext;
				MsnContact contact = context.Value as MsnContact;
				MsnGroup group = context.TreeView.GetContactGroup (context.Path) as MsnGroup;
				
				CopyMoveGroupDialog dialog = new CopyMoveGroupDialog((MsnSession)SessionUtility.ActiveSession, contact, false, false);
				ResponseType response = ResponseType.None;
				
				while(response == ResponseType.None)
				{
					response = (ResponseType)dialog.Run();
					
					if (response == ResponseType.Apply)
					{
						string error = String.Empty;
						bool success = false;
						
						MsnSession session = SessionUtility.ActiveSession as MsnSession;
						
						string guid = string.Empty;
						// Since MSN protocol doesn't use group names, but GUIDs, we should convert that now.
						foreach (MsnGroup group2 in session.GroupCollection)
						{
							if (group2.Name.Equals (dialog.SelectedGroup))
								guid = group2.UniqueIdentifier;
						}
						
						if (string.IsNullOrEmpty (guid))
						{
							Log.Error ("GUID was not found for group name: "+dialog.SelectedGroup);
							return;
						}
						
						success = session.MoveContactToGroup(out error, contact.UniqueIdentifier, group.UniqueIdentifier, guid);
						
						if (!success)
						{
							MessageDialog errordialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, error);
							errordialog.Run ();
							errordialog.Destroy ();
							response = ResponseType.None;
							continue;
						}
						
						dialog.Destroy();
						break;
					}
					else
					{
						dialog.Destroy();
						break;
					}
				}
			}
		}
	}
	
	public class CopyToGroupCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			if (SessionUtility.ActiveSession is MsnSession)
			{
				ContactTreeContext context = Object as ContactTreeContext;
				MsnContact contact = context.Value as MsnContact;
				
				CopyMoveGroupDialog dialog = new CopyMoveGroupDialog((MsnSession)SessionUtility.ActiveSession, contact, true, false);
				ResponseType response = ResponseType.None;
				
				while(response == ResponseType.None)
				{
					response = (ResponseType)dialog.Run();
					
					if (response == ResponseType.Apply)
					{
						string error = String.Empty;
						bool success = false;
						
						MsnSession session = SessionUtility.ActiveSession as MsnSession;
						
						string guid = string.Empty;
						// Since MSN protocol doesn't use group names, but GUIDs, we should convert that now.
						foreach (MsnGroup group in session.GroupCollection)
						{
							if (group.Name.Equals (dialog.SelectedGroup))
								guid = group.UniqueIdentifier;
						}
						
						if (string.IsNullOrEmpty (guid))
						{
							Log.Error ("GUID was not found for group name: "+dialog.SelectedGroup);
							return;
						}
						
						success = session.AddContactToGroups(out error, contact.UniqueIdentifier, guid);
						
						if (!success)
						{
							MessageDialog errordialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, error);
							errordialog.Run ();
							errordialog.Destroy ();
							response = ResponseType.None;
							continue;
						}
						
						dialog.Destroy();
						break;
					}
					else
					{
						dialog.Destroy();
						break;
					}
				}
			}
		}
	}
	
	public class SupressNameCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnContact contact = context.Value as MsnContact;
			
			if (contact != null)
				(MenuItem as CheckMenuItem).Active = contact.SupressName;
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnContact contact = context.Value as MsnContact;
			
			if (contact != null)
			{
				contact.SupressName = (MenuItem as CheckMenuItem).Active;
				contact.Save();
			}
		}
	}
	
	public class SupressImageCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnContact contact = context.Value as MsnContact;
			
			if (contact != null)
				(MenuItem as CheckMenuItem).Active = contact.SupressImage;
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnContact contact = context.Value as MsnContact;
			
			if (contact != null)
			{
				contact.SupressImage = (MenuItem as CheckMenuItem).Active;
				contact.Save();
			}
		}
	}
	
	public class SupressPersonalMessage : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnContact contact = context.Value as MsnContact;
			
			if (contact != null)
				(MenuItem as CheckMenuItem).Active = contact.SupressMessage;
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnContact contact = context.Value as MsnContact;
			
			if (contact != null)
			{
				contact.SupressMessage = (MenuItem as CheckMenuItem).Active;
				contact.Save();
			}
		}
	}
	
	public class AutoAcceptCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnContact contact = context.Value as MsnContact;
			
			if (contact != null)
				(MenuItem as CheckMenuItem).Active = contact.AutoAccept;
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnContact contact = context.Value as MsnContact;
			
			if (contact != null)
			{
				contact.AutoAccept = (MenuItem as CheckMenuItem).Active;
				contact.Save();
			}
		}
	}
	
	public class SendFileContactCommand :AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnContact contact = context.Value as MsnContact;
			
			if (contact != null)
				(MenuItem as ImageMenuItem).Sensitive = contact.Presence != MsnPresence.Offline;
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnContact contact = context.Value as MsnContact;
			MsnSession session = contact.Session as MsnSession;
			
			if (contact != null && contact.DisplayName != session.Account.DisplayName)
			{
				SendFileDialog dialog = new SendFileDialog (contact);
				
				// Here we would fill the dialog with the options we intend to use to send the file.
				
				ResponseType response = (ResponseType) dialog.Run ();
				
				if (response == ResponseType.Apply)
				{
					if (dialog.Filename != null && dialog.Filename.Length > 0)
					{
						MsnFileTransfer transfer = new MsnFileTransfer (contact.Session, contact, dialog.Filename);
						
						MsnP2PUtility.Add (transfer.P2PTransfer);
						(contact.Session as MsnSession).EmitTransferInvitationSent (new FileTransferEventArgs (transfer));
					}
				}
				
				dialog.Destroy();
			}
		}
	}
	
	public class RemoveGroupCommand :AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnGroup group = context.Value as MsnGroup;
			
			if (group != null)
			{
				(MenuItem as ImageMenuItem).Sensitive = group.UniqueIdentifier != "0";
			}
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnGroup group = context.Value as MsnGroup;
			
			if (group != null)
			{
				string error = String.Empty;
				
				RemoveGroupDialog dialog = new RemoveGroupDialog(group.Session as MsnSession, group);
				
				if (dialog.Run() == (int)ResponseType.Apply)
				{
					bool success = (group.Session as Galaxium.Protocol.Msn.MsnSession).RemoveGroup(out error, group.UniqueIdentifier, dialog.Clear);
					
					if (!success)
					{
						MessageDialog errordialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, error);
						errordialog.Run ();
						errordialog.Destroy ();
					}
				}
				
				dialog.Destroy();
			}
		}
	}
	
	public class RenameGroupCommand :AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnGroup group = context.Value as MsnGroup;
			
			if (group != null)
			{
				(MenuItem as ImageMenuItem).Sensitive = group.UniqueIdentifier != "0";
			}
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			MsnGroup group = context.Value as MsnGroup;
			
			if (group != null)
			{
				string error = String.Empty;
				
				RenameGroupDialog dialog = new RenameGroupDialog(group.Session as MsnSession, group);
				
				if (dialog.Run() == (int)ResponseType.Apply)
				{
					bool success = (group.Session as Galaxium.Protocol.Msn.MsnSession).RenameGroup(out error, group.UniqueIdentifier, dialog.Name);
					
					if (!success)
					{
						MessageDialog errordialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, error);
						errordialog.Run ();
						errordialog.Destroy ();
					}
				}
				
				dialog.Destroy();
			}
		}
	}
}