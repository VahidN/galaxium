/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol.Msn;

using Anculus.Core;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public class AddContactCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			if (SessionUtility.ActiveSession is MsnSession)
			{
				AddContactDialog dialog = new AddContactDialog((MsnSession)SessionUtility.ActiveSession, false);
				ResponseType response = ResponseType.None;
				
				while(response == ResponseType.None)
				{
					response = (ResponseType)dialog.Run();
					
					if (response == ResponseType.Ok)
					{
						string error = String.Empty;
						bool success = false;
						
						MsnSession session = SessionUtility.ActiveSession as MsnSession;
						
						if (dialog.SelectedGroup == "0")
							success = session.AddContact (out error, dialog.Address, dialog.Alias, dialog.Block);
						else
						{
							string guid = string.Empty;
							// Since MSN protocol doesn't use group names, but GUIDs, we should convert that now.
							foreach (MsnGroup group in session.GroupCollection)
							{
								if (group.Name.Equals (dialog.SelectedGroup))
									guid = group.UniqueIdentifier;
							}
							
							if (string.IsNullOrEmpty (guid))
							{
								Log.Error ("GUID was not found for group name: "+dialog.SelectedGroup);
								return;
							}
							
							success = session.AddContactWithGroups (out error, dialog.Address, dialog.Alias, dialog.Block, guid);
						}
						
						if (!success)
						{
							MessageDialog errordialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, error);
							errordialog.Run ();
							errordialog.Destroy ();
							response = ResponseType.None;
							continue;
						}
						
						dialog.Destroy();
						break;
					}
					else
					{
						dialog.Destroy();
						break;
					}
				}
			}
		}
	}

	public class AddGroupCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			if (SessionUtility.ActiveSession is MsnSession)
			{
				AddGroupDialog dialog = new AddGroupDialog((MsnSession)SessionUtility.ActiveSession);
				ResponseType response = ResponseType.None;
				
				while(response == ResponseType.None)
				{
					response = (ResponseType)dialog.Run();
					
					if (response == ResponseType.Ok)
					{
						string error = String.Empty;
						bool success = false;
						
						MsnSession session = SessionUtility.ActiveSession as MsnSession;
						success = session.AddGroup (out error, dialog.Name);
						
						if (!success)
						{
							MessageDialog errordialog = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, error);
							errordialog.Run ();
							errordialog.Destroy ();
							response = ResponseType.None;
							continue;
						}
						
						dialog.Destroy();
						break;
					}
					else
					{
						dialog.Destroy();
						break;
					}
				}
			}
		}
	}

	public class SetDisplayImageCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			if (SessionUtility.ActiveSession is MsnSession)
			{
				SetDisplayImageDialog dialog = new SetDisplayImageDialog((MsnSession)SessionUtility.ActiveSession);
				
				if ((ResponseType)dialog.Run() == ResponseType.Ok)
				{
					MsnDisplayImage image = new MsnDisplayImage(SessionUtility.ActiveSession as MsnSession);
					
					image.Filename = dialog.Filename;
					
					MsnSession session = SessionUtility.ActiveSession as MsnSession;
					
					session.Account.DisplayImage = image;
				}
				
				dialog.Destroy();
			}
		}
	}

	public class SessionSettingsCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			
		}
	}
}