/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol.Gui;

using Anculus.Core;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public class InviteContactCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as ImageMenuItem).Sensitive = (chatWidget != null) && (chatWidget.Conversation as MsnConversation).CanInvite;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				new InviteContactDialog (chatWidget.Conversation, delegate (IContact c)
				{
					MsnContact contact = c as MsnContact;
					
					return (contact.Network == Network.WindowsLive) &&
							(contact.Presence != MsnPresence.Offline) &&
							(!chatWidget.Conversation.ContactCollection.Contains (contact));
				},
				delegate (IContact c)
				{
					MsnContact contact = c as MsnContact;

					if (contact.Presence == MsnPresence.Idle)
						return IconUtility.GetIcon ("galaxium-idle", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Offline)
						return IconUtility.GetIcon ("galaxium-offline", IconSizes.Small);
					else if (!contact.IsInList(MsnListType.Reverse))
						return IconUtility.GetIcon ("galaxium-status-reverse", IconSizes.Small);
					else if (contact.IsBlocked)
						return IconUtility.GetIcon ("galaxium-status-block", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Away)
						return IconUtility.GetIcon ("galaxium-status-away", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Brb)
						return IconUtility.GetIcon ("galaxium-status-brb", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Busy)
						return IconUtility.GetIcon ("galaxium-status-busy", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Lunch)
						return IconUtility.GetIcon ("galaxium-status-lunch", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Online)
						return IconUtility.GetIcon ("galaxium-contact", IconSizes.Small);
					else if (contact.Presence == MsnPresence.Phone)
						return IconUtility.GetIcon ("galaxium-status-phone", IconSizes.Small);
					
					return null;
				});
			}
		}
	}
	
	public class ClearConversationCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			
		}
	}
	
	public class ViewLogsCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null && chatWidget.Conversation != null)
			{
				ViewHistoryDialog dialog = new ViewHistoryDialog (chatWidget.Conversation, chatWidget);
				ResponseType response = (ResponseType)dialog.Run();
				
				if (response == ResponseType.Apply)
				{
					
				}
				
				dialog.Destroy ();
			}
		}
	}
	
	public class FindTextCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			
		}
	}
	
	public class SendNudgeCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as ImageMenuItem).Sensitive = (chatWidget != null) && (chatWidget.Conversation as MsnConversation).CanSendNudge;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
				chatWidget.SendNudge ();
		}
	}
	
	public class SendFileCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			(MenuItem as ImageMenuItem).Sensitive = (chatWidget != null) &&(chatWidget != null) &&
													chatWidget.Conversation.IsPrivateConversation &&
													((chatWidget.Conversation.PrimaryContact as MsnContact).Network == Network.WindowsLive) &&
													(chatWidget.Conversation.PrimaryContact.Presence != MsnPresence.Offline);
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
				chatWidget.SendFile ();
		}
	}
	
	public class CloseConversationCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
				chatWidget.Close();
		}
	}
	
	public class CloseWindowCommand: AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			var chat_widget = Object as MsnChatWidget;
			
			if (chat_widget != null)
				chat_widget.ContainerWindow.Close(false);
		}
	}
	
	public class ShowActionToolbarCommand : ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = UsingDefaultView ? _config.GetBool (Configuration.Conversation.ShowActivityToolbar.Name, Configuration.Conversation.ShowActivityToolbar.Default) : _chat_widget.ShowActionToolbar;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultView)
				_config.SetBool (Configuration.Conversation.ShowActivityToolbar.Name, (MenuItem as CheckMenuItem).Active);
			else
			{
				_chat_widget.ShowActionToolbar = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class ShowInputToolbarCommand : ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = UsingDefaultView ? _config.GetBool (Configuration.Conversation.ShowInputToolbar.Name, Configuration.Conversation.ShowInputToolbar.Default) : _chat_widget.ShowInputToolbar;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultView)
				_config.SetBool (Configuration.Conversation.ShowInputToolbar.Name, (MenuItem as CheckMenuItem).Active);
			else
			{
				_chat_widget.ShowInputToolbar = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class ShowAccountImageCommand : ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			if (!_chat_widget.Conversation.IsPrivateConversation)
			{
				(MenuItem as CheckMenuItem).Active = false;
				(MenuItem as CheckMenuItem).Sensitive = false;
			}
			else
				(MenuItem as CheckMenuItem).Active = UsingDefaultView ? _config.GetBool (Configuration.Contact.ShowAccountImage.Name, Configuration.Contact.ShowAccountImage.Default) : _chat_widget.ShowAccountImage;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultView)
				_config.SetBool (Configuration.Contact.ShowAccountImage.Name, (MenuItem as CheckMenuItem).Active);
			else
			{
				_chat_widget.ShowAccountImage = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class ShowContactImageCommand : ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			if (!_chat_widget.Conversation.IsPrivateConversation)
			{
				(MenuItem as CheckMenuItem).Active = false;
				(MenuItem as CheckMenuItem).Sensitive = false;
			}
			else
				(MenuItem as CheckMenuItem).Active = UsingDefaultView ? _config.GetBool (Configuration.Contact.ShowContactImage.Name, Configuration.Contact.ShowContactImage.Default) : _chat_widget.ShowContactImage;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultView)
				_config.SetBool (Configuration.Contact.ShowContactImage.Name, (MenuItem as CheckMenuItem).Active);
			else
			{
				_chat_widget.ShowContactImage = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class ShowPersonalMessageCommand : ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			if (!_chat_widget.Conversation.IsPrivateConversation)
			{
				(MenuItem as CheckMenuItem).Active = false;
				(MenuItem as CheckMenuItem).Sensitive = false;
			}
			else
				(MenuItem as CheckMenuItem).Active = UsingDefaultView ? _config.GetBool (Configuration.Contact.ShowIdentification.Name, Configuration.Contact.ShowIdentification.Default) : _chat_widget.ShowPersonalMessage;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultView)
				_config.SetBool (Configuration.Contact.ShowIdentification.Name, (MenuItem as CheckMenuItem).Active);
			else
			{
				_chat_widget.ShowPersonalMessage = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class UseDefaultViewCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
				(MenuItem as CheckMenuItem).Active = chatWidget.UseDefaultView;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				chatWidget.UseDefaultView = (MenuItem as CheckMenuItem).Active;
				chatWidget.Conversation.PrimaryContact.Save ();
				chatWidget.SwitchTo ();
				chatWidget.Update ();
			}
		}
	}
	
	public class EnableLoggingCommand : ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = UsingDefaultSettings ? _config.GetBool (Configuration.Conversation.EnableLogging.Name, Configuration.Conversation.EnableLogging.Default) : _chat_widget.Conversation.EnableLogging;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultSettings)
			{
				_config.SetBool (Configuration.Conversation.EnableLogging.Name, (MenuItem as CheckMenuItem).Active);
				_chat_widget.Conversation.EnableLogging = (MenuItem as CheckMenuItem).Active;
			}
			else
			{
				_chat_widget.Conversation.EnableLogging = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class EnableSoundsCommand : ConversationMenuCommand
	{
		public override void SetDefaultMenuItem ()
		{
			(MenuItem as CheckMenuItem).Active = UsingDefaultSettings ? _config.GetBool (Configuration.Conversation.EnableSounds.Name, Configuration.Conversation.EnableSounds.Default) : _chat_widget.EnableSounds;
		}
		
		public override void RunDefault ()
		{
			if (UsingDefaultSettings)
				_config.SetBool (Configuration.Conversation.EnableSounds.Name, (MenuItem as CheckMenuItem).Active);
			else
			{
				_chat_widget.EnableSounds = (MenuItem as CheckMenuItem).Active;
				_chat_widget.Conversation.PrimaryContact.Save ();
			}
			
			_chat_widget.Update ();
		}
	}
	
	public class UseDefaultSettingsCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
				(MenuItem as CheckMenuItem).Active = chatWidget.Conversation.UseDefaultSettings;
		}
		
		public override void Run ()
		{
			MsnChatWidget chatWidget = Object as MsnChatWidget;
			
			if (chatWidget != null)
			{
				chatWidget.Conversation.UseDefaultSettings = (MenuItem as CheckMenuItem).Active;
				chatWidget.Conversation.PrimaryContact.Save ();
				chatWidget.SwitchTo ();
				chatWidget.Update ();
			//	chatWidget.ReportLogging (); FIXME
			}
		}
	}
}