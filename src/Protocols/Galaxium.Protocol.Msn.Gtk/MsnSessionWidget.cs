/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Text;

using Gtk;
using Gdk;
using Glade;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Client;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Protocol.Msn;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public class MsnSessionWidget : BasicSessionWidget
	{
		
		internal MsnSessionWidget (IControllerWindow<Widget> window, MsnSession session) : base (window, session)
		{
			MsnWidgetUtility.AddSessionWidget(session, this);
		}
		
		public override void Initialize ()
		{
			base.Initialize ();
			
			// We need to setup a MSN tree manager for our tree
			_tree_view.Manager = new MsnContactTreeManager ();
			
			// We have to insert the protocol specific statuses
			_status_combo.Append (MsnPresence.Online);
			_status_combo.Append (MsnPresence.Away);
			_status_combo.Append (MsnPresence.Brb);
			_status_combo.Append (MsnPresence.Busy);
			_status_combo.Append (MsnPresence.Phone);
			_status_combo.Append (MsnPresence.Lunch);
			_status_combo.Append (MsnPresence.Invisible);
			
			// Make sure everything is positioned properly.
			Update ();
		}
		
		public override IConversation CreateUsableConversation (IContact contact)
		{
			IConversation conversation = new MsnConversation (contact as MsnContact);
			_session.Conversations.Add (conversation);
			return conversation;
		}
		
		protected override string PresenceTextLookup (IPresence item)
		{
			return item.State;
		}
		
		protected override Gdk.Pixbuf PresenceImageLookup (IPresence item, IIconSize size)
		{
			return IconUtility.StatusLookup (item, size);
		}
		
		protected override void SetDisplayImage ()
		{
			new SetDisplayImageCommand().Run();
		}
		
		private void ShowNewContactDialog(NewContactActivity activity)
		{
				string passport = String.Empty;
				
				if (activity.Contact != null)
					passport = activity.Contact.UniqueIdentifier;
				else
					passport = activity.UniqueIdentifier;
				
				NewContactDialog dialog = new NewContactDialog((MsnSession)SessionUtility.ActiveSession, passport, true, false);
				ResponseType response = ResponseType.None;
				
				while(response == ResponseType.None)
				{
					response = (ResponseType)dialog.Run();
					
					if (response == ResponseType.Ok || response == ResponseType.DeleteEvent)
					{
						string error = String.Empty;
						bool success = false;
						bool block = response == ResponseType.DeleteEvent;
						
						MsnSession session = SessionUtility.ActiveSession as MsnSession;
						if (dialog.SelectedGroup == "Other Contacts")
							success = session.AddContact (out error, dialog.Address, dialog.Nickname, block);
						else
						{
							string guid = string.Empty;
							// Since MSN protocol doesn't use group names, but GUIDs, we should convert that now.
							foreach (MsnGroup group in session.GroupCollection)
							{
								if (group.Name.Equals (dialog.SelectedGroup))
									guid = group.UniqueIdentifier;
							}
							
							if (string.IsNullOrEmpty (guid))
							{
								Log.Error ("GUID was not found for group name: "+dialog.SelectedGroup);
								return;
							}
							
							success = session.AddContactWithGroups (out error, dialog.Address, dialog.Nickname, block, guid);
						}
						
						if (!success)
						{
							MessageDialog errordialog = new MessageDialog (null, DialogFlags.Modal, Gtk.MessageType.Error, ButtonsType.Ok, error);
							errordialog.Run ();
							errordialog.Destroy ();
							response = ResponseType.None;
							continue;
						}
						else
						{
							MsnContact contact = SessionUtility.ActiveSession.ContactCollection.GetContact(passport) as MsnContact;
							
							if (!contact.IsInList(MsnListType.Reverse))
								contact.ListType |= MsnListType.Reverse;
						}
						
						dialog.Destroy();
						break;
					}
					else
					{
						dialog.Destroy();
						break;
					}
				}
		}
		
#region Activity Listeners
#pragma warning disable 169
		static void OnNewContactActivity(object sender, NewContactActivity activity)
		{
			if (!(activity.Session is MsnSession))
				return;
				
			MsnSessionWidget widget = MsnWidgetUtility.GetSessionWidget((MsnSession)activity.Session);
			widget.ShowNewContactDialog(activity);
		}
#pragma warning restore 169
#endregion
	}
}