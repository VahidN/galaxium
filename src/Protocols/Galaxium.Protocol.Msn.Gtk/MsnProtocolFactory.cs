/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Core;

namespace Galaxium.Protocol.Msn.GtkGui
{
	public class MsnProtocolFactory : AbstractProtocolFactory, IProtocolGuiFactory<Widget>
	{
		public MsnProtocolFactory () : base (MsnProtocol.Instance)
		{
			
		}

		public ISessionWidget<Widget> CreateSessionWidget (IControllerWindow<Widget> window, ISession session)
		{
			ThrowUtility.ThrowIfNull ("window", window);
			ThrowUtility.ThrowIfNull ("session", session);
			
			return new MsnSessionWidget (window, session as MsnSession);
		}
		
		public IAccountWidget<Widget> CreateAccountWidget ()
		{
			return new MsnAccountWidget ();
		}

		public IChatWidget<Widget> CreateChatWidget (IContainerWindow<Widget> window, IConversation conversation)
		{
			return new MsnChatWidget (window, conversation);
		}
		
		public override ISession CreateSession (IAccount account)
		{
			return new MsnSession (account as MsnAccount);
		}

		public override IAccount LoadAccount (IConfigurationSection config)
		{
			MsnAccount account = new MsnAccount (config.Name);
			
			// General account settings
			account.Password = config.GetString (Configuration.Account.Password.Name, Configuration.Account.Password.Default);
			account.DisplayName = config.GetString (Configuration.Account.DisplayName.Name, account.UniqueIdentifier);
			account.DisplayMessage = config.GetString (Configuration.Account.DisplayMessage.Name, Configuration.Account.DisplayMessage.Default);
			account.DisplayImageContext = config.GetString (Configuration.Account.DisplayImage.Name, Configuration.Account.DisplayImage.Default);
			account.Nickname = config.GetString (Configuration.Account.Nickname.Name, Configuration.Account.Nickname.Default);
			account.InitialPresence = MsnPresence.ParseMsnPresence (config.GetString (Configuration.Account.InitialPresence.Name, "NLN"));
			account.AutoConnect = config.GetBool (Configuration.Account.AutoConnect.Name, Configuration.Account.AutoConnect.Default);
			account.RememberPassword = config.GetBool (Configuration.Account.RememberPassword.Name, Configuration.Account.RememberPassword.Default);
			
			account.ViewByGroup = config.GetBool (Configuration.Account.ViewByGroup.Name, Configuration.Account.ViewByGroup.Default);
			account.DetailLevel = (ContactTreeDetailLevel)config.GetInt (Configuration.Account.DetailLevel.Name, Configuration.Account.DetailLevel.Default);
			account.SortAlphabetic = config.GetBool (Configuration.Account.SortAlphabetic.Name, Configuration.Account.SortAlphabetic.Default);
			account.SortAscending = config.GetBool (Configuration.Account.SortAscending.Name, Configuration.Account.SortAscending.Default);
			account.ShowOfflineContacts = config.GetBool (Configuration.Account.ShowOfflineContacts.Name, Configuration.Account.ShowOfflineContacts.Default);
			account.ShowEmptyGroups = config.GetBool (Configuration.Account.ShowEmptyGroups.Name, Configuration.Account.ShowEmptyGroups.Default);
			account.ShowSearchBar = config.GetBool (Configuration.Account.ShowSearchBar.Name, Configuration.Account.ShowSearchBar.Default);
			account.GroupOfflineContacts = config.GetBool (Configuration.Account.GroupOfflineContacts.Name, Configuration.Account.GroupOfflineContacts.Default);
			account.ShowDisplayImages = config.GetBool (Configuration.Account.ShowContactImages.Name, Configuration.Account.ShowContactImages.Default);
			account.ShowDisplayNames = config.GetBool (Configuration.Account.ShowContactNames.Name, Configuration.Account.ShowContactNames.Default);
			account.ShowPersonalMessages = config.GetBool (Configuration.Account.ShowContactMessages.Name, Configuration.Account.ShowContactMessages.Default);
			account.ShowNicknames = config.GetBool (Configuration.Account.ShowContactNicknames.Name, Configuration.Account.ShowContactNicknames.Default);
			account.UseDefaultListView = config.GetBool (Configuration.Account.UseDefaultListView.Name, Configuration.Account.UseDefaultListView.Default);
			
			// MSN specific settings
			account.UseHTTP = config.GetBool ("UseHTTP", false);
			account.AllowUnknownContacts = config.GetBool ("AllowUnknownContacts", false);
			account.NotificationServerHostname = config.GetString ("NotificationServerHostname", MsnConstants.DefaultNotificationServerHostname);
			account.NotificationServerPort = config.GetInt ("NotificationServerPort", MsnConstants.DefaultNotificationServerPort);
			account.DisplayImageFilename = config.GetString ("DisplayImageFilename", string.Empty);
			
			return account;
		}
		
		public override void SaveAccount (IConfigurationSection config, IAccount acc)
		{
			MsnAccount account = acc as MsnAccount;
			
			// General account settings
			config.SetString ("Account", account.UniqueIdentifier);
			config.SetString (Configuration.Account.Password.Name, account.RememberPassword ? account.Password : String.Empty);
			config.SetString (Configuration.Account.InitialPresence.Name, MsnPresence.GetMsnPresenceCode (account.InitialPresence));
			config.SetString (Configuration.Account.DisplayName.Name, account.DisplayName);
			config.SetString (Configuration.Account.DisplayMessage.Name, account.DisplayMessage);
			config.SetString (Configuration.Account.DisplayImage.Name, account.DisplayImageContext);
			config.SetString (Configuration.Account.Nickname.Name, account.Nickname);
			config.SetBool (Configuration.Account.AutoConnect.Name, account.AutoConnect);
			config.SetBool (Configuration.Account.RememberPassword.Name, account.RememberPassword);
			
			config.SetBool (Configuration.Account.ViewByGroup.Name, account.ViewByGroup);
			config.SetInt (Configuration.Account.DetailLevel.Name, (int)account.DetailLevel);
			config.SetBool (Configuration.Account.SortAlphabetic.Name, account.SortAlphabetic);
			config.SetBool (Configuration.Account.SortAscending.Name, account.SortAscending);
			config.SetBool (Configuration.Account.ShowOfflineContacts.Name, account.ShowOfflineContacts);
			config.SetBool (Configuration.Account.ShowEmptyGroups.Name, account.ShowEmptyGroups);
			config.SetBool (Configuration.Account.ShowSearchBar.Name, account.ShowSearchBar);
			config.SetBool (Configuration.Account.GroupOfflineContacts.Name, account.GroupOfflineContacts);
			config.SetBool (Configuration.Account.ShowContactImages.Name, account.ShowDisplayImages);
			config.SetBool (Configuration.Account.ShowContactNames.Name, account.ShowDisplayNames);
			config.SetBool (Configuration.Account.ShowContactMessages.Name, account.ShowPersonalMessages);
			config.SetBool (Configuration.Account.ShowContactNicknames.Name, account.ShowNicknames);
			config.SetBool (Configuration.Account.UseDefaultListView.Name, account.UseDefaultListView);
			
			config.SetBool ("UseHTTP", account.UseHTTP);
			config.SetString ("NotificationServerHostname", account.NotificationServerHostname);
			config.SetInt ("NotificationServerPort", account.NotificationServerPort);
			config.SetBool ("AllowUnknownContacts", account.AllowUnknownContacts);
			config.SetString ("DisplayImageFilename", (account.DisplayImage != null) ? account.DisplayImage.Filename : string.Empty);
		}

		public override IAccountCache CreateAccountCache (IAccount account, string directory)
		{
			return new MsnAccountCache (account, directory);
		}
		
		public override IMessageSplitter CreateSplitter ()
		{
			return new MessengerPlusSplitter ();
		}
	}
}