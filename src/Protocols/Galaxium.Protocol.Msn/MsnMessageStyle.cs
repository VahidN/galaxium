/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol.Msn
{
	public class MsnMessageStyle : MessageStyle
	{
		public MsnMessageStyle (bool isRightAlligned, MsnCharacterSet charset, MsnPitchFamily pitchAndFamily)
		{
			_isRightAlligned = isRightAlligned;
			_charset = charset;
			_pitchAndFamily = pitchAndFamily;
		}
		
		public MsnMessageStyle ()
		{
		}
		
		bool _isRightAlligned;
		public bool IsRightAlligned
		{
			get { return _isRightAlligned; }
			set { _isRightAlligned = value; }
		}

		MsnCharacterSet _charset = MsnCharacterSet.Default;
		public MsnCharacterSet Charset
		{
			get { return _charset; }
			set { _charset = value; }
		}

		MsnPitchFamily _pitchAndFamily = MsnPitchFamily.Default;
		public MsnPitchFamily PitchAndFamily
		{
			get { return _pitchAndFamily; }
			set { _pitchAndFamily = value; }
		}
	}
}