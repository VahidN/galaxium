/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	public enum MsnCurrentMediaType { Music, Games, Office }
	
	public class MsnCurrentMedia
	{
		string _app = string.Empty;
		MsnCurrentMediaType _type = MsnCurrentMediaType.Music;
		bool _enabled = false;
		string _format = "{0} - {1}";
		string[] _values = new string[0];
		
		public string Application
		{
			get { return _app; }
			set { _app = value; }
		}
		
		public MsnCurrentMediaType Type
		{
			get { return _type; }
			set { _type = value; }
		}
		
		public bool Enabled
		{
			get { return _enabled; }
			set { _enabled = value; }
		}
		
		public string Format
		{
			get { return _format; }
			set { _format = value; }
		}
		
		public string[] Values
		{
			get { return _values; }
			set { _values = value; }
		}
		
		public string Display
		{
			get
			{
				try
				{
					return string.Format (_format, _values).Trim ();
				}
				catch (Exception)
				{
					Log.Warn ("Invalid format '{0}' with {1} values", _format, _values.Length);
					return string.Empty;
				}
			}
		}
		
		public MsnCurrentMedia (string artist, string track)
		{
			_type = MsnCurrentMediaType.Music;
			
			_values = new string[2];
			_values[0] = track;
			_values[1] = artist;
			
			_enabled = true;
		}
		
		public MsnCurrentMedia ()
		{
		}
		
		public override string ToString ()
		{
			string str = string.Format ("{0}\\0{1}\\0{2}\\0{3}\\0", _app, _type, _enabled ? "1" : "0", _format);
			
			foreach (string val in _values)
				str += val + "\\0";
			
			return str;
		}
		
		// Parse the string used in UUX/UBX
		public static MsnCurrentMedia FromString (string str)
		{
			string[] chunks = str.Split (new string[] { "\\0" }, StringSplitOptions.None);
			MsnCurrentMedia ret = new MsnCurrentMedia ();
			
			if (chunks.Length >= 4)
			{
				ret._app = chunks[0];
				
				try
				{
					ret._type = (MsnCurrentMediaType)Enum.Parse (typeof (MsnCurrentMediaType), chunks[1]);
				}
				catch (Exception)
				{
					Log.Warn ("Unknown current media type '{0}'", chunks[1]);
					ret._type = MsnCurrentMediaType.Music;
				}
				
				ret._enabled = chunks[2] == "1";
				ret._format = chunks[3];
				
				ret._values = new string[chunks.Length - 4];
				Array.Copy (chunks, 4, ret._values, 0, ret._values.Length);
			}
			
			return ret;
		}
	}
}
