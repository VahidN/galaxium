/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Xml;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	public class MsnDynamicDisplayPicture : MsnObject
	{
		XmlDocument _contentDesc;
		
		public MsnDynamicDisplayPicture (MsnSession session, string context)
			: base (session, context)
		{
			Type = MsnObjectType.DynamicDisplay;
		}
		
		public MsnDynamicDisplayPicture (MsnSession session)
			: base (session)
		{
			Type = MsnObjectType.DynamicDisplay;
		}
		
		protected override void OnDataChanged ()
		{
			_contentDesc = null;

			string dir = CabExtract.Instance.ExtractToDirectory (CacheFilename);
			
			if (string.IsNullOrEmpty (dir))
			{
				Log.Error ("Unable to extract {0}", CacheFilename);
				base.OnDataChanged ();
				return;
			}
			
			string contentFile = Path.Combine (dir, "content.xml");
			
			if (!File.Exists (contentFile))
			{
				Log.Error ("No content descriptor {0}", contentFile);
				base.OnDataChanged ();
				return;
			}
			
			try
			{
				_contentDesc = new XmlDocument ();
				_contentDesc.Load (contentFile);
			}
			catch (XmlException)
			{
				_contentDesc = null;
				Log.Error ("Invalid content descriptor");
				base.OnDataChanged ();
				return;
			}
			
			if (_contentDesc.DocumentElement.Name != "package")
			{
				Log.Error ("XML document element isn't package");
				base.OnDataChanged ();
				return;
			}
			
			Log.Debug ("Dynamic Display Content:\n{0}", _contentDesc.OuterXml);
			
			base.OnDataChanged ();
		}
	}
}
