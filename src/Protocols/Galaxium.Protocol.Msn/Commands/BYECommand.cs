/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	[MsnCommand ("BYE")]
	public class BYECommand: AbstractMsnCommand
	{
		public MsnContact Contact
		{
			get
			{
				string contact = GetArgument (0);
				if (Session.Protocol >= MsnProtocolVersion.MSNP16)
					if (contact.IndexOf (';') >= 0)
						contact = contact.Split(';')[0];
				
				return Session.FindContact (contact);
			}
			
			set
			{
				string contact = value.UniqueIdentifier;
				if (Session.Protocol >= MsnProtocolVersion.MSNP16)
					contact += ";"+MsnSession.MachineGuid.ToString("B");
				
				SetArgument (0, contact);
			}
		}
		
		public bool Timeout
		{
			get { return GetArgument (1) == "1"; }
			set
			{
				if (value)
					SetArgument (1, "1");
				else
				{
					while (Arguments.Count > 1)
						Arguments.RemoveAt (Arguments.Count - 1);
				}
			}
		}

		public BYECommand (MsnSession session, MsnContact contact, bool timeout)
			: base (session)
		{
			Contact = contact;
			Timeout = timeout;
		}
		
		public BYECommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
}
