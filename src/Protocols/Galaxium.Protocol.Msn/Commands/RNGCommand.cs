/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Msn
{
	[MsnCommand ("RNG")]
	public class RNGCommand: AbstractMsnContactCommand
	{
		public string SwitchboardIdentifier
		{
			get { return GetArgument (0); }
		}

		public string DisplayName
		{
			get { return GetArgument (5); }
		}

		public string HostName
		{
			get { return GetArgument (1).Split (':')[0]; }
		}

		public int Port
		{
			get { return int.Parse (GetArgument (1).Split (':')[1]); }
		}

		public string AuthenticationString
		{
			get { return GetArgument (3); }
		}
		
		public RNGCommand (MsnSession session, byte[] data)
			: base (session, null, data)
		{
			_contact = session.FindContact (GetArgument (4));
		}
	}
}
