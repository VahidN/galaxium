/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Msn
{
	[MsnCommand ("NLN")]
	[TransactionCommand (false, true)]
	public class NLNCommand : AbstractMsnCommand
	{
		public MsnContact Contact
		{
			get
			{
				string uid = null;
				Network network = Network.Unknown;
				
				if (Session.Protocol >= MsnProtocolVersion.MSNP16)
				{
					string networkUid = GetArgument (1);
					int iColon = networkUid.IndexOf (':');
					
					network = (Network)int.Parse (networkUid.Substring (0, iColon));
					uid = networkUid.Substring (iColon + 1);
				}
				else
				{
					uid = GetArgument (1);
					network = (Network)int.Parse (GetArgument (2));
				}
				
				return Session.FindContact (uid, network);
			}
		}
		
		public string DisplayName
		{
			get
			{
				if (Session.Protocol >= MsnProtocolVersion.MSNP16)
					return EncodingUtility.UrlDecode (GetArgument (2));
				
				return EncodingUtility.UrlDecode (GetArgument (3));
			}
		}

		public IPresence Presence
		{
			get { return MsnPresence.ParseMsnPresence (GetArgument (0)); }
		}

		public MsnClientCapabilities ClientIdentifier
		{
			get
			{
				if (Session.Protocol >= MsnProtocolVersion.MSNP16)
				{
					string[] capabilities = GetArgument (3).Split (':');
					
					return (MsnClientCapabilities)long.Parse (capabilities[0]);
				}
				
				return (MsnClientCapabilities)long.Parse (GetArgument (4));
			}
		}
		
		public MsnClientCapabilitiesExtended ClientCapabilitiesExtended
		{
			get
			{
				if (Session.Protocol >= MsnProtocolVersion.MSNP16)
				{
					string[] capabilities = GetArgument (3).Split (':');
					
					return (MsnClientCapabilitiesExtended)long.Parse (capabilities[1]);
				}
				
				return MsnClientCapabilitiesExtended.None;
			}
		}

		public MsnDisplayImage DisplayImage
		{
			get
			{
				if (Session.Protocol >= MsnProtocolVersion.MSNP16)
					return MsnObject.Load (Session, GetArgument (4)) as MsnDisplayImage;
				
				return MsnObject.Load (Session, GetArgument (5)) as MsnDisplayImage;
			}
		}
		
		public string NetworkIconURL
		{
			get
			{
				if (Session.Protocol >= MsnProtocolVersion.MSNP16)
					return GetArgument (5);
				
				return GetArgument (6);
			}
		}

		public NLNCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
}
