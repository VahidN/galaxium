/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Msn
{
	[MsnCommand ("UBM", MsnConnectionType.Any, typeof (UBMCommandParser))]
	[PayloadCommand]
	public class UBMCommand : ContentCommand
	{
		public override IMsnEntity Source
		{
			get { return Session.FindContact (GetArgument (0), Network) as IMsnEntity; }
			set
			{
				SetArgument (0, value.UniqueIdentifier);
				Network = value.Network;
			}
		}
		
		public Network Network
		{
			get { return (Network)int.Parse (GetArgument (1)); }
			set { SetArgument (1, ((int)value).ToString ()); }
		}
		
		public UUMType Type
		{
			get { return (UUMType)int.Parse (GetArgument (2)); }
			set { SetArgument (2, ((int)value).ToString ()); }
		}
		
		public UBMCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
	
	public class UBMCommandParser : ContentCommandParser
	{
		public UBMCommandParser ()
			: base (typeof (UBMCommand))
		{
		}
	}
}
