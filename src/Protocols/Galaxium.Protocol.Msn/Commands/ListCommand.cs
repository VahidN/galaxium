/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace Galaxium.Protocol.Msn
{	
	public class ListCommand: AbstractMsnCommand
	{
		public class ListItem: IComparable<ListItem>
		{
			public MsnContact Contact;
			public MsnListType ListType;
			
			public int CompareTo (ListItem x)
			{
				return Contact.CompareTo (x.Contact);
			}
		}
		
		bool _initial = false;
		Dictionary<string, List<ListItem>> _items = new Dictionary<string, List<ListItem>> ();
		
		public Dictionary<string, List<ListItem>> Items
		{
			get { return _items; }
		}
		
		public bool Initial
		{
			get { return _initial; }
		}
		
		public bool IsOK
		{
			get { return GetArgument (0) == "OK"; }
		}
		
		public override bool IsPayloadIn
		{
			get
			{
				if (IsOK)
					return false;
				
				return base.IsPayloadIn;
			}
		}
		
		public override byte[] Payload
		{
			get
			{
				if (IsOK)
					return new byte[0];
				
				MessageBuilder mb = new MessageBuilder ();
				
				if (_initial)
					mb.Append ("<ml l=\"1\">");
				else
					mb.Append ("<ml>");

				foreach (KeyValuePair<string, List<ListCommand.ListItem>> pair in _items)
				{
					mb.Append ("<d n=\"");
					mb.Append (pair.Key);
					mb.Append ("\">");
					
					foreach (ListCommand.ListItem item in pair.Value)
					{
						mb.Append ("<c n=\"");
						mb.Append (item.Contact.UniqueIdentifier.Substring (0, item.Contact.UniqueIdentifier.IndexOf ("@")));
						mb.Append ("\" l=\"");
						mb.Append ((int)item.ListType);
						mb.Append ("\" t=\"");
						mb.Append ((int)item.Contact.Network);
						mb.Append ("\"/>");
					}

					mb.Append ("</d>");
				}

				mb.Append ("</ml>");

				return mb.GetByteArray ();
			}
			set
			{
				if (IsOK)
					return;
				
				_items.Clear ();

				MemoryStream stream = new MemoryStream (value);
				XmlReader reader = new XmlTextReader (stream);

				string domain = null;
				while (reader.Read ())
				{
					if (reader.NodeType == XmlNodeType.Element)
					{
						if (reader.LocalName == "d")
						{
							domain = reader.GetAttribute ("n");
					
							if (!_items.ContainsKey (domain))
								_items.Add (domain, new List<ListCommand.ListItem> ());
						}
						else if (reader.LocalName == "c")
						{
							string contact = reader.GetAttribute ("n");
							MsnListType listType = (MsnListType)int.Parse (reader.GetAttribute ("l"));

							ListCommand.ListItem item = new ListCommand.ListItem ();
							item.Contact = Session.FindContact (string.Format ("{0}@{1}", contact, domain), (Network)int.Parse (reader.GetAttribute ("t"))) as MsnContact;
							item.ListType |= listType;
							
							_items[domain].Add (item);
						}
					}
				}

				reader.Close ();
				stream.Close ();
			}
		}
		
		public ListCommand (MsnSession session)
			: base (session)
		{
		}
		
		public ListCommand (MsnSession session, bool initial)
			: this (session)
		{
			_initial = initial;
		}
		
		public ListCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
		
		public bool Add (ListItem item)
		{
			int index = item.Contact.UniqueIdentifier.IndexOf ('@');
			
			if ((index < 0) || (index >= item.Contact.UniqueIdentifier.Length))
				return false;
			
			string domain = item.Contact.UniqueIdentifier.Substring (index + 1);
			
			MsnListType lists = item.ListType;
			CorrectLists (ref lists);
			item.ListType = lists;
			
			if (_items.ContainsKey (domain))
			{
				List<ListItem> list = _items[domain];
				int listIndex = list.BinarySearch (item);

				if (listIndex >= 0)
					list[listIndex].ListType |= item.ListType;
				else
					_items[domain].Add (item);
			}
			else
			{
				List<ListItem> items = new List<ListItem> ();
				items.Add (item);
				_items.Add (domain, items);
			}
			
			return true;
		}

		public bool Add (MsnContact contact)
		{
			MsnListType listType = contact.ListType;
			
			ListItem item = new ListItem ();
			item.Contact = contact;
			item.ListType = listType;

			return Add (item);
		}
		
		protected virtual void CorrectLists (ref MsnListType lists)
		{
			
		}

		public void Clear ()
		{
			_items.Clear ();
		}
	}
}
