/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Msn
{
	[MsnCommand ("CVR")]
	[TransactionCommand]
	public class CVRCommand: AbstractMsnCommand
	{
		public CVRCommand (MsnSession session)
			: base (session)
		{
			// Values from WLM 8.5.1302.1018
			
			SetArgument (0, "0x0409");
			SetArgument (1, "winnt");
			SetArgument (2, "6.0");
			SetArgument (3, "i386");
			SetArgument (4, "MSNMSGR");
			SetArgument (5, MsnConstants.ProductVersion);
			SetArgument (6, "msmsgs");
			SetArgument (7, session.Account.UniqueIdentifier);
		}
		
		public CVRCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
	
	[MsnCommand ("CVQ")]
	[TransactionCommand]
	public class CVQCommand: CVRCommand
	{
		public CVQCommand (MsnSession session)
			: base (session)
		{
		}
		
		public CVQCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
}
