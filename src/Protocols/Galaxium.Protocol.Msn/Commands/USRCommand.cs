/*
 * Galaxium Messenger
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Msn
{
	[MsnCommand ("USR", MsnConnectionType.Notification)]
	[TransactionCommand]
	public class NSUSRCommand: AbstractMsnCommand
	{
		public bool Initial
		{
			get { return (!IsOK) && (GetArgument (1) == "I"); }
			set
			{
				if (!IsOK)
					SetArgument (1, value ? "I" : "S");
			}
		}
		
		public string AuthScheme
		{
			get { return GetArgument (0); }
			set { SetArgument (0, value); }
		}
		
		public bool IsOK
		{
			get { return GetArgument (0) == "OK"; }
		}
		
		public bool Verified
		{
			get { return IsOK && (GetArgument (3) == "1"); }
		}
		
		public NSUSRCommand (MsnSession session, bool initial, params string[] data)
			: base (session)
		{
			AuthScheme = "SSO";
			
			Initial = initial;
			SetAuthData (data);
		}
		
		public NSUSRCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
		
		public string GetAuthData (int index)
		{
			return GetArgument (index + 2);
		}
		
		public void SetAuthData (params string[] data)
		{
			for (int i = 0; i < data.Length; i++)
				SetArgument (i + 2, data[i]);
		}
	}
	
	[MsnCommand ("USR", MsnConnectionType.Switchboard)]
	[TransactionCommand]
	public class SBUSRCommand: AbstractMsnCommand
	{
		public bool IsOK
		{
			get { return GetArgument (0) == "OK"; }
		}
		
		public string FriendlyName
		{
			get { return IsOK ? GetArgument (2) : string.Empty; }
		}
		
		public string Ticket
		{
			get { return IsOK ? string.Empty : GetArgument (1); }
			set
			{
				if (!IsOK)
					SetArgument (1, value);
			}
		}

		public SBUSRCommand (MsnSession session)
			: base (session)
		{
			if (Session.Protocol >= MsnProtocolVersion.MSNP16)
				SetArgument (0, session.Account.UniqueIdentifier + ";" + MsnSession.MachineGuid.ToString ("B"));
			else
				SetArgument (0, session.Account.UniqueIdentifier);
		}
		
		public SBUSRCommand (MsnSession session, string ticket)
			: this (session)
		{
			Ticket = ticket;
		}
		
		public SBUSRCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
}
