/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Msn
{
	public enum UUMType : int
	{
		TextMessage = 1,
		TypingUser = 2,
		Nudge = 3,
		Unknown = 4
	}
	
	[MsnCommand ("UUM")]
	[TransactionCommand]
	[PayloadCommand]
	public class UUMCommand : ContentCommand
	{
		public IMsnEntity Destination
		{
			get { return Session.FindContact (GetArgument (0), Network) as IMsnEntity; }
			set
			{
				SetArgument (0, value.UniqueIdentifier);
				Network = value.Network;
			}
		}
		
		public Network Network
		{
			get { return (Network)int.Parse (GetArgument (1)); }
			set { SetArgument (1, ((int)value).ToString ()); }
		}
		
		public UUMType Type
		{
			get { return (UUMType)int.Parse (GetArgument (2)); }
			set { SetArgument (2, ((int)value).ToString ()); }
		}
		
		public UUMCommand (MsnSession session)
			: base (session)
		{
			Payload = new byte[0];
		}
		
		public UUMCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
	}
}
