/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	public class ContentCommand : MIMECommand
	{
		IMsnContent _content = null;
		
		public IMsnContent Content
		{
			get
			{
				if (_content == null)
				{
					if (!MsnContentUtility.ContentTypes.ContainsKey (ContentType))
						return null;
				
					Type type = MsnContentUtility.ContentTypes[ContentType];
					_content = Activator.CreateInstance (type, this) as IMsnContent;
				}
				
				return _content;
			}
		}
		
		public string ContentType
		{
			get { return MIMEHeader["Content-Type"].Value; }
			set { MIMEHeader["Content-Type"] = value; }
		}
		
		public override byte[] Payload
		{
			get { return base.Payload; }
			set
			{
				base.Payload = value;
				_content = null;
			}
		}
		
		public virtual IMsnEntity Source
		{
			get { return null; }
			set { }
		}
		
		public ContentCommand (MsnSession session)
			: base (session)
		{
		}
		
		public ContentCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
		
		public override string ToString ()
		{
			return string.Format ("{0} ({1})", base.ToString (), ContentType);
		}
	}
	
	public class ContentCommandParser : AbstractMsnCommandParser
	{
		struct MultiPacketCommand
		{
			public int chunks;
			public byte[][] data;
		}
		
		static Dictionary<string, MultiPacketCommand> _multiPacket = new Dictionary<string, MultiPacketCommand> ();
		Type _commandType;
		
		protected ContentCommandParser (Type commandType)
		{
			_commandType = commandType;
		}
		
		public override IMsnCommand Parse (MsnSession session, byte[] data)
		{
			ContentCommand cmd = (ContentCommand)Activator.CreateInstance (_commandType, session, data);
			
			if (cmd.MIMEHeader.ContainsKey ("Message-ID") && cmd.MIMEHeader.ContainsKey ("Chunks"))
			{
				// Log the command here because we'll be returning null
				Log.Info ("<< {0}", cmd);
				
				MultiPacketCommand mpc = new MultiPacketCommand ();
				mpc.chunks = int.Parse (cmd.MIMEHeader["Chunks"]);
				
				string id = cmd.MIMEHeader["Message-ID"];
				
				// The final command shouldn't contain these
				cmd.MIMEHeader.Remove ("Message-ID");
				cmd.MIMEHeader.Remove ("Chunks");
				
				mpc.data = new byte[mpc.chunks][];
				mpc.data[0] = cmd.ToByteArray ();
				
				_multiPacket.Add (id, mpc);
				
				return null;
			}
			
			if (string.IsNullOrEmpty (cmd.ContentType) && cmd.MIMEHeader.ContainsKey ("Message-ID") && cmd.MIMEHeader.ContainsKey ("Chunk"))
			{
				// Again we'll be returning null, so log the command here
				Log.Info ("<< {0}", cmd);
				
				string id = cmd.MIMEHeader["Message-ID"];
				int chunk = int.Parse (cmd.MIMEHeader["Chunk"]);
				
				if (!_multiPacket.ContainsKey (id))
				{
					Log.Warn ("Received Multipacket Chunk With Unknown Message-ID ({0})", id);
					return null;
				}
				
				_multiPacket[id].data[chunk] = cmd.Payload;
				
				bool done = true;
				foreach (byte[] chunkData in _multiPacket[id].data)
					done = done && (chunkData != null);
				
				if (done)
				{
					int totalSize = 0;
					foreach (byte[] chunkData in _multiPacket[id].data)
						totalSize += chunkData.Length;
					
					int offset = 0;
					data = new byte[totalSize];
					foreach (byte[] chunkData in _multiPacket[id].data)
					{
						Array.Copy (chunkData, 0, data, offset, chunkData.Length);
						offset += chunkData.Length;
					}
					
					_multiPacket.Remove (id);
					
					return (ContentCommand)Activator.CreateInstance (_commandType, session, data);
				}
				else
					return null;
			}
			
			return cmd;
		}
	}
}
