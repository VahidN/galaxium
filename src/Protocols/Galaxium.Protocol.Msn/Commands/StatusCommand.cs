/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using System.Xml;

using Anculus.Core;

using Galaxium.Core;

namespace Galaxium.Protocol.Msn
{
	public abstract class StatusCommand: AbstractMsnCommand
	{
		XmlElement _data;
		XmlElement _psm;
		XmlElement _currentMedia;
		XmlElement _machineGuid;
		
		public string PersonalMessage
		{
			get { return _psm != null ? _psm.InnerText : null; }
			set { _psm.InnerText = value; }
		}

		public string CurrentMedia
		{
			get { return _currentMedia != null ? _currentMedia.InnerText : string.Empty; }
			set { _currentMedia.InnerText = value; }
		}
		
		public override byte[] Payload
		{
			get { return (_data != null) ? Encoding.UTF8.GetBytes (_data.OuterXml) : new byte[0]; }
			set
			{
				try
				{
					if (value.Length == 0)
						return;
					
					XmlDocument xml = new XmlDocument ();
					xml.LoadXml (Encoding.UTF8.GetString (value));
					
					ThrowUtility.ThrowIfFalse ("Invalid data", xml.DocumentElement.Name == "Data");
				
					_data = xml.DocumentElement;
					
					_psm = _data["PSM"];
					_currentMedia = _data["CurrentMedia"];
					_machineGuid = _data["MachineGuid"];
				}
				catch (Exception ex)
				{
					Log.Error (ex, "Couldn't decode status command payload");
					CreateDefault ();
				}
			}
		}

		public StatusCommand (MsnSession session, string personalMessage, string currentMedia)
			: base (session)
		{
			CreateDefault ();
			PersonalMessage = personalMessage;
			CurrentMedia = currentMedia;
		}
		
		public StatusCommand (MsnSession session, byte[] data)
			: base (session, data)
		{
		}
		
		void CreateDefault ()
		{
			XmlDocument xml = new XmlDocument ();
			
			_data = xml.CreateElement ("Data");
			
			_psm = xml.CreateElement ("PSM");
			_psm.InnerText = string.Empty;
			_data.AppendChild (_psm);
			
			_currentMedia = xml.CreateElement ("CurrentMedia");
			_currentMedia.InnerText = string.Empty;
			_data.AppendChild (_currentMedia);
			
			_machineGuid = xml.CreateElement ("MachineGuid");
			_machineGuid.InnerText = MsnSession.MachineGuid.ToString ("B");
			_data.AppendChild (_machineGuid);
		}
	}
}
