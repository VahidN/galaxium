/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Xml;

namespace Galaxium.Protocol.Msn
{
	public static class MsnXmlUtility
	{
		public static List<XmlElement> FindChildren (XmlElement xml, string name)
		{
			List<XmlElement> ret = new List<XmlElement> ();
			
			if (xml != null)
			{
				foreach (XmlElement child in xml)
				{
					if (child.LocalName == name)
						ret.Add (child);
				}
			}
			
			return ret;
		}
		
		public static XmlElement FindChild (XmlElement xml, string name)
		{
			foreach (XmlElement child in FindChildren (xml, name))
				return child;
			
			return null;
		}
		
		public static List<XmlElement> FindElements (XmlElement xml, string[] names, int index)
		{
			if (index == names.Length - 1)
				return FindChildren (xml, names[index]);
			
			List<XmlElement> children = FindChildren (xml, names[index]);
			List<XmlElement> ret = new List<XmlElement> ();
			
			foreach (XmlElement child in children)
				ret.AddRange (FindElements (child, names, index + 1));
			
			return ret;
		}
		
		public static List<XmlElement> FindElements (XmlElement xml, string path)
		{
			return FindElements (xml, path.Split (new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries), 0);
		}
		
		public static XmlElement FindElement (XmlElement xml, string path)
		{
			foreach (XmlElement elem in FindElements (xml, path))
				return elem;
			
			return null;
		}
		
		public static string FindText (XmlElement xml, string path)
		{
			XmlElement elem = FindChild (xml, path);

			if (elem == null)
				return string.Empty;
			
			return elem.InnerText;
		}
	}
}
