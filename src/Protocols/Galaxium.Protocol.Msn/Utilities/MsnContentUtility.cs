/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Reflection;

using Anculus.Core;

using Galaxium.Core;

namespace Galaxium.Protocol.Msn
{
	public static class MsnContentUtility
	{
		static Dictionary<string, Type> _contentTypes = new Dictionary<string, Type> ();
		
		public static Dictionary<string, Type> ContentTypes
		{
			get { return _contentTypes; }
		}
		
		static MsnContentUtility ()
		{
#if debug
			try
			{
#endif
			AutoAddAssembly (Assembly.GetExecutingAssembly ());
#if debug
			}
			catch (Exception ex)
			{
				Log.Error (ex, "Error initializing MsnContentUtility");
			}
#endif
		}
		
		public static void AutoAddAssembly (Assembly asm)
		{
			foreach (Type t in asm.GetTypes ())
			{
				if (t.GetCustomAttributes (typeof (MsnContentAttribute), false).Length > 0)
					AddType (t);
			}
		}
		
		public static void AddType (Type t)
		{
			foreach (MsnContentAttribute att in t.GetCustomAttributes (typeof (MsnContentAttribute), false))
			{
				foreach (string contentType in att.ContentTypes)
				{
					//Log.Debug ("{0}: {1}", contentType, t.Name);
					_contentTypes.Add (contentType, t);
				}
			}
		}
	}
}
