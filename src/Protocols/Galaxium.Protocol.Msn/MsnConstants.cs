/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using System.Collections.Generic;

namespace Galaxium.Protocol.Msn
{
	public static class MsnConstants
	{
		public const string DefaultNotificationServerHostname = "messenger.hotmail.com";
		public const int DefaultNotificationServerPort = 1863;
		
		public const string ProductID = "PROD0120PW!CCV9@";
		public const string ProductKey = "C1BX{V4W}Q3*10SM";
		public const string ProductVersion = "14.0.8064.0206";
		
		public static Guid ApplicationID = new Guid ("AAD9B99B-58E6-4F23-B975-D9EC1F9EC24A");
	}
}