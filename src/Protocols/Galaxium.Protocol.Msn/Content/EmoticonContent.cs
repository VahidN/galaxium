/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	[MsnContent ("text/x-mms-animemoticon", "text/x-mms-emoticon")]
	public class EmoticonContent : AbstractMsnContent
	{
		public List<MsnEmoticon> Emoticons
		{
			get
			{
				List<MsnEmoticon> emots = new List<MsnEmoticon> ();
				string[] split = DataString.Trim ().Split (new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);

				if (split.Length % 2 == 0)
				{
					int i = 0;
					while (i < split.Length)
					{
						string equiv = split[i++];
						string context = split[i++];
					
						emots.Add (new MsnEmoticon (Session, context, equiv));
					}
				}
				else
					Log.Warn ("Emoticon data split to odd number of chunks");
				
				return emots;
			}
			set
			{
				string str = string.Empty;
				
				foreach (MsnEmoticon emot in value)
					str += string.Format ("{0}\t{1}\t", emot.Equivalents[0], emot.Context);
				
				DataString = str;
			}
		}
		
		public EmoticonContent (MsnSession session)
			: base (session)
		{
		}
		
		public EmoticonContent (ContentCommand cmd)
			: base (cmd)
		{
		}
	}
}
