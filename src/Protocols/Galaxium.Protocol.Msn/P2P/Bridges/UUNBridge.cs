/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


using System;

namespace Galaxium.Protocol.Msn
{
	public class UUNBridge : AbstractMsnP2PBridge
	{
		const int _nsQueueSize = 0;
		
		MsnSession _session;
		NSConnection _ns;
		
		public override bool Open
		{
			get { return true; }
		}
		
		public override int MaxDataSize
		{
			get { return 1150; }
		}
		
		public override MsnContact Remote
		{
			get	{ return null; }
		}
		
		public UUNBridge (MsnSession session)
			: base (_nsQueueSize)
		{
			_session = session;
			_ns = _session.Connection;
		}
		
		protected override void TrueSend (MsnP2PSession session, MsnContact remote, P2PMessage msg)
		{
			P2PContent content = new P2PContent (msg.Session);
			content.Data = msg.ToByteArray (true);
			
			UUNCommand cmd = new UUNCommand (session.Session);
			cmd.Type = UUNType.MsnP2P;
			cmd.Destination = remote;
			cmd.Payload = msg.ToByteArray (false);
			
			_ns.Send (cmd, delegate (IMsnCommand response)
			{
				if (!(response is ACKCommand))
				{
					// Try again
					TrueSend (session, remote, msg);
					return;
				}
				
				OnBridgeSent (new P2PMessageEventArgs (session, msg));
			});
		}
	}
}
