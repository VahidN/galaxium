/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;

using Anculus.Core;

using Galaxium.Core;

namespace Galaxium.Protocol.Msn
{
	public class TCPv1Bridge : AbstractMsnP2PBridge
	{
		const int _tcpQueueSize = 0;
		
		bool _localListen = false;
		bool _fooHandled = false;
		bool _authenticated = false;
		
		byte[] _buffer = new byte[0];
		DirectTCPConnection _connection;
		Guid _nonce;
		MsnSession _session;
		MsnP2PSession _startupSession;
		
		public override int MaxDataSize
		{
			get { return 1352; }
		}
		
		public override bool Open
		{
			get { return _authenticated; }
		}
		
		public override MsnContact Remote
		{
			get
			{
				if (_startupSession != null)
					return _startupSession.Remote;
				
				foreach (MsnP2PSession p2pSession in SendQueues.Keys)
					return p2pSession.Remote;
				
				Log.Error ("Unable to find remote contact");
				
				return null;
			}
		}
		
		public TCPv1Bridge (MsnSession session, bool listen, IPAddress address, int port, Guid nonce)
			: base (_tcpQueueSize)
		{
			_localListen = listen;
			_session = session;
			_nonce = nonce;
			
			_connection = new DirectTCPConnection (listen, address, port);
			_connection.Connected += ConnectionConnected;
			_connection.Disconnected += ConnectionDisconnected;
			_connection.DataReceived += ConnectionDataReceived;
			_connection.SendComplete += ConnectionSendComplete;
		}
		
		public TCPv1Bridge (MsnP2PSession p2pSession, bool listen, IPAddress address, int port, Guid nonce)
			: this (p2pSession.Session, listen, address, port, nonce)
		{
			_startupSession = p2pSession;
		}
		
		protected override void TrueSend (MsnP2PSession session, MsnContact remote, P2PMessage msg)
		{
			byte[] data = msg.ToByteArray (false);
			
			Log.Debug (">> DC {0} [{1}]: {2}B P2P {3} {4} {5} {6} {7}", this, _connection.RemoteEndPoint, data.Length, msg.Header.SessionID, msg.Header.MessageID, msg.Header.AckID, msg.Header.ChunkOffset, msg.Header.Flags);
			
			Send (data, new KeyValuePair<MsnP2PSession,P2PMessage> (session, msg));
		}
		
		public void Send (byte[] data, object userData)
		{
			_connection.Send (BitUtility.FromInt32 (data.Length, false));
			_connection.Send (data, userData);
		}
		
		void ConnectionConnected (object sender, EventArgs args)
		{
			if (_connection.LocalServer)
				return;
			
			// We are the client, send the authentication
			
			byte[] fooData = Encoding.ASCII.GetBytes ("foo\0");
			
			Log.Debug ("Sending foo: {0}", BaseUtility.BytesToString (fooData));
			
			Send (fooData, null);
			_fooHandled = true;
			
			P2PMessage msg = new P2PMessage (_session);
			msg.Header.MessageID = _startupSession.NextID ();
			msg.Header.Flags = P2PHeaderFlag.DirectHandshake;
			
			byte[] msgData = msg.ToByteArray (false);
			byte[] nonceBytes = _nonce.ToByteArray ();
			
			Array.Copy (nonceBytes, 0, msgData, msgData.Length - nonceBytes.Length, nonceBytes.Length);
			
			Log.Debug ("Sending auth message: {0}", BaseUtility.BytesToString (msgData));
			
			Send (msgData, null);
		}
		
		void ConnectionDisconnected (object sender, EventArgs args)
		{
			_fooHandled = false;
			_authenticated = false;
			
			OnBridgeClosed ();
		}
		
		void ConnectionDataReceived (object sender, ByteArrayEventArgs args)
		{
			byte[] data = args.Data;
			
			int origLen = _buffer.Length;
			Array.Resize (ref _buffer, origLen + data.Length);
			Array.Copy (data, 0, _buffer, origLen, data.Length);
			
			while (true)
			{
				if (_buffer.Length < 4)
					return;
				
				int length = BitUtility.ToInt32 (_buffer, 0, false);
				
				//Log.Debug ("Need {0} bytes, got {1}", length, _buffer.Length - 4);
				
				if (_buffer.Length - 4 < length)
					return;
				
				//Log.Debug ("Enough data for a whole message present");
				
				byte[] msgData = new byte[length];
				Array.Copy (_buffer, 4, msgData, 0, msgData.Length);
				
				byte[] _newBuffer = new byte[_buffer.Length - length - 4];
				Array.Copy (_buffer, length + 4, _newBuffer, 0, _newBuffer.Length);
				_buffer = _newBuffer;
				
				HandleData (msgData);
			}
		}
		
		void HandleData (byte[] data)
		{
			//Log.Debug ("Received {0} byte message", data.Length);
			//Log.Debug (BaseUtility.BytesToString (data));
			
			if (!_fooHandled)
			{
				if (Encoding.ASCII.GetString (data) == "foo\0")
				{
					Log.Debug ("Received foo");
					_fooHandled = true;
				}
				else
				{
					Log.Error ("Invalid opening data {0}", BaseUtility.BytesToString (data));
					_connection.Disconnect ();
				}
				
				return;
			}
			
			P2PMessage msg = new P2PMessage (_session, data);
			
			if (!_authenticated)
			{
				if ((msg.Header.Flags & P2PHeaderFlag.DirectHandshake) != P2PHeaderFlag.DirectHandshake)
				{
					Log.Debug ("Handshake flag not set");
					_connection.Disconnect ();
					return;
				}
				
				if (!_localListen)
				{
					Log.Debug ("Direct connection established");
					_authenticated = true;
					
					OnBridgeOpened ();
					
					return;
				}
				
				byte[] guidData = new byte[16];
				Array.Copy (data, 32, guidData, 0, 16);
				
				Guid guid = new Guid (guidData);
				
				if (guid.Equals (_nonce))
				{
					Log.Debug ("Direct connection established");
					_authenticated = true;
					Send (_startupSession, Remote, msg);
					
					OnBridgeOpened ();
				}
				else
				{
					Log.Error ("Received nonce is {0}, expected {1}", guid, _nonce);
					_connection.Disconnect ();
				}
				
				return;
			}
			
			Log.Debug ("<< DC {0} [{1}]: {2}B P2P {3} {4} {5} {6} {7}", this, _connection.RemoteEndPoint, data.Length, msg.Header.SessionID, msg.Header.MessageID, msg.Header.AckID, msg.Header.ChunkOffset, msg.Header.Flags);
			
			MsnP2PUtility.ProcessMessage (this, Remote, msg);
		}
		
		void ConnectionSendComplete (object sender, ObjectEventArgs args)
		{
			KeyValuePair<MsnP2PSession, P2PMessage> pair = (KeyValuePair<MsnP2PSession, P2PMessage>)args.Object;
			
			OnBridgeSent (new P2PMessageEventArgs (pair.Key, pair.Value));
		}
	}
}
