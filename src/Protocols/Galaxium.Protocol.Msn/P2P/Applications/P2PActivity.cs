/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using System.Xml;

using Anculus.Core;

namespace Galaxium.Protocol.Msn
{
	[MsnP2PApplication (0, "6A13AF9C-5308-4F35-923A-67E8DDA40C2F")]
	public class P2PActivity : AbstractMsnP2PApplication
	{
		uint _appID;
		string _name;
		MsnConversation _conversation;
		
		public override uint AppID
		{
			get { return _appID; }
		}
		
		public string Name
		{
			get { return _name; }
		}
		
		public P2PActivity (MsnP2PSession p2pSession)
			: base (p2pSession)
		{
			string[] contextChunks = EncodingUtility.Base64DecodeSafe (P2PSession.Invite.MIMEBody["Context"].Value, Encoding.Unicode).Split (';');
			
			uint.TryParse (contextChunks[0], out _appID);
			_name = contextChunks[2];
			
			//TODO: will the MsnP2PSession always be using a switchboard?
			_conversation = (Session.Conversations as MsnConversationManager).GetConversation (P2PSession.Bridge as SBConnection);
			
			if (_conversation == null)
			{
				Log.Debug ("Creating conversation for session {0}", p2pSession.SessionID);
				_conversation = new MsnConversation (P2PSession.Bridge as SBConnection);
				Session.Conversations.Add (_conversation);
			}
			
			_conversation.EmitActivityInvite (this);
		}
		
		public override bool CheckInvite (SLPRequestMessage invite)
		{
			string[] contextChunks = EncodingUtility.Base64DecodeSafe (invite.MIMEBody["Context"].Value, Encoding.Unicode).Split (';');
			
			return base.CheckInvite (invite) && (contextChunks.Length >= 3);
		}
		
		public override string CreateInviteContext ()
		{
			//TODO: whats the 1 for?
			return EncodingUtility.Base64Encode (string.Format ("{0};1;{1}", AppID, Name));
		}
		
		public override bool ProcessMessage (IMsnP2PBridge bridge, P2PMessage msg)
		{
			Log.Debug ("{0}", msg);
			
			return false;
		}
	}
}
