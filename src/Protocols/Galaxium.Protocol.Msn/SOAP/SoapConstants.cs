/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol.Msn
{
	internal sealed class SoapConstants
	{
		public const string UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.0.04506; Windows Live Messenger 8.5.1302)";
		
		public const string nsAddressBook   = "http://www.msn.com/webservices/AddressBook";
		public const string nsDs            = "http://www.w3.org/2000/09/xmldsig#";
		public const string nsOIM           = "http://messenger.msn.com/ws/2004/09/oim/";
		public const string nsPassport      = "http://schemas.microsoft.com/Passport/SoapServices/PPCRL";
		public const string nsPassportFault =  "http://schemas.microsoft.com/Passport/SoapServices/SOAPFault";
		public const string nsStorage       = "http://www.msn.com/webservices/storage/w10";
		public const string nsWsa           = "http://schemas.xmlsoap.org/ws/2004/03/addressing";
		public const string nsWsp           = "http://schemas.xmlsoap.org/ws/2002/12/policy";
		public const string nsWsse          = "http://schemas.xmlsoap.org/ws/2003/06/secext";
		public const string nsWst           = "http://schemas.xmlsoap.org/ws/2004/04/trust";
		public const string nsWsu           = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
	}
}
