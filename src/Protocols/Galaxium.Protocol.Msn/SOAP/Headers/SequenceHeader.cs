/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Globalization;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.Headers
{
	[XmlRoot ("Sequence", Namespace="http://schemas.xmlsoap.org/ws/2003/03/rm", IsNullable=true)]
	public class SequenceHeader : SoapHeader, ICloneable
	{
		string _identifier = "http://messenger.msn.com";
		int _messageNumber = 1;
		
		[XmlElement (Namespace="http://schemas.xmlsoap.org/ws/2002/07/utility")]
		public string Identifier
		{
			get { return _identifier; }
			set { _identifier = value; }
		}
		
		public int MessageNumber
		{
			get { return _messageNumber; }
			set { _messageNumber = value; }
		}
		
		public SequenceHeader()
		{
		}
		
		public object Clone ()
		{
			SequenceHeader clone = new SequenceHeader ();
			
			clone._identifier = _identifier;
			clone._messageNumber = _messageNumber;
			
			return clone;
		}
	}
}
