/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Globalization;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.Headers
{
	[XmlRoot ("ServiceHeader", Namespace=SoapConstants.nsAddressBook, IsNullable=true)]
	public class ServiceHeader : SoapHeader, ICloneable
	{
		Version _version;
		string _cacheKey;
		bool _cacheKeyChanged;
		string _preferredHostName;
		
		public Version Version
		{
			get { return _version; }
			set { _version = value; }
		}
		
		public string CacheKey
		{
			get { return _cacheKey; }
			set { _cacheKey = value; }
		}
		
		public bool CacheKeyChanged
		{
			get { return _cacheKeyChanged; }
			set { _cacheKeyChanged = value; }
		}
		
		public string PreferredHostName
		{
			get { return _preferredHostName; }
			set { _preferredHostName = value; }
		}
		
		public object Clone ()
		{
			ServiceHeader clone = new ServiceHeader ();
			
			clone._cacheKey = _cacheKey;
			clone._cacheKeyChanged = _cacheKeyChanged;
			clone._preferredHostName = _preferredHostName;
			clone._version = _version;
			
			return clone;
		}
	}
}
