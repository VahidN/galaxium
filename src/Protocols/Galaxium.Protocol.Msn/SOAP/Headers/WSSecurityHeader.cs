/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.Headers
{
	[XmlRoot ("Security", Namespace=SoapConstants.nsWsse)]
	public class WSSecurityHeader : SoapHeader, ICloneable
	{
		UsernameToken _usernameToken;
		
		public UsernameToken UsernameToken
		{
			get { return _usernameToken; }
			set { _usernameToken = value; }
		}
		
		public WSSecurityHeader (MsnSession session)
		{
			_usernameToken = new UsernameToken (session);
		}
		
		public WSSecurityHeader ()
		{
		}
		
		public object Clone ()
		{
			WSSecurityHeader clone = new WSSecurityHeader ();
			
			clone._usernameToken = _usernameToken.Clone () as UsernameToken;
			
			return clone;
		}
	}
	
	[XmlRoot ("UsernameToken", Namespace="http://schemas.xmlsoap.org/ws/2003/06/secext")]
	public class UsernameToken: ICloneable
	{
		string _id = "user";
		string _username;
		string _password;
		
		[XmlAttribute]
		public string Id
		{
			get { return _id; }
			set { _id = value; }
		}
		
		public string Username
		{
			get { return _username; }
			set { _username = value; }
		}
		
		public string Password
		{
			get { return _password; }
			set { _password = value; }
		}
		
		public UsernameToken (MsnSession session)
		{
			_username = session.Account.UniqueIdentifier;
			_password = session.Account.Password;
		}
		
		public UsernameToken ()
		{
		}
		
		public object Clone ()
		{
			UsernameToken clone = new UsernameToken ();
			
			clone._id = _id;
			clone._username = _username;
			clone._password = _password;
			
			return clone;
		}
	}
}
