/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Globalization;
using System.IO;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.Headers
{
	[XmlRoot ("AuthInfo", Namespace=SoapConstants.nsPassport)]
	public class PassportAuthInfoHeader : SoapHeader
	{
		string _id = "PPAuthInfo";
		string _hostingApp = "{7108E71A-9926-4FCB-BCC9-9A9D3F32E423}";
		int _binaryVersion = 4;
		int _uiVersion = 1;
		string _requestParams;
		
		[XmlAttribute]
		public string Id
		{
			get { return _id; }
			set { _id = value; }
		}
		
		public string HostingApp
		{
			get { return _hostingApp; }
			set { _hostingApp = value; }
		}
		
		public int BinaryVersion
		{
			get { return _binaryVersion; }
			set { _binaryVersion = value; }
		}
		
		public int UIVersion
		{
			get { return _uiVersion; }
			set { _uiVersion = value; }
		}
		
		public string RequestParams
		{
			get { return _requestParams; }
			set { _requestParams = value; }
		}
		
		public PassportAuthInfoHeader ()
		{
			GenerateRequestParams ();
		}
		
		string GenerateRequestParams ()
		{
			string lcid = CultureInfo.CurrentCulture.LCID.ToString ("d4");
			
			byte[] paramData = new byte[14 + lcid.Length];
			MemoryStream stream = new MemoryStream (paramData);
			BinaryWriter writer = new BinaryWriter (stream);
			
			writer.Write ((Int32)1); //TODO: what's this?
			WriteString (writer, "lc");
			WriteString (writer, lcid);
			
			writer.Close ();
			stream.Close ();
			
			return Convert.ToBase64String (paramData);
		}
		
		void WriteString (BinaryWriter writer, string str)
		{
			writer.Write ((Int32)str.Length);
			
			foreach (char ch in str)
				writer.Write (ch);
		}
	}
}
