/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.Headers
{
	public enum ABPartnerScenario { Initial, ContactSave, GroupSave, Timer, BlockUnblock }
	
	[XmlRoot (Namespace=SoapConstants.nsAddressBook, IsNullable=true)]
	public class ABApplicationHeader : SoapHeader, ICloneable
	{
		string _applicationId = MsnConstants.ApplicationID.ToString("D");
		bool _isMigration;
		ABPartnerScenario _partnerScenario = ABPartnerScenario.Initial;
		string _cacheKey;
    
		public string ApplicationId
		{
			get { return _applicationId; }
			set { _applicationId = value; }
		}
    
		public bool IsMigration
		{
			get { return this._isMigration; }
			set { this._isMigration = value; }
		}

		public ABPartnerScenario PartnerScenario
		{
			get { return this._partnerScenario; }
			set { _partnerScenario = value; }
		}
		
		public string CacheKey
		{
			get { return _cacheKey; }
			set { _cacheKey = value; }
		}
		
		public object Clone ()
		{
			ABApplicationHeader clone = new ABApplicationHeader ();
			
			clone._applicationId = _applicationId;
			clone._cacheKey = _cacheKey;
			clone._isMigration = _isMigration;
			clone._partnerScenario = _partnerScenario;
			
			return clone;
		}
	}
}
