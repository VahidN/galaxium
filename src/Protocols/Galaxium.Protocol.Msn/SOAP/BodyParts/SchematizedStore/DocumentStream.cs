/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

using Anculus.Core;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	public enum DocumentStreamType { Unknown, UserTileStatic }
	
	[XmlInclude (typeof (PhotoStream))]
	public class DocumentStream
	{
		string _type;
		bool _typeSpecified;
		string _mimeType;
		bool _mimeTypeSpecified;
		byte[] _data;
		bool _dataSpecified;
		long _dataSize;
		bool _dataSizeSpecified;
		string _preAuthURL;
		bool _preAuthURLSpecified;
		string _preAuthURLPartner;
		bool _preAuthURLPartnerSpecified;
		
		[XmlElement ("DocumentStreamType")]
		public string _Type
		{
			get { return _type; }
			set
			{
				_type = value;
				TypeSpecified = true;
			}
		}
		
		[XmlIgnore]
		public DocumentStreamType Type
		{
			get
			{
				try
				{
					return (DocumentStreamType)Enum.Parse (typeof (DocumentStreamType), _type);
				}
				catch
				{
					Log.Warn ("Unknown value '{0}'", _type);
					return DocumentStreamType.Unknown;
				}
			}
			set { _Type = value.ToString (); }
		}
		
		[XmlIgnore]
		public bool TypeSpecified
		{
			get { return _typeSpecified; }
			set { _typeSpecified = value; }
		}
		
		public string MimeType
		{
			get { return _mimeType; }
			set
			{
				_mimeType = value;
				MimeTypeSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool MimeTypeSpecified
		{
			get { return _mimeTypeSpecified; }
			set { _mimeTypeSpecified = value; }
		}
		
		[XmlIgnore]
		public byte[] Data
		{
			get { return _data; }
			set
			{
				_data = value;
				DataSpecified = true;
			}
		}
		
		[XmlElement ("Data")]
		public string _Data
		{
			get { return Convert.ToBase64String (_data); }
			set { _data = Convert.FromBase64String (value); }
		}
		
		[XmlIgnore]
		public bool DataSpecified
		{
			get { return _dataSpecified; }
			set { _dataSpecified = value; }
		}
		
		public long DataSize
		{
			get { return _dataSize; }
			set
			{
				_dataSize = value;
				DataSizeSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool DataSizeSpecified
		{
			get { return _dataSizeSpecified; }
			set { _dataSizeSpecified = value; }
		}
		
		public string PreAuthURL
		{
			get { return _preAuthURL; }
			set
			{
				_preAuthURL = value;
				PreAuthURLSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool PreAuthURLSpecified
		{
			get { return _preAuthURLSpecified; }
			set { _preAuthURLSpecified = value; }
		}
		
		public string PreAuthURLPartner
		{
			get { return _preAuthURLPartner; }
			set
			{
				_preAuthURLPartner = value;
				PreAuthURLPartnerSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool PreAuthURLPartnerSpecified
		{
			get { return _preAuthURLPartnerSpecified; }
			set { _preAuthURLPartnerSpecified = value; }
		}
	}
	
	public class DocumentStreamCollection: List<DocumentStream>
	{
	}
	
	public class PhotoStream : DocumentStream
	{
		long _sizeX;
		bool _sizeXSpecified;
		long _sizeY;
		bool _sizeYSpecified;
		
		public long SizeX
		{
			get { return _sizeX; }
			set
			{
				_sizeX = value;
				SizeXSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool SizeXSpecified
		{
			get { return _sizeXSpecified; }
			set { _sizeXSpecified = value; }
		}
		
		public long SizeY
		{
			get { return _sizeY; }
			set
			{
				_sizeY = value;
				SizeYSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool SizeYSpecified
		{
			get { return _sizeYSpecified; }
			set { _sizeYSpecified = value; }
		}
	}
}
