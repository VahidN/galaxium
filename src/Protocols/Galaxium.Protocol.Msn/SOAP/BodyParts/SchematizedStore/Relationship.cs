/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Xml.Serialization;

using Anculus.Core;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	public enum SourceTypeEnum { Unknown, SubProfile }
	
	public class Relationship
	{
		string _sourceID;
		bool _sourceIDSpecified;
		string _sourceType;
		bool _sourceTypeSpecified;
		string _targetID;
		bool _targetIDSpecified;
		string _targetType;
		bool _targetTypeSpecified;
		string _relationshipName;
		bool _relationshipNameSpecified;
		
		public string SourceID
		{
			get { return _sourceID; }
			set
			{
				_sourceID = value;
				SourceIDSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool SourceIDSpecified
		{
			get { return _sourceIDSpecified; }
			set { _sourceIDSpecified = value; }
		}
		
		[XmlElement ("SourceType")]
		public string _SourceType
		{
			get { return _sourceType; }
			set
			{
				_sourceType = value;
				SourceTypeSpecified = true;
			}
		}
		
		[XmlIgnore]
		public SourceTypeEnum SourceType
		{
			get
			{
				try
				{
					return (SourceTypeEnum)Enum.Parse (typeof (SourceTypeEnum), _sourceType);
				}
				catch
				{
					Log.Warn ("Unknown value '{0}'", _sourceType);
					return SourceTypeEnum.Unknown;
				}
			}
			set { _SourceType = value.ToString (); }
		}
		
		[XmlIgnore]
		public bool SourceTypeSpecified
		{
			get { return _sourceTypeSpecified; }
			set { _sourceTypeSpecified = value; }
		}
		
		public string TargetID
		{
			get { return _targetID; }
			set
			{
				_targetID = value;
				TargetIDSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool TargetIDSpecified
		{
			get { return _targetIDSpecified; }
			set { _targetIDSpecified = value; }
		}

		[XmlElement ("TargetType")]
		public string _TargetType
		{
			get { return _targetType; }
			set
			{
				_targetType = value;
				TargetTypeSpecified = true;
			}
		}
		
		[XmlIgnore]
		public DocumentItemType TargetType
		{
			get
			{
				try
				{
					return (DocumentItemType)Enum.Parse (typeof (DocumentItemType), _targetType);
				}
				catch
				{
					Log.Warn ("Unknown value '{0}'", _targetType);
					return DocumentItemType.Unknown;
				}
			}
			set { _TargetType = value.ToString (); }
		}

		[XmlIgnore]
		public bool TargetTypeSpecified
		{
			get { return _targetTypeSpecified; }
			set { _targetTypeSpecified = value; }
		}

		public string RelationshipName
		{
			get { return _relationshipName; }
			set
			{
				_relationshipName = value;
				RelationshipNameSpecified = true;
			}
		}

		[XmlIgnore]
		public bool RelationshipNameSpecified
		{
			get { return _relationshipNameSpecified; }
			set { _relationshipNameSpecified = value; }
		}
	}
}
