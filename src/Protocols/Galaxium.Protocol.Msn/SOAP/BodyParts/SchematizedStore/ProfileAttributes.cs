/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot (Namespace=SoapConstants.nsStorage)]
	public class ProfileAttributes
	{
		bool _resourceID = true;
		bool _dateModified = true;
		ExpressionProfileAttributes _expressionProfileAttributes = new ExpressionProfileAttributes ();

		public bool ResourceID
		{
			get { return _resourceID; }
			set { _resourceID = value; }
		}
		
		public bool DateModified
		{
			get { return _dateModified; }
			set { _dateModified = value; }
		}
		
		public ExpressionProfileAttributes ExpressionProfileAttributes
		{
			get { return _expressionProfileAttributes; }
			set { _expressionProfileAttributes = value; }
		}
	}
}
