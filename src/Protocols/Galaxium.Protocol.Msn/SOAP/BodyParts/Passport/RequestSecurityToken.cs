/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot ("RequestSecurityToken", Namespace=SoapConstants.nsWst)]
	public class RequestSecurityToken
	{
		string _id = "RST0";
		string _requestType = "http://schemas.xmlsoap.org/ws/2004/04/security/trust/Issue";
		AppliesTo _appliesTo = new AppliesTo ();
		PolicyReference _policyReference = new PolicyReference ();
		
		[XmlAttribute]
		public string Id
		{
			get { return _id; }
			set { _id = value; }
		}
		
		public string RequestType
		{
			get { return _requestType; }
			set { _requestType = value; }
		}
		
		[XmlElement (Namespace=SoapConstants.nsWsp)]
		public AppliesTo AppliesTo
		{
			get { return _appliesTo; }
			set { _appliesTo = value; }
		}
		
		[XmlElement (Namespace=SoapConstants.nsWsse)]
		public PolicyReference PolicyReference
		{
			get { return _policyReference; }
			set { _policyReference = value; }
		}
		
		public RequestSecurityToken (string domain, string policyReference)
		{
			_appliesTo = new AppliesTo (domain);
			_policyReference = new PolicyReference (policyReference);
		}
		
		public RequestSecurityToken ()
		{
		}
	}
	
	public class RequestMultipleSecurityTokens : List<RequestSecurityToken>, IXmlSerializable
	{
		string _id = "RSTS";
		
		public string Id
		{
			get { return _id; }
			set { _id = value; }
		}
		
		public new void Add (RequestSecurityToken item)
		{
			item.Id = string.Format ("RST{0}", Count);
			base.Add (item);
		}
		
		public void Add (string domain, string policyReference)
		{
			Add (new RequestSecurityToken (domain, policyReference));
		}
		
		//TODO: Can we do this without using IXmlSerializable?
		// I couldn't get the Id attribute to show up without doing this
		public void WriteXml (XmlWriter writer)
		{
			//writer.WriteStartElement ("RequestMultipleSecurityTokens", SoapConstants.nsPassport);
			writer.WriteAttributeString ("Id", Id);
			
			XmlSerializer serializer = new XmlSerializer (typeof (RequestSecurityToken));
			
			foreach (RequestSecurityToken request in this)
				serializer.Serialize (writer, request);
			
			//writer.WriteEndElement ();
		}
		
		public void ReadXml (XmlReader reader)
		{
		}
		
		public XmlSchema GetSchema ()
		{
			return null;
		}
	}
}
