/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot (Namespace=SoapConstants.nsAddressBook)]
	public class ServiceHandle
	{
		int _id;
		bool _idSpecified = false;
		string _foreignId = string.Empty;
		bool _foreignIdSpecified = false;
		ServiceType _type = null;
		bool _typeSpecified = false;
		
		public int Id
		{
			get { return _id; }
			set
			{
				_id = value;
				_idSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool IdSpecified
		{
			get { return _idSpecified; }
			set { _idSpecified = value; }
		}
		
		[XmlElement (IsNullable=false)]
		public string ForeignId
		{
			get { return _foreignId; }
			set
			{
				_foreignId = value;
				_foreignIdSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool ForeignIdSpecified
		{
			get { return _foreignIdSpecified; }
			set { _foreignIdSpecified = value; }
		}
		
		public ServiceType Type
		{
			get { return _type; }
			set
			{
				_type = value;
				_typeSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool TypeSpecified
		{
			get { return _typeSpecified; }
			set { _typeSpecified = value; }
		}
		
		public ServiceHandle Clone ()
		{
			ServiceHandle sh = new ServiceHandle ();
			
			sh._foreignId = _foreignId;
			sh._foreignIdSpecified = _foreignIdSpecified;
			sh._id = _id;
			sh._idSpecified = _idSpecified;
			sh._type = (_type != null) ? _type.Clone () : null;
			sh._typeSpecified = _typeSpecified;
			
			return sh;
		}
		
		public void ApplyDelta (ServiceHandle sh)
		{
			if (_foreignIdSpecified)
				sh.ForeignId = _foreignId;
			if (_idSpecified)
				sh.Id = _id;
			if (_typeSpecified)
				sh.Type = _type.Clone ();
		}
	}
}
