/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot ("Contact", Namespace=SoapConstants.nsAddressBook)]
	public class ABContact
	{
		Guid _id = new Guid ();
		bool _idSpecified = false;
		ContactInfo _info = null;
		bool _infoSpecified = false;
		string _propertiesChanged;
		bool _propertiesChangedSpecified = false;
		bool _deleted;
		bool _deletedSpecified = false;
		DateTime _lastChange;
		bool _lastChangeSpecified;
		
		[XmlElement ("contactId")]
		public Guid Id
		{
			get { return _id; }
			set
			{
				_id = value;
				_idSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool IdSpecified
		{
			get { return _idSpecified; }
			set { _idSpecified = value; }
		}
		
		[XmlElement ("contactInfo")]
		public ContactInfo Info
		{
			get { return _info; }
			set
			{
				_info = value;
				_infoSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool InfoSpecified
		{
			get { return _infoSpecified; }
			set { _infoSpecified = value; }
		}
		
		[XmlElement ("propertiesChanged")]
		public string PropertiesChanged
		{
			get { return _propertiesChanged; }
			set
			{
				_propertiesChanged = value;
				_propertiesChangedSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool PropertiesChangedSpecified
		{
			get { return _propertiesChangedSpecified; }
			set { _propertiesChangedSpecified = value; }
		}
		
		[XmlElement ("fDeleted")]
		public bool Deleted
		{
			get { return _deleted; }
			set
			{
				_deleted = value;
				_deletedSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool DeletedSpecified
		{
			get { return _deletedSpecified; }
			set { _deletedSpecified = value; }
		}
		
		[XmlElement ("lastChange")]
		public DateTime LastChange
		{
			get { return _lastChange; }
			set
			{
				_lastChange = value;
				_lastChangeSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool LastChangeSpecified
		{
			get { return _lastChangeSpecified; }
			set { _lastChangeSpecified = value; }
		}
		
		public ABContact Clone ()
		{
			ABContact c = new ABContact ();
			
			c._deleted = _deleted;
			c._deletedSpecified = _deletedSpecified;
			c._id = _id;
			c._idSpecified = _idSpecified;
			c._info = (_info != null) ? _info.Clone () : null;
			c._infoSpecified = _infoSpecified;
			c._lastChange = _lastChange;
			c._lastChangeSpecified = _lastChangeSpecified;
			c._propertiesChanged = _propertiesChanged;
			c._propertiesChangedSpecified = _propertiesChangedSpecified;
			
			return c;
		}
	}
	
	public class ABContactCollection : List<ABContact>
	{
		public ABContact this [Guid id]
		{
			get
			{
				foreach (ABContact c in this)
					if (c.Id == id)
						return c;
				
				return null;
			}
			set
			{
				if (this[id] != null)
					Remove (this[id]);
				
				Add (value);
			}
		}
	}
}
