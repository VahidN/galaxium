/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot ("Group", Namespace=SoapConstants.nsAddressBook)]
	public class ABGroup
	{
		Guid _id = new Guid ();
		bool _idSpecified = false;
		GroupInfo _info = new GroupInfo ();
		bool _infoSpecified = false;
		string _propertiesChanged;
		bool _propertiesChangedSpecified = false;
		
		[XmlElement ("groupId")]
		public Guid Id
		{
			get { return _id; }
			set
			{
				_id = value;
				_idSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool IdSpecified
		{
			get { return _idSpecified; }
			set { _idSpecified = value; }
		}
		
		[XmlElement ("groupInfo")]
		public GroupInfo Info
		{
			get { return _info; }
			set
			{
				_info = value;
				_infoSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool InfoSpecified
		{
			get { return _infoSpecified; }
			set { _infoSpecified = value; }
		}
		
		[XmlElement ("propertiesChanged")]
		public string PropertiesChanged
		{
			get { return _propertiesChanged; }
			set
			{
				_propertiesChanged = value;
				_propertiesChangedSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool PropertiesChangedSpecified
		{
			get { return _propertiesChangedSpecified; }
			set { _propertiesChangedSpecified = value; }
		}
		
		public ABGroup Clone ()
		{
			ABGroup g = new ABGroup ();
			
			g._id = _id;
			g._idSpecified = _idSpecified;
			g._info = (_info != null) ? _info.Clone () : null;
			g._infoSpecified = _infoSpecified;
			g._propertiesChanged = _propertiesChanged;
			g._propertiesChangedSpecified = _propertiesChangedSpecified;
			
			return g;
		}
	}
	
	public class ABGroupCollection : List<ABGroup>
	{
		public ABGroup this [Guid id]
		{
			get
			{
				foreach (ABGroup g in this)
					if (g.Id == id)
						return g;
				
				return null;
			}
			set
			{
				if (this[id] != null)
					Remove (this[id]);
				
				Add (value);
			}
		}
	}
}
