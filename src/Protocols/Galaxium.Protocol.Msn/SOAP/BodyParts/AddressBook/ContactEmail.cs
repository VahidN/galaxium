/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

using Anculus.Core;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	public enum ContactEmailType { Unknown, ContactEmailOther, ContactEmailPersonal, Messenger2, Messenger3, ContactEmailMessenger }
	
	[XmlRoot (Namespace=SoapConstants.nsAddressBook)]
	public class ContactEmail
	{
		string _type;
		string _email;
		bool _isMessengerEnabled;
		int _capability;
		bool _messengerEnabledExternally;
		
		[XmlElement ("contactEmailType")]
		public string _Type
		{
			get { return _type; }
			set { _type = value; }
		}
		
		[XmlIgnore]
		public ContactEmailType Type
		{
			get
			{
				try
				{
					return (ContactEmailType)Enum.Parse (typeof (ContactEmailType), _type);
				}
				catch
				{
					Log.Warn ("Unknown value '{0}'", _type);
					return ContactEmailType.Unknown;
				}
			}
			set { _type = value.ToString (); }
		}
		
		[XmlElement ("email")]
		public string Email
		{
			get { return _email; }
			set { _email = value; }
		}
		
		[XmlElement ("isMessengerEnabled")]
		public bool IsMessengerEnabled
		{
			get { return _isMessengerEnabled; }
			set { _isMessengerEnabled = value; }
		}
		
		public int Capability
		{
			get { return _capability; }
			set { _capability = value; }
		}
		
		public bool MessengerEnabledExternally
		{
			get { return _messengerEnabledExternally; }
			set { _messengerEnabledExternally = value; }
		}
		
		public ContactEmail Clone ()
		{
			ContactEmail ce = new ContactEmail ();
			
			ce._capability = _capability;
			ce._email = _email;
			ce._isMessengerEnabled = _isMessengerEnabled;
			ce._messengerEnabledExternally = _messengerEnabledExternally;
			ce._type = _type;
			
			return ce;
		}
	}
	
	public class ContactEmailCollection : List<ContactEmail>
	{
		public ContactEmailCollection Clone ()
		{
			ContactEmailCollection c = new ContactEmailCollection ();
			
			foreach (ContactEmail e in this)
				c.Add (e.Clone ());
			
			return c;
		}
	}
}
