/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

using Anculus.Core;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	public enum MemberRole { Unknown, Allow, Block, Reverse, Pending, ProfileExpression, Contributor, ProfileGeneral, ProfilePersonalContact, ProfileProfessionalContact, ProfileSocial, TwoWayRelationship }
	
	[XmlRoot (Namespace=SoapConstants.nsAddressBook)]
	public class Membership
	{
		string _memberRole;
		bool _memberRoleSpecified = false;
		MemberCollection _members = null;
		bool _membersSpecified = false;
		
		[XmlElement ("MemberRole")]
		public string _MemberRole
		{
			get { return _memberRole; }
			set
			{
				_memberRole = value;
				_memberRoleSpecified = true;
			}
		}
		
		[XmlIgnore]
		public MemberRole MemberRole
		{
			get
			{
				try
				{
					return (MemberRole)Enum.Parse (typeof (MemberRole), _memberRole);
				}
				catch
				{
					Log.Warn ("Unknown value '{0}'", _memberRole);
					return MemberRole.Unknown;
				}
			}
			set { _MemberRole = value.ToString (); }
		}
		
		[XmlIgnore]
		public bool MemberRoleSpecified
		{
			get { return _memberRoleSpecified; }
			set { _memberRoleSpecified = value; }
		}
		
		[XmlArray]
		[XmlArrayItem (Type=typeof (Member))]
		public MemberCollection Members
		{
			get { return _members; }
			set
			{
				_members = value;
				_membersSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool MembersSpecified
		{
			get { return _membersSpecified; }
			set { _membersSpecified = value; }
		}
		
		public Membership Clone ()
		{
			Membership m = new Membership ();
			
			m._memberRole = _memberRole;
			m._memberRoleSpecified = _memberRoleSpecified;
			m._members = _members.Clone ();
			m._membersSpecified = _membersSpecified;
			
			return m;
		}
	}
	
	public class MembershipCollection : List<Membership>
	{
		public Membership this [MemberRole role]
		{
			get
			{
				foreach (Membership mship in this)
					if (mship.MemberRole == role)
						return mship;
				
				return null;
			}
			set
			{
				if (this[role] != null)
					Remove (this[role]);
				
				Add (value);
			}
		}
		
		public MembershipCollection Clone ()
		{
			MembershipCollection mc = new MembershipCollection ();
			
			foreach (Membership m in this)
				mc.Add (m.Clone ());
			
			return mc;
		}
	}
}
