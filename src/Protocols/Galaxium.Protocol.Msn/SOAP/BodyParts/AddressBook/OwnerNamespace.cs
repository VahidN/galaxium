/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Globalization;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot (Namespace=SoapConstants.nsAddressBook)]
	public class OwnerNamespace
	{
		string _lastChange;
		bool _lastChangeSpecified;
		
		[XmlElement ("LastChange")]
		public string LastChangeString
		{
			get { return _lastChange; }
			set
			{
				_lastChange = value;
				_lastChangeSpecified = true;
			}
		}
		
		[XmlIgnore]
		public DateTime LastChange
		{
			get { return DateTime.ParseExact (_lastChange, "yyyy-MM-ddThh:mm:ss.fffzzz", CultureInfo.InvariantCulture); }
		}
		
		[XmlIgnore]
		public bool LastChangeSpecified
		{
			get { return _lastChangeSpecified; }
			set { _lastChangeSpecified = value; }
		}
		
		public OwnerNamespace Clone ()
		{
			OwnerNamespace ns = new OwnerNamespace ();
			
			ns._lastChange = _lastChange;
			ns._lastChangeSpecified = _lastChangeSpecified;
			
			return ns;
		}
		
		public void ApplyDelta (OwnerNamespace n)
		{
			if (_lastChangeSpecified)
				n.LastChangeString = _lastChange;
		}
	}
}
