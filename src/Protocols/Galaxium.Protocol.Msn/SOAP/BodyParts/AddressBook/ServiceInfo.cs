/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot (Namespace=SoapConstants.nsAddressBook)]
	public class ServiceInfo
	{
		ServiceHandle _handle = null;
		bool _handleSpecified = false;
		bool _inverseRequired;
		bool _inverseRequiredSpecified = false;
		
		public ServiceHandle Handle
		{
			get { return _handle; }
			set
			{
				_handle = value;
				_handleSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool HandleSpecified
		{
			get { return _handleSpecified; }
			set { _handleSpecified = value; }
		}
		
		public bool InverseRequired
		{
			get { return _inverseRequired; }
			set
			{
				_inverseRequired = value;
				_inverseRequiredSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool InverseRequiredSpecified
		{
			get { return _inverseRequiredSpecified; }
			set { _inverseRequiredSpecified = value; }
		}
		
		public ServiceInfo Clone ()
		{
			ServiceInfo si = new ServiceInfo ();
			
			si._handle = _handle.Clone ();
			si._handleSpecified = _handleSpecified;
			si._inverseRequired = _inverseRequired;
			si._inverseRequiredSpecified = _inverseRequiredSpecified;
			
			return si;
		}
		
		public void ApplyDelta (ServiceInfo info)
		{
			if (_handleSpecified)
			{
				if (info._handleSpecified)
					_handle.ApplyDelta (info._handle);
				else
					info.Handle = _handle.Clone ();
			}
			if (_inverseRequiredSpecified)
				info.InverseRequired = _inverseRequired;
		}
	}
}
