/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot (Namespace=SoapConstants.nsAddressBook)]
	public class Annotation
	{
		string _name;
		string _value;
		
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}
		
		public string Value
		{
			get { return _value; }
			set { _value = value; }
		}
		
		public Annotation (string name, string val)
		{
			_name = name;
			_value = val;
		}
		
		public Annotation ()
		{
		}
		
		public Annotation Clone ()
		{
			Annotation a = new Annotation ();
			
			a._name = _name;
			a._value = _value;
			
			return a;
		}
	}
	
	[XmlRoot (Namespace=SoapConstants.nsAddressBook)]
	public class AnnotationCollection : List<Annotation>
	{
		public bool Contains (string name)
		{
			foreach (Annotation ann in this)
				if (ann.Name == name)
					return true;
			
			return false;
		}
		
		public string this [string name]
		{
			get
			{
				foreach (Annotation ann in this)
					if (ann.Name == name)
						return ann.Value;
			
				return string.Empty;
			}
			set
			{
				foreach (Annotation ann in this)
				{
					if (ann.Name == name)
					{
						ann.Value = value;
						return;
					}
				}
				
				this.Add (new Annotation (name, value));
			}
		}
		
		public AnnotationCollection Clone ()
		{
			AnnotationCollection ac = new AnnotationCollection ();
			
			foreach (Annotation a in this)
				ac.Add (a.Clone ());
			
			return ac;
		}
		
		public void ApplyDelta (AnnotationCollection ac)
		{
			foreach (Annotation a in this)
				ac[a.Name] = a.Value;
		}
	}
}
