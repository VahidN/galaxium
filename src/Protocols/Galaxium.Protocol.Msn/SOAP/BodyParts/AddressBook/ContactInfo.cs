/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

using Anculus.Core;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	public enum ContactType { Unknown, Me, Regular, Messenger, Live, LivePending, LiveDropped, LiveRejected }
	public enum ContactGender { Unspecified, Male, Female }
	
	[XmlRoot (Namespace=SoapConstants.nsAddressBook)]
	public class ContactInfo
	{
		AnnotationCollection _annotations = null;
		bool _annotationsSpecified = false;
		string _type;
		bool _typeSpecified = false;
		string _passportName;
		bool _passportNameSpecified = false;
		bool _isPassportNameHidden;
		bool _isPassportNameHiddenSpecified = false;
		bool _isSmtp;
		bool _isSmtpSpecified = false;
		long _cid;
		bool _cidSpecified = false;
		string _displayName;
		bool _displayNameSpecified = false;
		string _firstName;
		bool _firstNameSpecified = false;
		string _lastName;
		bool _lastNameSpecified = false;
		bool _hasSpace;
		bool _hasSpaceSpecified = false;
		DateTime _birthdate;
		bool _birthdateSpecified = false;
		ContactGender _gender;
		bool _genderSpecified = false;
		ContactEmailCollection _emails = null;
		bool _emailsSpecified = false;
		GuidList _groupIds = null;
		bool _groupIdsSpecified = false;
		bool _isMessengerUser;
		bool _isMessengerUserSpecified = false;
		MessengerMemberInfo _memberInfo = null;
		bool _memberInfoSpecified = false;
		
		[XmlArray ("annotations")]
		[XmlArrayItem (Type=typeof (Annotation))]
		public AnnotationCollection Annotations
		{
			get { return _annotations; }
			set
			{
				_annotations = value;
				_annotationsSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool AnnotationsSpecified
		{
			get { return _annotationsSpecified; }
			set { _annotationsSpecified = value; }
		}
		
		[XmlElement ("contactType")]
		public string _Type
		{
			get { return _type; }
			set
			{
				_type = value;
				_typeSpecified = true;
			}
		}
		
		[XmlIgnore]
		public ContactType Type
		{
			get
			{
				try
				{
					return (ContactType)Enum.Parse (typeof (ContactType), _type);
				}
				catch
				{
					Log.Warn ("Unknown value '{0}'", _type);
					return ContactType.Unknown;
				}
			}
			set { _Type = value.ToString (); }
		}
		
		[XmlIgnore]
		public bool TypeSpecified
		{
			get { return _typeSpecified; }
			set { _typeSpecified = value; }
		}
		
		[XmlElement ("passportName")]
		public string PassportName
		{
			get { return _passportName; }
			set
			{
				_passportName = value;
				_passportNameSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool PassportNameSpecified
		{
			get { return _passportNameSpecified; }
			set { _passportNameSpecified = value; }
		}
		
		public bool IsPassportNameHidden
		{
			get { return _isPassportNameHidden; }
			set
			{
				_isPassportNameHidden = value;
				_isPassportNameHiddenSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool IsPassportNameHiddenSpecified
		{
			get { return _isPassportNameHiddenSpecified; }
			set { _isPassportNameHiddenSpecified = value; }
		}
		
		public bool IsSmtp
		{
			get { return _isSmtp; }
			set
			{
				_isSmtp = value;
				_isSmtpSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool IsSmtpSpecified
		{
			get { return _isSmtpSpecified; }
			set { _isSmtpSpecified = value; }
		}
		
		public long CID
		{
			get { return _cid; }
			set
			{
				_cid = value;
				_cidSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool CIDSpecified
		{
			get { return _cidSpecified; }
			set { _cidSpecified = value; }
		}
		
		[XmlElement ("displayName")]
		public string DisplayName
		{
			get { return _displayName; }
			set
			{
				_displayName = value;
				_displayNameSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool DisplayNameSpecified
		{
			get { return _displayNameSpecified; }
			set { _displayNameSpecified = value; }
		}
		
		[XmlElement ("firstName")]
		public string FirstName
		{
			get { return _firstName; }
			set
			{
				_firstName = value;
				_firstNameSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool FirstNameSpecified
		{
			get { return _firstNameSpecified; }
			set { _firstNameSpecified = value; }
		}
		
		[XmlElement ("lastName")]
		public string LastName
		{
			get { return _lastName; }
			set
			{
				_lastName = value;
				_lastNameSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool LastNameSpecified
		{
			get { return _lastNameSpecified; }
			set { _lastNameSpecified = value; }
		}
		
		[XmlElement ("hasSpace")]
		public bool HasSpace
		{
			get { return _hasSpace; }
			set
			{
				_hasSpace = value;
				_hasSpaceSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool HasSpaceSpecified
		{
			get { return _hasSpaceSpecified; }
			set { _hasSpaceSpecified = value; }
		}
		
		[XmlElement ("birthdate")]
		public DateTime Birthdate
		{
			get { return _birthdate; }
			set
			{
				_birthdate = value;
				_birthdateSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool BirthdateSpecified
		{
			get { return _birthdateSpecified; }
			set { _birthdateSpecified = value; }
		}
		
		public ContactGender Gender
		{
			get { return _gender; }
			set
			{
				_gender = value;
				_genderSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool GenderSpecified
		{
			get { return _genderSpecified; }
			set { _genderSpecified = value; }
		}
		
		[XmlArray ("emails")]
		[XmlArrayItem (typeof (ContactEmail))]
		public ContactEmailCollection Emails
		{
			get { return _emails; }
			set
			{
				_emails = value;
				_emailsSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool EmailsSpecified
		{
			get { return _emailsSpecified; }
			set { _emailsSpecified = value; }
		}
		
		[XmlArray ("groupIds")]
		[XmlArrayItem ("guid", typeof (Guid))]
		public GuidList GroupIds
		{
			get { return _groupIds; }
			set
			{
				_groupIds = value;
				_groupIdsSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool GroupIdsSpecified
		{
			get { return _groupIdsSpecified; }
			set { _groupIdsSpecified = value; }
		}
		
		[XmlElement ("isMessengerUser")]
		public bool IsMessengerUser
		{
			get { return _isMessengerUser; }
			set
			{
				_isMessengerUser = value;
				_isMessengerUserSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool IsMessengerUserSpecified
		{
			get { return _isMessengerUserSpecified; }
			set { _isMessengerUserSpecified = value; }
		}
		
		[XmlElement ("MessengerMemberInfo")]
		public MessengerMemberInfo MemberInfo
		{
			get { return _memberInfo; }
			set
			{
				_memberInfo = value;
				_memberInfoSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool MemberInfoSpecified
		{
			get { return _memberInfoSpecified; }
			set { _memberInfoSpecified = value; }
		}
		
		public void AddAnnotation (string name, string val)
		{
			if (_annotations == null)
				Annotations = new AnnotationCollection ();
			
			_annotations.Add (new Annotation (name, val));
		}
		
		public ContactInfo Clone ()
		{
			ContactInfo c = new ContactInfo ();
			
			c._annotations = (_annotations != null) ? _annotations.Clone () : null;
			c._annotationsSpecified = _annotationsSpecified;
			c._birthdate = _birthdate;
			c._birthdateSpecified = _birthdateSpecified;
			c._cid = _cid;
			c._cidSpecified = _cidSpecified;
			c._displayName = _displayName;
			c._displayNameSpecified = _displayNameSpecified;
			c._emails = (_emails != null) ? _emails.Clone () : null;
			c._emailsSpecified = _emailsSpecified;
			c._firstName = _firstName;
			c._firstNameSpecified = _firstNameSpecified;
			c._gender = _gender;
			c._genderSpecified = _genderSpecified;
			c._groupIds = (_groupIds != null) ? _groupIds.Clone () : null;
			c._groupIdsSpecified = _groupIdsSpecified;
			c._hasSpace = _hasSpace;
			c._hasSpaceSpecified = _hasSpaceSpecified;
			c._isMessengerUser = _isMessengerUser;
			c._isMessengerUserSpecified = _isMessengerUserSpecified;
			c._isPassportNameHidden = _isPassportNameHidden;
			c._isPassportNameHiddenSpecified = _isPassportNameHiddenSpecified;
			c._isSmtp = _isSmtp;
			c._isSmtpSpecified = _isSmtpSpecified;
			c._lastName = _lastName;
			c._lastNameSpecified = _lastNameSpecified;
			c._memberInfo = (_memberInfo != null) ? _memberInfo.Clone () : null;
			c._memberInfoSpecified = _memberInfoSpecified;
			c._passportName = _passportName;
			c._passportNameSpecified = _passportNameSpecified;
			c._type = _type;
			c._typeSpecified = _typeSpecified;
			
			return c;
		}
	}
	
	public class GuidList : List<Guid>
	{
		public GuidList Clone ()
		{
			GuidList l = new GuidList ();
			
			foreach (Guid g in this)
				l.Add (new Guid (g.ToByteArray ()));
			
			return l;
		}
	}
}
