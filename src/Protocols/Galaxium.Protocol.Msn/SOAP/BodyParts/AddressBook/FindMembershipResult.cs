/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot (Namespace=SoapConstants.nsAddressBook)]
	public class FindMembershipResult
	{
		ServiceCollection _services = null;
		bool _servicesSpecified = false;
		OwnerNamespace _ownerNamespace = null;
		bool _ownerNamespaceSpecified = false;
		
		[XmlArray]
		[XmlArrayItem (Type=typeof (Service))]
		public ServiceCollection Services
		{
			get { return _services; }
			set
			{
				_services = value;
				_servicesSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool ServicesSpecified
		{
			get { return _servicesSpecified; }
			set { _servicesSpecified = true; }
		}
		
		public OwnerNamespace OwnerNamespace
		{
			get { return _ownerNamespace; }
			set
			{
				_ownerNamespace = value;
				_ownerNamespaceSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool OwnerNamespaceSpecified
		{
			get { return _ownerNamespaceSpecified; }
			set { _ownerNamespaceSpecified = value; }
		}
	}
	
	public class FindMembershipResultCollection : List<FindMembershipResult>
	{
	}
}
