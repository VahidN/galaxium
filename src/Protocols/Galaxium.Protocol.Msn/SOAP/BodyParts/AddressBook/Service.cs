/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Serialization;

namespace Galaxium.Protocol.Msn.Soap.BodyParts
{
	[XmlRoot (Namespace=SoapConstants.nsAddressBook)]
	public class Service
	{
		MembershipCollection _memberships = null;
		bool _membershipsSpecified = false;
		ServiceInfo _info = null;
		bool _infoSpecified = false;
		string _lastChange;
		bool _lastChangeSpecified = false;
		
		[XmlArray]
		[XmlArrayItem (Type=typeof (Membership))]
		public MembershipCollection Memberships
		{
			get { return _memberships; }
			set
			{
				_memberships = value;
				_membershipsSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool MembershipsSpecified
		{
			get { return _membershipsSpecified; }
			set { _membershipsSpecified = value; }
		}
		
		public ServiceInfo Info
		{
			get { return _info; }
			set
			{
				_info = value;
				_infoSpecified = true;
			}
		}
		
		[XmlIgnore]
		public bool InfoSpecified
		{
			get { return _infoSpecified; }
			set { _infoSpecified = value; }
		}
		
		[XmlElement ("LastChange")]
		public string LastChangeString
		{
			get { return _lastChange; }
			set
			{
				_lastChange = value;
				_lastChangeSpecified = true;
			}
		}
		
		[XmlIgnore]
		public DateTime LastChange
		{
			get { return DateTime.ParseExact (_lastChange, "yyyy-MM-ddThh:mm:ss.fffzzz", CultureInfo.InvariantCulture); }
		}
		
		[XmlIgnore]
		public bool LastChangeSpecified
		{
			get { return _lastChangeSpecified; }
			set { _lastChangeSpecified = value; }
		}
		
		public Service Clone ()
		{
			Service s = new Service ();
			
			s._info = (_info != null) ? _info.Clone () : null;
			s._infoSpecified = _infoSpecified;
			s._lastChange = _lastChange;
			s._lastChangeSpecified = _lastChangeSpecified;
			s._memberships = (_memberships != null) ? _memberships.Clone () : null;
			s._membershipsSpecified = _membershipsSpecified;
			
			return s;
		}
	}
	
	public class ServiceCollection : List<Service>
	{
		public Service this [string id]
		{
			get
			{
				foreach (Service service in this)
				{
					if ((service.Info != null) && (service.Info.Handle != null) && (service.Info.Handle.Type.Name == id))
						return service;
				}
				
				return null;
			}
		}
	}
}
