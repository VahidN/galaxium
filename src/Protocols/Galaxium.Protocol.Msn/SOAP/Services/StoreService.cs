/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Web.Services;
using System.Web.Services.Protocols;

using Galaxium.Protocol.Msn.Soap.BodyParts;
using Galaxium.Protocol.Msn.Soap.Headers;

namespace Galaxium.Protocol.Msn.Soap
{
	[WebServiceBinding (Name="soap:Envelope", Namespace="http://schemas.xmlsoap.org/soap/envelope/")]
	public class StoreService : MsnSoapService
	{
		public StorageApplicationHeader storageAppHeader;
		public StorageUserHeader storageUserHeader;
		
		public StoreService (MsnSession session)
			: base (session)
		{
			storageAppHeader = new StorageApplicationHeader ();
			storageUserHeader = new StorageUserHeader (session);
			
			AddUrl ("https://storage.msn.com/storageservice/schematizedstore.asmx");
		}
		
		[SoapDocumentMethod ("http://www.msn.com/webservices/storage/w10/GetProfile", RequestNamespace=SoapConstants.nsStorage, ResponseNamespace=SoapConstants.nsStorage)]
		[SoapHeader ("storageAppHeader"), SoapHeader ("storageUserHeader")]
		[RequireSecurityTokens (SecurityToken.Storage)]
		public GetProfileResult GetProfile (ProfileHandle profileHandle, ProfileAttributes profileAttributes)
		{
			object[] results = this.Invoke ("GetProfile", new object[] { profileHandle, profileAttributes });
			return (GetProfileResult)results[0];
		}
		
		public IAsyncResult BeginGetProfile (ProfileHandle profileHandle, ProfileAttributes profileAttributes, AsyncCallback callback, object asyncState)
		{
			return this.BeginInvoke ("GetProfile", new object[] { profileHandle, profileAttributes }, callback, asyncState);
		}
		
		public GetProfileResult EndGetProfile (IAsyncResult asyncResult)
		{
			object[] results = this.EndInvoke (asyncResult);
			return (GetProfileResult)results[0];
		}
		
		[SoapDocumentMethod ("http://www.msn.com/webservices/storage/w10/UpdateProfile", RequestNamespace=SoapConstants.nsStorage, ResponseNamespace=SoapConstants.nsStorage)]
		[SoapHeader ("storageAppHeader"), SoapHeader ("storageUserHeader")]
		[RequireSecurityTokens (SecurityToken.Storage)]
		public void UpdateProfile (Profile profile)
		{
			this.Invoke ("UpdateProfile", new object[] { profile });
		}
		
		public IAsyncResult BeginUpdateProfile (Profile profile, AsyncCallback callback, object asyncState)
		{
			return this.BeginInvoke ("UpdateProfile", new object[] { profile }, callback, asyncState);
		}
		
		public void EndUpdateProfile (IAsyncResult asyncResult)
		{
			this.EndInvoke (asyncResult);
		}
		
		[SoapDocumentMethod ("http://www.msn.com/webservices/storage/w10/DeleteRelationships", RequestNamespace=SoapConstants.nsStorage, ResponseNamespace=SoapConstants.nsStorage)]
		[SoapHeader ("storageAppHeader"), SoapHeader ("storageUserHeader")]
		[RequireSecurityTokens (SecurityToken.Storage)]
		public void DeleteRelationships (ProfileHandle sourceHandle, ObjectHandle[] targetHandles)
		{
			this.Invoke ("DeleteRelationships", new object[] { sourceHandle, targetHandles });
		}
		
		public IAsyncResult BeginDeleteRelationships (ProfileHandle sourceHandle, ObjectHandle[] targetHandles, AsyncCallback callback, object asyncState)
		{
			return this.BeginInvoke ("DeleteRelationships", new object[] { sourceHandle, targetHandles }, callback, asyncState);
		}
		
		public void EndDeleteRelationships (IAsyncResult asyncResult)
		{
			this.EndInvoke (asyncResult);
		}
		
		[SoapDocumentMethod ("http://www.msn.com/webservices/storage/w10/CreateRelationships", RequestNamespace=SoapConstants.nsStorage, ResponseNamespace=SoapConstants.nsStorage)]
		[SoapHeader ("storageAppHeader"), SoapHeader ("storageUserHeader")]
		[RequireSecurityTokens (SecurityToken.Storage)]
		public void CreateRelationships (Relationship[] relationships)
		{
			this.Invoke ("CreateRelationships", new object[] { relationships });
		}
		
		public IAsyncResult BeginCreateRelationships (Relationship[] relationships, AsyncCallback callback, object asyncState)
		{
			return this.BeginInvoke ("CreateRelationships", new object[] { relationships }, callback, asyncState);
		}
		
		public void EndCreateRelationships (IAsyncResult asyncResult)
		{
			this.EndInvoke (asyncResult);
		}
		
		[SoapDocumentMethod ("http://www.msn.com/webservices/storage/w10/CreateDocument", RequestNamespace=SoapConstants.nsStorage, ResponseNamespace=SoapConstants.nsStorage)]
		[SoapHeader ("storageAppHeader"), SoapHeader ("storageUserHeader")]
		[RequireSecurityTokens (SecurityToken.Storage)]
		public string CreateDocument (ProfileHandle parentHandle, Document document, string relationshipName)
		{
			object[] results = this.Invoke ("CreateDocument", new object[] { parentHandle, document, relationshipName });
			return (string)results[0];
		}
		
		public IAsyncResult BeginCreateDocument (ProfileHandle parentHandle, Document document, string relationshipName, AsyncCallback callback, object asyncState)
		{
			return this.BeginInvoke ("CreateDocument", new object[] { parentHandle, document, relationshipName }, callback, asyncState);
		}
		
		public string EndCreateDocument (IAsyncResult asyncResult)
		{
			object[] results = this.EndInvoke (asyncResult);
			return (string)results[0];
		}
	}
}
