/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Net;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Anculus.Core;

using Galaxium.Protocol.Msn.Soap.BodyParts;
using Galaxium.Protocol.Msn.Soap.Headers;

namespace Galaxium.Protocol.Msn.Soap
{
	[WebServiceBinding (Name="soap:Envelope", Namespace="http://schemas.xmlsoap.org/soap/envelope/")]
	public class PassportService : MsnSoapService
	{
		public PassportAuthInfoHeader authInfoHeader;
		public WSSecurityHeader securityHeader;
		
		public PassportService (MsnSession session)
			: base (session)
		{
			authInfoHeader = new PassportAuthInfoHeader ();
			securityHeader = new WSSecurityHeader (session);
			
			AddUrl ("https://login.live.com/RST.srf");
			ForceContentType = true;
		}
		
		protected override bool HandleFault (SoapException ex)
		{
			if ((ex.Code.Namespace == SoapConstants.nsPassportFault) && (ex.Code.Name == "Redirect"))
			{
				Url = PassportFaultFix._redirectUrl;
				
				Log.Info ("Caught redirect fault, switching url to {0}", Url);
				
				// We handled the fault, return true to retry the invoke
				return true;
			}
			
			return base.HandleFault (ex);
		}
		
		[SoapDocumentMethod (RequestNamespace=SoapConstants.nsPassport, ParameterStyle=SoapParameterStyle.Bare)]
		[SoapHeader ("authInfoHeader"), SoapHeader ("securityHeader")]
		[return: XmlArray ("RequestSecurityTokenResponseCollection", Namespace=SoapConstants.nsWst)]
		[return: XmlArrayItem ("RequestSecurityTokenResponse", typeof (RequestSecurityTokenResponse))]
		[PassportFaultFix]
		public RequestSecurityTokenResponseCollection RequestMultipleSecurityTokens ([XmlElement ("RequestMultipleSecurityTokens")] RequestMultipleSecurityTokens requests)
		{
			object[] results = this.Invoke ("RequestMultipleSecurityTokens", new object[] { requests });
			return (RequestSecurityTokenResponseCollection)results[0];
		}
		
		public IAsyncResult BeginRequestMultipleSecurityTokens ([XmlElement ("RequestMultipleSecurityTokens")] RequestMultipleSecurityTokens requests, AsyncCallback callback, object asyncState)
		{
			return this.BeginInvoke ("RequestMultipleSecurityTokens", new object[] { requests }, callback, asyncState);
		}
		
		public RequestSecurityTokenResponseCollection EndRequestMultipleSecurityTokens (IAsyncResult asyncResult)
		{
			object[] results = this.EndInvoke (asyncResult);
            return (RequestSecurityTokenResponseCollection)results[0];
		}
	}
}
