/*
 * Galaxium Messenger
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 *
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gdk;
using Gtk;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Protocol;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public partial class IrcContactListView : ContactListView
	{
		public IrcContactListView (IConversation conversation)
			: base (conversation)
		{
			IrcConversation ircConv = conversation as IrcConversation;
			
			ircConv.ChannelContactJoin += ConversationChannelContactJoin; 
			//don't add an event for initial join, since join will be emitted as well
			ircConv.ChannelContactPart += ConversationChannelContactPart;
			
			ircConv.ChannelContactQuit += ConversationChannelContactQuit;
			
			ircConv.ChannelContactKill += ConversationChannelContactKillKick;
			ircConv.ChannelContactKick += ConversationChannelContactKillKick;
			
			ircConv.ChannelContactModeChanged += ConversationChannelContactModeChanged;
		}

		void ConversationChannelContactJoin (object sender, ChannelContactEventArgs args)
		{
			
		}
		
		void ConversationChannelContactPart (object sender, ChannelContactEventArgs args)
		{
			
		}
		
		void ConversationChannelContactQuit (object sender, ChannelContactMessageEventArgs args)
		{
			Log.Debug ("Quit contact from chat tree: "+args.Contact.DisplayName);
			//RemoveContact (args.Contact);
		}
		
		void ConversationChannelContactKillKick (object sender, ChannelContactActionEventArgs args)
		{
			Log.Debug ("Kick contact from chat tree: "+args.Contact.DisplayName);
			//RemoveContact (args.Contact);
		}
		
		void ConversationChannelContactModeChanged (object sender, ChannelContactModeEventArgs args)
		{
			UpdateContact (args.Contact);
		}
	}
}
