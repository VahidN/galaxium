/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Client;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public abstract class ChannelContactListCommand : AbstractMenuCommand
	{
		protected virtual bool IsSensitive ()
		{
			return AccountHasModes (ChannelModeFlags.Operator);
		}
		
		protected bool AccountHasModes (ChannelModeFlags flags)
		{
			ContactListContext context = Object as ContactListContext;
			IrcContact contact = context.Value as IrcContact;
			if (contact == null)
				return false;
			
			IrcChannel channel = contact.Conversation.PrimaryContact as IrcChannel;
			IrcContact actor = channel.ContactCollection.GetContactByName (contact.Session.Account.DisplayName) as IrcContact;
			
			if (actor != null && channel != null)
			{
				if ((actor.ChannelUserMode & flags) == flags)
					return true;
			}
			
			return false;
		}
		
		protected bool HasChannelUserModes (ChannelModeFlags flags)
		{
			ContactListContext context = Object as ContactListContext;
			IrcContact contact = context.Value as IrcContact;
			if (contact == null)
				return false;
			
			if (contact != null)
			{
				if ((contact.ChannelUserMode & flags) == flags)
					return true;
			}
			
			return false;
		}
		
		protected void ChangeUserChannelMode (bool grant, ChannelModeFlags flags)
		{
			ContactListContext context = Object as ContactListContext;
			IrcContact contact = context.Value as IrcContact;
			IrcConversation conv = contact.Conversation;
			IrcSession session = contact.Session as IrcSession;
			
			session.ChangeUserChannelMode (conv.PrimaryContact.DisplayName, contact.DisplayName, grant, flags);
		}
		
		protected void Kick ()
		{
			ContactListContext context = Object as ContactListContext;
			IrcContact contact = context.Value as IrcContact;
			IrcConversation conv = contact.Conversation;
			IrcSession session = contact.Session as IrcSession;
			
			session.KickUser (conv.PrimaryContact.DisplayName, contact.DisplayName);
		}
		
		protected void Ban ()
		{
			ContactListContext context = Object as ContactListContext;
			IrcContact contact = context.Value as IrcContact;
			IrcConversation conv = contact.Conversation;
			IrcSession session = contact.Session as IrcSession;
			
			session.BanUserFromChannel (conv.PrimaryContact.DisplayName, contact);
		}
	}
	
	public class ChannelContactListStartConversationCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			ContactListContext context = Object as ContactListContext;
			IrcContact contact = context.Value as IrcContact;
			IrcSession session = contact.Session as IrcSession;
			
			var conversation = session.ObtainConversation (contact, true);
			WindowUtility<Widget>.Activate(conversation, true);
		}
	}
	
	public class AddContactToListCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			ContactListContext context = Object as ContactListContext;
			IrcContact contact = context.Value as IrcContact;
			IrcSession session = contact.Session as IrcSession;
			
			(MenuItem as MenuItem).Sensitive = !session.ContactCollection.ContactExists (contact.DisplayName);
		}
		
		public override void Run ()
		{
			ContactListContext context = Object as ContactListContext;
			IrcContact contact = context.Value as IrcContact;
			IrcSession session = contact.Session as IrcSession;
			
			if (!session.ContactCollection.ContactExists (contact.DisplayName))
			{
				session.AddContact (contact.DisplayName, contact.Ident);
			}
		}
	}
	
	public class ChannelContactListOperatorCommand : ChannelContactListCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as CheckMenuItem).Sensitive = IsSensitive ();
			(MenuItem as CheckMenuItem).Active = HasChannelUserModes(ChannelModeFlags.Operator);
		}
		
		public override void Run ()
		{
			ChangeUserChannelMode (!HasChannelUserModes (ChannelModeFlags.Operator), ChannelModeFlags.Operator);
		}
	}
	
	public class ChannelContactListVoiceCommand : ChannelContactListCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as CheckMenuItem).Sensitive = IsSensitive ();
			(MenuItem as CheckMenuItem).Active = HasChannelUserModes(ChannelModeFlags.Voice);
		}
		
		public override void Run ()
		{
			ChangeUserChannelMode (!HasChannelUserModes (ChannelModeFlags.Voice), ChannelModeFlags.Voice);
		}
	}
	
	public class ChannelContactListIgnoreCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as MenuItem).Sensitive = false; //disabled for now
		}
		
		public override void Run ()
		{
			//TODO:
		}
	}
	
	public class ChannelContactListKickCommand : ChannelContactListCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as MenuItem).Sensitive = IsSensitive ();
		}
		
		public override void Run ()
		{
			Kick ();
		}
	}
	
	public class ChannelContactListBanCommand : ChannelContactListCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as MenuItem).Sensitive = IsSensitive ();
		}
		
		public override void Run ()
		{
			Ban ();
		}
	}
	
	public class ChannelContactListKickbanCommand : ChannelContactListCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as MenuItem).Sensitive = IsSensitive ();
		}
		
		public override void Run ()
		{
			Kick ();
			Ban ();
		}
	}
	
	public class ChannelContactListUnignoreCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as MenuItem).Sensitive = false; //disabled for now
		}
		
		public override void Run ()
		{
			//TODO:
		}
	}
	
	public class ChannelContactListUnbanCommand : ChannelContactListCommand
	{
		public override void SetMenuItem ()
		{
			(MenuItem as MenuItem).Sensitive = IsSensitive ();
		}
		
		public override void Run ()
		{
			//TODO:
		}
	}
	
	public class ChannelContactListSendFileCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			ContactListContext context = Object as ContactListContext;
			IrcContact contact = context.Value as IrcContact;
			IrcSession session = contact.Session as IrcSession;
			
			if (contact != null && contact.DisplayName != session.Account.DisplayName)
			{
				SendFileDialog dialog = new SendFileDialog (contact);
				
				// Here we would fill the dialog with the options we intend to use to send the file.
				
				ResponseType response = (ResponseType) dialog.Run ();
				
				if (response == ResponseType.Apply)
				{
					if (dialog.Filename != null && dialog.Filename.Length > 0)
					{
						(contact.Session as IrcSession).SendFile (contact, dialog.Filename);
					}
				}
				
				dialog.Destroy();
			}
		}
	}
}