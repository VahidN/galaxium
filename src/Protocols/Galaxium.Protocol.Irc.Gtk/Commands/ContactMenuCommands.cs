/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Client;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public class ContactTreeContactDetailsCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			
		}
	}
	
	public class ContactTreeChangeIdentityCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcContact contact = context.Value as IrcContact;
			//IrcAccount account = contact.Session.Account as IrcAccount;
			
			ChangeIdentityDialog dialog = new ChangeIdentityDialog (contact.Session as IrcSession, contact);
			ResponseType response = (ResponseType)dialog.Run ();
			
			if (response == ResponseType.Apply)
			{
				contact.Ident = dialog.Identity;
				contact.Save ();
			}
			else if (response == ResponseType.No)
			{
				contact.Ident = String.Empty;
				contact.Save ();
			}
			
			dialog.Destroy();
		}
	}
	
	public class ContactTreeRemoveContactCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcContact contact = context.Value as IrcContact;
			//IrcAccount account = contact.Session.Account as IrcAccount;
			IrcSession session = contact.Session as IrcSession;
			
			session.RemoveContact (contact.DisplayName);
		}
	}
	
	public class ContactTreeStartConversationCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcContact contact = context.Value as IrcContact;
			IrcSession session = contact.Session as IrcSession;
			
			var conversation = session.ObtainConversation (contact, true);
			WindowUtility<Widget>.Activate(conversation, true);
		}
	}
	
	public class ContactTreeSendFileCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			ContactTreeContext context = Object as ContactTreeContext;
			IrcContact contact = context.Value as IrcContact;
			IrcSession session = contact.Session as IrcSession;
			
			if (contact != null && contact.DisplayName != session.Account.DisplayName)
			{
				SendFileDialog dialog = new SendFileDialog (contact);
				
				// Here we would fill the dialog with the options we intend to use to send the file.
				
				ResponseType response = (ResponseType) dialog.Run ();
				
				if (response == ResponseType.Apply)
				{
					if (dialog.Filename != null && dialog.Filename.Length > 0)
					{
						(contact.Session as IrcSession).SendFile (contact, dialog.Filename);
					}
				}
				
				dialog.Destroy();
			}
		}
	}
	
	public class IgnoreContactCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			
		}
	}
	
	public class AutoAcceptCommand : AbstractMenuCommand
	{
		public override void SetMenuItem ()
		{
			
		}
		
		public override void Run ()
		{
			
		}
	}
}