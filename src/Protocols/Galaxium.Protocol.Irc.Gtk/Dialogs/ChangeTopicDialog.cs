/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2009 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public sealed class ChangeTopicDialog
	{
		[Widget ("ChangeTopicDialog")]
		private Dialog _dialog;
		[Widget ("WindowImage")]
		private Image _imgDialog;
		[Widget ("TopicEntry")]
		private Entry _topicEntry;
		[Widget ("ApplyButton")]
		private Button _applyButton;

		public ChangeTopicDialog(IrcSession session, IrcChannel channel)
		{
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (ChangeTopicDialog).Assembly, "SetTopicDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-rename-channel", IconSizes.Large);
			_topicEntry.Changed += new EventHandler (DataChangedEvent);
			if (String.IsNullOrEmpty(channel.Topic))
				_topicEntry.Text = String.Empty;
			else
				_topicEntry.Text = channel.Topic;
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-set", IconSizes.Small);
			
			_applyButton.Sensitive = CheckInput();
			
			_dialog.ShowAll();
		}
		
		public string Topic
		{
			get { return _topicEntry.Text; }
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
		
		private void DataChangedEvent (object sender, EventArgs args)
		{
			_applyButton.Sensitive = CheckInput();
		}
		
		private bool CheckInput()
		{
			if (_topicEntry.Text.Length == 0)
				return false;
			else
				return true;
		}
	}
}