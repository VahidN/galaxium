/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Core;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public sealed class BrowseChannelsDialog
	{
		[Widget ("BrowseChannelsDialog")]
		private Dialog _dialog;
		[Widget ("imgDialog")]
		private Image _imgDialog;
		[Widget ("channelsWindow")]
		private ScrolledWindow _channelsWindow;
		
		[Widget ("entryChannel")]
		private Entry _entryChannel;
		[Widget ("entryText")]
		private Entry _entryText;
		[Widget ("entryKey")]
		private Entry _entryKey;
		
		[Widget ("checkName")]
		private CheckButton _checkName;
		[Widget ("checkTopic")]
		private CheckButton _checkTopic;
		[Widget ("checkAuto")]
		private CheckButton _checkAuto;
		[Widget ("checkPersistent")]
		private CheckButton _checkPersistent;
		
		[Widget ("spinFrom")]
		private SpinButton _spinFrom;
		[Widget ("spinTo")]
		private SpinButton _spinTo;
		
		[Widget ("btnAdd")]
		private Button _btnAdd;
		[Widget ("btnRefresh")]
		private Button _btnRefresh;
		
		[Widget ("searchLabel")]
		private Label _searchLabel;
		
		private TreeView _channelTree;
		private ListStore _channelStore;
		
		private IrcSession _session;
		
		public BrowseChannelsDialog(IrcSession session)
		{
			_session = session;
			
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (BrowseChannelsDialog).Assembly, "BrowseChannelsDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-browse-channels", IconSizes.Large);
			_entryChannel.Changed += new EventHandler (DataChangedEvent);
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-find", IconSizes.Small);
			
			_channelStore = new ListStore (typeof (String), typeof (String), typeof (String));
			_channelTree = new TreeView (_channelStore);
			_channelTree.HeadersClickable = false;
			_channelTree.HeadersVisible = true;
			
			TreeViewColumn nameColumn = new TreeViewColumn ("Name", new CellRendererText (), "text", 0);
			TreeViewColumn visibleColumn = new TreeViewColumn ("Users", new CellRendererText (), "text", 1);
			TreeViewColumn topicColumn = new TreeViewColumn ("Topic", new CellRendererText(), "text", 2);
			
			_channelTree.AppendColumn (nameColumn);
			_channelTree.AppendColumn (visibleColumn);
			_channelTree.AppendColumn (topicColumn);
			_channelTree.Selection.Changed += ChannelSelected;
			
			_channelsWindow.Add (_channelTree);
			
			session.ChannelListReceived += ChannelsReceived;
			if (session.ChannelList == null || session.ChannelList.Count < 1)
				session.ListChannels (null, null);
			else
			{
				ThreadUtility.Dispatch (() => {
				foreach (ChannelListEntry entry in _session.ChannelList)
					_channelStore.AppendValues (entry.Name, entry.Visible.ToString(), entry.Topic);
				});
			}
			
			_btnAdd.Sensitive = CheckInput();

			_dialog.ShowAll();
		}
		
		public string Channel
		{
			get { return _entryChannel.Text; }
		}
		
		public string Key
		{
			get { return _entryKey.Text; }
		}
		
		public bool AutoJoin
		{
			get { return _checkAuto.Active; }
		}
		
		public bool Persistent
		{
			get { return _checkPersistent.Active; }
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
		
		public void Refresh ()
		{
			_session.ListChannels (null, null);
		}
		
		private void ChannelSelected (object sender, EventArgs args)
		{
			TreeIter iter;
			if (!_channelTree.Selection.GetSelected (out iter))
				return;
			
			_entryChannel.Text = _channelTree.Model.GetValue (iter, (int)0) as String;
			
		}
		
		private void ChannelsReceived (object sender, EventArgs args)
		{
			ThreadUtility.Dispatch (() => {
				_channelStore.Clear();
				
				foreach (ChannelListEntry entry in _session.ChannelList)
					_channelStore.AppendValues (entry.Name, entry.Visible.ToString(), entry.Topic);
				
				_searchLabel.Markup = String.Format("<b>Search Channels:</b> {0}/{1}", _session.ChannelList.Count, _session.ChannelList.Count);
			});
		}
		
		private void DataChangedEvent (object sender, EventArgs args)
		{
			_btnAdd.Sensitive = CheckInput();
		}
		
		private bool CheckInput()
		{
			if (_entryChannel.Text.Length == 0)
				return false;
			else
				return true;
		}
	}
}