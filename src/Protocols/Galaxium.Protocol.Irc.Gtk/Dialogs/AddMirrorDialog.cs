/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public sealed class AddMirrorDialog
	{
		[Widget ("AddMirrorDialog")]
		private Dialog _dialog;
		[Widget ("imgDialog")]
		private Image _imgDialog;
		[Widget ("entryHost")]
		private Entry _entryHost;
		[Widget ("entryPort")]
		private Entry _entryPort;
		[Widget ("btnAdd")]
		private Button _btnAdd;
		
		public AddMirrorDialog ()
		{
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (AddMirrorDialog).Assembly, "AddMirrorDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			_entryHost.Changed += new EventHandler (DataChangedEvent);
			_entryPort.Changed += new EventHandler (DataChangedEvent);
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-add", IconSizes.Small);
			_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-server", IconSizes.Large);
			
			_btnAdd.Sensitive = CheckInput();

			_dialog.ShowAll();
		}
		
		public string Host
		{
			get { return _entryHost.Text; }
		}
		
		public string Port
		{
			get { return _entryPort.Text; }
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
		
		private void DataChangedEvent (object sender, EventArgs args)
		{
			try
			{
				Int32.Parse (_entryPort.Text);
			}
			catch
			{
				_entryPort.Text = "";
			}
			
			_btnAdd.Sensitive = CheckInput();
		}
		
		private bool CheckInput()
		{
			if (_entryHost.Text.Length == 0 || _entryPort.Text.Length == 0)
				return false;
			else
				return true;
		}
	}
}