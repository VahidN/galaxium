/*
 * Galaxium Messenger
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using System.Collections.Generic;

using Gtk;
using Gdk;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public class IrcChannelContactTreeManager : ListContactTreeManager
	{	
		public override void RenderText (object data, CellRendererContact renderer)
		{
			if (data is IrcContact) {
				IrcContact contact = data as IrcContact;
				
				renderer.ShowEmoticons = false;
				renderer.Markup = contact.DisplayIdentifier;
			} else
				base.RenderText (data, renderer);
		}
		
		public override void RenderLeftImage (object data, CellRendererPixbuf renderer)
		{
			if (!(data is IrcContact))
			{
				base.RenderLeftImage (data, renderer);
				return;
			}
			
			IrcContact contact = data as IrcContact;
			Pixbuf pixbuf = null;
			
			switch (contact.ChannelUserMode)
			{
				case ChannelModeFlags.ChannelCreator:
					pixbuf = IconUtility.GetIcon ("irc-channelcreator", IconSizes.Small);
					break;
				
				case ChannelModeFlags.HalfOperator:
					pixbuf = IconUtility.GetIcon ("irc-halfoperator", IconSizes.Small);
					break;
				
				case ChannelModeFlags.Operator:
					pixbuf = IconUtility.GetIcon ("irc-operator", IconSizes.Small);
					break;
				
				case ChannelModeFlags.Voice:
					pixbuf = IconUtility.GetIcon ("irc-voice", IconSizes.Small);
					break;
				
				default:
					pixbuf = IconUtility.GetIcon ("irc-default", IconSizes.Small);
					break;
			}

			renderer.Pixbuf = pixbuf;
			renderer.Width = pixbuf.Width;
			renderer.Visible = true;
		}
		
		public override int Compare (object data1, object data2)
		{
			IrcContact contact1 = data1 as IrcContact;
			IrcContact contact2 = data2 as IrcContact;
			
			if (contact1 != null && contact2 != null)
			{
				int comp = GetChannelModeValue (contact1.ChannelUserMode).CompareTo (GetChannelModeValue (contact2.ChannelUserMode));
				
				if (comp == 0)
					comp = contact1.DisplayIdentifier.CompareTo (contact2.DisplayIdentifier);
				
				return comp;
			}
			
			return base.Compare (data1, data2);
		}
		
		public override string GetMenuExtensionPoint (object data)
		{
			if (data is IContact)
				return "/Galaxium/Gui/IRC/ChannelContactList/ContextMenu/Contact";
			
			return base.GetMenuExtensionPoint (data);
		}
		
		private int GetChannelModeValue (ChannelModeFlags modeFlags)
		{
			switch (modeFlags)
			{
				case ChannelModeFlags.ChannelCreator:
					return 0;
				case ChannelModeFlags.Operator:
					return 1;
				case ChannelModeFlags.HalfOperator:
					return 2;
				case ChannelModeFlags.Voice:
					return 3;
				default:
					return 4;
			}
		}
	}
}
