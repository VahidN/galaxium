/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using GLib;
using Gdk;
using Gtk;

using Galaxium.Gui.GtkGui;

namespace Galaxium.Protocol.Irc.GtkGui
{
	public class ContactListTooltip : InfoTooltip
	{
		IrcContact _contact;
		
		public IrcContact ListItem { get { return _contact; } }
		
		public ContactListTooltip(IrcContact contact) : base ()
		{
			_contact = contact;
			
			Gtk.HBox hbox = new Gtk.HBox();
			Gtk.VBox vbox = new Gtk.VBox();
			Gtk.Image image = new Gtk.Image();
			
			Pixbuf pixbuf = null;
			
			if (contact.DisplayImage != null)
			{
				if (!string.IsNullOrEmpty(contact.DisplayImage.Filename))
					pixbuf = PixbufUtility.GetFramedPixbuf(new Pixbuf(contact.DisplayImage.Filename), PixbufRendererFrameSize.Huge);
				else if ((contact.DisplayImage.ImageBuffer != null) && (contact.DisplayImage.ImageBuffer.Length > 0))
				{
					try
					{
						pixbuf = PixbufUtility.GetFramedPixbuf(new Pixbuf(contact.DisplayImage.ImageBuffer), PixbufRendererFrameSize.Huge);
					}
					catch
					{
						
					}
				}
			}
			
			if (pixbuf == null)
				pixbuf = PixbufUtility.GetFramedPixbuf(IconUtility.GetIcon("galaxium-displayimage"), PixbufRendererFrameSize.Huge);
			
			image.Pixbuf = pixbuf;
			
			Gtk.Label nameLabel = new Gtk.Label(String.Format("<span size='large'><b>{0}</b></span>", Markup.EscapeText(_contact.UniqueIdentifier)));
			nameLabel.UseMarkup = true;
			nameLabel.Xalign = 0.0f;
			nameLabel.Yalign = 0.0f;
			nameLabel.UseUnderline = false;
			
			string personalMessage = "No message";
			
			if (!string.IsNullOrEmpty(_contact.DisplayMessage))
				personalMessage = Markup.EscapeText(_contact.DisplayMessage);
			
			Gtk.Label personalLabel = new Gtk.Label("<span size='small'>Comment: <i>"+personalMessage+"</i></span>");
			personalLabel.Xalign = 0.0f;
			personalLabel.Yalign = 0.0f;
			personalLabel.UseMarkup = true;
			personalLabel.Wrap = true;
			personalLabel.WidthRequest = 250;
			
			string nickname = "None";
			
			if (_contact.HasNickname)
				nickname = Markup.EscapeText(_contact.Nickname);
			
			Gtk.Label oAliasLabel = new Gtk.Label("<span size='small'>Nickname: <i>"+nickname+"</i></span>");
			oAliasLabel.UseMarkup = true;
			oAliasLabel.Xalign = 0.0f;
			oAliasLabel.Yalign = 0.0f;
			
			string sDisplay = "None";
			
			if (!string.IsNullOrEmpty(_contact.DisplayName))
				sDisplay = Markup.EscapeText(_contact.DisplayName);
			
			Gtk.Label oDisplayLabel = new Gtk.Label("<span size='small'>Display: <i>"+sDisplay+"</i></span>");
			oDisplayLabel.Ellipsize = Pango.EllipsizeMode.End;
			oDisplayLabel.Xalign = 0.0f;
			oDisplayLabel.Yalign = 0.0f;
			oDisplayLabel.Wrap = true;
			oDisplayLabel.UseMarkup = true;
			oDisplayLabel.WidthRequest = 250;
			
			Gtk.Label oStatusLabel = new Gtk.Label("<span size='small'>Status: <i>"+_contact.Presence.State+"</i></span>");
			oStatusLabel.UseMarkup = true;
			oStatusLabel.Xalign = 0.0f;
			oStatusLabel.Yalign = 0.0f;
			
			vbox.PackStart(nameLabel, false, false, 0);
			vbox.PackStart(oDisplayLabel, false, false, 0);
			vbox.PackStart(oAliasLabel, false, false, 0);
			vbox.PackStart(oStatusLabel, false, false, 0);
			vbox.PackEnd(personalLabel, true, true, 0);
			
			hbox.PackStart(image, false, false, 0);
			hbox.PackEnd(vbox, true, true, 0);
			
			vbox.Spacing = 2;
			hbox.Spacing = 5;
			
			Add(hbox);
		}
	}
}