/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2009 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Xml.Serialization;
using System.Collections.Generic;

using Galaxium.Core;
using Galaxium.Gui;

namespace Galaxium.Protocol.Irc
{
	[Serializable]
	public sealed class IrcStoredContact
	{
		private string name;
		private string ident;
		
		public IrcStoredContact ()
		{
		}
		
		public IrcStoredContact (string name, string ident)
		{
			if (name == null)
				throw new ArgumentNullException ("name");
			
			this.name = name;
			this.ident = ident;
		}
		
		[XmlAttribute ("name")]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		[XmlAttribute ("ident")]
		public string Ident
		{
			get { return ident; }
			set { ident = value; }
		}
	}
}