/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Xml.Serialization;
using System.Collections.Generic;

using Galaxium.Core;
using Galaxium.Gui;

namespace Galaxium.Protocol.Irc
{
	[Serializable]
	public sealed class IrcStoredChannel
	{
		private string name;
		private string key;
		private bool autoJoin;
		private bool persistent;
		
		public IrcStoredChannel ()
		{
		}
		
		public IrcStoredChannel (string name, string key, bool autoJoin, bool persistent)
		{
			if (name == null)
				throw new ArgumentNullException ("name");
			
			this.name = name;
			this.key = key;
			this.autoJoin = autoJoin;
			this.persistent = persistent;
		}
		
		[XmlAttribute ("name")]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		[XmlAttribute ("key")]
		public string Key
		{
			get { return key; }
			set { key = value; }
		}

		[XmlAttribute ("auto-join")]
		public bool AutoJoin
		{
			get { return autoJoin; }
			set { autoJoin = value; }
		}
		
		[XmlAttribute ("persistent")]
		public bool Persistent
		{
			get { return persistent; }
			set { persistent = value; }
		}
	}
}