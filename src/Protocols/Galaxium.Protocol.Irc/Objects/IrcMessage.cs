/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using System.Collections.Generic;
using System.Reflection;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Protocol.Irc
{
	public class IrcMessage
	{
		private IrcContactIdentifier _identifier;
		private string _command;
		private string[] _parameters;

		private IrcMessage (string ident, string command, string[] parameters)
		{
			ThrowUtility.ThrowIfNull ("command", command);
			
			if (ident != null)
				this._identifier = new IrcContactIdentifier (ident);
			_command = command;
			_parameters = parameters;
		}
		
		public string this [int index]
		{
			get { return _parameters[index]; }
		}
		
		public bool HasIdentifier
		{
			get { return _identifier != null; }
		}
		
		public bool HasParameters
		{
			get { return _parameters != null && _parameters.Length > 0; }
		}
		
		public IrcContactIdentifier Identifier
		{
			get { return _identifier; }
		}

		public string Command
		{
			get { return _command; }
		}
		
		public string[] Parameters
		{
			get { return _parameters; }
		}
		
		public string LastParameter
		{
			get {
				if (HasParameters)
					return _parameters[_parameters.Length - 1];
				return null;
			}
		}
		
		public static IrcMessage CreateMessage (string command, params string[] parameters)
		{
			return new IrcMessage (null, command, parameters);
		}
		
		public static IrcMessage CreateMessageWithIdent (string ident, string command, params string[] parameters)
		{
			return new IrcMessage (ident, command, parameters);
		}
		
		public override string ToString ()
		{
			StringBuilder sb = new StringBuilder ();
			
			if (_identifier != null)
			{
				sb.Append (_identifier.ToString ());
				sb.Append (' ');
			}
			
			if (_command != null)
				sb.Append (_command.ToUpper ());
			
			if (_parameters != null)
			{
				foreach (string p in _parameters)
				{
					sb.Append (' ');
					sb.Append (p);
				}
			}
			
			return sb.ToString ();
		}
	}
}
