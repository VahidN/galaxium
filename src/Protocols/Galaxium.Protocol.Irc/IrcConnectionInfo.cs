/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;

using Galaxium.Core;
using Anculus.Core;

namespace Galaxium.Protocol.Irc
{
	public sealed class IrcConnectionInfo : AbstractConnectionInfo
	{
		private bool isNetwork;
		private string network;
		
		public IrcConnectionInfo (IrcNetworkInfo network)
		{
			if (network == null)
				throw new ArgumentNullException ("network");
			
			this.isNetwork = true;
			this.network = network.Name;

			//TODO: support multiple mirrors
			HostName = network.Mirrors[0].Hostname;
			Port = network.Mirrors[0].Port;
		}			
			
		public IrcConnectionInfo (string network)
		{
			if (network == null)
				throw new ArgumentNullException ("network");
			
			this.isNetwork = true;
			this.network = network;

			IrcNetworkInfo info = IrcProtocolHelper.GetStoredNetworkInfo (network);
			
			if (info == null)
				throw new ArgumentException (String.Format ("Invalid network '{0}'", network));
			
			//TODO: support multiple mirrors
			HostName = info.Mirrors[0].Hostname;
			Port = info.Mirrors[0].Port;
		}

		public IrcConnectionInfo (string host, int port) : base (host, port, false)
		{
		}
		
		public bool IsNetwork
		{
			get { return isNetwork; }
		}

		public string Network
		{
			get { return network; }
		}
	}
}