/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2009 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;
using Galaxium.Gui;

namespace Galaxium.Protocol.Irc
{
	public sealed class IrcConversationManager : AbstractConversationManager
	{
		public override IConversation CreateConversation (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);
			IConversation conversation = null;
			
			if (contact is IrcChannel)
				conversation = new IrcConversation (contact.Session as IrcSession, contact as IrcChannel);
			else
				conversation = new IrcConversation (contact.Session as IrcSession, contact as IrcContact);
			
			Add (conversation);
			
			return conversation;
		}
		
		public IrcConversation GetChannelConversation (IrcChannel channel)
		{
			ThrowUtility.ThrowIfNull ("channel", channel);

			foreach (IConversation conv in _conversations)
			{
				if (conv.IsChannelConversation && (conv.PrimaryContact.UniqueIdentifier.ToLower().CompareTo (channel.UniqueIdentifier.ToLower()) == 0))
					return conv as IrcConversation;
			}
			
			return null;
		}
	}
}