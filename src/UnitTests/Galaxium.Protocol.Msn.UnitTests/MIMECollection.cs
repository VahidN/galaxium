/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using NUnit.Framework;

using Galaxium.Protocol.Msn;

[TestFixture]
public class MIMECollectionTests
{
	[Test]
	public void MIMEValueAttributeTest ()
	{
		string origStr = "test data;att1=1;att2=test;att3=att3 value";
		MIMEValue mime = origStr;
		
		Assert.AreEqual ("test data", mime.Value, "Value incorrect");
		Assert.AreEqual ("1", mime["att1"], "att1 incorrect");
		Assert.AreEqual ("test", mime["att2"], "att2 incorrect");
		Assert.AreEqual ("att3 value", mime["att3"], "att3 incorrect");
		
		string mimeStr = mime.ToString ();
		Assert.AreEqual (origStr, mimeStr, "Output string differs from input");
	}
	
	[Test]
	public void MIMEValueHasAttributeTest ()
	{
		MIMEValue mime = "test data;att1=1;att2=test;att3=att3 value";
		
		Assert.AreEqual (true, mime.HasAttribute ("att1"), "att1 not found");
		Assert.AreEqual (true, mime.HasAttribute ("att1"), "att1 not found");
		Assert.AreEqual (true, mime.HasAttribute ("att1"), "att1 not found");
		
		Assert.AreEqual (false, mime.HasAttribute ("att4"), "att4 found");
	}
	
	[Test]
	public void MIMECollectionTest ()
	{
		MIMECollection mime = new MIMECollection ();
		
		mime["Value1"] = "test";
		mime["Value2"] = "test2";
		mime["Value2"]["att1"] = "att test";
		
		Assert.AreEqual ("Value1: test\r\nValue2: test2;att1=att test\r\n", mime.ToString ());
	}
}
