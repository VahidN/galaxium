/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using NUnit.Framework;

using Galaxium.Protocol;

[TestFixture]
public class MessageTests
{
	[Test]
	public void SplitTest ()
	{
		IEmoticon emot = new Galaxium.Gui.BaseEmoticon ("Test", new string[] { ":test:", "(test)" }, "/test.png", new byte[0]);
		Message.SetEmoticons (new IEmoticon[] { emot });
		
		List<IMessageChunk> chunks = Message.Split ("Test Text", null, null, null);
		
		Assert.AreEqual (1, chunks.Count);
		Assert.IsTrue (chunks[0] is ITextMessageChunk, "Chunk 0 not an ITextMessageChunk");
		Assert.AreEqual ("Test Text", (chunks[0] as ITextMessageChunk).Text);
		
		chunks = Message.Split ("Hi :test: there!", null, null, null);
		
		Assert.AreEqual (3, chunks.Count);
		Assert.IsTrue (chunks[0] is ITextMessageChunk, "Chunk 0 not an ITextMessageChunk");
		Assert.IsTrue (chunks[1] is IEmoticonMessageChunk, "Chunk 1 not an IEmoticonMessageChunk");
		Assert.IsTrue (chunks[2] is ITextMessageChunk, "Chunk 2 not an ITextMessageChunk");
		Assert.AreEqual ("Hi ", (chunks[0] as ITextMessageChunk).Text);
		Assert.AreEqual (":test:", (chunks[1] as IEmoticonMessageChunk).Text);
		Assert.AreEqual (" there!", (chunks[2] as ITextMessageChunk).Text);
		
		chunks = Message.Split ("Hello (test) world! http://www.test.com", null, null, null);
		
		Assert.AreEqual (4, chunks.Count);
		Assert.IsTrue (chunks[0] is ITextMessageChunk, "Chunk 0 not an ITextMessageChunk");
		Assert.IsTrue (chunks[1] is IEmoticonMessageChunk, "Chunk 1 not an IEmoticonMessageChunk");
		Assert.IsTrue (chunks[2] is ITextMessageChunk, "Chunk 2 not an ITextMessageChunk");
		Assert.IsTrue (chunks[3] is IURIMessageChunk, "Chunk 3 not an IURIMessageChunk");
		Assert.AreEqual ("Hello ", (chunks[0] as ITextMessageChunk).Text);
		Assert.AreEqual ("(test)", (chunks[1] as IEmoticonMessageChunk).Text);
		Assert.AreEqual (" world! ", (chunks[2] as ITextMessageChunk).Text);
		Assert.AreEqual ("http://www.test.com", (chunks[3] as IURIMessageChunk).URI);
		
		chunks = Message.Split ("A\nB\r\nC", null, null, null);
		
		Assert.AreEqual (5, chunks.Count);
		Assert.IsTrue (chunks[0] is ITextMessageChunk, "Chunk 0 not an ITextMessageChunk");
		Assert.IsTrue (chunks[1] is ITextMessageChunk, "Chunk 1 not an ITextMessageChunk");
		Assert.IsTrue (chunks[2] is ITextMessageChunk, "Chunk 2 not an ITextMessageChunk");
		Assert.IsTrue (chunks[3] is ITextMessageChunk, "Chunk 3 not an ITextMessageChunk");
		Assert.IsTrue (chunks[4] is ITextMessageChunk, "Chunk 4 not an ITextMessageChunk");
		Assert.AreEqual ("A", (chunks[0] as ITextMessageChunk).Text);
		Assert.AreEqual ("\n", (chunks[1] as ITextMessageChunk).Text);
		Assert.AreEqual ("B", (chunks[2] as ITextMessageChunk).Text);
		Assert.AreEqual ("\r\n", (chunks[3] as ITextMessageChunk).Text);
		Assert.AreEqual ("C", (chunks[4] as ITextMessageChunk).Text);
	}
}
