/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections.Generic;

namespace Galaxium.Core
{
	public static class XmlUtility
	{
		public static string Encode (string str)
		{
			ThrowUtility.ThrowIfNull ("str", str);

			StringBuilder sb = new StringBuilder ((int)(str.Length * 1.2)); //the string generally gets a bit longer
			foreach (char c in str) {
				switch (c) {
					case '"':
						sb.Append ("&quot;");
						break;
					case '<':
						sb.Append ("&lt;");
						break;
					case '>':
						sb.Append ("&gt;");
						break;
					case '\'':
						sb.Append ("&apos;");
						break;
					case '&':
						sb.Append ("&amp;");
						break;
					default:
						sb.Append (c);
						break;
				}
			}
			return sb.ToString ();
		}

		public static string Decode (string str)
		{
			ThrowUtility.ThrowIfNull ("str", str);

			StringBuilder sb = new StringBuilder (str.Length);
			StringBuilder esc = new StringBuilder (5);
			int amp = -1;

			int index = 0;
			foreach (char c in str) {
				switch (c) {
					case '&':
						amp = index;
						if (esc.Length > 0) {
							sb.Append (esc.ToString ());
							esc.Remove (0, esc.Length - 1);
						}
						esc.Append (c);
						break;
					case ';':
						if (amp >= 0) {
							esc.Append (c);

							string escchar = esc.ToString ();
							esc.Remove (0, esc.Length);

							char decchar = char.MinValue;
							if (DecodeEntity (escchar, out decchar))
								sb.Append (decchar);
							else
								sb.Append (escchar);
							amp = -1;
						} else {
							sb.Append (c);
						}
						break;
					default:
						if (amp >= 0)
							esc.Append (c);
						else
							sb.Append (c);
						break;
				}
				index++;
			}
			if (esc.Length > 0)
				sb.Append (esc.ToString ());
			return sb.ToString ();
		}

		public static bool DecodeEntity (string str, out char c)
		{
			ThrowUtility.ThrowIfNull ("str", str);

			c = char.MinValue;
			if (String.Compare (str, "&quot;", true) == 0)
				c = '"';
			else if (String.Compare (str, "&lt;", true) == 0)
				c = '<';
			else if (String.Compare (str, "&gt;", true) == 0)
				c = '>';
			else if (String.Compare (str, "&apos;", true) == 0)
				c = '\'';
			else if (String.Compare (str, "&amp;", true) == 0)
				c = '&';
			return c != char.MinValue;
		}
	}
}