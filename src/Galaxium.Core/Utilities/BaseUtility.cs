/*
 * Galaxium Messenger
 * Copyright (C) 2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography;

using Anculus.Core;

namespace Galaxium.Core
{
	public static class BaseUtility
	{
		static MD5 _md5 = MD5.Create ();
		
		static string[] _sizeSuffixes = {
			GettextCatalog.GetString (" Bytes"),
			GettextCatalog.GetString (" kB"),
			GettextCatalog.GetString (" MB"),
			GettextCatalog.GetString (" GB"),
			GettextCatalog.GetString (" TB"),
			GettextCatalog.GetString (" PB")
		};
		
		public static string BytesToString (byte[] bytes)
		{
			return BitConverter.ToString (bytes).Replace ("-", "");
		}
		
		public static string GetHashedName (string name)
		{
			ThrowUtility.ThrowIfEmpty ("name", name);
			
			byte[] hash = _md5.ComputeHash (Encoding.UTF8.GetBytes (name));
			return BytesToString (hash);
		}
		
		public static string SizeString (long size)
		{
			if (size < 1)
				return "0" + _sizeSuffixes[0];
			else if (size == 1)
				return GettextCatalog.GetString ("1 Byte");
			
			int n = (int)System.Math.Log (size, 1024);
			
			if (n >= _sizeSuffixes.Length)
				return GettextCatalog.GetString ("XB");
			
			float div = (float)(size / Math.Pow (1024, n));
			
			string str = (n == 0) ? div.ToString () : div.ToString ("F2");
			
			return str + _sizeSuffixes[n];
		}
		
		public static string ReplaceString (string original, string search, string replace)
		{
			return ReplaceString (original, search, replace, false);
		}
		
		public static string ReplaceString (string original, string search, string replace, bool replaceAll)
		{
			string replaced = original;
			int pos = replaced.IndexOf (search);
			
			while (pos >= 0)
			{
				replaced = string.Format ("{0}{1}{2}", replaced.Substring (0, pos), replace, replaced.Substring (pos + search.Length));
				
				if (!replaceAll)
					break;
				
				pos = replaced.IndexOf (search, pos + replace.Length);
			}
			
			return replaced;
		}
		
		public static void CreateDirectoryIfNeeded (string path)
		{
			if (!System.IO.Directory.Exists (path))
			{
				try
				{
					System.IO.Directory.CreateDirectory (path);
				}
				catch (Exception ex)
				{
					Log.Warn (ex, "Couldn't create the directory: {0}", path);
				}
			}
		}
	}
}