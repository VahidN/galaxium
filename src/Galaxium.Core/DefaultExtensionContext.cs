
using System;
using System.Collections.Generic;

namespace Galaxium.Core
{
	public class DefaultExtensionContext : IExtensionContext
	{
		private object _object;
		
		public DefaultExtensionContext ()
		{

		}
		
		public DefaultExtensionContext (object obj) : this ()
		{
			_object = obj;
		}

		public virtual object Object
		{
			get { return _object; }
			set { _object = value; }
		}
		
		public bool HasObject
		{
			get { return _object != null; }
		}
	}
}