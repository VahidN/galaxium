/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2009 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public sealed class PickDateDialog
	{
		[Widget ("PickDateDialog")] private Dialog _dialog = null;
		[Widget ("Calendar")] private Gtk.Calendar _calendar = null;
		
		public DateTime Selected
		{
			get { return _calendar.Date; }
		}
		
		public PickDateDialog (Gtk.Dialog dialog, DateTime current)
		{
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (PickDateDialog).Assembly, "PickDateDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			_dialog.TransientFor = dialog;
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-set", IconSizes.Small);
			
			_calendar.Date = current;
			
			_dialog.ShowAll();
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
	}
}
