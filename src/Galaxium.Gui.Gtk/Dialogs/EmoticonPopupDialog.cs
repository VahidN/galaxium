/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gdk;
using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Protocol;

using Anculus.Core;

namespace Galaxium.Gui.GtkGui
{
	public enum EmoticonPopupMode { Common, Standard, Custom };
	
	public class EmoticonPopupDialog : SelectionPopupDialog<IEmoticon>
	{
		IAccount _account;
		IContact _contact;
		EmoticonPopupMode _mode;
		List<IEmoticon> _customList;
		
		public EmoticonPopupDialog (IAccount account, IContact contact, EmoticonPopupMode mode)
		{
			_account = account;
			_contact = contact;
			_mode = mode;
			
			if (mode != EmoticonPopupMode.Standard)
				_customList = EmoticonUtility.GetCustomEmoticons (account, contact, null);
		}
		
		protected override void PopulateModel (ListStore store)
		{
			// Now we gotta load the appropriate emoticons.
			
			switch (_mode)
			{
				case EmoticonPopupMode.Common:
					int i = 0;
					
					foreach (IEmoticon emoticon in EmoticonUtility.ActiveSet.Emoticons)
					{
						store.AppendValues (new Pixbuf (emoticon.Data), emoticon);
						if (++i == 12) break;
					}
					break;
				
				case EmoticonPopupMode.Standard:
					IconView.Columns = 10;
					foreach (IEmoticon emoticon in EmoticonUtility.ActiveSet.Emoticons)
						store.AppendValues (new Pixbuf (emoticon.Data), emoticon);
					
					break;
				
				case EmoticonPopupMode.Custom:
					IconView.Columns = 10;
					foreach (IEmoticon emoticon in _customList)
						store.AppendValues (new Pixbuf (emoticon.Data), emoticon);
				
					break;
			}
		}
		
		protected override void AddCustomControls (VBox vbox)
		{
			if (_mode == EmoticonPopupMode.Common)
			{
				Button btn = new Button ();
				btn.Label = GettextCatalog.GetString ("Standard Emoticons");
				btn.Clicked += StandardButtonClicked;
				vbox.Add (btn);
				
				btn = new Button ();
				btn.Label = GettextCatalog.GetString ("Custom Emoticons");
				btn.Clicked += CustomButtonClicked;
				btn.Sensitive = _customList.Count > 0;
				vbox.Add (btn);
			}
		}
		
		private void StandardButtonClicked (object sender, EventArgs args)
		{
			EmoticonPopupDialog dialog = new EmoticonPopupDialog (_account, _contact, EmoticonPopupMode.Standard);
			dialog.Selected += ChainSelected;
			dialog.Show ();
		}
		
		private void CustomButtonClicked (object sender, EventArgs args)
		{
			EmoticonPopupDialog dialog = new EmoticonPopupDialog (_account, _contact, EmoticonPopupMode.Custom);
			dialog.Selected += ChainSelected;
			dialog.Show ();
		}
		
		private void ChainSelected (object sender, IEmoticon val)
		{
			OnSelected (val);
		}
	}
}
