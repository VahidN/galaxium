/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gdk;
using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public delegate bool CanInviteDelegate (IContact contact);
	public delegate Pixbuf GetPixbufDelegate (IContact contact);
	
	public class InviteContactDialog
	{
		[Widget ("InviteContactDialog")] Gtk.Window _dialog;
		[Widget ("contactListWindow")] ScrolledWindow _contactListWindow;
		[Widget ("imgWindow")] Gtk.Image _image;
		
		[Widget ("btnCancel")] Button _cancel_button;
		[Widget ("btnInvite")] Button _invite_button;
		
		TreeView _treeContacts = new TreeView();
		IConversation _conversation;
		
		public InviteContactDialog (IConversation conversation, CanInviteDelegate canInvite, GetPixbufDelegate getPixbuf)
		{
			_conversation = conversation;
			
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (InviteContactDialog).Assembly, "InviteContactDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			_cancel_button.Clicked += CancelClicked;
			_invite_button.Clicked += InviteClicked;
			
			GtkUtility.EnableComposite (_dialog);
			
			_treeContacts.AppendColumn (GettextCatalog.GetString ("Presence"), new CellRendererPixbuf (), "pixbuf", 0);
			CellRendererText textCell = new CellRendererText ();
			textCell.Ellipsize = Pango.EllipsizeMode.End;
			
			_treeContacts.AppendColumn (GettextCatalog.GetString ("Display Name"), textCell, "text", 1);
			_treeContacts.HeadersVisible = false;
			
			ListStore listStore = new ListStore (typeof (Pixbuf), typeof (string), typeof (IContact));
			
			foreach (IContact contact in _conversation.Session.ContactCollection)
				if (canInvite (contact))
					listStore.AppendValues (getPixbuf (contact), contact.DisplayIdentifier, contact);
			
			_treeContacts.Model = listStore;
			
			_contactListWindow.Add(_treeContacts);
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-group", IconSizes.Small);
			_image.Pixbuf = IconUtility.GetIcon("galaxium-group", IconSizes.Large);
			
			_dialog.ShowAll ();
		}
		
		private void InviteClicked (object sender, EventArgs args)
		{
			TreeIter iter;
			_treeContacts.Selection.GetSelected (out iter);
			
			if (iter.Equals (TreeIter.Zero))
				return;
			
			IContact contact = _treeContacts.Model.GetValue (iter, 2) as IContact;
			
			if (contact != null)
				_conversation.InviteContact (contact);
			
			_dialog.Destroy ();
		}
		
		private void CancelClicked (object sender, EventArgs args)
		{
			_dialog.Destroy ();
		}
	}
}
