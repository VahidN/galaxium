/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public sealed class RemoveGroupDialog
	{
		[Widget ("RemoveGroupDialog")]
		private Dialog _dialog = null;
		[Widget ("imgDialog")]
		private Image _imgDialog = null;
		[Widget ("chkClear")]
		private CheckButton _chkClear = null;
		
		public bool Clear { get { return _chkClear.Active; } }
		
		public RemoveGroupDialog (ISession session, IGroup group)
		{
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (RemoveGroupDialog).Assembly, "RemoveGroupDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_dialog);
			
			_dialog.Icon = IconUtility.GetIcon ("galaxium-delete", IconSizes.Small);
			_imgDialog.Pixbuf = IconUtility.GetIcon ("galaxium-remove-group", IconSizes.Large);
			
			_dialog.ShowAll();
		}
		
		public int Run()
		{
			return _dialog.Run();
		}
		
		public void Destroy()
		{
			_dialog.Hide();
			_dialog.Destroy();
		}
	}
}