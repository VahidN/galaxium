/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2009 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
using System;
using Glade;
using G=Gtk;

using Galaxium.Protocol;
using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;

using Anculus.Core;

namespace Galaxium.Gui.GtkGui
{
	public sealed class SendFileDialog
	{
		[Widget("SendFileDialog")] G.Dialog _window;
		[Widget("WindowImage")] G.Image _window_image;
		[Widget("FilenameComboEntry")] G.ComboBoxEntry _filename_combo_entry;
		[Widget("CancelButton")] G.Button _cancel_button;
		[Widget("SendButton")] G.Button _send_button;
		[Widget("BrowseButton")] G.Button _browse_button;
		
		private IContact _contact;
		
		public string Filename
		{
			get { return _filename_combo_entry.Entry.Text; }
		}
		
		public SendFileDialog (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);
			
			_contact = contact;
			
			XML gxml = new XML (GladeUtility.GetGladeResourceStream (typeof (SendFileDialog).Assembly, "SendFileDialog.glade"), null, null);
			gxml.Autoconnect (this);
			
			GtkUtility.EnableComposite (_window);
			
			_window.Icon = IconUtility.GetIcon ("galaxium-transfer-open", IconSizes.Small);
			
			// Load up the filename combo
			
			// FIXME: This would be nice?
			//_filename_combo_entry.SelectRegion (0, _nickname_entry.Text.Length);
			
			_window_image.Pixbuf = IconUtility.GetIcon("galaxium-transfer-send", IconSizes.Large);
			_window_image.Show();
			
			_browse_button.Clicked += HandleBrowseClicked;
			_window.ShowAll();
		}

		void HandleBrowseClicked(object sender, EventArgs e)
		{
			G.FileChooserDialog dialog = new G.FileChooserDialog(
					GettextCatalog.GetString ("Select File"),
					null, G.FileChooserAction.Open, G.Stock.Cancel, G.ResponseType.Cancel, G.Stock.Apply, G.ResponseType.Accept);
			
			dialog.SetCurrentFolder(Environment.GetFolderPath(Environment.SpecialFolder.Personal));
			
			G.ResponseType response = (G.ResponseType) dialog.Run ();
			
			if (response == G.ResponseType.Accept)
			{
				_filename_combo_entry.Entry.Text = dialog.Filename;
			}
			
			dialog.Destroy ();
		}
		
		public int Run ()
		{
			return _window.Run();
		}
		
		public void Destroy ()
		{
			_window.Destroy ();
		}
		
		private void OnSendButtonActivated (object sender, EventArgs args)
		{
			_window.Destroy();
		}
		
		private void OnCancelButtonActivated (object sender, EventArgs args)
		{
			_window.Destroy();
		}
	}
}