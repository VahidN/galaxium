/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;

using Gtk;
using Glade;

using Galaxium.Core;
using Anculus.Core;

namespace Galaxium.Gui.GtkGui
{
	public static class GladeUtility
	{
		public static Stream GetGladeResourceStream (Assembly assembly, string resource)
		{
			ThrowUtility.ThrowIfNull ("assembly", assembly);
			ThrowUtility.ThrowIfEmpty ("resource", resource);

			foreach (string res in assembly.GetManifestResourceNames ())
			{
				if (res == resource || res.EndsWith (resource))
					return assembly.GetManifestResourceStream (res);
			}
			
			//Log.Error ("Unable to load glade resource: "+resource);
			
			return null;
		}
		
		public static T ExtractWidget<T> (Stream gladeResource, string name, object signalHandler) where T : Widget
		{
			return ExtractWidget<T> (gladeResource, name, CoreUtility.TranslationDomain, signalHandler);
		}

		public static T ExtractWidget<T> (Stream gladeResource, string name, string translationDomain, object signalHandler) where T : Widget
		{
			ThrowUtility.ThrowIfNull ("gladeResource", gladeResource);
			ThrowUtility.ThrowIfEmpty ("name", name);

			XML gxml = new XML (gladeResource, null, translationDomain);
			Widget widget = gxml.GetWidget (name);
			Container oldParent = widget.Parent as Container;
			oldParent.Remove (widget);
			oldParent.Destroy ();

			if (signalHandler != null)
				gxml.Autoconnect (signalHandler);

			return (T)widget;
		}
		
		public static T ExtractWidget<T> (Stream gladeResource, string name) where T : Widget
		{
			return ExtractWidget<T> (gladeResource, name, CoreUtility.TranslationDomain, null);
		}

		public static T ExtractWidget<T> (Stream gladeResource, string name, string translationDomain) where T : Widget
		{
			return ExtractWidget<T> (gladeResource, name, translationDomain, null);
		}

		public static T ExtractWidget<T> (Assembly assembly, string gladeResourceName, string name, string translationDomain, object signalHandler) where T : Widget
		{
			ThrowUtility.ThrowIfEmpty ("gladeResourceName", gladeResourceName);
			ThrowUtility.ThrowIfNull ("assembly", assembly);

			Stream stream = assembly.GetManifestResourceStream (gladeResourceName);
			return ExtractWidget<T> (stream, name, translationDomain, signalHandler);
		}

		public static T ExtractWidget<T> (Assembly assembly, string gladeResourceName, string name, string translationDomain) where T : Widget
		{
			return ExtractWidget<T> (assembly, gladeResourceName, name, translationDomain, null);
		}
		
		public static T ExtractWidget<T> (Assembly assembly, string gladeResourceName, string name) where T : Widget
		{
			return ExtractWidget<T> (assembly, gladeResourceName, name, CoreUtility.TranslationDomain, null);
		}
	}
}