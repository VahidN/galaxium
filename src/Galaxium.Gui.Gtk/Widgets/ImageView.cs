/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Reflection;

using Gtk;

using Swfdec;
using Swfdec.Gtk;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Gui.GtkGui;

namespace Galaxium.Gui.GtkGui
{
	public delegate void ShowDoneDelegate ();
	
	public class ImageView : Gtk.Notebook
	{
		Gtk.Image _image;
		SwfdecWidget _swf;
		ShowDoneDelegate _swfDone;
		
		public new event ButtonPressEventHandler ButtonPressEvent;
		
		public ImageView ()
		{
			BorderWidth = 0;
			ShowTabs = false;
			ShowBorder = true;
			
			_image = new Gtk.Image ();
			
			EventBox eBox = new EventBox ();
			eBox.ButtonPressEvent += EBoxButtonPress;
			eBox.Add (_image);
			eBox.BorderWidth = 0;
			Add (eBox);
			_image.Show ();
			
			if (GtkUtility.EnableSwf)
			{
				_swf = new SwfdecWidget ();
				Add (_swf);
				_swf.Show ();
			}
		}
		
		void NeedImage ()
		{
			Page = 0;
		}
		
		void NeedFlash ()
		{
			if (GtkUtility.EnableSwf)
				Page = 1;
		}
		
		public void FadeTo (Gdk.Pixbuf pixbuf, FadeCompleteDelegate complete)
		{
			NeedImage ();
			PixbufUtility.FadeInImage (_image, pixbuf, complete);
		}
		
		public void FadeTo (Gdk.Pixbuf pixbuf)
		{
			NeedImage ();
			FadeTo (pixbuf, null);
		}
		
		public void SwitchTo (Gdk.Pixbuf pixbuf)
		{
			NeedImage ();
			_image.Pixbuf = pixbuf;
		}
		
		public void SwitchTo (Gdk.PixbufAnimation anim)
		{
			NeedImage ();
			_image.FromAnimation = anim;
		}
		
		public void ShowLoading ()
		{
			SwitchTo (new Gdk.PixbufAnimation (Assembly.GetExecutingAssembly (), "loading.gif"));
		}
		
		public void ShowSwf (string filename, ShowDoneDelegate doneCallback)
		{
			if (!GtkUtility.EnableSwf)
			{
				if (doneCallback != null)
					doneCallback ();
				
				return;
			}
			
			NeedFlash ();
			_swfDone = doneCallback;
			
			_swf.Player = new GtkPlayer (filename);
			_swf.Player.FsCommand += SwfFsCommand;
			_swf.Player.Playing = true;
		}
		
		public void ShowSwf (string filename)
		{
			ShowSwf (filename, null);
		}
		
		void SwfFsCommand (object sender, FsCommandEventArgs args)
		{
			if (args.Command == "quit")
			{
				Swfdec.Player player = _swf.Player;
				
				_swf.Player = null;
				player.Dispose ();
				
				if (_swfDone != null)
					_swfDone ();
			}
		}
		
		void EBoxButtonPress (object sender, ButtonPressEventArgs args)
		{
			if (ButtonPressEvent != null)
				ButtonPressEvent (this, args);
		}
	}
}
