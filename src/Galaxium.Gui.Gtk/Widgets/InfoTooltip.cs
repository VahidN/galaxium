/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gdk;
using Gtk;

namespace Galaxium.Gui.GtkGui
{
	public class InfoTooltip : Gtk.Window
	{
		public InfoTooltip() : base(Gtk.WindowType.Popup)
		{
			// Setup the tooltip window area.
			AppPaintable = true;
			Name = "gtk-tooltips";
			BorderWidth = 5;
		}
		
		protected override bool OnExposeEvent(EventExpose oEvent)
		{
			// Call the base widget's expose event.
			base.OnExposeEvent(oEvent);
			
			// Get the size that was requested by this widget.
			Gtk.Requisition oReq = SizeRequest();
			
			// Ensure that this widget has a style.
			EnsureStyle();
			
			// Paint the window area with this box.
			Gtk.Style.PaintFlatBox(Style, GdkWindow, Gtk.StateType.Normal, Gtk.ShadowType.Out, Rectangle.Zero, this, "tooltip", 0, 0, oReq.Width, oReq.Height);
			
			return(true);
		}
		
		public void PositionToWidget(Gtk.Widget oParent)
		{
			int iX, iY;
			Rectangle oRectangle = new Rectangle();
			
			if(oParent.GdkWindow == null)
				return;
			
			// Get the X,Y coordinates of the window area of the parent.
			oParent.GdkWindow.GetRootOrigin(out iX, out iY);
			
			// Setup the rectangle object to contain the proper size.
			oRectangle.X = oParent.Allocation.X + iX;
			oRectangle.Y = oParent.Allocation.Y + iY;
			oRectangle.Width = oParent.Allocation.Width;
			oRectangle.Height = oParent.Allocation.Height;
			
			// Position this widget based on the rectangle and screen objects.
			PositionToRectangle(oRectangle, oParent.Screen);
		}
		
		public void PositionToRectangle(Rectangle oRectangle, Screen oScreen)
		{
			int iMonitor;
			int iX, iY, iW, iH;
			
			Rectangle oMonitor;
			
			Gtk.Requisition oReq = SizeRequest();
			iW = oReq.Width;
			iH = oReq.Height;
			
			iX = oRectangle.X;
			iY = oRectangle.Y;
			
			iX += oRectangle.Width / 2;		
			iX -= (iW / 2 + 4);
			
			iMonitor = oScreen.GetMonitorAtPoint(iX, iY);
			oMonitor = oScreen.GetMonitorGeometry(iMonitor);
			
			if((iX + iW) > oMonitor.X + oMonitor.Width)
				iX -= (iX + iW) - (oMonitor.X + oMonitor.Width);
			else if(iX < oMonitor.X)
				iX = oMonitor.X;
			
			if((iY + iH + oRectangle.Height + 4) > oMonitor.Y + oMonitor.Height) 
			{
				iY = iY - iH - 4;
			} 
			else
			{
				iY = iY + oRectangle.Height + 4;
			}
			
			Move(iX, iY);
		}
	}
}