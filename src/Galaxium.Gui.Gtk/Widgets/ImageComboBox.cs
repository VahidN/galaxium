/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Gdk;
using Gtk;

using Galaxium.Core;

namespace Galaxium.Gui.GtkGui
{
	public delegate string ImageComboTextLookup<T> (T item);
	public delegate Pixbuf ImageComboPixbufLookup<T> (T item, IIconSize size);
	
	public class ImageComboBox<T> : ComboBox
	{
		private ListStore _store;
		
		private ImageComboTextLookup<T> _textLookup;
		private ImageComboPixbufLookup<T> _pixbufLookup;
		
		object _separatorObj = new object ();
		
		public ImageComboBox (ImageComboTextLookup<T> textLookup, ImageComboPixbufLookup<T> pixbufLookup)
		{
			ThrowUtility.ThrowIfNull ("textLookup", textLookup);
			
			this._textLookup = textLookup;
			this._pixbufLookup = pixbufLookup;
			
			this._store = new ListStore (typeof (T));
			this.Model = _store;
			
			RowSeparatorFunc = RowSeparatorMethod;
			
			if (pixbufLookup != null)
			{
				CellRendererPixbuf pixbufRenderer = new CellRendererPixbuf ();
				PackStart (pixbufRenderer, false);
				SetCellDataFunc (pixbufRenderer, new CellLayoutDataFunc (PixbufDataFunc));
			}
			
			CellRendererText textRenderer = new CellRendererText ();
			PackStart (textRenderer, true);
			SetCellDataFunc (textRenderer, new CellLayoutDataFunc (TextDataFunc));
		}
		
		public new void Clear ()
		{
			this._store = new ListStore (typeof (T));
			this.Model = _store;
		}
		
		public void Append (T item)
		{
			ThrowUtility.ThrowIfNull ("item", item);
			
			Append (item, false);
		}
		
		public void Append (T item, bool select)
		{
			ThrowUtility.ThrowIfNull ("item", item);
			
			TreeIter iter = _store.AppendValues (item);
			if (select)
				SetActiveIter (iter);
		}
		
		public void AppendSeperator ()
		{
			_store.AppendValues (_separatorObj);
		}
		
		public void Remove (T item)
		{
			ThrowUtility.ThrowIfNull ("item", item);
			
			TreeIter iter;
			
			if (_store.GetIterFirst (out iter))
			{
				do
				{
					object val =_store.GetValue (iter, 0);
					
					if (val is T)
					{
						T t = (T)val;
						
						if (item.Equals (t))
						{
							_store.Remove(ref iter);
							return;
						}
					}
				}
				while (_store.IterNext (ref iter));
			}
		}
		
		public void SelectFirst ()
		{
			TreeIter iter;
			
			if (_store.GetIterFirst (out iter))
				SetActiveIter (iter);
		}
		
		public void Select (T item)
		{
			ThrowUtility.ThrowIfNull ("item", item);
			
			TreeIter iter;
			
			if (_store.GetIterFirst (out iter))
			{
				do
				{
					object val =_store.GetValue (iter, 0);
					
					if (val is T)
					{
						T t = (T)val;
						
						if (item.Equals (t))
						{
							SetActiveIter (iter);
							return;
						}
					}
				}
				while (_store.IterNext (ref iter));
			}
		}
		
		public T GetSelectedItem ()
		{
			TreeIter iter;
			
			if (GetActiveIter (out iter))
				return (T)_store.GetValue (iter, 0);
			
			return default (T);
		}
		
		private void TextDataFunc (CellLayout layout, CellRenderer cell, TreeModel model, TreeIter iter)
		{
			object val = model.GetValue (iter, 0);
			
			if (!(val is T))
				return;
			
			T item = (T)val;
			
			CellRendererText textRenderer = cell as CellRendererText;
			
			textRenderer.Ellipsize = Pango.EllipsizeMode.End;
			textRenderer.Markup = _textLookup (item);
			textRenderer.Xalign = 0.0f;
			textRenderer.Xpad = 5;
		}
		
		private void PixbufDataFunc (CellLayout layout, CellRenderer cell, TreeModel model, TreeIter iter)
		{
			object val = model.GetValue (iter, 0);
			
			if (!(val is T))
				return;
			
			T item = (T)val;
			
			CellRendererPixbuf pixbufRenderer = cell as CellRendererPixbuf;
			
			pixbufRenderer.Pixbuf = _pixbufLookup (item, IconSizes.Small);
			pixbufRenderer.Width = IconSizes.Small.Size;
			pixbufRenderer.Height = IconSizes.Small.Size;
		}
		
		public bool RowSeparatorMethod (TreeModel model, TreeIter iter)
		{
			return model.GetValue (iter, 0) == _separatorObj;
		}
	}
}