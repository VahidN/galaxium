/*
 * Galaxium Messenger
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

using Gdk;
using Gtk;
using G=Gtk;
using Pango;
using GtkSpell;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public class MessageEntryWidget: G.TextView
	{
		public event EventHandler<DragReceivedEventArgs> DragReceived;
		public event EventHandler<SubmitTextEventArgs> TextSubmitted;
		public event EventHandler PreprocessKeyPressed;
		
		private bool _showURLs = true;
		private bool _showEmoticons = true;
		private IConversation _conversation = null;
		protected int _inputHistorySize;
		protected int _currentHistoryItem;
		protected List<string> _inputHistory;
		
		private OrderedDictionary<string, IEmoticon> _emoticons = new OrderedDictionary<string, IEmoticon> ();
		
		private Dictionary<G.Image, string> _imageEquivalents = new Dictionary<G.Image, string> ();
	
	// CHECKTHIS
	//	SpellCheck _spell;
		
		private bool _bold;
		private bool _italic;
		private bool _underline;
		private bool _strikethrough;
		
		private TextTag _tagBase = new TextTag ("base");
		private TextTag _tagBold = new TextTag ("bold");
		private TextTag _tagItalic = new TextTag ("italic");
		private TextTag _tagUnderline = new TextTag ("underline");
		private TextTag _tagStrikethrough = new TextTag ("strikethrough");
		private TextTag _tagUri = new G.TextTag ("uri");
		
		public string Family
		{
			get { return _tagBase.Family; }
			set
			{
				_tagBase.Family = value;
				ApplyTags ();
			}
		}
		
		public double Size
		{
			get { return _tagBase.SizePoints; }
			set
			{
				if ((value < 1) || (value > 36))
					return;
				
				_tagBase.SizePoints = value;
				ApplyTags ();
			}
		}
		
		public bool Bold
		{
			get { return _bold; }
			set
			{
				_bold = value;
				ApplyTags ();
			}
		}
		
		public bool Italic
		{
			get { return _italic; }
			set
			{
				_italic = value;
				ApplyTags ();
			}
		}
		
		public bool Underline
		{
			get { return _underline; }
			set
			{
				_underline = value;
				ApplyTags ();
			}
		}
		
		public bool Strikethrough
		{
			get { return _strikethrough; }
			set
			{
				_strikethrough = value;
				ApplyTags ();
			}
		}
		
		public bool ShowURLs
		{
			get { return _showURLs; }
			set
			{
				_showURLs = value;
			}
		}
		
		public bool ShowEmoticons
		{
			get { return _showEmoticons; }
			set
			{
				_showEmoticons = value;
				RebuildEmoticonList ();
			}
		}
		
		public IConversation Conversation
		{
			get { return _conversation; }
			set
			{
				_conversation = value;
				RebuildEmoticonList ();
			}
		}
		
		public string Text
		{
			get { return GetText (Buffer.StartIter, Buffer.EndIter); }
		}
		
		public Gdk.Color Color
		{
			get { return _tagBase.ForegroundGdk; }
			set
			{
				_tagBase.ForegroundGdk = value;
				ApplyTags ();
			}
		}
		
		public int ColorInt
		{
			get
			{
				byte r = (byte)(Color.Red >> 8);
				byte g = (byte)(Color.Green >> 8);
				byte b = (byte)(Color.Blue >> 8);
				
				return (r << 16) + (g << 8) + b;
			}
			set
			{
				byte r = (byte)((value & 0xFF0000) >> 16);
				byte g = (byte)((value & 0xFF00) >> 8);
				byte b = (byte)(value & 0xFF);
				
				Color = new Gdk.Color (r, g, b);
			}
		}
		
		public MessageEntryWidget (IConversation conversation)
		{
			_inputHistorySize = 20;
			_inputHistory = new List<string> ();
			
			// Targets
			TargetEntry entryTarget = new TargetEntry ();
			entryTarget.Flags = TargetFlags.Widget;
			entryTarget.Target = "text/uri-list";
			
			G.Drag.DestSetTargetList (this, new TargetList (new TargetEntry[] {entryTarget}));
			
			AcceptsTab = true;
			BorderWidth = 0;
			CursorVisible = true;
			Editable = true;
			WrapMode = G.WrapMode.Word;
			
			_tagBase.SizePoints = 10;
			
			Buffer.TagTable.Add (_tagBase);
			
			_tagBold.Weight = Weight.Bold;
			Buffer.TagTable.Add (_tagBold);

			_tagItalic.Style = Pango.Style.Italic;
			Buffer.TagTable.Add (_tagItalic);
			
			_tagUnderline.Underline = Pango.Underline.Single;
			Buffer.TagTable.Add (_tagUnderline);
			
			_tagStrikethrough.Strikethrough = true;
			Buffer.TagTable.Add (_tagStrikethrough);
			
			//TODO: can this be gotten from the gtk theme?
			_tagUri.ForegroundGdk = new Gdk.Color (0, 0, 255);
			_tagUri.Underline = Pango.Underline.Single;
			Buffer.TagTable.Add (_tagUri);
			
			ResetFont ();
			
			try
			{
			// CHECKTHIS
			//	_spell = new SpellCheck (this, null);
			}
			catch (Exception ex)
			{
				Log.Error (ex, "Unable to initialize GtkSpell");
			}
			
			Conversation = conversation;
		}
		
		public void Clear ()
		{
			Buffer.Clear ();
			_imageEquivalents.Clear ();
		}
		
		protected override void OnDragDataReceived (Gdk.DragContext context, int x, int y, SelectionData selection, uint info, uint time)
		{
			if (selection != null && selection.Text != null)
			{
				string uristring = selection.Text;
				List<string> uris = new List<string> ();
				
				while (uristring.IndexOf("\r\n") >= 0)
				{
					uris.Add(uristring.Substring(0, uristring.IndexOf("\r\n")));
					uristring = uristring.Substring (uristring.IndexOf("\r\n")+2);
				}
				
				if (DragReceived != null)
					DragReceived (this, new DragReceivedEventArgs (uris.ToArray()));
				
				G.Drag.Finish(context, true, false, time);
			}
		}
		
		TextIter GetCurrentWordStartIter ()
		{
			TextIter iter = Buffer.GetIterAtOffset (Buffer.CursorPosition);
			
			while (!iter.IsStart)
			{
				iter.BackwardChar ();
				
				if ((!string.IsNullOrEmpty (iter.Char)) && char.IsWhiteSpace (iter.Char, 0))
					break;
			}
			if (!iter.IsStart)
				iter.ForwardChar ();
			
			return iter;
		}
		
		TextIter GetCurrentWordEndIter ()
		{
			TextIter iter = Buffer.GetIterAtOffset (Buffer.CursorPosition);
			while (!iter.IsEnd)
			{
				if ((!string.IsNullOrEmpty (iter.Char)) && char.IsWhiteSpace (iter.Char, 0))
					break;
				
				iter.ForwardChar ();
			}
			
			return iter;
		}
		
		string GetText (TextIter start, TextIter end)
		{
			TextIter s = start;
			string txt = string.Empty;
			
			while ((!s.IsEnd) && (!s.Equal (end)))
			{
				if ((s.ChildAnchor != null) && (s.ChildAnchor.Widgets.Length > 0) && (s.ChildAnchor.Widgets[0] is Gtk.Image))
					txt += _imageEquivalents[s.ChildAnchor.Widgets[0] as Gtk.Image];
				else if (!string.IsNullOrEmpty (s.Char))
					txt += s.Char;
				
				if (!s.ForwardChar ())
					break;
			}
			
			return txt;
		}
		
		bool IsUrl (string str)
		{
			//foreach (string urlStart in MessageUtility.UriStarts)
			//	if (str.StartsWith (urlStart, StringComparison.InvariantCultureIgnoreCase))
			//		return true;
			
			return false;
		}
		
		[GLib.ConnectBefore]
		protected override bool OnKeyReleaseEvent (EventKey evnt)
		{
			base.OnKeyReleaseEvent (evnt);
			return false;
		}
		
		[GLib.ConnectBefore]
		protected override bool OnKeyPressEvent (EventKey evnt)
		{
			if (PreprocessKeyPressed != null)
				PreprocessKeyPressed (this, EventArgs.Empty);
			
			bool ctrl_pressed = (evnt.State & ModifierType.ControlMask) != 0;
			
			if (ctrl_pressed && evnt.Key == Gdk.Key.Up)
			{
				if ((_inputHistory.Count > 0) && (_currentHistoryItem > 0))
				{
					_currentHistoryItem--;
					Buffer.Text = _inputHistory[_currentHistoryItem];
					ApplyTags ();
				}
				
				return true;
			}
			
			if (ctrl_pressed && evnt.Key == Gdk.Key.Down)
			{
				if (_inputHistory.Count > 0)
				{
					if (_currentHistoryItem < (_inputHistory.Count - 1))
					{
						_currentHistoryItem++;
						Buffer.Text = _inputHistory[_currentHistoryItem];
						ApplyTags ();
					}
					else if (_currentHistoryItem == (_inputHistory.Count - 1))
					{
						_currentHistoryItem++;
						Buffer.Text = String.Empty;
						ApplyTags ();
					}
				}
				
				return true;
			}
			
			if (evnt.Key == Gdk.Key.Return || evnt.Key == Gdk.Key.KP_Enter)
			{
				if (ctrl_pressed)
				{
					AppendNewline ();
				}
				else
				{
					string text = Text;
					SubmitTextEventArgs args = new SubmitTextEventArgs (text);
					
					OnTextSubmitted (args);
					
					if (args.Submitted)
					{
						Buffer.Clear ();
						_imageEquivalents.Clear ();
						AddInputHistory (text);
					}
				}
				
				return true;
			}
			
			if (evnt.Key == Gdk.Key.BackSpace)
			{
				TextIter startIter = Buffer.EndIter;
				
				if (startIter.BackwardChar ())
				{
					TextIter endIter = Buffer.EndIter;
					
					if ((startIter.ChildAnchor != null) && (startIter.ChildAnchor.Widgets.Length > 0))
					{
						Gtk.Image img = startIter.ChildAnchor.Widgets[0] as Gtk.Image;
						
						if ((img != null) && _imageEquivalents.ContainsKey (img))
						{
							Buffer.Delete (ref startIter, ref endIter);
							Buffer.Insert (ref endIter, _imageEquivalents[img]);
							
							_imageEquivalents.Remove (img);
						}
					}
				}
			}
			
			string origText = Buffer.GetText (Buffer.StartIter, Buffer.EndIter, true);
			
			bool ret = base.OnKeyPressEvent (evnt);
			
			string newText = Buffer.GetText (Buffer.StartIter, Buffer.EndIter, true);
			
			if (origText == newText)
				return ret;
			
			TextIter iterWordStart = GetCurrentWordStartIter ();
			TextIter iterWordEnd = GetCurrentWordEndIter ();
			
			string word = GetText (iterWordStart, iterWordEnd);
			
			if (!string.IsNullOrEmpty (word))
			{
				if (_emoticons.ContainsKey (word))
				{
					// This is an emoticon, try to load the image
					G.Image img = GtkUtility.LoadEmoticon (_emoticons[word]);
					
					if (img != null)
					{
						// Delete the emoticons text
						Buffer.Delete (ref iterWordStart, ref iterWordEnd);
						
						// Add the emoticons image
						TextChildAnchor anchor = Buffer.CreateChildAnchor (ref iterWordEnd);
						AddChildAtAnchor (img, anchor);
						_imageEquivalents.Add (img, word);
					}
				}
				else if (IsUrl (word))
					Buffer.ApplyTag (_tagUri, iterWordStart, iterWordEnd);
				else
					Buffer.RemoveTag (_tagUri, iterWordStart, iterWordEnd);
			}
			
			ApplyTags ();
			
			return ret;
		}
		
		public void InsertEmoticon (IEmoticon emoticon, int index)
		{
			G.Image img = GtkUtility.LoadEmoticon (emoticon);
			
			if (img == null)
				return;
			
			TextIter iter = Buffer.GetIterAtOffset(index);
			
			G.TextChildAnchor anchor = Buffer.CreateChildAnchor (ref iter);
			
			AddChildAtAnchor (img, anchor);
			
			_imageEquivalents.Add (img, emoticon.Equivalents[0]);
		}
		
		void RebuildEmoticonList ()
		{
			_emoticons = null;
			
			if ((!_showEmoticons) || (_conversation == null))
				return;
			
			_emoticons = EmoticonUtility.GetEmoticonDict (_conversation.Session.Account, null, null, true);
		}
		
		void ApplyTags ()
		{
			Buffer.RemoveTag (_tagBase, Buffer.StartIter, Buffer.EndIter);
			Buffer.RemoveTag (_tagBold, Buffer.StartIter, Buffer.EndIter);
			Buffer.RemoveTag (_tagItalic, Buffer.StartIter, Buffer.EndIter);
			Buffer.RemoveTag (_tagStrikethrough, Buffer.StartIter, Buffer.EndIter);
			Buffer.RemoveTag (_tagUnderline, Buffer.StartIter, Buffer.EndIter);
			
			Buffer.ApplyTag (_tagBase, Buffer.StartIter, Buffer.EndIter);
			
			if (_bold)
				Buffer.ApplyTag (_tagBold, Buffer.StartIter, Buffer.EndIter);
			if (_italic)
				Buffer.ApplyTag (_tagItalic, Buffer.StartIter, Buffer.EndIter);
			if (_underline)
				Buffer.ApplyTag (_tagUnderline, Buffer.StartIter, Buffer.EndIter);
			if (_strikethrough)
				Buffer.ApplyTag (_tagStrikethrough, Buffer.StartIter, Buffer.EndIter);
		}
		
		public void ResetFont ()
		{
			Bold = false;
			Italic = false;
			Underline = false;
			Strikethrough = false;
			Family = string.Empty;
			Size = 10;
			ColorInt = 0;
			
			ApplyTags ();
		}
		
		protected virtual void AddInputHistory (string line)
		{
			if (_inputHistory.Count == _inputHistorySize)
				_inputHistory.RemoveAt (0);
			
			bool addAgain = true;
			
			if (_inputHistory.Count > 0)
				if (_inputHistory[_inputHistory.Count-1].CompareTo (line) == 0)
					addAgain = false;
			
			if (addAgain)
				_inputHistory.Add(line);
			
			_currentHistoryItem = _inputHistory.Count;
		}
		
		public virtual void AppendNewline ()
		{
			TextIter iter = Buffer.EndIter;
			Buffer.Insert (ref iter, Environment.NewLine);
		}
		
		public virtual void AppendText (string text, bool applyTags)
		{
			TextIter iter = Buffer.EndIter;
			Buffer.Insert (ref iter, text);
			if (applyTags)
				ApplyTags();
		}
		
		protected virtual void OnTextSubmitted (SubmitTextEventArgs args)
		{
			if (TextSubmitted != null)
				TextSubmitted (this, args);
		}
	}
}
