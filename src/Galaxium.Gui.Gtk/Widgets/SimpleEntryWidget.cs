/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2009 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Gtk;
using Cairo;
using Pango;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public class SimpleEntryEventArgs : EventArgs
	{
		private string _text;
		
		public string Text
		{
			get { return _text; }
		}
		
		public SimpleEntryEventArgs (string text)
		{
			_text = text;
		}
	}
	
	public class SimpleEntry : Entry
	{
		public event EventHandler<SimpleEntryEventArgs> TextSubmitted;
		
		private bool _clearAfter = false;
		
		public SimpleEntry (bool clearAfter)
		{
			_clearAfter = clearAfter;
		}
		
		protected override bool OnKeyPressEvent (Gdk.EventKey evnt)
		{
			if (evnt.Key == Gdk.Key.Return || evnt.Key == Gdk.Key.KP_Enter)
			{
				if (TextSubmitted != null)
					TextSubmitted (this, new SimpleEntryEventArgs (Text));
				
				if (_clearAfter)
					Text = "";
				
				return true;
			}
			else
				return base.OnKeyPressEvent (evnt);
		}
	}
}
