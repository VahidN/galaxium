/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

using Gtk;
using GLib;

using Anculus.Core;
using Anculus.Gui;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

namespace Galaxium.Gui.GtkGui
{
	public delegate void ContainerEventHandler (IConversation conversation);
	
	public sealed class NotebookLabel : HBox
	{
		public event ContainerEventHandler TabClosed;

		private string _contextMenuExtensionPoint;
		private IChatWidget<Widget> _chatWidget;
		private Label _label;
		private VBox _vbox;
		private Image _image;
		private string _caption;
		
		public IChatWidget<Widget> ChatWidget { get { return(_chatWidget); } }
		
		public NotebookLabel (IChatWidget<Widget> chatWidget, string contextMenuExtensionPoint, string caption, string icon)
		{
			ThrowUtility.ThrowIfEmpty ("caption", caption);
			ThrowUtility.ThrowIfNull ("chatWidget", chatWidget);
			
			int placement = Configuration.Conversation.Section.GetInt (Configuration.Conversation.TabPlacement.Name, Configuration.Conversation.TabPlacement.Default);
			
			_contextMenuExtensionPoint = contextMenuExtensionPoint;
			_chatWidget = chatWidget;
			_chatWidget.BecomingActive += ChatWidgetBecomingActive;
			_chatWidget.BecomeActive += ChatWidgetBecomeActive;
			
			if (_chatWidget.Conversation != null && _chatWidget.Conversation.PrimaryContact != null)
			{
				_chatWidget.Conversation.PrimaryContact.NicknameChange += PrimaryContactNicknameChanged;
				_chatWidget.Conversation.PrimaryContact.DisplayNameChange += PrimaryContactDisplayNameChanged;
				_chatWidget.Conversation.PrimaryContact.DisplayImageChange += PrimaryContactDisplayImageChanged;
			}
			else
			{
				Anculus.Core.Log.Warn ("We do not have a primary contact to attach the NotebookLabel to, it will not update!");
			}
			
			_caption = caption;
			
			_label = new Label (caption);
			_label.Markup = "<span foreground=\"#000000\">"+_caption+"</span>";
			_label.Xalign = 0.5f;
			
			if (placement == 4 || placement == 5)
				_label.Angle = 90.0;
			else
			{
				if (placement == 0 || placement == 1)
					_label.Ellipsize = Pango.EllipsizeMode.End;
			}
			
			EventBox eventBox = new EventBox ();
			eventBox.ButtonReleaseEvent += new ButtonReleaseEventHandler (EventBoxButtonReleaseEvent);
			eventBox.Add (_label);
			eventBox.VisibleWindow = false;
			
			if (_chatWidget.Conversation.IsPrivateConversation)
				_image = new Image (IconUtility.GetIcon (String.IsNullOrEmpty (icon) ? "galaxium-contact" : icon, IconSizes.Small));
			else
				_image = new Image (IconUtility.GetIcon (String.IsNullOrEmpty (icon) ? "galaxium-group" : icon, IconSizes.Small));
			
			Button btnClose = new Button ();
			btnClose.Image = new Image (IconUtility.GetIcon ("galaxium-tab-close", IconSizes.Tiny));
			btnClose.Clicked += new EventHandler (CloseClicked);
			btnClose.Relief = ReliefStyle.None;
			
			_vbox = new VBox ();
			
			if (placement == 4 || placement == 5)
			{
				_vbox.PackStart (_image, false, false, 0);
				_vbox.PackStart (eventBox, true, true, 0);
				_vbox.PackStart (btnClose, false, false, 0);
				_vbox.Spacing = 3;
				this.PackStart (_vbox, true, true, 0);
			}
			else
			{
				this.PackStart (_image, false, false, 0);
				this.PackStart (eventBox, true, true, 0);
				this.PackEnd (btnClose, false, false, 0);
			}
			
			this.Spacing = 3;
			this.ShowAll ();
		}
		
		private void EventBoxButtonReleaseEvent (object o, ButtonReleaseEventArgs args)
		{
			if (args.Event.Button == 3) {//rmb
				if (_contextMenuExtensionPoint != null) {
					//TODO: check if EP has items
					Menu menu = MenuUtility.CreateContextMenu (_contextMenuExtensionPoint, null);
					menu.Popup (null, null, null, args.Event.Button, args.Event.Time);
					menu.ShowAll ();
				}
			}
		}
		
		public void QuietActivity ()
		{
			_label.Markup = "<span foreground=\"#000000\">"+_caption+"</span>";
		}
		
		private void PrimaryContactDisplayImageChanged (object sender, EventArgs args)
		{
			_chatWidget.ContainerWindow.GenerateTitle ();
		}
		
		private void PrimaryContactDisplayNameChanged (object sender, EventArgs args)
		{
			IContact contact = _chatWidget.Conversation.PrimaryContact;
			
			_caption = Markup.EscapeText(contact.DisplayIdentifier);
			_label.Markup = "<span foreground=\"#000000\">"+_caption+"</span>";
			
			_chatWidget.ContainerWindow.GenerateTitle ();
		}
		
		private void PrimaryContactNicknameChanged (object sender, EventArgs args)
		{
			IContact contact = _chatWidget.Conversation.PrimaryContact;
			
			_caption = Markup.EscapeText(contact.DisplayIdentifier);
			
			_label.Markup = "<span foreground=\"#000000\">"+_caption+"</span>";
			
			_chatWidget.ContainerWindow.GenerateTitle ();
		}
		
		private void ChatWidgetBecomingActive (object sender, EventArgs args)
		{
			_label.Markup = "<span foreground=\"#00FF00\">"+_caption+"</span>";
		}
		
		private void ChatWidgetBecomeActive (object sender, EventArgs args)
		{
			_label.Markup = "<span weight=\"bold\" foreground=\"#FF0000\">"+_caption+"</span>";
		}
		
		private void CloseClicked (object sender, EventArgs e)
		{
			if (TabClosed != null)
				TabClosed (_chatWidget.Conversation);
		}
	}
}