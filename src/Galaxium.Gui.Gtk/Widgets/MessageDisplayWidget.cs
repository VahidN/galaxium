/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

using Gtk;
using Pango;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Protocol;

using Anculus.Core;

namespace Galaxium.Gui.GtkGui
{
	public class MessageDisplayWidget : ScrolledWindow
	{
		IConversation _conversation;
		IConfigurationSection _config;
		IMessageDisplay _display;
		
		public MessageDisplayWidget (IConversation conversation) : base ()
		{
			_conversation = conversation;
			_config = Configuration.MessageDisplay.Section;
			
			MessageDisplayExtension ext = AddinUtility.GetExtensionNode<MessageDisplayExtension> (
				"/Galaxium/Gui/Widgets/MessageDisplay",
				_config.GetString (Configuration.MessageDisplay.Active.Name, Configuration.MessageDisplay.Active.Default), true);
			
			this.ShadowType = ShadowType.EtchedIn;
			this.HscrollbarPolicy = PolicyType.Never;
			this.VscrollbarPolicy = PolicyType.Always;
			
			try
			{
				_display = ext.CreateInstance ();
				_display.Init (conversation);
				
				this.Add (_display.Widget as Widget);
			}
			catch
			{
				foreach (MessageDisplayExtension ex in AddinUtility.GetExtensionNodes ("/Galaxium/Gui/Widgets/MessageDisplay"))
				{
					try
					{
						_display = ex.CreateInstance ();
						_display.Init (conversation);
						
						this.Add (_display.Widget as Widget);
					}
					catch (Exception e)
					{
						Log.Error (e, "Unable to create {0} message display", ex.Name);
						_display = null;
					}
				}
				
				if (_display == null)
					this.AddWithViewport (new Label ("Unable to create message display"));
			}
			
			this.DeleteEvent += delegate { this.Remove (this.Child); };
		}
		
		public void AddMessage (IMessage msg)
		{
			ThreadUtility.Check ();
			
			if (_display != null)
				_display.AddMessage (msg);
		}
		
		public void AddMisc (MessageFlag flags, string txt)
		{
			ThreadUtility.Check ();
			
			Message msg = new Message (flags, _conversation.Session.Account, null);
			msg.SetMarkup (txt, null);
			AddMessage (msg);
		}
		
		public void AddError (string txt)
		{
			ThreadUtility.Check ();
			
			AddMisc (MessageFlag.Error, txt);
		}
		
		public void AddEvent (string txt)
		{
			ThreadUtility.Check ();
			
			AddMisc (MessageFlag.Event, txt);
		}
		
		public void Clear ()
		{
			if (_display != null)
				_display.Clear ();
		}
		
		public bool Save (string filename)
		{
			if (_display == null)
				return false;
			
			return _display.Save (filename);
		}
		
		public void UpdateEmoticon (IEmoticon emoticon)
		{
			if (_display != null)
				_display.UpdateEmoticon (emoticon);
		}
	}
}