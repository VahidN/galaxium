/*
 * Galaxium Messenger
 * Copyright (C) 2005-2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Mono.Addins;

using Galaxium.Gui;
using Galaxium.Core;
using Anculus.Core;

namespace Galaxium.Gui.GtkGui
{
	public class GtkIconExtension : IconExtension<Gdk.Pixbuf>
	{
		public override Gdk.Pixbuf GetIcon ()
		{
			if (Resource == null) {
				Log.Error ("Invalid resource for addin '{0}' (addin '{1}').", Identifier, Addin.Id);
				return null;
			}
			
			Stream stream = Addin.GetResource (Resource);
			if (stream == null) {
				Log.Error ("Resource '{0}' not found in addin '{1}'.", Resource, Addin.Id);
				return null;
			}

			using (stream) {
				Gdk.Pixbuf pixbuf = new Gdk.Pixbuf (stream);
				return pixbuf;
			}
		}
		
		public override IIconSize GetIconSize (string iconSize)
		{
			return IconUtility.ParseIconSize(iconSize);
		}

	}
}