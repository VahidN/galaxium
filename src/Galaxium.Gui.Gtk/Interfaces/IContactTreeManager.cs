/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gtk;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public interface IContactTreeManager
	{
		ISession Session { get; set; }
		IConversation Conversation { get; set; }
		IConfigurationSection Config { get; }
		ContactTreeDetailLevel Detail { get; set; }
		ContactTreeView Tree { get; set; }
		
		void Init (GalaxiumTreeView tree);
		
		void RenderText (object data, CellRendererContact renderer);
		void RenderLeftImage (object data, CellRendererPixbuf renderer);
		void RenderRightImage (object data, CellRendererPixbuf renderer);
		
		string GetMenuExtensionPoint (object data);
		InfoTooltip GetTooltip (object data);
		
		int Compare (object data1, object data2);
		
		bool Visible (object data, string filter, bool caseSensitive);
	}
}
