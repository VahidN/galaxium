/*
 * Galaxium Messenger
 * Copyright (C) 2008 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gdk;
using Gtk;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public class ListContactTreeManager : AbstractContactTreeManager
	{
		ContactListView _list;
		
		public override ContactTreeView Tree
		{
			get { return null; }
			set { Log.Debug ("Unimplemented"); }
		}
		
		protected ContactListView List
		{
			get { return _list; }
		}
		
		public override IConfigurationSection Config
		{
			get
			{
				if (Session.Account.UseDefaultListView)
					return Configuration.ContactList.Section;
				
				return ConfigurationUtility.Accounts[_list.Manager.Session.Account.Protocol.Name][_list.Manager.Session.Account.UniqueIdentifier];
			}
		}
		
		public override ContactTreeDetailLevel Detail
		{
			get { return (ContactTreeDetailLevel)Config.GetInt (Configuration.ContactList.DetailLevel.Name, Configuration.ContactList.DetailLevel.Default); }
			set { Config.SetInt (Configuration.ContactList.DetailLevel.Name, (int)value); }
		}
		
		public override void Init (GalaxiumTreeView list)
		{
			_list = list as ContactListView;
		}
		
		public override void RenderText (object data, CellRendererContact renderer)
		{
			if (data is IContact)
			{
				IContact contact = data as IContact;
				renderer.ShowEmoticons = true;
				renderer.Markup = GLib.Markup.EscapeText (contact.DisplayIdentifier);
			}
			else
			{
				renderer.ShowEmoticons = false;
				renderer.Markup = "<b>ERROR: Unknown row</b>";
			}
		}
		
		public override void RenderLeftImage (object data, CellRendererPixbuf renderer)
		{
			Pixbuf pbuf = null;
			
			if (data is IContact)
			{
				IContact contact = data as IContact;
				
				if (contact.Presence.BasePresence == BasePresence.Idle)
					pbuf = IconUtility.GetIcon ("galaxium-idle", IconSizes.Small);
				else if (contact.Presence.BasePresence == BasePresence.Offline)
					pbuf = IconUtility.GetIcon ("galaxium-offline", IconSizes.Small);
				else if (contact.Presence.BasePresence == BasePresence.Away)
					pbuf = IconUtility.GetIcon ("galaxium-status-away", IconSizes.Small);
				else if (contact.Presence.BasePresence == BasePresence.Busy)
					pbuf = IconUtility.GetIcon ("galaxium-status-busy", IconSizes.Small);
				else if (contact.Presence.BasePresence == BasePresence.Online)
					pbuf = IconUtility.GetIcon ("galaxium-contact", IconSizes.Small);
			}
			
			if (pbuf != null)
			{
				renderer.Pixbuf = pbuf;
				renderer.Width = pbuf.Width;
				renderer.Visible = true;
			}
			else
			{
				renderer.Pixbuf = null;
				renderer.Visible = false;
			}
		}
		
		public override void RenderRightImage (object data, CellRendererPixbuf renderer)
		{
			renderer.Pixbuf = null;
			renderer.Visible = false;
		}
		
		public override string GetMenuExtensionPoint (object data)
		{
			if (data is IContact)
				return "/Galaxium/Gui/" + _session.Account.Protocol.Name + "/GroupChatList/ContextMenu/Contact";
			
			return base.GetMenuExtensionPoint (data);
		}
		
		public override int Compare (object data1, object data2)
		{
			if (data1 is IContact)
				return (data1 as IContact).DisplayIdentifier.CompareTo ((data2 as IContact).DisplayIdentifier);
			
			return 0;
		}
		
		public override bool Visible (object data, string filter, bool caseSensitive)
		{
			if (data is IContact)
			{
				if (caseSensitive)
					return (data as IContact).DisplayIdentifier.Contains (filter);
				else
					return (data as IContact).DisplayIdentifier.ToLower ().Contains (filter.ToLower ());
			}
			
			return false;
		}
	}
}
