/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 *
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Text;

using Gdk;
using Gtk;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;
using Galaxium.Protocol.Gui;

namespace Galaxium.Gui.GtkGui
{
	public class CellRendererContact: Gtk.CellRendererText
	{
		int _emoticonmargin = 1;
		string _markup = null;
		List<IMessageChunk> _chunks = null;
		IContact _contact;
		bool _showEmoticons = true;
		
		public IContact Contact
		{
			get { return _contact; }
			set { _contact = value; }
		}
		
		public bool ShowEmoticons
		{
			get { return _showEmoticons; }
			set { _showEmoticons = value; }
		}

		public new string Markup
		{
			get { return _markup; }
			set
			{
				if (_markup == value)
					return;
				
				_markup = value;
				SplitMarkup ();
			}
		}
		
		public int EmoticonMargin
		{
			get { return _emoticonmargin; }
			set { _emoticonmargin = value; }
		}
		
		public CellRendererContact ()
		{
		}
		
		void SplitMarkup ()
		{
			if (!string.IsNullOrEmpty (_markup))
				_chunks = PangoUtility.Split (_markup, _contact, null, null);
			else
				_chunks = null;
		}
		
		protected override void Render (Drawable window,
		                                Gtk.Widget widget,
		                                Gdk.Rectangle background_area,
		                                Gdk.Rectangle cell_area,
		                                Gdk.Rectangle expose_area,
		                                Gtk.CellRendererState flags)
		{
			if (_chunks != null)
			{
				int xoffset, yoffset, width, height;
				GetSize (widget, ref cell_area, out xoffset, out yoffset, out width, out height);
				
				Gtk.StateType state;
				if (!this.Sensitive)
					state = Gtk.StateType.Insensitive;
				else if ((flags & Gtk.CellRendererState.Selected) == Gtk.CellRendererState.Selected)
					state = (widget.HasFocus ? Gtk.StateType.Selected : Gtk.StateType.Active);
				else if (((flags & Gtk.CellRendererState.Prelit) == Gtk.CellRendererState.Prelit) && (widget.State == Gtk.StateType.Prelight))
					state = Gtk.StateType.Prelight;
				else
					state = (widget.State == Gtk.StateType.Insensitive ? Gtk.StateType.Insensitive : Gtk.StateType.Normal);
				
				Cairo.Context cx = Gdk.CairoHelper.Create (window);
				
				GtkUtility.RenderFormatted (_chunks, _contact, ShowEmoticons, widget.Style, state, cx, widget.PangoContext,
				                            cell_area.X + (int)Xpad, cell_area.Y + (int)Ypad,
				                            cell_area.Width - (int)(2 * Xpad), cell_area.Height - (int)(2 * Ypad));
				
				((IDisposable)cx.Target).Dispose ();
				((IDisposable)cx).Dispose ();
			}
		}
	}
}
