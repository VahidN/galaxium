/*
 * Galaxium Messenger
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

using Gdk;
using Gtk;

namespace Galaxium.Gui.GtkGui
{
	public class LinkTag: TextTag
	{
		TextTagTable _table;
		string _url;
		
		public string URL
		{
			get { return _url; }
		}		
		
		public LinkTag(TextTagTable table, string url): base(null)
		{
			_table = table;
			_url = url;
			
			ForegroundGdk = new Gdk.Color(9, 9, 252);
			Underline = Pango.Underline.Single;
			
			_table.Add(this);
		}
		
		public override void Dispose()
		{
			_table.Remove(this);
		}
	}
}
