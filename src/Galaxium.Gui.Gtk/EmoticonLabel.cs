/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2007 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Xml;

using Gdk;
using Gtk;

using Anculus.Core;

using Galaxium.Core;
using Galaxium.Gui;
using Galaxium.Gui.GtkGui;
using Galaxium.Protocol;

namespace Galaxium.Gui.GtkGui
{
	public class EmoticonLabel: Label
	{
		string _markup;
		List<IMessageChunk> _chunks = null;
		
		public new string Text
		{
			get { return Markup; }
			set { Markup = GLib.Markup.EscapeText (value); }
		}

		public new string Markup
		{
			get { return _markup; }
			set
			{
				_markup = value;
				base.Markup = value;
				
				if (!string.IsNullOrEmpty (_markup))
					_chunks = PangoUtility.Split (_markup, null, null, null);
				else
					_chunks = null;
			}
		}
		
		public EmoticonLabel (string markup)
			: base (string.Empty)
		{
			Markup = markup;
			
			AppPaintable = true;
		}
		
		protected override bool OnExposeEvent (EventExpose evnt)
		{
			if (_chunks != null)
			{
				Cairo.Context cx = Gdk.CairoHelper.Create (evnt.Window);
				
				GtkUtility.RenderFormatted (_chunks, null, true, Style, State, cx, PangoContext,
				                            Allocation.X, Allocation.Y, Allocation.Width, Allocation.Height);
				
				((IDisposable)cx.Target).Dispose ();
				((IDisposable)cx).Dispose ();
				
				return true;
			}
			
			return false;
		}
	}
}
