/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public class ChatEventArgs : EventArgs
	{
		private IContact _contact;
		private IConversation _conversation;
		
		public ChatEventArgs (IConversation conversation)
		{
			this._conversation = conversation;
		}
		
		public ChatEventArgs (IContact contact)
		{
			this._contact = contact;
		}
		
		public IConversation Conversation
		{
			get { return _conversation; }
		}
		
		public IContact Contact
		{
			get { return _contact; }
		}
	}
}