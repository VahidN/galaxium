/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Mono.Addins;

using Galaxium.Core;

using Anculus.Core;

namespace Galaxium.Protocol
{
	public class ActiveSessionCondition : ConditionType
	{
		public ActiveSessionCondition ()
		{
			SessionUtility.SessionAdded += delegate {
				NotifyChanged ();
			};
			
			SessionUtility.SessionRemoved += delegate {
				NotifyChanged ();
			};
			
			SessionUtility.ActiveSessionChanged += delegate {
				NotifyChanged ();
			};
			
			SessionUtility.AddingSessionChanged += delegate {
				NotifyChanged ();
			};
		}

		public override bool Evaluate (NodeElement conditionNode)
		{
			bool final = false;
			string protocol = String.Empty;
			
			ISession session = SessionUtility.ActiveSession;
			
			if (session != null)
			{
				protocol = conditionNode.GetAttribute ("protocol");
				
				if (protocol == null || protocol == String.Empty)
				{
					final = true;
				}
				else
				{
					// Adding ability to specify multiple protocols.
					string[] protocols = protocol.Split(',');
					
					for (int i = 0; i < protocols.Length; i++)
					{
						if (protocols[i].ToLower() == session.Account.Protocol.Name.ToLower())
						{
							final = true;
							break;
						}
					}
				}
			}
			else return false;
			
			if (SessionUtility.AddingSession)
				return false;
			
			return final;
		}
	}
}