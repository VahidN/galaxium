/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Protocol
{
	public sealed class BufferManager
	{
		private List<byte[]> _buffers;
		private readonly int _bufferSize;

		private int _capacity;
		private int _allocCount;

		private List<int> _fingerprints;

		public BufferManager (int bufferSize, int capacity)
		{
			_bufferSize = bufferSize;
			_capacity = capacity;
			_buffers = new List<byte[]> (capacity);
			_fingerprints = new List<int> ();

			InitializeBuffers (capacity);
		}

		public byte[] Allocate ()
		{
			byte[] buffer = null;
			if (_buffers.Count > 0) {
				//we have an available buffer
				buffer = _buffers[0];
				_buffers.RemoveAt (0);
			} else {
				//create a new buffer
				buffer = new byte[_bufferSize];
				_fingerprints.Add (buffer.GetHashCode ());
				_allocCount++;
			}

			return buffer;
		}

		public void Free (ref byte[] buffer)
		{
			if (buffer == null)
				throw new ArgumentNullException ("buffer");
			
			int fingerprint = buffer.GetHashCode ();
			if (_fingerprints.Contains (fingerprint)) {
				//the buffer was created by this manager
				_fingerprints.Remove (fingerprint);
				if (_allocCount > _capacity) {
					//we have more buffers allocated then we want, GC this one
					buffer = null;
				} else {
					//keep the buffer for later reuse
					_buffers.Add (buffer);
				}
				_allocCount--;
			} else {
				//the buffer wasn't created by this manager, ignore and dispose
				buffer = null;
			}
		}

		public void IncreaseCapacity (int capacity)
		{
			_capacity += capacity;
			_buffers.Capacity = _capacity;
			InitializeBuffers (capacity);
		}

		public void DecreaseCapacity (int capacity)
		{
			_capacity -= capacity;
			if (_capacity < 0) {
				_capacity = 0;
				Log.Warn ("Negative capacity, only call DecreaseCapacity when really needed.");
			}
			FreeBuffers (capacity);
			if (_capacity < _buffers.Count)
				_capacity = _buffers.Count;
			
			_buffers.Capacity = _capacity;
		}

		private void InitializeBuffers (int count)
		{
			for (int i = 0; i < count; i++)
				_buffers.Add (new byte[_bufferSize]);
		}

		private void FreeBuffers (int count)
		{
			ThrowUtility.ThrowIfNull ("buffers", _buffers);
			
			int realcount = _allocCount - count;
			if (realcount > 0)
			{
				for (int i = 0; i < realcount; i++)
				{
					if (_buffers.Count < 1)
					{
						Log.Warn ("There was no buffer data to remove.");
						return;
					}
					
					byte[] buffer = _buffers[0];
					_buffers.RemoveAt (0);
					int fp = buffer.GetHashCode ();
					_fingerprints.Remove (fp);
				}
			}
		}
	}
}