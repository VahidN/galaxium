/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

namespace Galaxium.Protocol
{
	public class ARGBGradient
	{
		List<KeyValuePair<double, ARGBColor>> _stops = new List<KeyValuePair<double, ARGBColor>> ();
		
		public IEnumerable<KeyValuePair<double, ARGBColor>> Stops
		{
			get { return _stops; }
		}
		
		public static ARGBGradient None
		{
			get
			{
				ARGBGradient grad = new ARGBGradient ();
				grad._stops = null;
				return grad;
			}
		}
		
		public ARGBGradient ()
		{
		}
		
		public void AddStop (double pos, ARGBColor color)
		{
			_stops.Add (new KeyValuePair<double, ARGBColor> (pos, color));
		}
		
		public void Clear ()
		{
			_stops.Clear ();
		}
		
		public ARGBColor ColorAtPos (double pos)
		{
			if ((_stops == null) || (_stops.Count == 0))
				return ARGBColor.None;
			
			if (_stops.Count == 1)
				return _stops[0].Value;
			
			return ARGBColor.None;
		}
		
#region Override equals & operator
		public override bool Equals (object o)
		{
			return Equals (o as ARGBGradient);
		}
		
		public bool Equals (ARGBGradient g)
		{
			if ((object)g == null)
				return false;
			
			if ((g._stops == null) && (_stops == null))
				return true;
			
			return base.Equals (g);
		}
		
		public override int GetHashCode ()
		{
			if (_stops == null)
				return 0;
			
			return base.GetHashCode ();
		}
		
		public static bool operator == (ARGBGradient a, ARGBGradient b)
		{
			if ((object)a == null)
				return ((object)b == null);
			
			if ((object)b == null)
				return false;
			
			return a.Equals (b);
		}
		
		public static bool operator != (ARGBGradient a, ARGBGradient b)
		{
			return !(a == b);
		}
#endregion
		
#region Implicit operators
		public static implicit operator ARGBColor (ARGBGradient val)
		{
			if (val == ARGBGradient.None)
				return ARGBColor.None;
			
			return val.ColorAtPos (0.5);
		}
		
		public static implicit operator ARGBGradient (ARGBColor val)
		{
			if (val == ARGBColor.None)
				return ARGBGradient.None;
			
			ARGBGradient grad = new ARGBGradient ();
			grad.AddStop (0, val);
			return grad;
		}
#endregion
	}
}
