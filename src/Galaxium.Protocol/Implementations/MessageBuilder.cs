/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Text;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public sealed class MessageBuilder
	{
		private List<byte> _buffer;

		public MessageBuilder ()
			: this (128)
		{
		}

		public MessageBuilder (int capacity)
		{
			ThrowUtility.ThrowIfLessThenOne ("capacity", capacity);

			_buffer = new List<byte> (capacity);
		}

		public int Count
		{
			get { return _buffer.Count; }
		}

		public void Append (string str)
		{
			ThrowUtility.ThrowIfNull ("str", str);
			_buffer.AddRange (Encoding.UTF8.GetBytes (str));
		}

		public void Append (char value)
		{
			_buffer.AddRange (Encoding.UTF8.GetBytes (new char[] { value }));
		}

		public void Append (int value)
		{
			Append (value.ToString ());
		}

		public void Append (uint value)
		{
			Append (value.ToString ());
		}

		public void Append (short value)
		{
			Append (value.ToString ());
		}

		public void Append (ushort value)
		{
			Append (value.ToString ());
		}

		public void Append (long value)
		{
			Append (value.ToString ());
		}

		public void Append (ulong value)
		{
			Append (value.ToString ());
		}

		public void Append (bool value)
		{
			Append (value.ToString ());
		}

		public void Append (double value)
		{
			Append (value.ToString ());
		}

		public void Append (float value)
		{
			Append (value.ToString ());
		}

		public void Append (decimal value)
		{
			Append (value.ToString ());
		}

		public void Append (byte[] value)
		{
			ThrowUtility.ThrowIfNull ("value", value);

			_buffer.AddRange (value);
		}

		public byte[] GetByteArray ()
		{
			return _buffer.ToArray ();
		}

		public override string ToString ()
		{
			return Encoding.UTF8.GetString (GetByteArray ());
		}
	}
}