/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Protocol
{
	public abstract class AbstractConversation : IConversation, IComparable<AbstractConversation>
	{
		public event EventHandler<ConversationEventArgs> Established;
		public event EventHandler<ConversationEventArgs> Closed;
		
		public event EventHandler<MessageEventArgs> MessageReceived;
		public event EventHandler<MessageEventArgs> EventReceived;
		
		public event EventHandler<ContactActionEventArgs> ContactJoined;
		public event EventHandler<ContactActionEventArgs> ContactLeft;
		public event EventHandler<ConversationEventArgs> AllContactsLeft;
		
		public event EventHandler CapabilitiesChanged;
		
		protected Guid _uid;
		protected bool _active = false;
		protected bool _ready = false;
		protected ISession _session;
		protected IContact _primaryContact;
		protected ContactCollection _contacts;
		
		protected IConversationLog _log;
		
		protected IConfigurationSection _config;
		protected IConfigurationSection _logging_config;
		
		public virtual bool EnableLogging
		{
			get
			{
				if (!PrimaryContact.UseDefaultSettings)
					return PrimaryContact.EnableLogging;
				else
					return _config.GetBool (Configuration.Conversation.EnableLogging.Name, Configuration.Conversation.EnableLogging.Default);
			}
			
			set
			{
				if (!PrimaryContact.UseDefaultSettings) {
					PrimaryContact.EnableLogging = value;
					
					if (value)
						LogEvent ("You are now logging this conversation.");
					else
						LogEvent ("You are no longer logging this conversation.");
				}
			}
		}
		
		public virtual bool UseDefaultSettings
		{
			get { return PrimaryContact.UseDefaultSettings; }
			set { PrimaryContact.UseDefaultSettings = value; }
		}
		
		protected AbstractConversation (IContact primaryContact, ISession session)
		{
			ThrowUtility.ThrowIfNull ("session", session);
			
			_uid = Guid.NewGuid();
			_primaryContact = primaryContact;
			_session = session;
			_contacts = new ContactCollection ();
			
			ConversationLogUtility.LoggingEnabledChanged += HandleLoggingEnabledChanged;
			
			_log = ConversationLogUtility.GetConversationLog (this);
			
			// Load configuration options
			_config = Configuration.Conversation.Section;
			_logging_config = Configuration.Logging.Section;
		}

		void HandleLoggingEnabledChanged(object sender, EventArgs e)
		{
			bool enableLogging = UseDefaultSettings ? _config.GetBool (Configuration.Conversation.EnableLogging.Name, Configuration.Conversation.EnableLogging.Default) : EnableLogging;
			_log.WritingEnabled = ConversationLogUtility.EnableLogging && enableLogging;
		}
		
		~AbstractConversation ()
		{
			ConversationLogUtility.LoggingEnabledChanged -= HandleLoggingEnabledChanged;
			
			if (_log != null)
				_log.Close();
		}
		
		public virtual bool Ready
		{
			get { return _ready; }
			set { _ready = value; }
		}
		
		public Guid UniqueIdentifier
		{
			get { return _uid; }
		}
		
		public ISession Session
		{
			get { return _session; }
		}
		
		public virtual bool Active
		{
			get { return _active; }
			set { _active = value; }
		}
		
		public IConversationLog ConversationLog
		{
			get { return _log; }
		}
		
		//FIXME: this should be internal
		public ContactCollection ContactCollection
		{
			get { return _contacts; }
		}
		
		public IContact PrimaryContact
		{
			get { return _primaryContact; }
		}
		
		public virtual bool IsPrivateConversation
		{
			get { return _contacts.Count < 2; }
		}
		
		public virtual bool IsChannelConversation
		{
			get { return false; }
		}
		
		public abstract void Close ();
		
		public abstract void InviteContact (IContact contact);
		
		public int CompareTo(AbstractConversation conversation)
		{
			return conversation.UniqueIdentifier.CompareTo(this.UniqueIdentifier);
		}
		
		public int CompareTo(IConversation conversation)
		{
			return conversation.UniqueIdentifier.CompareTo(this.UniqueIdentifier);
		}
		
		public virtual void AddContact (IContact contact)
		{
			if (contact == null)
				throw new ArgumentNullException ("contact");
			
			_contacts.Add (contact);
			
			OnContactJoined (new ContactActionEventArgs (Session, this, contact));
		}
		
		public virtual void RemoveContact (IContact contact)
		{
			if (contact == null)
				throw new ArgumentNullException ("contact");
			
			_contacts.Remove (contact);
			
			OnContactLeft (new ContactActionEventArgs (Session, this, contact));
			if (_contacts.Count == 0)
				OnAllContactsLeft (new ConversationEventArgs (this));
		}
		
		protected virtual void LogMessage (IMessage msg, bool read)
		{
			bool enableLogging = UseDefaultSettings ? _config.GetBool (Configuration.Conversation.EnableLogging.Name, Configuration.Conversation.EnableLogging.Default) : EnableLogging;
			_log.WritingEnabled = ConversationLogUtility.EnableLogging && enableLogging;
			
			try
			{
				if (_log != null)
					_log.LogMessage (msg, read);
				else
					Log.Warn ("No log object to log messages with!");
			}
			catch (Exception ex)
			{
				Anculus.Core.Log.Error (ex, "Exception thrown whilst logging message");
			}
		}
		
		protected virtual void LogEvent (string message)
		{
			// Create and log this event.
			IMessage msg = new Message (MessageFlag.Event, Session.Account, null);
			msg.SetMarkup (message, null);;
			LogMessage (msg, Ready);
			
			// Create a message event args, and dispatch it.
			MessageEventArgs args = new MessageEventArgs (msg);
			OnEventReceived (args);
		}
		
		protected virtual void OnEstablished (ConversationEventArgs args)
		{
			if (Established != null)
				Established (this, args);
		}
		
		protected virtual void OnClosed (ConversationEventArgs args)
		{
			Active = false;
			
			if (Closed != null)
				Closed (this, args);
		}
		
		protected virtual void OnMessageReceived (MessageEventArgs args)
		{
			if (MessageReceived != null)
				MessageReceived (this, args);
		}
		
		protected virtual void OnEventReceived (MessageEventArgs args)
		{
			if (EventReceived != null)
				EventReceived (this, args);
		}
		
		protected virtual void OnContactJoined (ContactActionEventArgs args)
		{
			if (ContactJoined != null)
				ContactJoined (this, args);
		}
		
		protected virtual void OnContactLeft (ContactActionEventArgs args)
		{
			if (_contacts.Count < 1)
				Active = false;
			
			if (ContactLeft != null)
				ContactLeft (this, args);
		}
		
		protected void OnCapabilitiesChanged ()
		{
			if (CapabilitiesChanged != null)
				CapabilitiesChanged (this, EventArgs.Empty);
		}
		
		protected virtual void OnAllContactsLeft (ConversationEventArgs args)
		{
			Active = false;
			
			if (AllContactsLeft != null)
				AllContactsLeft (this, args);
		}
	}
}