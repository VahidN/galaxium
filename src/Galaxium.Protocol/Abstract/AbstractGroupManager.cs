/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public abstract class AbstractGroupManager : IGroupManager
	{
		protected Dictionary<string, IGroup> _groups;

		protected AbstractGroupManager ()
		{
			this._groups = new Dictionary<string, IGroup> ();
		}

		public IGroup GetGroup (string uid)
		{
			ThrowUtility.ThrowIfNull ("uid", uid);

			IGroup group = null;
			if (_groups.TryGetValue (uid, out group))
				return group;
			return null;
		}

		public void Add (IGroup group)
		{
			ThrowUtility.ThrowIfNull ("group", group);

			if (!_groups.ContainsKey (group.UniqueIdentifier))
				_groups.Add (group.UniqueIdentifier, group);
		}

		public void Clear ()
		{
			_groups.Clear ();
		}

		public bool Contains (IGroup group)
		{
			ThrowUtility.ThrowIfNull ("group", group);

			return _groups.ContainsKey (group.UniqueIdentifier);
		}

		public void CopyTo (IGroup[] array, int arrayIndex)
		{
			throw new NotImplementedException ();
		}

		public int Count
		{
			get { return _groups.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool Remove (IGroup group)
		{
			return _groups.Remove (group.UniqueIdentifier);
		}

		public IEnumerator<IGroup> GetEnumerator ()
		{
			return _groups.Values.GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return (_groups.Values as IEnumerable).GetEnumerator ();
		}
	}
}