/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;

namespace Galaxium.Protocol
{
	public abstract class AbstractEmoticon : IEmoticon
	{
		string _name;
		string[] _equivalents;
		byte[] _data;
		string _filename;
		int _width = -1;
		int _height = -1;
		
		public string Name
		{
			get { return _name; }
			protected set { _name = value; }
		}
			
		public string[] Equivalents
		{
			get { return _equivalents; }
			protected set { _equivalents = value; }
		}
		
		public byte[] Data
		{
			get
			{
				if (((_data == null) || (_data.Length == 0)) &&
				    (!string.IsNullOrEmpty (_filename)) && File.Exists (_filename))
				{
					//TODO: should we set the _data field?
					// We'd need some way to free the data if its not used for a while
					
					return File.ReadAllBytes (_filename);
				}
				
				return _data;
			}
			set { _data = value; }
		}
		
		public string Filename
		{
			get { return _filename; }
			set { _filename = value; }
		}
		
		public int Width
		{
			get
			{
				if (_width < 0)
					_width = GetWidth ();
				
				return _width;
			}
		}
		
		public int Height
		{
			get
			{
				if (_height < 0)
					_height = GetHeight ();
				
				return _height;
			}
		}
		
		protected AbstractEmoticon (string name, string[] equivalents)
		{
			_name = name;
			_equivalents = equivalents;
		}
		
		protected AbstractEmoticon (string name)
			: this (name, new string[0])
		{
		}
		
		protected AbstractEmoticon (string name, string[] equivalents, string filename)
			: this (name, equivalents)
		{
			_filename = filename;
		}
		
		protected AbstractEmoticon (string name, string[] equivalents, byte[] data)
			: this (name, equivalents)
		{
			_data = data;
		}
		
		protected abstract int GetWidth ();
		protected abstract int GetHeight ();
	}
}
