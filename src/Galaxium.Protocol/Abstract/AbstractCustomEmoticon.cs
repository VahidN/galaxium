/*
 * Galaxium Messenger
 * Copyright (C) 2008 Paul Burton <paulburton89@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace Galaxium.Protocol
{
	public abstract class AbstractCustomEmoticon : AbstractEmoticon, ICustomEmoticon
	{
		string _source;
		string _dest;
		
		public string Source
		{
			get { return _source; }
			set { _source = value; }
		}
		
		public string Destination
		{
			get { return _dest; }
			set { _dest = value; }
		}

		public new string[] Equivalents
		{
			get { return base.Equivalents; }
			set { base.Equivalents = value; }
		}
		
		protected AbstractCustomEmoticon (string name)
			: base (name)
		{
		}
		
		protected AbstractCustomEmoticon (string name, string[] equivalents)
			: base (name, equivalents)
		{
		}
		
		protected AbstractCustomEmoticon (string name, string[] equivalents, string filename)
			: base (name, equivalents, filename)
		{
		}
		
		protected AbstractCustomEmoticon (string name, string[] equivalents, byte[] data)
			: base (name, equivalents, data)
		{
		}
		
		public bool Allow (IEntity source, IEntity dest)
		{
			bool allow = true;
			
			if (!string.IsNullOrEmpty (_source))
				allow &= (source != null) && (_source == ConfigName (source));
			if (!string.IsNullOrEmpty (_dest))
				allow &= (dest != null) && (_dest == ConfigName (dest));
			
			return allow;
		}
		
		protected string ConfigName (IEntity entity)
		{
			if (entity == null)
				return string.Empty;
			
			return string.Format ("{0}:{1}:{2}",
			                      entity.Session.Account.Protocol,
			                      entity.Session.Account.UniqueIdentifier,
			                      entity.UniqueIdentifier);
		}
	}
}
