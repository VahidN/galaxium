/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public abstract class AbstractGroup : IGroup
	{
		public event EventHandler<NameChangeEventArgs> NameChange;
		
		protected ISession _session;

		protected string _uid;
		protected string _name;
		protected bool _isVirtual;

		protected Dictionary<string, IContact> _contacts;

		protected AbstractGroup (ISession session, string uid)
			: this (session, uid, false)
		{
		}

		protected AbstractGroup (ISession session, string uid, bool isVirtual)
		{
			ThrowUtility.ThrowIfNull ("session", session);
			ThrowUtility.ThrowIfEmpty ("uid", uid);
			
			this._session = session;
			this._uid = uid;
			this._isVirtual = isVirtual;

			this._contacts = new Dictionary<string, IContact> ();
		}

		protected AbstractGroup (ISession session, string uid, string name, bool isVirtual)
			: this (session, uid, isVirtual)
		{
			ThrowUtility.ThrowIfEmpty ("name", name);

			this._name = name;
		}
		
		public ISession Session
		{
			get { return _session; }
		}

		public string UniqueIdentifier
		{
			get { return _uid; }
		}

		public string Name
		{
			get { return _name; }
			set
			{
				ThrowUtility.ThrowIfEmpty ("Name", value);
				if (_name != value) {
					NameChangeEventArgs args = new NameChangeEventArgs (value, _name);
					_name = value;
					OnNameChange (args);
				}
			}
		}

		public bool IsVirtual
		{
			get { return _isVirtual; }
		}

		public abstract int OnlineCount { get; }

		public abstract int OfflineCount { get; }

		public IContact GetContact (string uid)
		{
			ThrowUtility.ThrowIfEmpty ("uid", uid);

			IContact contact = null;
			if (_contacts.TryGetValue (uid, out contact))
				return contact;
			return null;
		}

		public void Add (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);

			if (!_contacts.ContainsKey (contact.UniqueIdentifier))
				_contacts.Add (contact.UniqueIdentifier, contact);
		}

		public void Clear ()
		{
			_contacts.Clear ();
		}

		public bool Contains (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);

			return _contacts.ContainsKey (contact.UniqueIdentifier);
		}

		public void CopyTo (IContact[] array, int arrayIndex)
		{
			throw new NotImplementedException ();
		}

		public int Count
		{
			get { return _contacts.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool Remove (IContact contact)
		{
			ThrowUtility.ThrowIfNull ("contact", contact);

			return _contacts.Remove (contact.UniqueIdentifier);
		}

		public IEnumerator<IContact> GetEnumerator ()
		{
			return _contacts.Values.GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return (_contacts.Values as IEnumerable).GetEnumerator ();
		}

		public int CompareTo (IGroup other)
		{
			return _uid.CompareTo (other.UniqueIdentifier);
		}

		protected virtual void OnNameChange (NameChangeEventArgs args)
		{
			if (NameChange != null)
				NameChange (this, args);
		}
	}
}