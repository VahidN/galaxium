/*
 * Galaxium Messenger
 * 
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * Copyright (C) 2008 Philippe Durand <draekz@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Galaxium.Core;

using Anculus.Core;

namespace Galaxium.Protocol
{
	public abstract class AbstractContact: AbstractEntity, IContact
	{
		protected IConfigurationSection _config;
		
		protected bool _show_action_toolbar = true;
		protected bool _show_input_toolbar = true;
		protected bool _show_account_image = true;
		protected bool _show_contact_image = true;
		protected bool _show_timestamps = true;
		protected bool _show_identification = true;
		protected bool _enable_sounds = true;
		protected bool _logging = true;
		protected bool _auto_accept = false;
		protected bool _use_default_view = true;
		protected bool _use_default_settings = true;
		
		public bool AutoAccept
		{
			get { return _auto_accept; }
			set { _auto_accept = value; }
		}
		
		public bool ShowActionToolbar
		{
			get { return _show_action_toolbar; }
			set { _show_action_toolbar = value; }
		}
		
		public bool ShowInputToolbar
		{
			get { return _show_input_toolbar; }
			set { _show_input_toolbar = value; }
		}
		
		public bool ShowAccountImage
		{
			get { return _show_account_image; }
			set { _show_account_image = value; }
		}
		
		public bool ShowContactImage
		{
			get { return _show_contact_image; }
			set { _show_contact_image = value; }
		}
		
		public bool ShowPersonalMessage
		{
			get { return _show_identification; }
			set { _show_identification = value; }
		}
		
		public bool ShowTimestamps
		{
			get { return _show_timestamps; }
			set { _show_timestamps = value; }
		}
		
		public bool UseDefaultView
		{
			get { return _use_default_view; }
			set { _use_default_view = value; }
		}
		
		public bool EnableSounds
		{
			get { return _enable_sounds; }
			set { _enable_sounds = value; }
		}
		
		public bool EnableLogging
		{
			get { return _logging; }
			set { _logging = value; }
		}
		
		public bool UseDefaultSettings
		{
			get { return _use_default_settings; }
			set { _use_default_settings = value; }
		}
		
		protected AbstractContact (ISession session, string uid, string name, IPresence presence)
			: this (session, uid, name, string.Empty, presence)
		{
			
		}
		
		protected AbstractContact (ISession session, string uid, string name, string nickname, IPresence presence)
			: base(session, false, uid, name, nickname, string.Empty, null, presence)
		{
			ThrowUtility.ThrowIfNull ("session", session);
			
			// You MUST initialize the config otherwise the contact will not save.
			_config = Configuration.Contact.Section[Session.Account.Protocol.Name][Session.Account.UniqueIdentifier][UniqueIdentifier];
		}
		
		public virtual void Save ()
		{
			if (_config == null)
			{
				Log.Warn ("Configuration not available to save contact: "+UniqueIdentifier);
				return;
			}
			
			_config.SetBool(Configuration.Contact.ShowActionToolbar.Name, ShowActionToolbar);
			_config.SetBool(Configuration.Contact.ShowInputToolbar.Name, ShowInputToolbar);
			_config.SetBool(Configuration.Contact.ShowAccountImage.Name, ShowAccountImage);
			_config.SetBool(Configuration.Contact.ShowContactImage.Name, ShowContactImage);
			_config.SetBool(Configuration.Contact.ShowIdentification.Name, ShowPersonalMessage);
			_config.SetBool(Configuration.Contact.EnableLogging.Name, EnableLogging);
			_config.SetBool(Configuration.Contact.EnableSounds.Name, EnableSounds);
			_config.SetBool(Configuration.Contact.SupressImage.Name, SupressImage);
			_config.SetBool(Configuration.Contact.SupressName.Name, SupressName);
			_config.SetBool(Configuration.Contact.SupressMessage.Name, SupressMessage);
			_config.SetBool(Configuration.Contact.AutoAccept.Name, AutoAccept);
			_config.SetString (Configuration.Contact.DisplayName.Name, DisplayName);
			_config.SetString (Configuration.Contact.DisplayMessage.Name, DisplayMessage);
			_config.SetString (Configuration.Contact.Nickname.Name, Nickname);
		}
		
		public virtual void Load ()
		{
			if (_config == null)
			{
				Log.Warn ("Configuration not available to load contact: "+UniqueIdentifier);
				return;
			}
			
			ShowActionToolbar = _config.GetBool(Configuration.Contact.ShowActionToolbar.Name, Configuration.Contact.ShowActionToolbar.Default);
			ShowInputToolbar = _config.GetBool(Configuration.Contact.ShowInputToolbar.Name, Configuration.Contact.ShowInputToolbar.Default);
			ShowAccountImage = _config.GetBool(Configuration.Contact.ShowAccountImage.Name, Configuration.Contact.ShowAccountImage.Default);
			ShowContactImage = _config.GetBool(Configuration.Contact.ShowContactImage.Name, Configuration.Contact.ShowContactImage.Default);
			ShowPersonalMessage = _config.GetBool(Configuration.Contact.ShowIdentification.Name, Configuration.Contact.ShowIdentification.Default);
			EnableLogging = _config.GetBool(Configuration.Contact.EnableLogging.Name, Configuration.Contact.EnableLogging.Default);
			EnableSounds = _config.GetBool(Configuration.Contact.EnableSounds.Name, Configuration.Contact.EnableSounds.Default);
			SupressImage = _config.GetBool(Configuration.Contact.SupressImage.Name, Configuration.Contact.SupressImage.Default);
			SupressName = _config.GetBool(Configuration.Contact.SupressName.Name, Configuration.Contact.SupressName.Default);
			SupressMessage = _config.GetBool(Configuration.Contact.SupressMessage.Name, Configuration.Contact.SupressMessage.Default);
			AutoAccept = _config.GetBool(Configuration.Contact.AutoAccept.Name, Configuration.Contact.AutoAccept.Default);
			_displayName = _config.GetString (Configuration.Contact.DisplayName.Name, Configuration.Contact.DisplayName.Default);
			_displayMessage = _config.GetString (Configuration.Contact.DisplayMessage.Name, Configuration.Contact.DisplayMessage.Default);
			_nickname = _config.GetString (Configuration.Contact.Nickname.Name, Configuration.Contact.Nickname.Default);
		}
		
		public int CompareTo (IContact other)
		{
			return UniqueIdentifier.CompareTo(other.UniqueIdentifier);
		}
	}
}