// SessionDisconnectedMessage.cs created with MonoDevelop
// User: draek at 7:14 P 15/06/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;

namespace Galaxium.Protocol
{
	public class SessionDisconnectedMessage : SessionMessage
	{
		public SessionDisconnectedMessage (ISession session) : base (SessionMessageType.Disconnected, session)
		{
			_message = "Session ["+session.Account.UniqueIdentifier+"] on the ["+session.Account.Protocol.Name+"] protocol has been taken offline.";
		}
	}
}