/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

using Anculus.Core;
using Galaxium.Core;

namespace Galaxium.Protocol
{
	public static class AccountUtility
	{
		public static EventHandler<AccountEventArgs> AccountAdded;
		public static EventHandler<AccountEventArgs> AccountRemoved;

		private static Dictionary<string, Dictionary<string, IAccount>> _accounts;
		private static IConfigurationSection _section;

		static AccountUtility ()
		{
			_accounts = new Dictionary<string, Dictionary<string, IAccount>> ();
		}

		public static IAccount ActiveAccount
		{
			get
			{
				ISession session = SessionUtility.ActiveSession;
				if (session != null)
					return session.Account;
				return null;
			}
		}

		public static void Initialize ()
		{
			_section = Configuration.Account.Section;
			
			//1 section for each protocol
			foreach (IConfigurationSection section in _section.Sections)
			{
				if (section.Name.CompareTo ("GEN") == 0)
					continue;
				
				IProtocolFactory fac = ProtocolUtility.GetProtocolFactory (section.Name);
				
				if (fac == null)
				{
					Log.Warn ("No Protocol factory for account of protocol '{0}'", section.Name);
					continue;
				}
				
				foreach (IConfigurationSection accountSection in section.Sections)
				{
					IAccount account = fac.LoadAccount (accountSection);
					
					if (account == null)
					{
						Log.Warn ("Unable to load account {0}", accountSection.Name);
						continue;
					}
					
					AddAccount (account);
				}
			}
		}
		
		public static void Save (IAccount account)
		{
			if (account == null)
				throw new ArgumentNullException ("account");
			
			IConfigurationSection section = Configuration.Account.Section[account.Protocol.Name][account.UniqueIdentifier];
			IProtocolFactory fac = ProtocolUtility.GetProtocolFactory (account.Protocol);
			
			fac.SaveAccount (section, account);
			ConfigurationUtility.SaveAccounts ();
		}
		
		public static void Save ()
		{
			//write the account config data to the buffer
			foreach (Dictionary<string,IAccount> dict in _accounts.Values)
				foreach (IAccount account in dict.Values)
					Save (account);
			
			ConfigurationUtility.SaveAccounts ();
		}
		
		public static void Unload ()
		{
			Save ();
			_accounts.Clear ();
		}

		public static IAccount GetAccount (IProtocol protocol, string name)
		{
			ThrowUtility.ThrowIfNull ("protocol", protocol);
			ThrowUtility.ThrowIfNull ("name", name);
			
			Dictionary<string, IAccount> accounts = null;
			if (_accounts.TryGetValue (protocol.Name, out accounts)) {
				IAccount account = null;
				if (accounts.TryGetValue (name, out account))
					return account;
			}
			return null;
		}

		public static bool AccountExists (IProtocol protocol, string name)
		{
			ThrowUtility.ThrowIfNull ("protocol", protocol);
			ThrowUtility.ThrowIfNull ("name", name);
			
			Dictionary<string, IAccount> accounts = null;
			if (_accounts.TryGetValue (protocol.Name, out accounts)) {
				return accounts.ContainsKey (name);
			}
			return false;
		}

		public static void AddAccount (IAccount account)
		{
			ThrowUtility.ThrowIfNull ("account", account);
			
			Dictionary<string, IAccount> accounts = null;
			if (!_accounts.TryGetValue (account.Protocol.Name, out accounts)) {
				accounts = new Dictionary<string,IAccount> ();
				_accounts.Add (account.Protocol.Name, accounts);
			}

			if (!accounts.ContainsKey (account.UniqueIdentifier)) {
				accounts.Add (account.UniqueIdentifier, account);
				OnAccountAdded (new AccountEventArgs (account));
			}
		}

		public static void RemoveAccount (IAccount account)
		{
			ThrowUtility.ThrowIfNull ("account", account);
			
			Dictionary<string, IAccount> accounts = null;
			if (_accounts.TryGetValue (account.Protocol.Name, out accounts)) {
				if (accounts.Remove (account.UniqueIdentifier))
					OnAccountRemoved (new AccountEventArgs (account));
			}
		}
		
		public static void RemoveAccountFromConfig (IAccount account)
		{
			ThrowUtility.ThrowIfNull ("account", account);

			IConfigurationSection section = Configuration.Account.Section[account.Protocol.Name];
			if (section.ContainsSection (account.UniqueIdentifier))
				section.RemoveSection (account.UniqueIdentifier);
		}

		public static IEnumerable<IAccount> Accounts
		{
			get {
				foreach (Dictionary<string, IAccount> accounts in _accounts.Values)
					foreach (IAccount account in accounts.Values)
						yield return account;
			}
		}

		public static IEnumerable<IAccount> GetAccounts (IProtocol protocol)
		{
			foreach (IAccount account in Accounts)
			{
				if (account.Protocol.CompareTo (protocol) == 0)
					yield return account;
			}
		}

		private static void OnAccountAdded (AccountEventArgs args)
		{
			if (AccountAdded != null)
				AccountAdded (null, args);
		}

		private static void OnAccountRemoved (AccountEventArgs args)
		{
			if (AccountRemoved != null)
				AccountRemoved (null, args);
		}
	}
}