/*
 * Galaxium Messenger
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public interface IConversationLog
	{
		//bool KeepAlive { get; set; }
		bool WritingEnabled { get; set; }
		
		void LogMessage (IMessage msg, bool read);
		
		void Close ();
		
		IEnumerable<ConversationLogEntry> GetRangeEntries (DateTime from, DateTime to);
		IEnumerable<ConversationLogEntry> GetUnreadEntries ();
		IEnumerable<ConversationLogEntry> GetHistoryEntries ();

		IEnumerable<ConversationLogEntry> SearchAll (string keyword);
		
		ConversationLogEntry Search (string keyword);
		ConversationLogEntry SearchNext (string keyword, ConversationLogEntry entry);
	}
}