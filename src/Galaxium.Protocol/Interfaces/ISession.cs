/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public interface ISession : IDisposable
	{
		event EventHandler<SessionEventArgs> Connected;
		event EventHandler<SessionEventArgs> Disconnected;
		event EventHandler<SessionEventArgs> LoginCompleted;
		event EventHandler<SessionProgressEventArgs> LoginProgress;
		
		event EventHandler<ContactListEventArgs> ContactAdded;
		event EventHandler<ContactListEventArgs> ContactRemoved;
		event EventHandler<ContactEventArgs> ContactChanged;
		event EventHandler<ContactEventArgs> ContactReverse;
		event EntityChangeEventHandler<string> ContactDisplayNameChanged;
		event EntityChangeEventHandler<string> ContactDisplayMessageChanged;
		event EntityChangeEventHandler<IPresence> ContactPresenceChanged;
		event EntityChangeEventHandler<IDisplayImage> ContactDisplayImageChanged;
		
		event EventHandler<GroupEventArgs> GroupAdded;
		event EventHandler<GroupEventArgs> GroupRemoved;
		event EventHandler<GroupEventArgs> GroupRenamed;
		
		event EventHandler<ConversationEventArgs> ConversationRequested;
		event EventHandler<ConversationEventArgs> ConversationInitiated;
		
		event EventHandler<FileTransferEventArgs> TransferInvitationReceived;
		event EventHandler<FileTransferEventArgs> TransferInvitationSent;
		
		event EventHandler<ErrorEventArgs> ErrorOccurred;
		
		IAccount Account { get; }
		IConversationManager Conversations { get; }
		ContactCollection ContactCollection { get; }
		GroupCollection GroupCollection { get; }
		
		IList<IFileTransfer> AllFileTransfers { get; }
		IList<IFileTransfer> ActiveFileTransfers { get; }
		
		void Connect ();
		void Disconnect ();
		void SetPresence (BasePresence presence);
		IFileTransfer SendFile (IContact contact, string fileName);
		IEntity FindEntity(string uid);
		bool InContactList (IContact contact);
	}
}