/*
 * Galaxium Messenger
 * Copyright (C) 2003-2007 Philippe Durand <draekz@gmail.com>
 * Copyright (C) 2007 Ben Motmans <ben.motmans@gmail.com>
 * 
 * License: GNU General Public License (GPL)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Collections.Generic;

using Galaxium.Core;

namespace Galaxium.Protocol
{
	public interface IConversation : IComparable<IConversation>
	{
		event EventHandler<ConversationEventArgs> Established;
		event EventHandler<ConversationEventArgs> Closed;
		
		event EventHandler<MessageEventArgs> MessageReceived;
		event EventHandler<MessageEventArgs> EventReceived;
		
		event EventHandler<ContactActionEventArgs> ContactJoined;
		event EventHandler<ContactActionEventArgs> ContactLeft;
		event EventHandler<ConversationEventArgs> AllContactsLeft;
		
		event EventHandler CapabilitiesChanged;
		
		Guid UniqueIdentifier { get; }
		IConversationLog ConversationLog { get; }
		
		ISession Session { get; }
		bool Active { get; set; }
		bool Ready { get; set; }
		
		bool EnableLogging { get; set; }
		bool UseDefaultSettings { get; set; }
		
		ContactCollection ContactCollection { get; }
		
		bool IsPrivateConversation { get; }
		bool IsChannelConversation { get; }
		
		IContact PrimaryContact { get; }
		
		void Close ();
		
		void InviteContact (IContact contact);
	}
}