// ISessionMessage.cs created with MonoDevelop
// User: draek at 6:51 P 15/06/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;

namespace Galaxium.Protocol
{
	public interface ISessionMessage
	{
		ISession Session { get; }
		string Message { get; }
		SessionMessageType Type { get; }
	}
}
